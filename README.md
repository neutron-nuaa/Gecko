# Gecko-inspired robots

We are developing four verion of gecko-inspired robots

## Nyxbot

## The black panther

## Slalom
We study a gecko-inspired robot with an optimal bendable body for climbing enhancement on inclined surfaces. The robot is controlled by CPG-based neural control as well as our learning mechanism (called PICEO) for coordinating between body and legs to achieve both body waveforms standing and traveling wave at different slope angle and speed. To this end, the study provides a gecko-inspired robot with a flexible body for efficient inclined surface climbing with agile and versatile locomotion, such as real geckos.

- **[Slalom CAD Models](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%203D%20Models)** <br >
    The CAD and electronic boards of Slalom


- **[Slalom Basic Locomotion Controller](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20Basic%20Locomotion%20Controller)** <br >
    Basic Locomotion Controller of Slalom

- **[Slalom CPGRBFN](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20CPGRBFN)** <br >
    Slalom with CPGRBF Network

- **[Slalom Dynamixel Software](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20Dynamixel%20Software)** <br >
    Setting up Dynamixel motor of Slalom  (Note: next version is comming)


- **[Slalom controller for inclined surface climbing](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20controller%20for%20inclined%20surface%20climbing)** <br >
    Supplementary material of RA-L with IROS2021 paper - ***Lateral Undulation of the Bendable Body of a Gecko-Inspired Robot for Energy-Efficient Inclined Surface Climbing***

- **[Slalom controller for self-righting](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20controller%20for%20self-righting)** <br >
    Neural control and learning for self-righting of gecko-inspired robots

- **[Slalom controller for body-wave transition gradient](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20controller%20for%20body-wave%20transition%20gradient)** <br >
    Supplementary material of RA-L paper - ***Transition Gradient from Standing to Traveling Waves for Energy-Efficient Slope Climbing of a Gecko-Inspired Robot***


## Soft gecko-inspired robot


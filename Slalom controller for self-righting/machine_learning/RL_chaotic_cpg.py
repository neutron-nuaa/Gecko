#!/usr/bin/env python

import sys
import math
from operator import add
import numpy as np

class CHAOTICCPG:
    def __init__(self):
        self.avg_output = 0
        self.previous_avg_output = 0
        self.output1 = 0
        self.output2 = 0
        self.act1 = 0
        self.act2 = 0
        self.bias1 = -5.73
        self.bias2 = 0.25
        self.w11 = -5.5
        self.w12 = 1.48
        self.w21 = -1.65
        self.w22 = 0.0

    
    def run(self, _numbers):
        chaotic_out = np.array([])
        for _ in range(_numbers):
            self.act1 = self.w11 * self.output1 + self.w12 * self.output2 + self.bias1
            self.act2 = self.w22 * self.output2 + self.w21 * self.output1 + self.bias2
            self.output1 = math.tanh(self.act1)
            self.output2 = math.tanh(self.act2)

            self.avg_output = (self.output1+self.output2)/2
            # self.avg_output = math.tanh(self.output1*self.output2)

            # Low-pass filter
            # self.avg_output = 0.5*self.avg_output - 0.99*self.previous_avg_output
            # self.previous_avg_output = self.avg_output
            
            chaotic_out = np.append(chaotic_out, self.avg_output)
        
        return chaotic_out
#!/usr/bin/env python

import sys
import math
from operator import add

class encoder(object):
    def __init__(self):

        ## MORF NO PRIOR ##
        self.set_BC_morf = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_morf = [0.5044372080402942, 0.11179573179088269, 0.22336131133168322, 0.5238323908959595, 0.4897837537401469, 0.23336879209210276, 0.156900132397411, 0.2705401047539295, 0.5045759819857375, 0.4904423579823461, 0.2409376907772342, 0.1710147855545038, 0.2840915832472842, 0.5079500341720962, 0.4593346845127408, 0.21835216964225468, 0.1690032511888188, 0.3258838945822318, 0.5074750373643965, 0.2620683981985637]
        self.set_FT_morf = [-0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04, -0.04]

        self.set_BC_morf_CPG = [1.0, 0.0]
        self.set_CF_morf_CPG = [1.0, 2.05948852]
        self.set_FT_morf_CPG = [1.0, -0.4]

        ## ALPHA NO PRIOR ##
        self.set_BC_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_FT_alpha = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        self.set_BC_alpha_CPG = [0.5, 0.0]
        self.set_CF_alpha_CPG = [0.5, 0.0]
        self.set_FT_alpha_CPG = [0.5, 0.0]

        ## LAIKAGO NO PRIOR ##
        self.set_BC_laika = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_laika = [0.2236424038363451, 0.10374758225125288, 0.1325345023164375, 0.21309243408110085, 0.2008498013633831, 0.12499944453497111, 0.09701220357872349, 0.1534277494108785, 0.22003872236626223, 0.19559888022361924, 0.11318054969820898, 0.0972276310918886, 0.16178043696769182, 0.22854558692145077, 0.18590051643525377, 0.10340550753498838, 0.0962463730641556, 0.18511283003460186, 0.2623899729123366, 0]
        self.set_FT_laika = [-0.5286093184975102, -0.24522155820562072, -0.31326336931240384, -0.5036730263327652, -0.47473589443597247, -0.2954532327266108, -0.2293015722421638, -0.36264740793092315, -0.5200915259263991, -0.4623246262794827, -0.2675176630945383, -0.22981076454633453, -0.3823901239869558, -0.5401986603424628, -0.43940122094683287, -0.2444130179666586, -0.2274914273883881, -0.4375394167259168, -0.6201944818267482, 0]

        self.set_BC_laika_CPG = [0.0, 0.0]
        self.set_CF_laika_CPG = [1.0, 0.936332]
        self.set_FT_laika_CPG = [0.5, -2.44532925]

        ## GECKOBOTIV ##
        self.set_BC_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_CF_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_FT_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_TT_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.set_BB_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]   # Backbone axial joint
        self.set_T1_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]   # Tail joints
        self.set_T2_geckobotiv = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]


        self.set_BC_geckobotiv_CPG = [0.0, 0.0]
        self.set_CF_geckobotiv_CPG = [1.0, 0.936332]
        self.set_FT_geckobotiv_CPG = [0.5, -2.44532925]
        self.set_TT_geckobotiv_CPG = [0.5, -2.44532925]


    def get_init_parameter_set(self, robot, encoding, numKernals):

        if numKernals == 20:
            if robot == 'MORF':
                set_BC = self.set_BC_morf
                set_CF = self.set_CF_morf
                set_FT = self.set_FT_morf
            elif robot == 'MORF_CPG':
                set_BC = self.set_BC_morf_CPG
                set_CF = self.set_CF_morf_CPG
                set_FT = self.set_FT_morf_CPG
            elif robot == 'ALPHA':
                set_BC = self.set_BC_alpha
                set_CF = self.set_CF_alpha
                set_FT = self.set_CF_alpha
            elif robot == 'ALPHA_CPG':
                set_BC = self.set_BC_alpha_CPG
                set_CF = self.set_CF_alpha_CPG
                set_FT = self.set_FT_alpha_CPG
            elif robot == 'LAIKAGO':
                set_BC = self.set_BC_laika
                set_CF = self.set_CF_laika
                set_FT = self.set_FT_laika
            elif robot == 'LAIKAGO_CPG':
                set_BC = self.set_BC_laika_CPG
                set_CF = self.set_CF_laika_CPG
                set_FT = self.set_FT_laika_CPG
            elif robot == 'GECKOBOTIV':
                set_BC = self.set_BC_geckobotiv
                set_CF = self.set_CF_geckobotiv
                set_FT = self.set_FT_geckobotiv
                set_TT = self.set_TT_geckobotiv
            elif robot == 'GECKOBOTIV_CPG':
                set_BC = self.set_BC_geckobotiv_CPG
                set_CF = self.set_CF_geckobotiv_CPG
                set_FT = self.set_FT_geckobotiv_CPG
                set_TT = self.set_TT_geckobotiv_CPG
            elif robot == 'GECKOBOTIV_V2':
                set_BC = self.set_BC_geckobotiv
                set_CF = self.set_CF_geckobotiv
                set_FT = self.set_FT_geckobotiv
                set_TT = self.set_TT_geckobotiv
                set_BB = self.set_BB_geckobotiv
            elif robot == 'GECKOBOTIV_V3':
                set_BC = self.set_BC_geckobotiv
                set_CF = self.set_CF_geckobotiv
                set_FT = self.set_FT_geckobotiv
                set_TT = self.set_TT_geckobotiv
                set_BB = self.set_BB_geckobotiv
                set_T1 = self.set_T1_geckobotiv
                set_T2 = self.set_T2_geckobotiv
            else:
                print('[ ERROR] Unknown robot')
        elif numKernals == 10:
            if robot == 'MORF':
                set_BC = self.set_BC_morf_10
                set_CF = self.set_CF_morf_10
                set_FT = self.set_FT_morf_10
            elif robot == 'ALPHA':
                set_BC = self.set_BC_alpha_10
                set_CF = self.set_CF_alpha_10
                set_FT = self.set_CF_alpha_10
            elif robot == 'LAIKAGO':
                set_BC = self.set_BC_laika_10
                set_CF = self.set_CF_laika_10
                set_FT = self.set_FT_laika_10
            elif robot == 'GECKOBOTIV':
                set_BC = self.set_BC_geckobotiv_10
                set_CF = self.set_CF_geckobotiv_10
                set_FT = self.set_FT_geckobotiv_10
                set_TT = self.set_TT_geckobotiv_10
            else:
                print('[ ERROR] Unknown robot')

        if encoding == "indirect":
            if robot == 'GECKOBOTIV':
                init_parameter_set = set_BC + set_CF + set_FT + set_TT
                
                # 0 deg 150 iterations
                # init_parameter_set = [ 0.035000997963108196,
                #     0.06082398955265404,
                #     0.05069230870582722,
                #     -0.08130609250421304,
                #     0.31260489477618153,
                #     0.03606727925415992,
                #     0.1809404411367044,
                #     -0.03571771812799575,
                #     -0.3297700483813483,
                #     -0.040844490202205146,
                #     -0.275444961977652,
                #     -0.30907479127244075,
                #     -0.1561823049664731,
                #     -0.019392319118898726,
                #     0.004545806605735213,
                #     -0.16689470920544247,
                #     0.10411274840706178,
                #     -0.09547689377358737,
                #     0.023896312496623606,
                #     0.20791957571275152,
                #     0.4115267490402389,
                #     0.15660823930462175,
                #     -0.13168248397962456,
                #     0.0751452121867672,
                #     -0.1289806963754799,
                #     -0.01707554641697463,
                #     -0.05614243464613306,
                #     -0.0001154681316617558,
                #     -0.1351502529458679,
                #     -0.06453134545275914,
                #     -0.06548485658171566,
                #     -0.18163728899148146,
                #     0.03132869586100287,
                #     0.2646612782992938,
                #     0.0969327577047291,
                #     0.18364141132165845,
                #     0.16356256274111214,
                #     0.175328301576021,
                #     -0.05059967712441657,
                #     0.21307370935212402,
                #     0.10617480520194766,
                #     0.19815839288961182,
                #     0.20508883777373366,
                #     0.021932253618352278,
                #     -0.01714423162344352,
                #     0.18425375090191,
                #     0.07433435580280232,
                #     -0.06440254957179548,
                #     -0.272921305653296,
                #     0.053925887840134044,
                #     0.08094027412162236,
                #     -0.08330421402026507,
                #     -0.028921080630222154,
                #     -0.2808627355919083,
                #     0.04056850723156622,
                #     -0.15545008432755228,
                #     0.19634915128600733,
                #     0.03391865084731439,
                #     -0.027662701625352122,
                #     0.3057438566891123,
                #     0.19557410640011408,
                #     -0.062196477390863085,
                #     0.02752490192159286,
                #     0.012922099721614224,
                #     0.162659489878888,
                #     0.07350365435560936,
                #     -0.42247310725142445,
                #     -0.2749309687225686,
                #     -0.1991419753786583,
                #     0.019263323878947313,
                #     -0.05239136762024855,
                #     -0.2324595254018434,
                #     0.030509754021009694,
                #     0.1537757037408415,
                #     0.12105367959968388,
                #     0.19335327732128155,
                #     -0.15384895658643888,
                #     -0.07462871091479222,
                #     0.10266608976768868,
                #     -0.036358329048729786
                # ]

                # init_parameter_set = [
                #     0.009533269813990337,
                #     0.020044676037287282,
                #     0.021694410309170877,
                #     0.0025701839474671856,
                #     0.012576867736138227,
                #     0.011749186388555178,
                #     0.003953568099348896,
                #     -0.02340285052622377,
                #     -0.02705653743836739,
                #     1.87960382824815e-05,
                #     -0.008781601459062507,
                #     -0.021190962383656975,
                #     0.006199158016452488,
                #     0.017847992111365868,
                #     0.0007091402697545719,
                #     -0.003175088311588541,
                #     0.026823418752853278,
                #     -0.03326717176061062,
                #     0.0023754291208026705,
                #     -0.01584431217199215,
                #     0.016100487897210573,
                #     -0.003991784398373939,
                #     0.005423454395627711,
                #     -0.028621908321939355,
                #     -0.01893029249172009,
                #     -0.026448490880298447,
                #     0.003477305375045963,
                #     0.009788499466830853,
                #     0.011274716989913792,
                #     -0.012344828582791779,
                #     0.012161388734470967,
                #     -0.006426139337253725,
                #     0.014180718003359074,
                #     0.0121876908282586,
                #     0.023897897330953047,
                #     0.023980704374691235,
                #     -0.019518050381743205,
                #     0.005054096497446555,
                #     -0.0061291045004770745,
                #     -0.006468775971376461,
                #     -0.033756246803057444,
                #     0.009695460778321308,
                #     -0.016045587852769872,
                #     0.006380439739007511,
                #     -0.00948631523264613,
                #     0.01755936469939366,
                #     0.0037704054446828297,
                #     -0.004252220702988758,
                #     -0.0020780926104509738,
                #     0.01156922131101884,
                #     0.006623523364394776,
                #     -0.002271655855387091,
                #     -0.011805769024225452,
                #     -0.012761760518225458,
                #     0.00806954490403035,
                #     0.012068709971568035,
                #     -0.009840885034050222,
                #     0.004861249996398487,
                #     -0.018097976807807486,
                #     0.011552892154888006,
                #     0.0027662054910342865,
                #     -0.011602338070761624,
                #     -0.0012915549816976096,
                #     -0.0021852029040635343,
                #     0.008603378219301662,
                #     -0.014361820660620089,
                #     -0.019302547302771634,
                #     -0.013507316601428147,
                #     -0.010344445824485496,
                #     0.00034483743820547713,
                #     -0.0003891747714859245,
                #     0.01974047647391419,
                #     -0.0078027231802927045,
                #     0.002234644988010461,
                #     -0.005138603190124944,
                #     -0.004559247870761967,
                #     0.014273752218990667,
                #     0.0037996194834883862,
                #     -0.0028827807522185376,
                #     0.01738165526486796
                # ]

            elif robot == 'GECKOBOTIV_V2':
                init_parameter_set = set_BC + set_CF + set_FT + set_TT + set_BB
            
            elif robot == 'GECKOBOTIV_V3':
                init_parameter_set = set_BC + set_CF + set_FT + set_TT + set_BB + set_T1 + set_T2

            else:
                init_parameter_set = set_BC + set_CF + set_FT

        elif encoding == "sindirect":
            if robot == 'LAIKAGO':
                init_parameter_set = set_BC + set_BC + set_CF + set_CF + set_FT + set_FT
            elif robot == 'GECKOBOTIV':
                init_parameter_set = set_BC + set_BC + set_CF + set_CF + set_FT + set_FT + set_TT + set_TT
            elif robot == 'GECKOBOTIV_V2':
                init_parameter_set = set_BC + set_BC + set_CF + set_CF + set_FT + set_FT + set_TT + set_TT + set_BB
            elif robot == 'GECKOBOTIV_V3':
                init_parameter_set = set_BC + set_BC + set_CF + set_CF + set_FT + set_FT + set_TT + set_TT + set_BB + set_T1 + set_T2
                
            else:
                init_parameter_set = set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT
                
        elif encoding == "direct":
            if robot == 'LAIKAGO':
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT
            elif robot == 'GECKOBOTIV':
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT + set_TT + set_TT + set_TT + set_TT
            elif robot == 'GECKOBOTIV_V2':
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT + set_TT + set_TT + set_TT + set_TT + set_BB
            elif robot == 'GECKOBOTIV_V3':
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT + set_TT + set_TT + set_TT + set_TT + set_BB + set_T1 + set_T2
            else:
                init_parameter_set = set_BC + set_BC + set_BC + set_BC + set_BC + set_BC + set_CF + set_CF + set_CF + set_CF + set_CF + set_CF + set_FT + set_FT + set_FT + set_FT + set_FT + set_FT
        else:
            print('[ ERROR] Unknown encoding')

        # Sensor Parameter Set
        init_sensor_parameter_set = [0] * (numKernals*9)

        return init_parameter_set, init_sensor_parameter_set

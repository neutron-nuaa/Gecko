# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 44):
# This software was written by Mathias Thor <mathias@mmmi.sdu.dk> in 2018
# As long as you retain this notice you can do whatever you want with it.
# If we meet some day, and you think this stuff is worth it, you can
# buy me a beer in return.
# ----------------------------------------------------------------------------

#!/bin/bash
set -e

PROGNAME="test_angles.sh"

die() {
    echo "$PROGNAME: $*" >&2
    exit 1
}

usage() {
    if [ "$*" != "" ] ; then
        echo "Error: $*"
    fi

    cat << EOF
Usage: $PROGNAME [OPTION ...] [foo] [bar]
Options:
-h, --help                          display this usage message and exit
-n, --testname [STRING]             name of the test
-e, --encoding [STRING]             encoding of CPGRBFN
-r, --robot [STRING]                robot type
-a, --max-angle [FLOAT]             maximum angle to test (degree)
-s, --step                          angle step

EOF
    exit 1
}

sleeptime=2
dataname="noname-"
timestamp=`date "+%d%m%H%M"`
encoding="noinput"
robot="noinput"
maxAngle=80
step=20

if [ -z "$1" ]
  then
    usage "No argument supplied"
fi

while [ $# -gt 0 ] ; do
    case "$1" in
    -h|--help)
        usage
        ;;
    -n|--testname)
        dataname="$2-"
        shift
        ;;
    -e|--encoding)
        encoding="$2"
        shift
        ;;
    -r|--robot)
        robot="$2"
        shift
        ;;
    -a|--max-angle)
        maxAngle="$2"
        shift
        ;;
    -s|--step)
        step="$2"
        shift
        ;;
    -*)
        usage "Unknown option '$1'"
        ;;
    *)
        usage "Invalid parameter was provided: '$1'"
        ;;
    esac
    shift
done

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

i=80
timestamp=`date "+D%d%mT%H%M%S"`
newdirname="RL_data_${timestamp}.dat"
mkdir ./../data/storage/${newdirname}

createFile=1

find $parent_path/../data/jobs/ -type f -delete

while [ $i -le $maxAngle ]; do
    # Delete everything in Jobs
    find $parent_path/../data/jobs/ -type f ! -name "angles_test_log.txt" -delete

    # Perform Trial
    echo " "
    echo "----"
    echo "Angle $i / $maxAngle!"
    echo "----"
    if [ $encoding == "noinput" ] && [ $robot == "noinput" ]; then
        python3 test_angles.py -a $i -f $createFile # >/dev/null
    elif [ $encoding == "noinput" ]; then
        python3 test_angles.py -a $i -f $createFile -r $robot # >/dev/null
    elif [ $robot == "noinput" ]; then
        python3 test_angles.py -a $i -f $createFile -e $encoding # >/dev/null
    else
        python3 test_angles.py -a $i -f $createFile -e $encoding -t $robot # >/dev/null
    fi

    # Move result to storage
    newname="RL_data-t$i.dat"
    cp -r $parent_path/../data/jobs/ $parent_path/../data/storage/${newdirname}/${newname}

    i=$(( i + step ))
    createFile=0
done

echo "Ending Program!"
#!/usr/bin/env python

import sys


def cleanup(*args):
    node.stop_sim()
    sys.exit()


def main(argv):
    global node

    if len(sys.argv) < 4+1:
        print("Please provide following arguments: \nTrial length in seconds (int/arg1) \nNumber of trials (int/arg2) \nSimulation number (int/arg3) \nRollout number (int/arg4)\nBlackout (bool/arg5)")
        exit()

    sleep_time      = int(sys.argv[1])
    trials          = int(sys.argv[2])
    simulationID    = int(sys.argv[3])
    rollout         = int(sys.argv[4])
    blackout        = int(sys.argv[5])
    trial_count     = 1
    dt              = 0.0167




    sys.exit()


if __name__ == '__main__':
    main(sys.argv[1:])

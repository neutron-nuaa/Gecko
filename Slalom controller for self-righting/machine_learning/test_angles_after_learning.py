#!/usr/bin/env python

import atexit
from ctypes import sizeof
import json
import math
from mimetypes import init
import os
import os.path
import csv
import shutil
import sys, getopt
import time
import RL_plotter
import RL_workerhandle
import RL_encoding
import numpy as np
import subprocess

def main(argv):
    global parameter_arr, noise_arr, prob_arr
    start = time.time()

    ############################
    #  FILESYSTEM PARAMETERS   #
    ############################
    file_dir        = './../data/jobs'
    file_name       = file_dir+'/RL_job'
    file_answer_dir = file_dir+'/answers'
    file_answer_name= file_answer_dir+'/answer'
    file_output_dir = file_dir+'/test_angles_outputs'

    ############################
    #   LEARNING PARAMETERS    #
    ############################
    rollouts    = 11        # 10 Runs pr. angles
    variance    = 0.015     # 0.015 Variance on the parameter set 
    iteration   = 0         # Number of iterations
    sim_length  = 10        # 6 eller 10 Length of one roll out in s.  3.11 hour = (((10*(8/4))*350)/3600)*1.6       training time = (((sim_time * (rollouts/workers)))*max_iterations)/3600)*1.6  
    workers     = 1         # 1 Worker available
    max_fitness = 0         # Maximum fitness for the run
    max_iterations = 1      # Only 1 set of 10 rollouts
    rbfneurons  = 20        # default 20


    robot    = "GECKOBOTIV"     # GECKOBOTIV, MORF, LAIKAGO, ALPHA
    encoding = "direct"       # direct, indirect, sindirect
    angle    = 0
    createFile = 0
    addWalking = False
    
    myopts, args = getopt.getopt(sys.argv[1:], "e:r:i:t:a:f:w:")
    for o, a in myopts:
        if o == '-e':
            encoding = a
        elif o == '-r':
            rollouts = int(a)
        elif o == '-i':
            max_iterations = int(a)
        elif o == '-t':
            robot = a
        elif o == '-a':
            angle = int(a)
        elif o == '-f':
            createFile = int(a)
        elif o == '-w':
            addWalking = bool(int(a))
        else:
            print("Usage: %s -i input -o output" % sys.argv[0])

    print("Encoding:     \t" + str(encoding))
    print("rollouts:     \t" + str(rollouts))
    print("iterations:   \t" + str(max_iterations))
    print("robot:        \t" + str(robot))
    print("initial angle:\t" + str(angle))
    print("add walking:  \t" + str(addWalking))

    # Converting angle to rad
    angle = (angle/180)*math.pi

    workerhandle    = RL_workerhandle.WORKERHANLDE(workers, rollouts, sim_length, file_answer_dir, robot, angle, addWalking)

    atexit.register(cleanup, workerhandle)

    ############################
    # PARAMETER SETUP FOR RBFN #
    ############################
    encoder         = RL_encoding.encoder()
    init_parameter_set, init_sensor_parameter_set = encoder.get_init_parameter_set(robot, encoding, rbfneurons)
    # Load parameter set
    prior_knowledge = file_dir+"/../best_data/RL_job_best.json"
    with open(prior_knowledge) as json_file:
       time.sleep(0.25)
       data = json.load(json_file)
       init_parameter_set = data['ParameterSet']+[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]+[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    ############################
    #  END OF PARAMETER SETUP  #
    ############################

    # Initialize answer directory
    if os.path.exists(file_answer_dir):
        shutil.rmtree(file_answer_dir)

    os.mkdir(file_answer_dir)


    # Init log file if it does not exist
    if (createFile==1):
        log_progress(True, file_output_dir, 0, 0, 0)

    # Create first work paper -> for more simplicity we keep the data structure already existing
    noise_arr = [[0]*len(init_parameter_set)] * rollouts
    with open(file_name + '.json', 'w') as json_file:
        data = {}
        data['iteration']   = iteration
        data['rollouts']    = rollouts


        for k in range(rollouts):
            # Zeros again -> we don't want any inputs
            noise = np.zeros(len(init_parameter_set))


            data['noise_'+str(k)] = noise.tolist()
            noise_arr[k] = noise
        
        data['ParameterSet'] = init_parameter_set
        data['SensorParameterSet'] = init_sensor_parameter_set
        data['checked'] = encoding

        json.dump(data, json_file, indent=4, sort_keys=True)
        json_file.write("\n\n\n\n\n\n\n\n")

    parameter_arr = init_parameter_set

#####
    while True:
        # Reset arrays
        fitness_arr         = [-1.0]*rollouts
        landing_success     = [-1.0]*rollouts

        print("\nInitial angle: " + str(angle) + " → " + "Rollout: ", end="", flush=True)

        # Start Working
        dt_start = time.time()
        workerhandle.work()

        # Collect answers
        for k in range(rollouts):
            with open(file_answer_name + '_' + str(k) + '.json') as json_file:
                time.sleep(0.25)
                data = json.load(json_file)
                fitness_arr[k]     = data['FitnessValue']
                landing_success[k] = (data['Success']+1)/2


        # Removes all files in 'file_answer_dir'
        shutil.rmtree(file_answer_dir)
        os.mkdir(file_answer_dir)

        fit_mean = sum(fitness_arr) / len(fitness_arr)
        landing_success_mean = sum(landing_success) / len(landing_success)

        print("\n=== Avg ===")
        print("fit  ", fit_mean)
        print("Landing success  ", landing_success_mean)


        # -- Print out -- 

        # Simple Normal Dist.
        print(" (V: " + str(round(variance,5)) + ", dt: " + str(round(time.time()-dt_start,2)) + ", t: " + str(round(time.time()-start,2)) + ")", end="", flush=True)



        # Save fitness and success infos
        log_progress(False, file_output_dir, angle, fitness_arr[1:], landing_success[1:])


        # Backup and cleanup
        os.rename(file_name + '.json', file_name + "_" + str(iteration) + '.json')
        if max(fitness_arr) > max_fitness:
            max_fitness = max(fitness_arr)
            shutil.copyfile(file_name + "_" + str(iteration) + '.json', file_name + "_" + "best" + '.json')

        if iteration >= max_iterations-1 != -1:
            break
        else:
            iteration += 1

        # Generate new json for next iteration
        with open(file_name + '.json', 'w+') as json_file:
            data = {}
            data['iteration'] = iteration
            data['rollouts']  = rollouts
            
            for k in range(rollouts):
                # Zeros
                noise = np.zeros(len(init_parameter_set))
                
                data['noise_'+str(k)] = noise.tolist()
                noise_arr[k] = noise

            data['ProbabilitySet']  = [0]*rollouts
            data['ParameterSet']    = parameter_arr
            data['checked']         = encoding

            json.dump(data, json_file, indent=4, sort_keys=True)
            json_file.write("\n\n\n\n\n\n\n\n")


def log_progress(init, file_dir, _angle, fitness_arr, landing_success_arr):
    if init:
        RL_log = open(file_dir + "/angles_test_log.txt", "w")
        RL_log.write("angle\tsuccess_avg\tfitness_arr_avg\tfitness_arr_max\tfitness_arr_min")
        RL_log.close()
    else:
        max_FT          = max(fitness_arr)

        avg_FT          = sum(fitness_arr)/len(fitness_arr)
        avg_SCCSS       = sum(landing_success_arr) / len(landing_success_arr)

        min_FT         = min(fitness_arr)

        RL_log = open(file_dir + "/angles_test_log.txt", "a")
        RL_log.write("\n%f\t%f\t%f\t%f\t%f" % (_angle, avg_SCCSS, avg_FT, max_FT, min_FT))
        RL_log.close()



def cleanup(workerhand):

    workerhand.process_cleaner_all()

    print('cleaned up!')


if __name__ == '__main__':
    main(sys.argv[1:])

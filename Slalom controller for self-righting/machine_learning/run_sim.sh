# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 44):
# This software was written by Mathias Thor <mathias@mmmi.sdu.dk> in 2018
# As long as you retain this notice you can do whatever you want with it.
# If we meet some day, and you think this stuff is worth it, you can
# buy me a beer in return.
# ----------------------------------------------------------------------------

#!/bin/bash
set -e

PROGNAME="run_sim.sh"

die() {
    echo "$PROGNAME: $*" >&2
    exit 1
}

usage() {
    if [ "$*" != "" ] ; then
        echo "Error: $*"
    fi

    cat << EOF

Usage: $PROGNAME [OPTION ...] [foo] [bar]
Options:
-h, --help                          display this usage message and exit
-t, --time [INTEGER]                length of simulation in seconds
-i, --idsim [INEGER]                ID of the simulation that should host the simulation

EOF
    exit 1
}

simtime=10 # length of the simulation instance (seconds)
simID=1    # simulation ID that should host the simulation
rollout=-1 # uses no noise
blackout=0 # dont black out the simulation
optimize_selector=-1 # 1="only parameters", 2="only feedback", 3="both", -1="base parameters"

while [ $# -gt 0 ] ; do
    case "$1" in
    -h|--help)
        usage
        ;;
    -t|--time)
        simtime=$2
        shift
        ;;
    -i|--idsim)
        simID=$2
        shift
        ;;
    -*)
        usage "Unknown option '$1'"
        ;;
    *)
        usage "Invalid parameter was provided: '$1'"
        ;;
    esac
    shift
done

echo "*******************"
echo "-"1 = "base parameters"
echo " "1 = "only parameters"
echo " "2 = "only feedback"
echo " "3 = "both"
echo ""
read -p 'Option: ' optimize_selector

echo " "
echo "*** CONTROLLER OUTPUT ***"
./../interfaces/geckobotiv/sim/build_dir/bin/geckobotiv_controller $simID $rollout $simtime $blackout $optimize_selector


#!/usr/bin/env python

# USELESS

import sys, getopt
import rospy
from std_msgs.msg import Float32

def run_node(argv):
    
    myopts, args = getopt.getopt(sys.argv[1:], "a:")
    for o, a in myopts:
        if o == '-a':
            initAngle = int(a)
        else:
            print("Usage: %s -i input -o output" % sys.argv[0])

    rospy.init_node("talker")
    initAnglePub = rospy.Publisher("/initAngle", Float32, queue_size=1)

    rate = rospy.Rate(2) #Hz
    while not rospy.is_shutdown():
        initAnglePub.publish(initAngle)
        rate.sleep()

if __name__ == '__main__':
    try :
        run_node(sys.argv[1:])
    except rospy.ROSInterruptException:
        pass

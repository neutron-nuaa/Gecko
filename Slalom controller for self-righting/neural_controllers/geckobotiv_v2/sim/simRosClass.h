//
// Created by mat on 8/2/17.
//

#ifndef ROS_HEXAPOD_CONTROLLER_SIMROSCLASS_H
#define ROS_HEXAPOD_CONTROLLER_SIMROSCLASS_H

#include <cstdio>
#include <cstdlib>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/ColorRGBA.h"
#include <std_msgs/Int32.h>
#include "sensor_msgs/Joy.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/Temperature.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include "neutronMotorDefinition.h"
#include <random>
#include <iostream>

#include "extApi.h"
#include "extApiPlatform.h"
#include "simConst.h"


class simRosClass {
private:
    // Topics
    std::string MotorTopic;
    std::string MotorBackboneTopic;
    std::string jointPositionTopic;
    std::string jointTorqueTopic;
    std::string jointVelocityTopic;
    std::string testParametersTopic;
    std::string landingParametersTopic;
    std::string nameAddOn;
    std::string historyTopic;

    // Subscribers
    ros::Subscriber jointPositionSub;
    ros::Subscriber jointTorqueSub;
    ros::Subscriber jointVelocitySub;
    ros::Subscriber testParametersSub;
    ros::Subscriber landingParametersSub;
    ros::Subscriber joySub;
    ros::Subscriber imu_imu;
    ros::Subscriber imu_euler;
    ros::Subscriber historySub;

    // Publishers
    ros::Publisher MotorPositionPub;
    ros::Publisher MotorBackbonePositionPub;

    // Private Methods
    void jointPositionCallback(const std_msgs::Float32MultiArray& jointPositions);
    void jointTorqueCallback(const std_msgs::Float32MultiArray& jointTorques);
    void jointVelocityCallback(const std_msgs::Float32MultiArray& jointVelocities);
    void testParametersCallback(const std_msgs::Float32MultiArray& jointPositions);
    void landingParametersCallback(const std_msgs::Float32MultiArray& _landingParameters);
    void joy_CB(const sensor_msgs::Joy::ConstPtr& joy);
    void imu_imu_CB(const sensor_msgs::Imu::ConstPtr& imu);
    void imu_euler_CB(const geometry_msgs::Vector3::ConstPtr& euler);
    void historyCallback(const std_msgs::Float32MultiArray& _history);

    // Simulation remote API
    int clientID = -1;


public:
    // Public Methods
    simRosClass(int simulationID);
    ~simRosClass();
    void setLegMotorPosition(std::vector<float> positions);
    void setBackboneMotorPosition(std::vector<float> positions);
    void rosSpinOnce();
    bool shortMorf = true;

    // Public Global Variables
    std::vector<float> testParameters    ={0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> landingParameters ={0,0,0};
    std::vector<float> jointPositions    ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointTorques      ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> jointVelocities   ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::vector<float> axes              ={0,0,0,0,0,0,0,0};
    std::vector<int>   buttons           ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

    int bodyFloorCollisions     = 100;
    float robotCollision        = 100.0;
    float avgPower              = 100.0;
    float avgBodyVel            = 100.0;
    float distance              = 100.0;
    float heightVariance        = 100.0;
    float headingDirection      = 100.0;
    float tilt                  = 100.0;
    float roll                  = 100.0;
    float slipping              = 100.0;

    int upside_down = 0;
    int landing_completed = -1;

    float cos_alpha = 0.0;
    float nb_landing_tips = 0;

    std::vector<float> history;
};


#endif //ROS_HEXAPOD_CONTROLLER_SIMROSCLASS_H

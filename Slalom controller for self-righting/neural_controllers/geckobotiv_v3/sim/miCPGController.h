//*******************************************
//*                                         *
//*         mi cpg controller               *
//*                                         *
//*******************************************
// author: Arthicha Srisuchinnawong, Dong Hao
// edited: Worasuchad Haomachai
// contract: haomachai@gmail.com
// data: 11/07/2018
// version: 4.0.0

#ifndef MICPGCONTROLLER_H
#define MICPGCONTROLLER_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdint.h>

using namespace std;


class MICPGController {
public:

    /* The constructor doesn't take any input parameter.
    the CPG consist of two neuron with weigth w12, w21
    w11 and w22, this give 2 periodic output signals */
    MICPGController();

    // set the initial parameter of CPG controller
    void setParameter(double output1_2, double bias1_2,
        double w11_22, double wd1, double MI);

    // function to change MI for adapation
    void setMI(double newMI);

    // function that updates the output of the CPG network
    void run();

    /* function that return the output signal of the CPG
    network, 
            if number is 1 the output is signal 1
            if number is 2 the output is signal 2     */
    double getSignal(int number);

private:
    // private attribute
    // weigth and activity
    double output1;
    double output2;
    double bias1_2;
    double w11_22;
    double w12;
    double w21;
    double act1;
    double act2;
    double wd;
};


#endif //MICPGCONTROLLER_H

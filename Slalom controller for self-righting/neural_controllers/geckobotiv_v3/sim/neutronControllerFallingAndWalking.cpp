//
// Created by mat on 12/30/17.
//

#include "neutronControllerFallingAndWalking.h"

neutronControllerFallingAndWalking::neutronControllerFallingAndWalking(int argc,char* argv[]) : neutronController(argc, argv) {
    
    fallingPolicyWeights   = readParameterSet(false);
    walkingPolicyWeights   = readParameterSet(true);
    isWalking = false;

    if(CPGLearning) {
        CPGAdaptive_cpg = new modularController(1, true);
        CPGAdaptive_cpg->setPhii(0.01*M_PI);
    } else {
        // Falling :
        fallingCPGAdaptive = new rbfcpg(fallingPolicyWeights, encoding, 20);
        // CPGAdaptive->setPhii(0.01*M_PI);                             // cpg so2 model
        fallingCPGAdaptive->setMI(0.18);                                       // cpg mi model (set new mi)

        // Walking
        walkingCPGAdaptive = new rbfcpg(walkingPolicyWeights, encoding, 20);
        walkingCPGAdaptive->setMI(0.18);                                       // cpg mi model (set new mi)

        CPGPeriodPostprocessor = new postProcessing();
    }

    // Calculate init period and fill all delay line's
    for (int i = 0; i < tau+1; ++i) {
        // CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));          // cpg so2 model
        CPGPeriodPostprocessor->calculateAmplitude(fallingCPGAdaptive->getCpgOfMI(2), fallingCPGAdaptive->getCpgOfMI(1));                 // cpg mi model
        CPGPeriod = CPGPeriodPostprocessor->getPeriod();
        fallingCPGAdaptive->step();
        // vector<double> RBF_output = CPGAdaptive->getNetworkOutput();  // TODO change by happ
        // for (int j = 0; j < 16; ++j)
        //     delayline[j].Write(RBF_output[j]);

        // TODO added by happ
        // vector<double> Travel_body = {CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(0)};         // cpg so2 model
        // vector<double> Travel_body = {fallingCPGAdaptive->getCpgOfMI(1), fallingCPGAdaptive->getCpgOfMI(1), fallingCPGAdaptive->getCpgOfMI(1)};                  // cpg mi model
        // for (int j = 0; j < 3; ++j)
        //     delayline[j].Write(Travel_body[j]);

        // Walking
        CPGPeriodPostprocessor->calculateAmplitude(walkingCPGAdaptive->getCpgOfMI(2), walkingCPGAdaptive->getCpgOfMI(1));                 // cpg mi model
        CPGPeriod = CPGPeriodPostprocessor->getPeriod();
        walkingCPGAdaptive->step();

        for (auto &i : delayline)
            i.Step();
    }
    justLanded = true;

}

int neutronControllerFallingAndWalking::runController() {
    if(ros::ok()) {
        if(simulation)
            env->synchronousTrigger();

        if(CPGLearning) { // False
            CPGAdaptive_cpg->step();
            tripodGaitRangeOfMotion(walkingPolicyWeights, encoding);
        } else {
            if (isWalking) {
                CPGPeriodPostprocessor->calculateAmplitude(walkingCPGAdaptive->getCpgOfMI(2), walkingCPGAdaptive->getCpgOfMI(1));                         // cpg mi model
                
                CPGPeriod = CPGPeriodPostprocessor->getPeriod();
                walkingCPGAdaptive->step();

                walkingTripodGaitRBFN_noDelayline();
            } else {
                // CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));                  // cpg so2 model
                CPGPeriodPostprocessor->calculateAmplitude(fallingCPGAdaptive->getCpgOfMI(2), fallingCPGAdaptive->getCpgOfMI(1));                         // cpg mi model
                
                CPGPeriod = CPGPeriodPostprocessor->getPeriod();
                fallingCPGAdaptive->step();
                //tripodGaitRBFN();     // changed commented out
                fallingTripodGaitRBFN_noDelayline();
            }
        }

        rosHandle->setLegMotorPosition(positions);

        //---- setting backbone joints ----//
        rosHandle->setBackboneMotorPosition({0,AxialJointPosition,0});
        rosHandle->setTailMotorPosition(tailPositions);

        for (auto &i : delayline)
            i.Step();

        rosHandle->rosSpinOnce();

    } else {
        return 0;
    }

    // Stop simulation after 'simulationTime' seconds
    if(simulation) {
        if ( env->getSimulationTime() > simulationTime * 3000) {
            if ( rosHandle->slipping == 100.0 ){
                cout << "'";
                return 2;
            } else {
                fitnessLogger();
                historyLogger();
            }
            return 0;
        }


        // Stop simulation if the robot is upside-down on the floor
        if (rosHandle->upside_down==1) {
            cout << "\n[INFO] Robot upside-down: stoping simulation." << endl;
            fitnessLogger();
            historyLogger();
            return 0;
        }
        // Start walking if the landing is successfull
        if (rosHandle->landing_completed==1) {
            if (justLanded) {
                cout << "\n[INFO] Landing success. Start walking" << endl;
                justLanded = false;
            }
            isWalking=true;
        }
    }
    return 1;
}

void neutronControllerFallingAndWalking::fallingTripodGaitRBFN_noDelayline() {    // added by phil

    vector<double> RBF_output = fallingCPGAdaptive->getNetworkOutput();

    float BC_offset = 0.0;
    float CF_offset = -0.785398163;
    float FT_offset = 0.0;
    float TT_offset = 0.785398163;


    positions.at(BC0) = RBF_output[0];
    positions.at(BC1) = RBF_output[1];
    positions.at(BC2) = RBF_output[2];
    positions.at(BC3) = RBF_output[3];
    positions.at(BC4) = 0;
    positions.at(BC5) = 0;

    positions.at(CF0) = RBF_output[4];
    positions.at(CF1) = RBF_output[4];
    positions.at(CF2) = RBF_output[6];
    positions.at(CF3) = RBF_output[7];
    positions.at(CF4) = 0;
    positions.at(CF5) = 0;

    positions.at(FT0) = RBF_output[8];
    positions.at(FT1) = RBF_output[9];
    positions.at(FT2) = RBF_output[10];
    positions.at(FT3) = RBF_output[11];
    positions.at(FT4) = 0;
    positions.at(FT5) = 0;

    positions.at(TT0) = RBF_output[12];
    positions.at(TT1) = RBF_output[13];
    positions.at(TT2) = RBF_output[14];
    positions.at(TT3) = RBF_output[15];

    AxialJointPosition = RBF_output[16];

    tailPositions.at(0) = RBF_output[17];
    tailPositions.at(1) = RBF_output[18];
}

void neutronControllerFallingAndWalking::walkingTripodGaitRBFN_noDelayline() {    // added by phil

    vector<double> RBF_output = walkingCPGAdaptive->getNetworkOutput();

    float BC_offset = 0.0;
    float CF_offset = -0.785398163;
    float FT_offset = 0.0;
    float TT_offset = 0.785398163;


    positions.at(BC0) = RBF_output[0];
    positions.at(BC1) = RBF_output[1];
    positions.at(BC2) = RBF_output[2];
    positions.at(BC3) = RBF_output[3];
    positions.at(BC4) = 0;
    positions.at(BC5) = 0;

    positions.at(CF0) = RBF_output[4];
    positions.at(CF1) = RBF_output[4];
    positions.at(CF2) = RBF_output[6];
    positions.at(CF3) = RBF_output[7];
    positions.at(CF4) = 0;
    positions.at(CF5) = 0;

    positions.at(FT0) = RBF_output[8];
    positions.at(FT1) = RBF_output[9];
    positions.at(FT2) = RBF_output[10];
    positions.at(FT3) = RBF_output[11];
    positions.at(FT4) = 0;
    positions.at(FT5) = 0;

    positions.at(TT0) = RBF_output[12];
    positions.at(TT1) = RBF_output[13];
    positions.at(TT2) = RBF_output[14];
    positions.at(TT3) = RBF_output[15];

    AxialJointPosition = 0;

    tailPositions.at(0) = 0;
    tailPositions.at(1) = 0;
}


vector<float> neutronControllerFallingAndWalking::readParameterSet(bool walking) {
    ifstream ifs("./../data/jobs/RL_job.json");
    if (walking)
        ifs = ifstream("./../data/walking_ability/RL_job_best.json");
    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document document;
    document.ParseStream(isw);

    assert(document.IsObject());
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    assert(document.HasMember("ParameterSet"));
    assert(document["ParameterSet"].IsArray());
    rapidjson::Value& paramset = document["ParameterSet"];

    std::string noise_name = "noise_" + std::to_string(rollout); //rollout not set in time!

    if(!document.HasMember(noise_name.c_str())){
        cout << "[ERROR] No noise member called: " << noise_name.c_str() << endl;
        cout << "[ERROR] Using noise member \"noise_0\" again" << endl;
        noise_name = "noise_0";
    }

    rapidjson::Value& noise = document[noise_name.c_str()];

    vector<float> policyWeights;
    for (rapidjson::SizeType i = 0; i < paramset.Size(); i++) // Uses SizeType instead of size_t
        policyWeights.push_back(paramset[i].GetDouble() + noise[i].GetDouble());

    // Get encoding
    assert(document.HasMember("checked"));
    assert(document["checked"].IsString());
    rapidjson::Value& _encoding = document["checked"];
    encoding = _encoding.GetString();

    ifs.close();

    return policyWeights;
}


neutronControllerFallingAndWalking::~neutronControllerFallingAndWalking() {
    if(CPGLearning)
        delete CPGAdaptive_cpg;
    else { delete walkingCPGAdaptive; delete fallingCPGAdaptive; }
    delete CPGPeriodPostprocessor;

    env->stop();
    env->endConnection();
    delete env;

    ros::shutdown();
    delete rosHandle;

    // motorFile.close ();
}
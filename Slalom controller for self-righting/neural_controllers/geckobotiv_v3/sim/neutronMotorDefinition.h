//
// Created by mat on 8/26/17.
//

#ifndef NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H
#define NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H

enum NeutronSensorNames{
    NEURON_SENSORAX = 0,
};

enum NeutronMotorNames{
    BC0 = 0,
    CF0 = 1,
    FT0 = 2,
    TT0 = 3,

    BC1 = 4,
    CF1 = 5,
    FT1 = 6,
    TT1 = 7,

    BC2 = 8,
    CF2 = 9,
    FT2 = 10,
    TT2 = 11,

    BC3 = 12,
    CF3 = 13,
    FT3 = 14,
    TT3 = 15,

    BC4 = 16,
    CF4 = 17,
    FT4 = 18,

    BC5 = 19,
    CF5 = 20,
    FT5 = 21,

    //Changing according to the maximum motor number
    NEURON_MOTOR_MAX = 16,
};

#endif //NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H



#ifndef NEUTRON_CONTROLLER_NEUTRONCONTROLLERFALLINGANDWALKING_H
#define NEUTRON_CONTROLLER_NEUTRONCONTROLLERFALLINGANDWALKING_H

#include "neutronController.h"

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <string>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "simRosClass.h"            // TODO this is changed          
//#include "realRosClass.h" 
#include "modularController.h"
#include "neutronMotorDefinition.h"
#include "delayline.h"
#include "dualIntegralLearner.h"
#include "postProcessing.h"
#include "rbfcpg.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "environment.h"
#include "Data_logger.h"

#include <chrono>
#include <thread>
#include <math.h>

class dualIntegralLearner;
class simRosClass;
class postProcessing;
class rbfcpg;

class neutronControllerFallingAndWalking : public neutronController {
public:
    neutronControllerFallingAndWalking(int argc,char* argv[]);
    ~neutronControllerFallingAndWalking();
    int runController();

private:
    vector<float> readParameterSet(bool walking);
    void fallingTripodGaitRBFN_noDelayline();  // added by phil
    void walkingTripodGaitRBFN_noDelayline();

    bool isWalking;

    vector<float>       fallingPolicyWeights;
    vector<float>       walkingPolicyWeights;

    rbfcpg * fallingCPGAdaptive;
    rbfcpg * walkingCPGAdaptive;

    bool justLanded;

};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLERFALLINGANDWALKING_H

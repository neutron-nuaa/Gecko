//
// Created by mat on 12/30/17.
//

#ifndef NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
#define NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <string>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "realRosClass.h"
#include "modularController.h"
#include "neutronMotorDefinition.h"
#include "delayline.h"
#include "dualIntegralLearner.h"
#include "postProcessing.h"
#include "joystick.h"
#include "rbfcpg.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"

class dualIntegralLearner;
class realRosClass;
class postProcessing;
class rbfcpg;

class neutronController {
public:
    neutronController(int argc,char* argv[]);
    bool runController();
private:
    double rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter);

    void tripodGaitRangeOfMotion(vector<float> weightsMatlab, string encoding);
    void tripodGaitRBFN();
    void logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                  double positionX, double bodyVel, double angularVelocity, double jointTorque,
                  double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL);

    void fitnessLogger();
    vector<float> readParameterSet(string job);

    ofstream myfile;

    vector<float> positions;
    std::vector<float> data;
    vector<float> weightsMatlab;
    vector<float> weightsMatlab_bm;

    bool CPGLearning = false;

    double waiter = 200; // 5 SECONDS
    bool simulation = true;
    double phiParam = 0.01*M_PI;

    bool transientState = true;

    int CPGmethod = 0;
    double errorMargin = 0.000;

    double BC_pos = 0.3;
    double CF_pos = 1.7;
    double FT_pos = -0.2;
    float CF_threshold = 0.275;

    bool run_controller = false;
    bool broken_middleleg = false;

    string encoding;

    int CPGPeriod=0;
    int tau=300;
    vector<Delayline> phaseShift;

    dualIntegralLearner * learner;
    rbfcpg * CPGAdaptive;
    modularController * CPGAdaptive_cpg;
    realRosClass * rosHandle;
    postProcessing * sensorPostprocessor;
    postProcessing * controllerPostprocessor;
    postProcessing * sensorAmpPostprocessor;
    postProcessing * controllerAmpPostprocessor;
    postProcessing * CPGPeriodPostprocessor;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

# Generic neural locomotion control framework

__The framework is based on neural control and black-box optimization. The neural control combines a central pattern generator (CPG) and a radial basis function (RBF) network into a CPG-RBF network. The control network acts as a neural basis that can produce arbitrary rhythmic trajectories for the joints of robots.__

- [Install](#install)
- [Getting Started](#getting-started)

## Install

First, we need to set up simulation workers. Typically the workers are installed in your home directory - but it can be anywhere.
1. Download the latest version of CoppeliaSim (aka V-REP) [from the downloads page](http://www.coppeliarobotics.com/downloads.html)
2. Extract the downloaded .zip file in the CoppeliaSim directory as many times as you need workers (e.g., four times)
3. Go to the extracted directories - these should be called `SIM1`, `SIM2`, `SIM3`, `SIM4`, etc.
4. In `/SIM#/remoteApiConnections.txt` change `portIndex1_port` so that SIM1 has 19997, SIM2 has 19996, SIM3 has 19995, SIM4 has 19994, and SIM**n** has 19997-**n**
5. copy and (re)place `libv_repExtRosInterface.so` in `/CPGRBFN_slalom/utils/libv_repExtRosInterface.so` ​to all `SIM#` directories.
6. copy and (re)place `libsimExtVortex.so` in `/SIM#/vortexPlugin/libsimExtVortex.so` ​to all `SIM#` directories.

Now we need to install the required python libraries:

```bash
sudo apt install python3-pip
pip3 install -r requirements.txt
```

The neural controllers use ROS to communicate with CoppeliaSim. So make sure that you have `ros-xxx-desktop-full` installed ([Install ROS](http://wiki.ros.org/ROS/Installation))

And optionally also:

```bash
sudo add-apt-repository ppa:giuspen/ppa
sudo apt-get update
sudo apt-get install x-tile
```

You should be good to go!

#### Running Headless

If you plan to run on a headless machine, you will also need to run with a virtual framebuffer. E.g.

```bash
sudo apt-get install xvfb
xvfb-run python3 my_pyrep_app.py
```

#### Troubleshooting

Below are some problems you may encounter during installation. If none of these solves your problem, please raise an issue.
- error: command `x86_64-linux-gnu-gcc` failed
  - You may be missing packages needed for building python extensions. Try: `sudo apt-get install python3-dev`, and then re-run the installation.

## Getting Started

1. Open a terminal and run:
```bash
roscore
```
2. Open a new terminal (or tab) and run:
```bash
cd {SIM1_WORKER_ROOT}/SIM1/
./coppeliaSim.sh ~/CPGRBFN_slalom/simulations/RL_slalom_soft_spine_fast_low_timeStep.ttt
```
3. Open a new terminal (or tab) and run:
```bash
cd {SIM2_WORKER_ROOT}/SIM2/
./coppeliaSim.sh ~/CPGRBFN_slalom/simulations/RL_slalom_soft_spine_fast_low_timeStep.ttt
```
4. Open a new terminal (or tab) and run:
```bash
cd {SIM3_WORKER_ROOT}/SIM3/
./coppeliaSim.sh ~/CPGRBFN_slalom/simulations/RL_slalom_soft_spine_fast_low_timeStep.ttt
```
5. Open a new terminal (or tab) and run:
```bash
cd {SIM4_WORKER_ROOT}/SIM4/
./coppeliaSim.sh ~/CPGRBFN_slalom/simulations/RL_slalom_soft_spine_fast_low_timeStep.ttt
```
6. Open a new terminal (or tab) and run:
```bash
x-tile
```
Follow this [Tutorial](https://haomachai.com/2022/05/23/cpgrbfn-slalom-tutorial/)

7. Then build the controller for Slalom
```bash
cd ~/CPGRBFN_slalom/interfaces/geckobotiv/sim/build_dir/
rm CMakeCache.txt
cmake .
make
```
8. Go to the machine learning directory
```bash
cd ~/CPGRBFN_slalom/machine_learning/
./RL_repeater.sh -t 1 -e indirect -r GECKOBOTIV
```
9. The program will now be running Slalom (​-r GECKOBOTIV)​ for 350 iterations one time (​-t 1) using an indirect (​-e indirect) encoding. Every 5th iteration will be shown visually. All others will be blacked out for performance boost.

The first time running, there will be an error about a safety concern.
You can enabled it and every other unsafe function with `executeUnsafe=true` in `/SIM#/system/usrset.txt`


### Error ignoring
Since we run multiple simulation, there will be an error about ZMQ server port (rcpPort).
So, please ignore the error this time, because we didn't used it recently.

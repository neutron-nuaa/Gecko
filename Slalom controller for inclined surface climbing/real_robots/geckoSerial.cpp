//*******************************************
//*                                         *
//*        real geckobot controller         *
//*                                         *
//*******************************************
// author: Arthicha Srisuchinnawong
// contract: zumoarthicha@gmail.com,
// update: 01/01/2019 HAPPY NEWYEAR
// version: 1.0.0

//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************
//


// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>


//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10 // reflesh rate of ros

#define LIFT_INIT 0.7



//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray dynamixelSignal;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable


float motorSig[16] = {0}; // array store motor signal

//float sim_motorDir[16] = {-LEFT_GAIN,1,1,LEFT_GAIN, 1,-1,-1,-1, 1,-1,-1,-1, -LEFT_GAIN,1,1,LEFT_GAIN};
float LIFT = 1.5;
float SHIFT = 0.0;
float SHIFT2 = 0.0;
float SHIFT3 = 0.0;

#if 0
const float motorDirection[16] =    {1,LIFT,-LIFT,-0.8
                                    ,1,-LIFT,LIFT,-0.8
                                    ,-1,LIFT,-LIFT,0.8
                                    ,-1,-LIFT,LIFT,0.8};

const float motorInitPos[16] =      {-0.3,0-SHIFT,-0.6-SHIFT2,00.3
                                    ,0,-0.1+SHIFT,0+SHIFT3,1.0
                                    ,0,-0.1-SHIFT,0-SHIFT3,-1.0
                                    ,0,0+SHIFT,0+SHIFT2,-0.3};
#else
const float motorDirection[16] =    {1,-1,1,1
                                    ,1,1,1,-1
                                    ,-1,-1,-1,1
                                    ,-1,1,-1,-1};

const float motorInitPos[16] =      {-0.2,-0.8,0,-0.8
                                    ,0,0.82,0,0.8
                                    ,0,-0.8,0,-0.5
                                    ,0,0.8,0,0.8};

#endif

const int LEG_NUM[4] = {1,2,3,4};
const int JOINT_NUM[4] = {1,2,3,4};



//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************

void motorCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        motorSig[i] = *it;
        i++;
    }
    return;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node
    std::string nodeName("geckoDynamixel");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    // create publisher and subscriber
    ros::Publisher dynamixelPub;
    dynamixelPub = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",10);


    ros::Subscriber motorSub = node.subscribe("/motor_topic",10,motorCB);
    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************

    float temp = 0;
    int indx = 0;
    while(ros::ok())
    {


        dynamixelSignal.data.clear();

        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                if((LEG_NUM[i] <= 0)||(LEG_NUM[i] == 0))
                {
                    continue;
                }
                dynamixelSignal.data.push_back(JOINT_NUM[j]*10 + LEG_NUM[i]);
                indx = (LEG_NUM[i]-1)*4+(JOINT_NUM[j]-1);
                temp = motorSig[indx];
                temp = (temp * motorDirection[indx]) + motorInitPos[indx];

            		// if(i == 1)
            		// {
            		// temp = -1*temp;
            		// }

                dynamixelSignal.data.push_back(temp);//+JOINT_NUM[j]-1]);
            }
        }

        dynamixelPub.publish(dynamixelSignal);
        ros::spinOnce();
        loop_rate.sleep();



    } // main loop -> ros::ok
    return 0;
}

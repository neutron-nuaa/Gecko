//*******************************************
//*                                         *
//*             basic pcpg controller       *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com
// data: 03/02/2020
// version: 1.0.0

#include "Basic_pcpg_controller.h"


// initialize post processing CPG network with default value.
Basic_pcpg_controller::Basic_pcpg_controller()
{
    // initial class attributes
}

// set the parameter of PCPG controller (slope)
void Basic_pcpg_controller::setParameter(float threshold)
{
    this->threshold = threshold;
}

// pass the input signals through the PCPG network
void Basic_pcpg_controller::run(float signal1, float signal2)
{
    this->pcpg_in[0] = signal1;
    this->pcpg_in[1] = signal2;

    if (this->pcpg_in[0] >=  this->threshold) {this->set[0] = 1.0;}
    if (this->pcpg_in[0] <   this->threshold) {this->set[0] = -1.0;}

    if (this->pcpg_in[1] >=  this->threshold) {this->set[1] = 1.0;}
    if (this->pcpg_in[1] <   this->threshold) {this->set[1] = -1.0;}

    this->diffset[0] = this->set[0] - this->setold[0];
    this->diffset[1] = this->set[1] - this->setold[1];

    this->diff_in[0] = this->pcpg_in[0] - this->pcpg_in_old[0];
    this->diff_in[1] = this->pcpg_in[1] - this->pcpg_in_old[1];

    // combine y up and down
    if (this->diff_in[0] >= 0.0 and this->set[0] == -1) {
        this->pcpg_out[0] = this->pcpg_in[0];
        this->set_dummy[0] = 1.0;
    }

    if (this->diffset[0] == 2.0 ) {
        this->pcpg_out[0] = this->pcpg_dummy[0];
        this->set_dummy[0] = -1.0;
    }

    if (this->diff_in[1] >= 0.0 and this->set[1] == -1) {
        this->pcpg_out[1] = this->pcpg_in[1];
        this->set_dummy[1] = 1.0;
    }

    if (this->diffset[1] == 2.0 ) {
        this->pcpg_out[1] = this->pcpg_dummy[1];
        this->set_dummy[1] = -1.0;
    }


    if(this->set_dummy_old[0] == -1.0 and this->set_dummy[0] == 1.0 )
    {
      this->pcpg_dummy[0] = this->pcpg_in[0];
    }
    if(this->set_dummy_old[1] == -1.0 and this->set_dummy[1] == 1.0 )
    {
      this->pcpg_dummy[1] = this->pcpg_in[1];
    }


    this->pcpg_in_old[0] = this->pcpg_in[0];
    this->pcpg_in_old[1] = this->pcpg_in[1];

    this->setold[0] = this->set[0];
    this->setold[1] = this->set[1];

    this->set_dummy_old[0] = this->set_dummy[0];
    this->set_dummy_old[1] = this->set_dummy[1];

    // limit upper and lower boundary
    if (this->pcpg_out[0] > 1.0) {
        this->pcpg_out[0] = 1.0;
    }

    if (this->pcpg_out[0] < -1.0) {
        this->pcpg_out[0] = -1.0;
    }

    if (this->pcpg_out[1] > 1.0) {
        this->pcpg_out[1] = 1.0;
    }

    if (this->pcpg_out[1] < -1.0) {
        this->pcpg_out[1] = -1.0;
    }
}


/* this function return the output signal of the PCPG
    network,
            if number is 1 the output is signal 1
            if number is 2 the output is signal 2     */
float Basic_pcpg_controller::getSignal(int number)
{
    if(number == 1)
    {
        return this->pcpg_out[0];
    }else{
        return this->pcpg_out[1];
    }
}

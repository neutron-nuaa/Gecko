//*******************************************
//*                                         *
//*        main geckobot controller         *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com
// update: 17/02/2020
// version: 1.0.0

//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************
// generate motor signal from open-loop modular neural control
// the leg and body motor signal are seperated


// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>

// modular robot controller library
#include "Basic_cpg_controller.h"
#include "Basic_pcpg_controller.h"
#include "VRN.h"
#include "Pcpg_controller.h"
#include "Delay_line.h"
#include "Flex_body_controller.h"
#include "Data_logger.h"

//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10                 // reflesh rate of ros
#define GAIT_CHANGE_TIME 20     // time when MI is changed from CPG_MI to CPG_MI2
#define DATA_LOGGER 0           // logging data or not
#define DELAY_LINE 1            // drive motor signal by delay line or not

// cpg parameters
#define CPG_OUTPUT 0.01
#define CPG_BIAS 0.0
#define CPG_W11_22 1.4
#define CPG_WD1 0.18
#define CPG_MI 0.12
#define CPG_MI2 0.12

//**   MI used in experiments   **//
// 0.08, 0.12, 0.18, 0.22, 0.26 (MI)
// 0.10, 0.15, 0.19, 0.22, 0.25 (Hz)

//** In case of using delay line **//
// MI = 0.04 for wave gait
// MI = 0.08 for new wave gait
// MI = 0.12 for trot gait


// vrn parameters
#define VRN_OUTPUT 0.01
#define VRN_W1358 1.7246
#define VRN_W2467 -1.7246
#define VRN_W9_10 0.5
#define VRN_W11_12 -0.5
#define VRN_BIAS -2.48285
#define VRN_INPUTY_1 1
#define VRN_INPUTY_2 -1

// basic pcpg parameters
#define B_PCPG_THRES_1 0.60
#define B_PCPG_THRES_2 0.85

// pcpg parameters
#define PCPG_THRES 0.77     //0.87    // control upward slope of pcpg signal
#define PCPG_1_SLOPE 2      //20.     // control the downward slope of pcpg signal
#define PCPG_2_SLOPE 20     //2.
#define PCPG_P_SLOPE 200    //200
#define PCPG_P_THRES 0.85   //0.85

// motor neuron parameter
#define SIGNAL_AMP 0.1      // amplitude of step length
#define LIFT_AMP 0.15       // amplitute of foot lifting
#define LEFT_GAIN 0.97      // relative step length
#define GAMMA 1             // pealing gain

// delayline parameters
#define DELAYSIZE 80        // 80 size of the delay
#define DELAY_RH 1          // 0 delay for right hind leg
#define DELAY_RF 60         // 60 delay for right front leg
#define DELAY_LH 20         // 20 delay for left hind leg
#define DELAY_LF 40         // 40 delay for left front leg
#define DELAY_PEEL 40       // 40

// body amplitude gain
#define BODY_AMP_GAIN 0.0  // 0.3

//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray cpgSignal;
std_msgs::Float32MultiArray motorSignal;
std_msgs::Float32MultiArray sim_motorSignal;
std_msgs::Float32MultiArray sim_bodySignal;
std_msgs::Float32MultiArray multiJointCommandSignal;

//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable

float motorSig[16] = {0};             // array store motor signal
float sim_motorSig[16] = {0};         // array store joint signal
float freezeSignal = 0;               // freeze or update

float c1 = 0;                         // cpg signals
float c2 = 0;

float v1 = 0;                         // vrn signals
float v2 = 0;

float pc1 = 0;                        // pcpg signals
float pc2 = 0;
float pcp = 0;
float b_pcpg_11 = 0;
float b_pcpg_12 = 0;
float b_pcpg_21 = 0;
float b_pcpg_22 = 0;

float c_bodyJ1 = 0;                   // body signal
float c_bodyJ2 = 0;
float c_bodyJ3 = 0;
float s_bodyJ1 = 0;
float s_bodyJ2 = 0;
float s_bodyJ3 = 0;
float standing_wave_J1 = 0;
float standing_wave_J2 = 0;
float standing_wave_J3 = 0;

float sim_time = 0;                   // simulation time
float MI_dynamic = CPG_MI;            // MI param which can change


float sim_motorDir[16] = {LEFT_GAIN,1,1,LEFT_GAIN,
                             1,1,1,1,
                             1,1,1,1,
                             LEFT_GAIN,1,1,LEFT_GAIN};


//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************

void miDynamicCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        MI_dynamic = *it;
        i++;
    }
    return;
}

void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        sim_time = *it;
        i++;
    }
    return;
}

void freezeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        freezeSignal = *it;
        i++;
    }
    return;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node
    std::string nodeName("geckoNodeiii_flexbody");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    // create publisher and subscriber
    ros::Publisher outputCPG;
    outputCPG = node.advertise<std_msgs::Float32MultiArray>("/cpg_topic",1);

    ros::Publisher outputMOTOR;
    outputMOTOR = node.advertise<std_msgs::Float32MultiArray>("/motor_topic",1);

    ros::Publisher outputSIMMOTOR;
    outputSIMMOTOR = node.advertise<std_msgs::Float32MultiArray>("/sim_motor_topic",1);

    ros::Publisher outputSIMBODY;
    outputSIMBODY = node.advertise<std_msgs::Float32MultiArray>("/sim_body_topic",1);

    ros::Publisher outputMultiJointCommand;
    outputMultiJointCommand = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",1);


    ros::Subscriber miDynamicSub = node.subscribe("/mi_dynamic_topic",10,miDynamicCB);
    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);
    ros::Subscriber freezeSub = node.subscribe("/freezeSignal_topic",10,freezeCB);

    // data logger
    #if (DATA_LOGGER)
      Data_logger logger_cpg("cpglog.txt");
      Data_logger logger_motor("motorlog.txt");
      Data_logger logger_flex_body("flexbodylog.txt");
    #endif

    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************

    // basic cpg
    Basic_cpg_controller basic_cpg;
    basic_cpg.setParameter(CPG_OUTPUT,CPG_BIAS,CPG_W11_22,CPG_WD1,CPG_MI);

    // vrn
    VRN vrn1;
    vrn1.setParameter(VRN_OUTPUT,VRN_INPUTY_1,VRN_W1358,VRN_W2467,VRN_W9_10,VRN_W11_12,VRN_BIAS);

    VRN vrn2;
    vrn2.setParameter(VRN_OUTPUT,VRN_INPUTY_2,VRN_W1358,VRN_W2467,VRN_W9_10,VRN_W11_12,VRN_BIAS);

    // basic pcpg
    Basic_pcpg_controller basic_pcpg_1;
    basic_pcpg_1.setParameter(B_PCPG_THRES_1);

    Basic_pcpg_controller basic_pcpg_2;
    basic_pcpg_2.setParameter(B_PCPG_THRES_2);

    // pcpg
    Pcpg_controller pcpg_1;
    pcpg_1.setParameter(PCPG_1_SLOPE,PCPG_THRES);

    Pcpg_controller pcpg_2;
    pcpg_2.setParameter(PCPG_2_SLOPE,PCPG_THRES);

    Pcpg_controller pcpg_peel;
    pcpg_peel.setParameter(PCPG_P_SLOPE,PCPG_P_THRES);

    // flex body signal
    Flex_body_controller flex_body;

    // create delay line for each joint
    Delay_line peeling_line;
    peeling_line.setParameter(DELAYSIZE);

    Delay_line joint0_delay;
    joint0_delay.setParameter(DELAYSIZE);

    Delay_line joint1_delay;
    joint1_delay.setParameter(DELAYSIZE);

    Delay_line joint2_delay;
    joint2_delay.setParameter(DELAYSIZE);

    Delay_line joint3_delay;
    joint3_delay.setParameter(DELAYSIZE);

    // body delay
    Delay_line body_j1_delay;
    body_j1_delay.setParameter(DELAYSIZE);

    Delay_line body_j2_delay;
    body_j2_delay.setParameter(DELAYSIZE);

    Delay_line body_j3_delay;
    body_j3_delay.setParameter(DELAYSIZE);

    // sine body delay
    Delay_line body_sin_j1_delay;
    body_sin_j1_delay.setParameter(DELAYSIZE);

    Delay_line body_sin_j2_delay;
    body_sin_j2_delay.setParameter(DELAYSIZE);

    Delay_line body_sin_j3_delay;
    body_sin_j3_delay.setParameter(DELAYSIZE);

    bool updateCondition = true;                 // update or freeze cpg signal
    float pcpg_p[2] = {0};

    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*               neural control            *
        //*                                         *
        //*******************************************

        if((sim_time > GAIT_CHANGE_TIME) && (GAIT_CHANGE_TIME != 0))
        {
            basic_cpg.setMI(CPG_MI2);
        }

        if(freezeSignal >= 1)
        {
            updateCondition = false;
        }else{
            updateCondition = true;
        }

        // body singal
        if(updateCondition)
        {
          sim_bodySignal.data.clear();
          multiJointCommandSignal.data.clear();
        }

        // cpg signal
        if(updateCondition)
        {
            basic_cpg.run();
        }
        cpgSignal.data.clear();
        c1 = basic_cpg.getSignal(1);
        c2 = basic_cpg.getSignal(2);

        // vrn signal
        if(updateCondition)
        {
            vrn1.run(c2);
            vrn2.run(c2);
        }
        v1 = vrn1.getSignal();
        v2 = vrn2.getSignal();

        // pcpg and basic pcpg signal
        if(updateCondition)
        {
            pcpg_1.run(c1,c2);
            pcpg_2.run(c1,c2);
            pcpg_peel.run(c1,c2);

            basic_pcpg_1.run(c1,-c1);
            basic_pcpg_2.run(c2,-c2);
        }
        pc1 = pcpg_1.getSignal(1);
        pc2 = pcpg_2.getSignal(1);

        b_pcpg_11 = basic_pcpg_1.getSignal(1);
        b_pcpg_12 = basic_pcpg_1.getSignal(2);

        b_pcpg_21 = basic_pcpg_2.getSignal(1);
        b_pcpg_22 = basic_pcpg_2.getSignal(2);


        //*******************************************
        //*                                         *
        //*             legs driver                 *
        //*    drive with or without delay line     *
        //*******************************************
        #if (DELAY_LINE)
            //*******************************************
            // ***  write singal to leg dalay line    ***
            //*******************************************
            //  joint0_delay.writeIn(0.2*pc2);//0.2
            joint0_delay.writeIn(SIGNAL_AMP*3.14*pc1);

            //  for simulation: -0.7*(0.5*(pc1+1.0))-0.5
            //  for real robot:  0.35*(0.5*(pc1+1.0))-0.6
            joint1_delay.writeIn(-0.5*(0.5*(pc2+1.0))-0.6);

            //  joint2_delay.writeIn(-SIGNAL_AMP*2*pc2);
            if(pc2 > 0)
            {
                joint2_delay.writeIn(-SIGNAL_AMP*0.0*pc1);
            }else{
                joint2_delay.writeIn(-SIGNAL_AMP*0.0*pc1);
            }

            //  joint3_delay.writeIn(0.2*0.4*pc2+0.2); // 0.2 + 1.4
            joint3_delay.writeIn(0.2*0.4*pc1+0.3);

            //*******************************************
            // ***  write singal to body dalay line   ***
            //*******************************************
            body_j1_delay.writeIn(c2);
            body_j2_delay.writeIn(c2);
            body_j3_delay.writeIn(c2);

            body_sin_j1_delay.writeIn(flex_body.flex_body_wave(1, sim_time));
            body_sin_j2_delay.writeIn(flex_body.flex_body_wave(2, sim_time));
            body_sin_j3_delay.writeIn(flex_body.flex_body_wave(3, sim_time));


            //*******************************************
            // ***  read singal leg dalay line        ***
            //*******************************************
            // shoulder joint
            motorSig[12] = joint0_delay.readFr(DELAY_RH);
            motorSig[0]  = joint0_delay.readFr(DELAY_RF);
            motorSig[8]  = joint0_delay.readFr(DELAY_LH);
            motorSig[4]  = joint0_delay.readFr(DELAY_LF);

            // delay for right hind leg
            motorSig[13] = joint1_delay.readFr(DELAY_RH);
            motorSig[14] = joint2_delay.readFr(DELAY_RH);
            motorSig[15] = joint3_delay.readFr(DELAY_RH);

            // delay for right front leg
            motorSig[1] = joint1_delay.readFr(DELAY_RF);
            motorSig[2] = joint2_delay.readFr(DELAY_RF);
            motorSig[3] = joint3_delay.readFr(DELAY_RF);

            // delay for left hind leg
            motorSig[9]  = joint1_delay.readFr(DELAY_LH) ;
            motorSig[10] = joint2_delay.readFr(DELAY_LH);
            motorSig[11] = joint3_delay.readFr(DELAY_LH);

            // delay for left front leg
            motorSig[5] = joint1_delay.readFr(DELAY_LF) ;
            motorSig[6] = joint2_delay.readFr(DELAY_LF);
            motorSig[7] = joint3_delay.readFr(DELAY_LF);

            // delay for using cpg mimic real gecko body posture
            c_bodyJ1 = body_j1_delay.readFr(flex_body.mapping_mi_delay(1, CPG_MI2));    // 11.976, 14
            c_bodyJ2 = body_j2_delay.readFr(flex_body.mapping_mi_delay(2, CPG_MI2));    // 18, 19
            c_bodyJ3 = body_j3_delay.readFr(flex_body.mapping_mi_delay(3, CPG_MI2));    // 26.5212, 32

            // delay for standing wave
            standing_wave_J1 = body_j1_delay.readFr(1);
            standing_wave_J2 = body_j2_delay.readFr(1);
            standing_wave_J3 = body_j3_delay.readFr(1);

            // delay for using sine wave mimic real gecko body posture
            s_bodyJ1 = body_sin_j1_delay.readFr(1);
            s_bodyJ2 = body_sin_j2_delay.readFr(1);
            s_bodyJ3 = body_sin_j3_delay.readFr(1);

        #else
            //*******************************************
            //    drive the leg without dalay line    ***
            //*******************************************
            // shoulder joint for real robot
            motorSig[12] =  (0.3*c1);                               // RF joint0
            motorSig[0]  = (-0.3*c1) + 0.15;                        // LF joint0
            motorSig[8]  = (-0.3*c1) - 0.15;                        // RH joint0
            motorSig[4]  =  (0.3*c1);                               // LH joint0

            // shoulder joint for sim
            // motorSig[12] =  (-0.3*c1);                           // RF joint0
            // motorSig[0]  = (0.3*c1);                             // LF joint0
            // motorSig[8]  = (0.3*c1);                             // RH joint0
            // motorSig[4]  =  (-0.3*c1);                           // LH joint0

            // right front leg (RF)
            motorSig[13] = 0.05*(b_pcpg_11 + 1) - 0.4;              // RF joint1   // 0.05*b_pcpg_11 - 0.35
            if(c2 > 0 && b_pcpg_11 < 0)
            {
              if(c1 > 0 and c2 > 0)
              {
                motorSig[14] = 0.00*b_pcpg_11;
                motorSig[15] = 0.00*(c2) + 0.2;
              }
              else
              {
                motorSig[14] = 0.40*b_pcpg_11 + 0.45;               // RF joint2
                motorSig[15] = 0.45*(c2) + 0.2;                     // RF joint3
              }
            }
            else
            {
              motorSig[14] = 0.00*b_pcpg_11;
              motorSig[15] = 0.00*(c2) + 0.2;
            }


            // left front leg
            motorSig[1] = 0.05*(b_pcpg_12 + 1) - 0.3;                // LF joint1  // 0.05*b_pcpg_11 - 0.35
            if(-c2 > 0 && b_pcpg_12 < 0)
            {
              if(c1 < 0 && c2 < 0)
              {
                motorSig[2] = 0.00*b_pcpg_12;
                motorSig[3] = 0.00*(-c2) + 0.2;
              }
              else
              {
                motorSig[2] = 0.40*b_pcpg_12 + 0.45;                // LF joint2
                motorSig[3] = 0.45*(-c2) + 0.2;                     // LF joint3
              }
            }
            else
            {
              motorSig[2] = 0.00*b_pcpg_12;
              motorSig[3] = 0.00*(-c2) + 0.2;
            }


            // ringht hind leg
            motorSig[9]  = 0.5*(b_pcpg_12 + 1) - 0.6;              // RH joint1   // 0.5*b_pcpg_12 - 0.1
            if(-c2 > 0 && b_pcpg_12 < 0)
            {
              if(c1 < 0 && c2 < 0)
              {
                motorSig[10] = 0.00*b_pcpg_12;
                motorSig[11] = 0.00*(-c2) + 0.1;
              }
              else
              {
                motorSig[10] = 0.40*b_pcpg_12 + 0.35;               // RH joint2
                motorSig[11] = 0.55*(-c2) + 0.1;                    // RH joint3
              }
            }
            else
            {
              motorSig[10] = 0.00*b_pcpg_12;
              motorSig[11] = 0.00*(-c2) + 0.1;
            }


            // left hind leg
            motorSig[5] = 0.5*(b_pcpg_11 + 1) - 0.6;               // LH joint1   // 0.5*b_pcpg_12 - 0.1
            if(c2 > 0 && b_pcpg_11 < 0)
            {
              if(c1 > 0 and c2 > 0)
              {
                motorSig[6] = 0.00*b_pcpg_11;
                motorSig[7] = 0.00*(c2) + 0.3;
              }
              else
              {
                motorSig[6] = 0.40*b_pcpg_11 + 0.35;                // LH joint2
                motorSig[7] = 0.55*(c2) + 0.3;                      // LH joint3
              }
            }
            else
            {
              motorSig[6] = 0.00*b_pcpg_11;
              motorSig[7] = 0.00*(c2) + 0.3;
            }

        #endif


        for(int i =0 ;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
              sim_motorSig[4*i+j] = sim_motorDir[4*i+j] * (motorSig[4*i+j]);
            }
        }


        #if (DATA_LOGGER)
          // use data logger for analize cpg and pcpg
          logger_cpg.cpg_pcpg_logs("cpglog.txt", c1, c2, v1, v2, b_pcpg_11, b_pcpg_12, sim_time);

          // use data logger for analize motor signals
          logger_motor.motor_sig_logs("motorlog.txt", sim_time,
                                              motorSig[0],  motorSig[1],   motorSig[2],  motorSig[3],
                                              motorSig[4],  motorSig[5],   motorSig[6],  motorSig[7],
                                              motorSig[8],  motorSig[9],   motorSig[10], motorSig[11],
                                              motorSig[12], motorSig[13],  motorSig[14], motorSig[15]);

          // use data logger for flex body
          logger_flex_body.flex_body_logs("flexbodylog.txt",
                                              c_bodyJ1, c_bodyJ2, c_bodyJ3,
                                              s_bodyJ1, s_bodyJ2, s_bodyJ3,
                                              standing_wave_J1,  standing_wave_J2,  standing_wave_J3 );
        #endif


        if(updateCondition)
        {
            joint0_delay.step_one();
            joint1_delay.step_one();
            joint2_delay.step_one();
            joint3_delay.step_one();
            body_j1_delay.step_one();
            body_j2_delay.step_one();
            body_j3_delay.step_one();
            body_sin_j1_delay.step_one();
            body_sin_j2_delay.step_one();
            body_sin_j3_delay.step_one();
        }

        //*******************************************
        //*                                         *
        //*         put data to ros variable        *
        //*                                         *
        //*******************************************
        // drive simulation body
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*0.5*(standing_wave_J1));
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*0.5*(standing_wave_J2));
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*0.5*(standing_wave_J3));


        //*******************************************
        //***    drive real body robot            ***
        //*******************************************
        multiJointCommandSignal.data.push_back(15);                                                     // 3.0 is motor id of body joint1
        multiJointCommandSignal.data.push_back( BODY_AMP_GAIN*0.5*(c1) -0.02);                          // BODY_AMP_GAIN*0.5*(c1)// 0.5 -0.5
        multiJointCommandSignal.data.push_back(25);                                                     // 14.0 is motor id of body joint2
        multiJointCommandSignal.data.push_back( BODY_AMP_GAIN*0.5*(c1) -0.02);                          // 0.32 -0.82
        multiJointCommandSignal.data.push_back(35);                                                     // 1.0 is motor id of body joint3
        multiJointCommandSignal.data.push_back( BODY_AMP_GAIN*0.5*(c1) -0.02);                          // 0.40 -0.47


        cpgSignal.data.push_back(c1);
        cpgSignal.data.push_back(c2);
        motorSignal.data.clear();
        sim_motorSignal.data.clear();
        for(int j=0;j<16;j++)
        {
            motorSignal.data.push_back(motorSig[j]);
            sim_motorSignal.data.push_back(sim_motorSig[j]);
        }

        outputSIMBODY.publish(sim_bodySignal);
        outputMultiJointCommand.publish(multiJointCommandSignal);
        outputCPG.publish(cpgSignal);
        outputMOTOR.publish(motorSignal);
        outputSIMMOTOR.publish(sim_motorSignal);

        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok
    return 0;
}

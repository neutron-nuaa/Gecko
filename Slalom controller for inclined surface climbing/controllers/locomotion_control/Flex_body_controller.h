//*******************************************
//*                                         *
//*         Flex body controller            *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 23/05/2019
// version: 1.0.0

#ifndef FLEX_BODY_CONTROLLER_H
#define FLEX_BODY_CONTROLLER_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdint.h>

using namespace std;

class Flex_body_controller
{
public:

  /* constructor */
  Flex_body_controller();

  /* generate body wave function */
  float flex_body_wave(int number, float t);

  /* mapping MI to delay line */
  float mapping_mi_delay(int joint_no, float mi);


private:
  float A1;         //  Amplitude of wave
  float A2;
  float A3;
  float T1;         //  Periods of wave
  float T2;
  float T3;

  float GainA;      // Amplitude Gain
  float GainT;      // Periods Gain

  float Phs;        // phase shift
  float Ph1;        // phase of joint 1
  float Ph2;        // phase of joint 2
  float Ph3;        // phase of joint 3
};

#endif //FLEX_BODY_CONTROLLER_H

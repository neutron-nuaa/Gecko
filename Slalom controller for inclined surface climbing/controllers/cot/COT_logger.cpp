//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// date: 29/12/2020
// version: 3.0.0

#include "COT_logger.h"

COT_logger::COT_logger(string file_name)
{
  file_dir = "/home/os/Documents/gorobots_dev/controllers/slalom/data_logger/";

  // Open output wrl file
  ofstream myFile;
  myFile.open(file_dir+file_name, ios::out);
  if(!myFile)
  {
    cerr << "Cannot open this file for output.\n";
		exit (-1);
  }

  // myFile << setw(10) << "c1" << setw(10) << "c2" << setw(10) << "pc1" << setw(10) << "pc2\n";

  // close the output file
  myFile.close ();
}

COT_logger::~COT_logger()
{
  file_dir = "";
}



// cot data logger

float COT_logger::cot_logs(string file_name,
                                            float time,         float time_change,
                                            float power_in,     float power_out,
                                            float energy_in,    float energy_out,
                                            float energy_in_ck, float energy_out_ck)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << time    <<    " " << time_change <<   " " << power_in   <<  " "
            << power_out <<  " " << energy_in    <<  " " << energy_out <<  " " << energy_in_ck    <<  " " << energy_out_ck <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}



// current data logger

float COT_logger::current_logs(string file_name, float time,
                                  float i_m1, float i_m2, float i_m3, float i_m4, float i_m5, float i_m6, float i_m7, float i_m8, float i_m9, 
                                  float i_m10, float i_m11, float i_m12, float i_m13, float i_m14, float i_m15, float i_m16, float i_m17, float i_m18, float i_m19)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << time  <<   " " << i_m1   <<  " " << i_m2   <<  " " << i_m3   <<  " " << i_m4   <<  " " << i_m5   <<  " " << i_m6   <<  " " << i_m7   <<  " " << i_m8   <<  " " << i_m9   <<  " "
            << i_m10 <<   " " << i_m11  <<  " " << i_m12  <<  " " << i_m13  <<  " " << i_m14  <<  " " << i_m15  <<  " " << i_m16  <<  " " << i_m17  <<  " " << i_m18   << " " << i_m19  <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}



// voltage data logger

float COT_logger::voltage_logs(string file_name, float time,
                                  float v_m1,  float v_m2,  float v_m3,  float v_m4,  float v_m5,  float v_m6,  float v_m7,  float v_m8,  float v_m9, 
                                  float v_m10, float v_m11, float v_m12, float v_m13, float v_m14, float v_m15, float v_m16, float v_m17, float v_m18, float v_m19)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << time  <<   " " << v_m1   <<  " " << v_m2   <<  " " << v_m3   <<  " " << v_m4   <<  " " << v_m5   <<  " " << v_m6   <<  " " << v_m7   <<  " " << v_m8   <<  " " << v_m9   <<  " "
            << v_m10 <<   " " << v_m11  <<  " " << v_m12  <<  " " << v_m13  <<  " " << v_m14  <<  " " << v_m15  <<  " " << v_m16  <<  " " << v_m17  <<  " " << v_m18   << " " << v_m19  <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}
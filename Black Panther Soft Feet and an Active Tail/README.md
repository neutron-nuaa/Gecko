# <center> Black Panther: Bio-Inspired Climbing Robot with Soft Feet and an Active Tail with a Soft Adhesive Tip </center>

## Introduction
We propose a bio- inspired climbing robot based on a new approach wherein the synergy between soft feet and an active tail with a soft adhesive tip allows the robot to climb stably on even and uneven terrains at different slope angles. Black Panther consists of two rigid parts made of carbon fiber for its body: an active tail (manufactured using a three-dimensional (3D) printer with polylactic acid) with a silicone rubber covering the tip (soft adhesive tip) and four legs; each leg has four joints and a silicone rubber foot (soft foot) (Fig_GIT1), which are coordinated and driven by central pattern generator (CPG)-based neural control.

<div align="center">
<img src="https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Black%20Panther%20Soft%20Feet%20and%20an%20Active%20Tail/fig1.jpeg?ref_type=heads" width="400"> 
</div>

**Fig_GIT1:** Bio-inspired climbing robot (Black Panther) that utilizes the synergy between soft feet and an active tail with a soft adhesive tip to climb stably on even and uneven terrains at different slope angles.


## Soft Tail Tip and Soft Feet Fabrication 
In this study, we fabricated the soft tip and feet using a mixture of 10 g of silicone rubber Dragon Skin 30 part A and 10 g of Dragon Skin 30 part B. After we stirred the mixture, we kept it in a vacuum chamber for 2-3 min to extract the air. Subsequently, we poured the mixture into the tip and foot molds. Finally, we incubated the mixture at 80o for 20 min, and then the fabrication of soft tip and feet was completed (Fig_GIT2).

P.S. The soft tip comprises small teeth, each with a height of 1.2 mm and an angle of 20o with the horizontal (Fig_GIT2). For the soft feet, we designed a flat palm on the feet, which had a diameter of 45 mm, to maximize the contact area with the terrain (Fig_GIT2).


<div align="center">
<img src="https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Black%20Panther%20Soft%20Feet%20and%20an%20Active%20Tail/fig2.jpeg?ref_type=heads" width="400">
</div>

**Fig_GIT2:** Fabrication procedure for the soft adhesive tip and soft feet. The serrated structure of the soft tip is based on our previous study [tramsen2018inversion].


## Framework

The project is organized by four sub-folders including **controllers** and **projects**.

- **controllers** consists of the code of the control methods, including main_gecko_controller.cpp, CPG code, and PCPG code.
- **projects** contains the configuration files for managing the software.


## The implementation of the project
### Install necessary software on Ubuntu 16.04 or later version.
### Steps to run the real robot
- Open a terminal and run ROS by a command: roscore
- Open a second termial to launch motors by a command: roslaunch my_dynamixel_workbench_tutorial multiple_motor_test.launch    
- Open a third termial to run the real robot in ../projects/geckobot/The_Black_panther/catkin_ws by a command: rosrun gecko_controller gecko_controller  


## Reference
Tramsen, H. T., Borb, S. N., Zhang, H., Manoonpong, P., Dai, Z. D., & Heepe, L. (2018). Inversion of friction anisotropy in a bio-inspired asymmetrically structured surface. Journal of the Royal Society Interface, 15(135), 20170629.

If you have any questions/doubts  about how to implement this project on your computer, you are welcome to raise issues and email to me. My email address is prince_jai-yen@hotmail.com

//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 7/10/2019
// version: 1.0.0

#ifndef DATA_LOGGER_H
#define DATA_LOGGER_H

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

class Data_logger
{
public:

  // constructor
  Data_logger(string file_name);

  // constructor
  ~Data_logger();

  // cpg and pcpg logger
  float cpg_pcpg_logs(string file_name, float c1, float c2, float sim_time);

  // motor signals logger
  float motor_sig_logs(string file_name,float sim_time,
                                        float mg0, float mg1, float mg2, float mg3,
                                        float mg4, float mg5, float mg6, float mg7,
                                        float mg8, float mg9, float mg10,float mg11,
                                        float mg12,float mg13,float mg14,float mg15,float mg16); 

private:
  string file_dir;

};

#endif  //DATA_LOGGER_H

//*******************************************
//*                                         *
//*        main geckobot controller         *
//*                                         *
//*******************************************
// author: Arthicha Srisuchinnawong
// contract: zumoarthicha@gmail.com,
// update: 18/12/2018
// version: 2.0.0

//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************
// generate motor signal from open-loop modular neural control
// recieve "stretching_topic" and generate stretching signal and update "joint_topic"
// motor signal can be freeze by "freeze_topic" - freeze signal can be grate than 1
// however the robot is freezed when freeze signal is more than 1


// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>

// modular robot controller library
#include "Basic_cpg_controller.h"
#include "Pcpg_controller.h"
#include "Delay_line.h"
#include "Data_logger.h"
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>

//#include "joystick.h"
//#include "realRosClass.h"
//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 13 // reflesh rate of ros // 13 for the BP //18 for joystick controll

#define DATA_LOGGER 0 // logging data or not

#define VERSION_ZUMO 0 // choose zumo structure or donghao structure

#define GAIT_CHANGE_TIME 20
// time when MI is changed from CPG_MI to CPG_MI2

// cpg parameters
#define CPG_OUTPUT 0.01
#define CPG_BIAS 0.0
#define CPG_W11_22 1.4
#define CPG_WD1 0.18
#define CPG_MI 0.12
#define CPG_MI2 0.12
// MI = 0.04 for wave gait
// MI = 0.08 for transition gait
// MI = 0.12 for trot gait

//pcpg parameters
#define PCPG_THRES 0.67 // control upward slope of pcpg signal
#define PCPG_1_SLOPE 20. // control the downward slope of pcpg signal
#define PCPG_2_SLOPE 2.
#define PCPG_P_SLOPE 200
#define PCPG_P_THRES 0.85

// motor neuron parameter
#define SIGNAL_AMP 0.2// amplitude of step length//default0.1//TheBP 0.2
#define LIFT_AMP 0.15// amplitute of foot lifting
#define LEFT_GAIN 0.97 // relative step length
#define GAMMA 1 // pealing gain

// delayline parameters
#define DELAYSIZE 80//size of the delay line
#define DELAY_RH 0// delay for right hind leg
#define DELAY_RF 60// delay for right front leg
#define DELAY_LH 20 // delay for left hind leg
#define DELAY_LF 40// delay for left front leg
#define DELAY_PEEL 40



//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray cpgSignal;
std_msgs::Float32MultiArray motorSignal;
std_msgs::Float32MultiArray sim_motorSignal;
std_msgs::Float32MultiArray multiJointCommandSignal;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable


float motorSig[16] = {0}; // array store motor signal
float sim_motorSig[16] = {0}; // array store joint signal
float stretchingSig[16] = {0};
int16_t joystickSig[20] = {0};
float freezeSignal = 0; // freeze or update

float c1 = 0; // cpg signals
float c2 = 0;


float pc1 = 0; // pcpg signals
float pc2 = 0;
float pcp = 0;

float sim_time = 0; // simulation time

#if VERSION_ZUMO == 1
    float sim_motorDir[16] = {-LEFT_GAIN,1,1,LEFT_GAIN, 1,-1,-1,-1, 1,-1,-1,-1, -LEFT_GAIN,1,1,LEFT_GAIN};
#else
    float sim_motorDir[16] = {LEFT_GAIN,1,1,LEFT_GAIN,
                                 1,1,1,1,
                                 1,1,1,1,
                                 LEFT_GAIN,1,1,LEFT_GAIN};
#endif


//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************
float i;

void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        sim_time = *it;
        i++;
    }
    return;
}

void freezeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        freezeSignal = *it;
        i++;
    }
    return;
}

void stretchingCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        stretchingSig[i] = *it;
        i++;
    }
    return;
}

void joystickCB(const std_msgs::Int16MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<int16_t>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        joystickSig[i] = *it;
        i++;
     //   printf("%.2f",joystickSig[i]);
    }
    return;
}
//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node
    std::string nodeName("geckoNodeiii");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    // create publisher and subscriber
    ros::Publisher outputCPG;
    outputCPG = node.advertise<std_msgs::Float32MultiArray>("/cpg_topic",1);

    ros::Publisher outputMOTOR;
    outputMOTOR = node.advertise<std_msgs::Float32MultiArray>("/motor_topic",1);

    ros::Publisher outputSIMMOTOR;
    outputSIMMOTOR = node.advertise<std_msgs::Float32MultiArray>("/sim_motor_topic",1);

    ros::Publisher outputMultiJointCommand;
    outputMultiJointCommand = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",1);


    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);
    ros::Subscriber freezeSub = node.subscribe("/freezeSignal_topic",10,freezeCB);
    ros::Subscriber stretchingSub = node.subscribe("/stretching_topic",10,stretchingCB);
    ros::Subscriber joystickSub = node.subscribe("/joyStick",30,joystickCB);

    // data logger
    #if (DATA_LOGGER)	
 	Data_logger logger_cpg("cpglog.txt");
	Data_logger logger_motor("motorlog.txt");
    #endif 


    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************
  /*  realRosClass * rosHandle;
    Joystick * joystick;

     if(1){
        joystick = new Joystick;
        if (!joystick->isFound())
            printf("Joystick open failed.\n");
    }
    rosHandle = new realRosClass(argc, argv);
 */

    Basic_cpg_controller basic_cpg;
    basic_cpg.setParameter(CPG_OUTPUT,CPG_BIAS,CPG_W11_22,CPG_WD1,CPG_MI);

    // create delay line for each joint
    Delay_line peeling_line;
    peeling_line.setParameter(DELAYSIZE);

    Delay_line joint0_delay;
    joint0_delay.setParameter(DELAYSIZE);

    Delay_line joint1_delay;
    joint1_delay.setParameter(DELAYSIZE);

    Delay_line joint2_delay;
    joint2_delay.setParameter(DELAYSIZE);

    Delay_line joint3_delay;
    joint3_delay.setParameter(DELAYSIZE);

   

    Delay_line joint0h_delay;
    joint0h_delay.setParameter(DELAYSIZE);

    Delay_line joint1h_delay;
    joint1h_delay.setParameter(DELAYSIZE);

    Delay_line joint2h_delay;
    joint2h_delay.setParameter(DELAYSIZE);

    Delay_line joint3h_delay;
    joint3h_delay.setParameter(DELAYSIZE);


    Pcpg_controller pcpg_1;
    pcpg_1.setParameter(PCPG_1_SLOPE,PCPG_THRES);
    Pcpg_controller pcpg_2;
    pcpg_2.setParameter(PCPG_2_SLOPE,PCPG_THRES);
    Pcpg_controller pcpg_peel;
    pcpg_peel.setParameter(PCPG_P_SLOPE,PCPG_P_THRES);



    bool updateCondition = true; // update or freeze cpg signal
    float pcpg_p[2] = {0};

    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*               neural control            *
        //*                                         *
        //*******************************************

        if((sim_time > GAIT_CHANGE_TIME) && (GAIT_CHANGE_TIME != 0))
        {
            basic_cpg.setMI(CPG_MI2);
        }

        if(freezeSignal >= 1)
        {
            updateCondition = false;
        }else{
            updateCondition = true;
        }

        // multi_joint_command
        if(updateCondition)
        {
          multiJointCommandSignal.data.clear();
        }

        // cpg signal
        if(updateCondition)
        {
            basic_cpg.run();
        }
        cpgSignal.data.clear();
        c1 = basic_cpg.getSignal(1);
        c2 = basic_cpg.getSignal(2);

        // pcpg signal
        if(updateCondition)
        {
            pcpg_1.run(c1,c2);
            pcpg_2.run(c1,c2);
            pcpg_peel.run(c1,c2);
        }


        pc1 = pcpg_1.getSignal(2);
        pc2 = pcpg_2.getSignal(2);

        #if (VERSION_ZUMO)
            joint0_delay.writeIn(SIGNAL_AMP*3.14*pc2);
            joint1_delay.writeIn(LIFT_AMP*3.14*(pc1+1.0));

            pcpg_p[1] = (pc1+1.0);
            if(pcpg_p[1] > pcpg_p[0])
            {
                pcpg_p[1] = pow((pcpg_p[1]),GAMMA) ;
            }
            peeling_line.writeIn(-LIFT_AMP*2*3.14*(pcpg_peel.getSignal(2)+1));
            peeling_line.step_one();
            float temp = -0.0*peeling_line.readFr(DELAY_PEEL)-LIFT_AMP*3.14*pcpg_p[1];
            joint2_delay.writeIn(temp);
            pcpg_p[0] = pcpg_p[1];

            joint3_delay.writeIn((SIGNAL_AMP*3.14*pc2)-0.0*peeling_line.readFr(DELAY_PEEL));
        #else
            
//if(joystickSig[1]==-32767){ //Forward 
          joint0_delay.writeIn(0.2*pc2);//Walking 0.2*pc2 //Climbing(1,2,4) 1 //Climbing(3,5) -1 
         // joint0h_delay.writeIn(0.2*pc2);
           // joint0_delay.writeIn(SIGNAL_AMP*3.14*pc2);//0.2
            joint1_delay.writeIn(-0.7*(0.5*(pc1+1.0))-0.7); //walking -0.7 //Climbing(2,3,4,5) -0.7
            if(pc2 > 0)
            {
                joint2_delay.writeIn(-0.3);//Walking -0.0*pc2//Climbing1 -0.8 //for steep climbing -0.5 (acrylic) -0.3 (FM & CP) // -0.2 Rigid and Flexible tials on rough FM 
            }else{
                joint2_delay.writeIn(-0.3); //Walking -0.0*pc2//Climbing1 -0.8 //for steep climbing -0.5 (acrylic) -0.3 (FM & CP) // -0.2 Rigid and Flexible tials on rough FM 
            }
// for walking situation
            joint3_delay.writeIn(0*0.2*0.4*pc2+0.67); // 0.2 + 1.4 //default 0.2*0.4*pc2+0.67//TheBP 0*0.2*0.4*pc2+0.85//0.55 with -0.5 of Joint 1 // 0.67 Rigid and Flexible tails on rough & FM
//}


        #endif

        // shoulder joint
        motorSig[12] = joint0_delay.readFr(DELAY_RH); 
        motorSig[0] = joint0_delay.readFr(DELAY_RF);
        motorSig[8] = joint0_delay.readFr(DELAY_LH); 
        motorSig[4] = 1.3*joint0_delay.readFr(DELAY_LF); // *1.3 for path adjustment of The BP


	// delay for right hind leg
        motorSig[13] = joint1_delay.readFr(DELAY_RH); 
        motorSig[14] = joint2_delay.readFr(DELAY_RH);
        motorSig[15] = joint3_delay.readFr(DELAY_RH); 
        
	// delay for right front leg
        motorSig[1] = joint1_delay.readFr(DELAY_RF); 
        motorSig[2] = joint2_delay.readFr(DELAY_RF);
        motorSig[3] = joint3_delay.readFr(DELAY_RF);
        
	// delay for left hind leg
        motorSig[9] = joint1_delay.readFr(DELAY_LH) ; 
        motorSig[10] = joint2_delay.readFr(DELAY_LH);
        motorSig[11] = joint3_delay.readFr(DELAY_LH); 
        
	// delay for left front leg
        motorSig[5] = joint1_delay.readFr(DELAY_LF) ; 
        motorSig[6] = joint2_delay.readFr(DELAY_LF);
        motorSig[7] = joint3_delay.readFr(DELAY_LF);
        // delay for right hind leg

        for(int i =0 ;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {

                if(j == 1)
                {
                    sim_motorSig[4*i+j] = sim_motorDir[4*i+j] * (motorSig[4*i+j] - stretchingSig[i]);
                }else if(j == 2)
                {
                    sim_motorSig[4*i+j] = sim_motorDir[4*i+j] * (motorSig[4*i+j] + stretchingSig[i]);
                }else{
                    sim_motorSig[4*i+j] = sim_motorDir[4*i+j] * (motorSig[4*i+j]);
                }
            }

        }
        motorSig[1] -= stretchingSig[0]; // leg1
        motorSig[5] -= stretchingSig[2]; // leg2
        motorSig[9] -= stretchingSig[3]; // leg3
        motorSig[13] -= stretchingSig[1]; // leg4
        float stretchingGain = 0.5;
        motorSig[3] -= stretchingGain * stretchingSig[0];
        motorSig[7] -= stretchingGain * stretchingSig[2];
        motorSig[11] -= stretchingGain * stretchingSig[3];
        motorSig[15] -= stretchingGain * stretchingSig[1];

        //motorSig[5] = 0.5;
        //motorSig[13] = 0.5;

	#if (DATA_LOGGER)
          // use data logger for analize cpg and pcpg
          logger_cpg.cpg_pcpg_logs("cpglog.txt", c1, c2, sim_time);

          // use data logger for analize motor signals
          logger_motor.motor_sig_logs("motorlog.txt", sim_time,
                                              motorSig[0],  motorSig[1],   motorSig[2],  motorSig[3],
                                              motorSig[4],  motorSig[5],   motorSig[6],  motorSig[7],
                                              motorSig[8],  motorSig[9],   motorSig[10], motorSig[11],
                                              motorSig[12], motorSig[13],  motorSig[14], motorSig[15],i);
        #endif 

        if(updateCondition)
        {
            joint0_delay.step_one();
            joint1_delay.step_one();
            joint2_delay.step_one();
            joint3_delay.step_one();

            joint0h_delay.step_one();
            joint1h_delay.step_one();
            joint2h_delay.step_one();
            joint3h_delay.step_one();
        }



        //*******************************************
        //*                                         *
        //*         put data to ros variable        *
        //*                                         *
        //*******************************************
        // create package for vrep ros interface

        multiJointCommandSignal.data.push_back(11);
        multiJointCommandSignal.data.push_back((-1)*motorSig[0]);
        multiJointCommandSignal.data.push_back(12);
        multiJointCommandSignal.data.push_back((-1)*motorSig[1]);
        multiJointCommandSignal.data.push_back(13);
        multiJointCommandSignal.data.push_back(motorSig[2]);
        multiJointCommandSignal.data.push_back(14);
        multiJointCommandSignal.data.push_back(motorSig[3]);

        multiJointCommandSignal.data.push_back(21);
        multiJointCommandSignal.data.push_back((-1)*motorSig[12]);
        multiJointCommandSignal.data.push_back(22);
        multiJointCommandSignal.data.push_back(motorSig[13]);
        multiJointCommandSignal.data.push_back(23);
        multiJointCommandSignal.data.push_back(motorSig[14]);
        multiJointCommandSignal.data.push_back(24);
        multiJointCommandSignal.data.push_back((-1)*motorSig[15]);

        multiJointCommandSignal.data.push_back(31);
        multiJointCommandSignal.data.push_back(motorSig[8]);
        multiJointCommandSignal.data.push_back(32);
        multiJointCommandSignal.data.push_back((-1)*motorSig[9]);
        multiJointCommandSignal.data.push_back(33);
        multiJointCommandSignal.data.push_back((-1)*motorSig[10]);
        multiJointCommandSignal.data.push_back(34);
        multiJointCommandSignal.data.push_back(motorSig[11]);

        multiJointCommandSignal.data.push_back(41);
        multiJointCommandSignal.data.push_back(motorSig[4]);
        multiJointCommandSignal.data.push_back(42);
        multiJointCommandSignal.data.push_back(motorSig[5]);
        multiJointCommandSignal.data.push_back(43);
        multiJointCommandSignal.data.push_back((-1)*motorSig[6]);
        multiJointCommandSignal.data.push_back(44);
        multiJointCommandSignal.data.push_back((-1)*motorSig[7]);

        multiJointCommandSignal.data.push_back(51);
        multiJointCommandSignal.data.push_back(-0.3); // default rubber -0.3, w/tail -0.25 , adhesive tail -0.27 FM -0.2 CP -0.17 //



        cpgSignal.data.push_back(c1);
        cpgSignal.data.push_back(c2);
        motorSignal.data.clear();
        sim_motorSignal.data.clear();
        for(int j=0;j<16;j++)
        {
            motorSignal.data.push_back(motorSig[j]);
            sim_motorSignal.data.push_back(sim_motorSig[j]);
        }

  
 
        outputCPG.publish(cpgSignal);
        outputMOTOR.publish(motorSignal);
        outputSIMMOTOR.publish(sim_motorSignal);
        outputMultiJointCommand.publish(multiJointCommandSignal);

        // wait
        ros::spinOnce();
        loop_rate.sleep();



    } // main loop -> ros::ok
    return 0;
}

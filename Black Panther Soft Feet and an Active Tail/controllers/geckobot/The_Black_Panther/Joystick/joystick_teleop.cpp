
#include "joystick.h"
#include <unistd.h>

#include "ros/ros.h"
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>

using namespace std;

#define NODE_NAME                       "joystick_teleop_node"
#define JOYNAME                         "/dev/input/js0"

#define POSITION_PRESENT_TOPIC          "dxl_prs_pos"
#define VELOCITY_PRESENT_TOPIC          "dxl_prs_vel"
#define CURRENT_PRESENT_TOPIC           "dxl_prs_cur"
#define POSITION_COMMAND_TOPIC          "dxl_cmd_pos"
#define VELOCITY_COMMAND_TOPIC          "dxl_cmd_vel"
#define CURRENT_COMMAND_TOPIC           "dxl_cmd_cur"
#define THETA_COMMAND_TOPIC             "dxl_cmd_theta"
#define DYNAMIXEL_COMMAND_TOPIC         "dxl_cmd"
#define JOYSTICK_TOPIC                  "/joyStick"
#define BUFFER_SIZE                     2
#define LOOP_RATE                       60

#define NUMBER_OF_ACT                   7
#define number_of_axis                  8
#define number_of_button                11


#define STATE_STICK_LEFT_HOR            0
#define STATE_STICK_LEFT_VER            1
#define STATE_LT                        2
#define STATE_STICK_RIGHT_HOR           3
#define STATE_STICK_RIGHT_VER           4
#define STATE_RT                        5
#define STATE_ARROW_HOR                 6
#define STATE_ARROW_VER                 7
#define STATE_A                         8
#define STATE_B                         9
#define STATE_X                         10
#define STATE_Y                         11
#define STATE_LB                        12
#define STATE_RB                        13
#define STATE_BACK                      14
#define STATE_START                     15
#define STATE_LOGITECH                  16
#define STATE_STICK_button_LEFT         17
#define STATE_STICK_button_RIGHT        18
//*********************************************************************//
//                          FUNCTION DECLARATION                       //
//*********************************************************************//

void joy2msgs(int button, int16_t joy_value);
void trans2joytable();
// void publishPositon(uint16_t array_position[]);
uint16_t joy2dxl(int16_t joy_value);
// void callBack_pos(const std_msgs::Int16MultiArray::ConstPtr &msg);


//*********************************************************************//
//                                VARIABLE                             //
//*********************************************************************//
bool is_command_pos = false;
bool is_command_vel = false;
int action_button = 0;


std_msgs::Int16MultiArray joy_msgs;
// std_msgs::Int16MultiArray pos_cmd_msg;
// std_msgs::Int16MultiArray vel_cmd_msg;
int16_t joyAxis[number_of_axis] = {0};
int16_t joyBott[number_of_button] = {0};
int16_t joy_X = 0;
int16_t joy_Y = 0;
int16_t joy_A = 0;
int16_t joy_B = 0;
int16_t joy_LB = 0;
int16_t joy_RB = 0;
int16_t joy_LT = 0;
int16_t joy_RT = 0;
int16_t joy_BACK = 0;
int16_t joy_START = 0;
int16_t joy_LOGITECH = 0;
int16_t joy_ARROW_HOR = 0;
int16_t joy_ARROW_VER = 0;
int16_t joy_STICK_LEFT_HOR = 0;
int16_t joy_STICK_LEFT_VER = 0;
int16_t joy_STICK_RIGHT_HOR = 0;
int16_t joy_STICK_RIGHT_VER = 0;
int16_t joy_STICK_button_LEFT = 0;
int16_t joy_STICK_button_RIGHT = 0;
int16_t STATE = 0;
int16_t joy_table_value[number_of_axis + number_of_button +1] = {
    joy_STICK_LEFT_HOR,
    joy_STICK_LEFT_VER,
    joy_LT,
    joy_STICK_RIGHT_HOR,
    joy_STICK_RIGHT_VER,
    joy_RT,
    joy_ARROW_HOR,
    joy_ARROW_VER,
    joy_A,
    joy_B,
    joy_X,
    joy_Y,
    joy_LB,
    joy_RB,
    joy_BACK,
    joy_START,
    joy_LOGITECH,
    joy_STICK_button_LEFT,
    joy_STICK_button_RIGHT,
    STATE
};

bool button_state[number_of_axis + number_of_button + 1] = {false};
float button_previous[number_of_axis + number_of_button + 1] = {0};

float bodyoffset = 0.0;
float stretch = 0.0;
float pitch = 0.0;
float pressing = 0.0;
int transition = 0;





//*********************************************************************//
//                                  MAIN                               //
//*********************************************************************//

int main(int argc, char** argv)
{
    joy_msgs.data.resize(0);
    //ROS init
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;
    ros::Rate loop_rate(LOOP_RATE);

    ros::Publisher JOY_PUB = nh.advertise<std_msgs::Int16MultiArray>(JOYSTICK_TOPIC, BUFFER_SIZE);

    // Create an instance of Joystick
    Joystick joystick(JOYNAME);
    
    // Ensure that it was found and that we can use it
    if (!joystick.isFound())
    {
        printf("open failed.\n");
        exit(1);
    }
    
    printf("Start joystick........\n");
    button_state[STATE_LOGITECH] = true;

    while (ros::ok()){

        JoystickEvent event;
        // Attempt to sample an event from the joystick
        
        if (joystick.sample(&event)){
            
            if (event.isButton()){
                joyBott[event.number] = event.value;
                action_button = number_of_axis + event.number;

            }

            else if (event.isAxis()){
                joyAxis[event.number] = event.value;
                action_button = event.number;
            }

            if ((action_button == STATE_ARROW_VER) && (event.value != 0) && (button_state[STATE_LOGITECH]))
            {
                    if (transition == 1){
                        stretch += -0.02*event.value/32767.0;
                        nh.setParam("/AVIS_param/stretch", stretch);
                        printf("stretch: %f\n",stretch);   
                    }else if (transition == 0){
                        bodyoffset += 0.02*event.value/32767.0;
                        nh.setParam("/AVIS_param/bodyoffset", bodyoffset);
                        printf("bodyoffset: %f\n",bodyoffset);  
                    }                        
            }
            if ((action_button == STATE_ARROW_HOR) && (event.value != 0) && (button_state[STATE_LOGITECH]))
            {
                    if ((transition == 2)||(transition == 3))
                    {
                        pressing += 0.01*event.value/32767.0;
                        nh.setParam("/AVIS_param/pressing", pressing);
                        printf("pressing: %f\n",pressing); 
                    }else if (transition == 1){
                        pitch += 0.01*event.value/32767.0;
                        nh.setParam("/AVIS_param/pitch", pitch);
                        printf("pitch: %f\n",pitch);    
                    }                   
            }

            if ((action_button == STATE_LT))
            {
                if(event.value >= 0){
                    nh.setParam("/AVIS_param/freemagnet", 1);
                    //printf("freemagnet: 1\n");   
                }
                else if(event.value < 0)
                {
                    nh.setParam("/AVIS_param/freemagnet", 0);
                    //printf("freemagnet: 0\n");   
                }
            }
            //printf("action_button = %d : %d\n",action_button,event.value);
            trans2joytable();
            JOY_PUB.publish(joy_msgs);
        }//ifjoystick event

        
            for(int i = 0; i < (number_of_axis+number_of_button+1); i++)
            {
                int cond = int(joy_table_value[i])-button_previous[i];
                if(cond>0)
                {
                    button_state[i] = !button_state[i];

                    if (button_state[STATE_LOGITECH])
                    {
                        switch(i){ // discrete command
                        case STATE_A: // inspection mode
                            nh.setParam("/AVIS_param/autoadapt", int(button_state[i]));
                            nh.setParam("/AVIS_param/forcehind", int(button_state[i]));
                            printf("inspection state: %d\n",int(button_state[i]));
                            break;
                        case STATE_B: // enable autoadapt
                            nh.setParam("/AVIS_param/autoadapt", int(button_state[i]));
                            printf("autoadapt state: %d\n",int(button_state[i]));
                            break;
                        case STATE_X: // enable autoadapt
                            nh.setParam("/AVIS_param/cross", int(button_state[i]));
                            printf("obstacle crossing state: %d\n",int(button_state[i]));
                            break;
                        case STATE_Y: // enable autoadapt
                            cout << transition << endl;
                            if(transition == 0)
                            {
                                nh.setParam("/AVIS_param/change", 1);
                                nh.setParam("/AVIS_param/touch", 0);
                                nh.setParam("/AVIS_param/on", 0);
                                nh.setParam("/AVIS_param/finish", 0);
                                printf("transition state: %d\n",int(1));
                                transition = 1;
                                pressing = 0.0;
                            }else if(transition == 1)
                            {
                                nh.setParam("/AVIS_param/touch", 1);
                                nh.setParam("/AVIS_param/change", 0);
                                nh.setParam("/AVIS_param/on", 0);
                                nh.setParam("/AVIS_param/finish", 0);
                                printf("touch the pipe\n");
                                transition = 2;
                            }else if(transition == 2){
                                nh.setParam("/AVIS_param/touch", 0);
                                nh.setParam("/AVIS_param/change", 0);
                                nh.setParam("/AVIS_param/on", 1);
                                nh.setParam("/AVIS_param/finish", 0);
                                printf("two foot on the pipe\n");
                                transition = 3;
                                stretch = 0.0;
                                pitch = 0.0;
                            }else{
                                nh.setParam("/AVIS_param/touch", 0);
                                nh.setParam("/AVIS_param/change", 0);
                                nh.setParam("/AVIS_param/on", 0);
                                nh.setParam("/AVIS_param/finish", 1);
                                printf("transition complete\n");
                                transition = 0;
                                stretch = 0.0;
                                pitch = 0.0;
                            }
                            break;
                        case STATE_LB:
                            nh.setParam("/AVIS_param/lift", 10*int(button_state[i]));
                            printf("leg lifting: %d\n",int(button_state[i]));
                            break;
                        case STATE_START:
                            int rate = 40;
                            if (button_state[i] < 0.5) rate = 30;
                            nh.setParam("/AVIS_param/rosrate", rate);
                            printf("rosrate: %d\n",rate);
                            break;
                        }
                    }

                    if (i == STATE_LOGITECH)
                    {
                        printf("developer mode: %d\n",int(1.0-button_state[i]));
                    }
                }
                button_previous[i] = int(joy_table_value[i]);
            }

            if (button_state[STATE_LOGITECH])
            {
                if(!button_state[STATE_A]) // continuous command
                {
                    nh.setParam("/AVIS_param/stride", -1.0*float(joy_table_value[STATE_STICK_LEFT_VER])/32767.0);
                    nh.setParam("/AVIS_param/manualsideway", 1.0*float(joy_table_value[STATE_STICK_RIGHT_HOR])/32767.0);
                }else{
                    nh.setParam("/AVIS_param/checkang", -1.0*float(joy_table_value[STATE_STICK_RIGHT_HOR])/32767.0);
                    nh.setParam("/AVIS_param/checklift", int(joy_table_value[STATE_RB]));

                }
            }

            //nh.setParam("/AVIS_param/lift", 10*int(button_state[i]));
            
        

        //ros::spinOnce();
        loop_rate.sleep();
    }//while
    return 0;
}//main



//*********************************************************************//
//                                FUNCTION                             //
//*********************************************************************//

// void callBack_pos(const std_msgs::Int16MultiArray::ConstPtr &msg)
// {
//   // printf("pos = %d %d %d %d %d\n",msg->data[0],msg->data[1],msg->data[2],msg->data[3],msg->data[4]);
//   printf("pos = ");
//   for (int i = 0; i < msg->data.size(); i++)
//   {
// 	DXL_present_position[i] = msg->data[i];
// 	printf(" %d ", DXL_present_position[i]);
//   } //for
//   printf("\n");
// //   command_pos = true;
// } //callBack


//____________________________________________________________//

uint16_t joy2dxl(int16_t joy_value){
    return (joy_value + 32767)/16;
}//joy2dxl

// void publishPositon(uint16_t array_position[]){
//     pos_cmd_msg.data.resize(0);
//     for(int i = 0; i < NUMBER_OF_ACT; i++){
//         pos_cmd_msg.data.push_back(array_position[i]);
//     }//for
//     is_command_pos = true;
// }//publishPositon

//____________________________________________________________//

void joy2msgs(int button, int16_t joy_value){
    joy_msgs.data[button] = joy_value;
}//joy2msgs

//____________________________________________________________//

void trans2joytable(){

    for(int i = 0; i < number_of_axis; i++){
        joy_table_value[i] = joyAxis[i];
    }
    for(int j = 0; j < number_of_button; j++){
        joy_table_value[j + number_of_axis] = joyBott[j];
    }
    joy_table_value[19] = action_button;
    
    joy_msgs.data.resize(0);
    for(int i = 0; i < number_of_axis + number_of_button + 1; i++){
        joy_msgs.data.push_back(joy_table_value[i]);
    }//for*/
    
}//trans2joytable
//____________________________________________________________//


// void actionFunction(int state){
//     switch (state)
//     {
//     case STATE_START:
//         if(joy_START == 1){
//             publishPositon(standard_posture);
//         }
//         state = -1;
//         break;
    
//     case STATE_BACK:
//         if(joy_BACK == 1){
//             publishPositon(standard_posture);
//         }
//         state = -1;
//         break;
    
//     default:
//         break;
//     }
// }//actionFunction

//____________________________________________________________//

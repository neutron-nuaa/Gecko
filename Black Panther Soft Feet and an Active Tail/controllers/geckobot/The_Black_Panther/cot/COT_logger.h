//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 8/11/2020
// version: 1.0.0

#ifndef COT_LOGGER_H
#define COT_LOGGER_H

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

class COT_logger
{
public:

  // constructor
  COT_logger(string file_name);

  // constructor
  ~COT_logger();

  // cot logger
  float cot_logs(string file_name,
                                  float time,       float time_change,
                                  float power_in,   float power_out, 
                                  float energy_in,  float energy_out,
                                  float energy_in_ck,  float energy_out_ck);

  // current logger
  float current_logs(string file_name, float time,
                                  float i_m1, float i_m2, float i_m3, float i_m4, float i_m5, float i_m6, float i_m7, float i_m8, float i_m9, 
                                  float i_m10, float i_m11, float i_m12, float i_m13, float i_m14, float i_m15, float i_m16, float i_m17);

  // voltage logger
  float voltage_logs(string file_name, float time,
                                  float v_m1, float v_m2, float v_m3, float v_m4, float v_m5, float v_m6, float v_m7, float v_m8, float v_m9, 
                                  float v_m10, float v_m11, float v_m12, float v_m13, float v_m14, float v_m15, float v_m16, float v_m17);

// position logger
  float position_logs(string file_name, float time,
                                  float p_m1, float p_m2, float p_m3, float p_m4, float p_m5, float p_m6, float p_m7, float p_m8, float p_m9, 
                                  float p_m10, float p_m11, float p_m12, float p_m13, float p_m14, float p_m15, float p_m16, float p_m17);

private:
  string file_dir;

};

#endif  //COT_LOGGER_H

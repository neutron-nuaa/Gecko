//*******************************************
//*                                         *
//*        main cost of transport           *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// update: 29/12/2020
// version: 3.0.0

// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>
#include <array>

#include "COT_logger.h"

using namespace std;

//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10           // reflesh rate of ros
#define DATA_LOGGER 1     // logging data or not

//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray energySignal;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable
const int numMotor = 17;                  // number of motors 
array<float, numMotor> positionSig;       // position feedback from real robot
array<float, numMotor> currentSig;        // current feedback from real robot
array<float, numMotor> voltageSig;        // voltage feedback from real robot
array<float, numMotor> torqueSig;         // torque feedback from real robot (Nm)
array<float, numMotor> velocitySig;       // velocity feedback from real robot (rad/s)
float sim_time = 0;                       // simulation time (sec)

float Energy_In = 0;                      // energy (J)
float Energy_Out = 0;                     // energy (J)
float Energy_In_CK = 0;                   // energy (J)  -for checking
float Energy_Out_CK = 0;                  // energy (J)  -for checking
float previousTime = 0;                   // time (sec)
float timeChange = 0;                     // dulation (sec)

float Current_I[numMotor] = {};           // robot use current (mA)
float Voltage_V[numMotor] = {};           // (v)
float Position_P[numMotor] = {};          // position 
// float Torque_t[numMotor] = {};
// float Velocity_v[numMotor] = {};

float Power_In[numMotor] = {};            // power (watt), this is the power that robot consumed for producing work
float Power_Out[numMotor] = {};           // power (watt), this is the power that robot can work.

//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************
void positionCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      positionSig[i] = *it;
      i++;
    }
    return;
}

void currentCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      currentSig[i] = *it;
      i++;
    }
    return;
}


void voltageCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      voltageSig[i] = *it;
      i++;
    }
    return;
}


void torqueCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      torqueSig[i] = *it;
      i++;
    }
    return;
}


void velocityCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      velocitySig[i] = *it;
      i++;
    }
    return;
}


void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      sim_time = *it;
    }
    return;
}


//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){

    // create ros node
    std::string nodeName("COT");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    // data logger
    #if (DATA_LOGGER)
      COT_logger logger_cot("cotlog.txt");
      COT_logger logger_current("currentlog.txt");
      COT_logger logger_voltage("voltagelog.txt");
      COT_logger logger_position("positionlog.txt");
    #endif


    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    ros::Publisher outputEnergy;
    outputEnergy = node.advertise<std_msgs::Float32MultiArray>("/energy_topic",1);

    ros::Subscriber simCorrentSub = node.subscribe("/morf_hw/joint_current",10, currentCB);
    ros::Subscriber simVoltageSub = node.subscribe("/morf_hw/joint_inputVoltage",10, voltageCB);
    ros::Subscriber simTorqueSub = node.subscribe("/morf_hw/joint_torques",10, torqueCB);
    ros::Subscriber simVolocitySub = node.subscribe("/morf_hw/joint_velocities",10, velocityCB);
    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);
    ros::Subscriber simPositionSub = node.subscribe("/morf_hw/joint_positions",10, positionCB);

    float Power_In_Sum = 0;
    float Power_Out_Sum = 0;

    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*              ros parameter              *
        //*                                         *
        //*******************************************

        timeChange = sim_time - previousTime;

        if(timeChange > 0.25)  // compute energy every 0.25 sec  
        {
          for(int i = 0; i < torqueSig.size() ; i++)
          {
            // collected the values of current and voltage
            Current_I[i] = currentSig[i];       // (mA)
            Voltage_V[i] = voltageSig[i];       // (v)
            Position_P[i] = positionSig[i];      // (rad)

            // collected the values of power in/out for each motor
            Power_In[i]  = (abs(currentSig[i]/1000) * abs(voltageSig[i]));     // Watt ((mA/1000)*volt)
            Power_Out[i] = (abs(torqueSig[i]) * abs(velocitySig[i]));          // Watt (Nm*rad/s)

            // sum of power for all motors
            Power_In_Sum  = Power_In_Sum + (abs(currentSig[i]/1000) * abs(voltageSig[i]));     // Watt ((mA/1000)*volt)
            Power_Out_Sum = Power_Out_Sum + (abs(torqueSig[i]) * abs(velocitySig[i]));         // Watt (Nm*rad/s)
          }
          previousTime = sim_time;
        }
        
        for(int i = 0; i < torqueSig.size() ; i++)
        {
          // calculate energy in/out from each motor power 
          Energy_In  = Energy_In  + (Power_In[i]  * timeChange);              // milliJoule (Watt*Sec)
          Energy_Out = Energy_Out + (Power_Out[i] * timeChange);              // Joule (Watt*Sec)
        }

        // calculate energy in/out from sum of power (CHECKER)
        Energy_In_CK  = Energy_In  + (Power_In_Sum  * timeChange);            // milliJoule (Watt*Sec)
        Energy_Out_CK = Energy_Out + (Power_Out_Sum * timeChange);            // Joule (Watt*Sec)

        #if (DATA_LOGGER)
          // use data logger for analize cpg and pcpg
          logger_cot.cot_logs("cotlog.txt", sim_time, timeChange, Power_In_Sum, Power_Out_Sum, Energy_In, Energy_Out, Energy_In_CK, Energy_Out_CK);

          logger_current.current_logs("currentlog.txt", sim_time, Current_I[0],  Current_I[1],  Current_I[2],  Current_I[3],  Current_I[4],  Current_I[5],  Current_I[6],  Current_I[7],  Current_I[8], Current_I[9],
                                                                  Current_I[10], Current_I[11], Current_I[12], Current_I[13], Current_I[14], Current_I[15], Current_I[16]);


          logger_voltage.voltage_logs("voltagelog.txt", sim_time, Voltage_V[0],  Voltage_V[1],  Voltage_V[2],  Voltage_V[3],  Voltage_V[4],  Voltage_V[5],  Voltage_V[6],  Voltage_V[7],  Voltage_V[8], Voltage_V[9],
                                                                  Voltage_V[10], Voltage_V[11], Voltage_V[12], Voltage_V[13], Voltage_V[14], Voltage_V[15], Voltage_V[16]);

          logger_position.position_logs("positionlog.txt", sim_time, Position_P[0],  Position_P[1],  Position_P[2],  Position_P[3],  Position_P[4],  Position_P[5],  Position_P[6],  Position_P[7],  Position_P[8], Position_P[9],
                                                                     Position_P[10], Position_P[11], Position_P[12], Position_P[13], Position_P[14], Position_P[15], Position_P[16]);
                                                        
        #endif
        
        // clear power in/out
        Power_In_Sum = 0;
        Power_Out_Sum = 0;


        energySignal.data.clear();
        energySignal.data.push_back(Energy_In);
        energySignal.data.push_back(Energy_Out);
        outputEnergy.publish(energySignal);
        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok
    return 0;
}

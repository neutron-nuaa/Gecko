-- DO NOT WRITE CODE OUTSIDE OF THE if-then-end SECTIONS BELOW!! (unless the code is a function definition)

if (sim_call_type==sim_childscriptcall_initialization) then
--[[ ************                           single CPG                             ************]]
    --define a baisc CPG output class named "basic_cpg"
    basic_cpg ={output1=0,output2=0,act1=0,act2=0, bias1_2=0, w11_22=0, w12=0, w21=0}
    function basic_cpg:new(o)
        o = o or {}
        setmetatable(o,self)
        self.__index = self
        return o
    end
    --set parameters which define the properties of single CPG
    function basic_cpg:setparameter(output1_2, bias1_2, w11_22, wd1, MI)
        self.output1 = output1_2
        self.output2 = output1_2
        self.bias1_2 = bias1_2
        self.w11_22 = w11_22
        self.w12 = wd1 + MI
        self.w21 = -(wd1 + MI)
    end
    --let CPG run for once
    function basic_cpg:run()

        self.act1 = self.w11_22 * self.output1 + self.w12 * self.output2 + self.bias1_2
        self.act2 = self.w11_22 * self.output2 + self.w21 * self.output1 + self.bias1_2
        self.output1 = math.tanh(self.act1)
        self.output2 = math.tanh(self.act2)
    end
    --get the CPG output1/output2
    function basic_cpg:getsignal(number)
        if (number == 1) then
            return self.output1
        else
            return self.output2
        end
    end
--[[ ************                           single CPG                             ************]]


--[[ ************                           Motor neuron                            ************]]
    --define a Motor neuron class named "motor_neuron"
    motor_neuron = {}
    function motor_neuron:new(o)
        o = o or {}
        setmetatable(o,self)
        self.__index = self
        return o
    end

    --define a funcion clip to guarantee the foot smoothly locomtion on the ground
    function motor_neuron:clip(curve0)
        if (curve0 < 0) then
            return 0
        else
            return curve0
        end
    end

    function motor_neuron:addforce(curve1,handle1,force1)
        if (curve1 <= 0) then
            simAddForce(handle1,{0,0,0},{0,0,-force1})
        end
    end
--[[ ************                           Motor neuron                            ************]]

--[[ ************                           delay line                            ************]]
    --define a delayline class named "delayline"
    delayline = {delay_lf = {}, step = 1,tablesize = 0}
    function delayline:new(o)
        o = o or {}
        setmetatable(o,self)
        self.__index = self
        return o
    end

    --set the total delaysize for delayline
    function delayline:set_delaysize(size)
        t = {}
        self.tablesize = size
        for i=1,size,1 do
            table.insert(t,0)
        end
        self.delay_lf = t
    end

    --write data into the delay_lf
    function delayline:writein(out)
        self.delay_lf[self.step] = out
    end

    --define mod function to set the postion of read
    function delayline:mod(x, m)
        r = x % m
        if (r <= 0) then
            return r + m
        else
            return r
        end
    end

    --read the data from delayline
    function delayline:readfr(delay)
        y = self.delay_lf[delayline:mod(self.step - delay, self.tablesize)]
        return y
    end

    --renew the step
    function delayline:step_one()
        self.step = self.step + 1
        if (self.step % (self.tablesize+1) == 0) then
            self.step = 1
        end
    end
--[[ ************                           delay line                            ************]]


--[[ ************                            Create CPG                             ************]]
    output = basic_cpg:new()
    output:setparameter(0.01,0.00,1.4,0.18,0.06)
--[[ ************                            Create CPG                             ************]]

--[[ ************                            Create motor neuron                             ************]]
    motor = motor_neuron:new()
--[[ ************                            Create motor neuron                             ************]]

--[[ ************                            Create delayline                             ************]]
-- delayline for left_up/down joints
    l_up_down_delay = delayline:new()
    l_up_down_delay:set_delaysize(65)
-- delayline for left_forward/backward joints
    l_for_back_delay = delayline:new()
    l_for_back_delay:set_delaysize(65)
-- delayline for right_up/down joints
    r_up_down_delay = delayline:new()
    r_up_down_delay:set_delaysize(17)
-- delayline for right_forward/backward joints
    r_for_back_delay = delayline:new()
    r_for_back_delay:set_delaysize(17)

--[[ ************                            Create delayline                             ************]]


--[[ ************                            get objecthandle                            ************]]
-- get joint handle
    lbjointhandle2 = simGetObjectHandle('gecko_joint_lb2')
    lbjointhandle1 = simGetObjectHandle('gecko_joint_lb1')
    lbjointhandle0 = simGetObjectHandle('gecko_joint_lb0')

    rfjointhandle2 = simGetObjectHandle('gecko_joint_rf2')
    rfjointhandle1 = simGetObjectHandle('gecko_joint_rf1')
    rfjointhandle0 = simGetObjectHandle('gecko_joint_rf0')

    lfjointhandle2 = simGetObjectHandle('gecko_joint_lf2')
    lfjointhandle1 = simGetObjectHandle('gecko_joint_lf1')
    lfjointhandle0 = simGetObjectHandle('gecko_joint_lf0')

    rbjointhandle2 = simGetObjectHandle('gecko_joint_rb2')
    rbjointhandle1 = simGetObjectHandle('gecko_joint_rb1')
    rbjointhandle0 = simGetObjectHandle('gecko_joint_rb0')

-- get pad handle
    h_pad_lb = simGetObjectHandle('pad_lb')
    h_pad_rb = simGetObjectHandle('pad_rb')
    h_pad_lf = simGetObjectHandle('pad_lf')
    h_pad_rf = simGetObjectHandle('pad_rf')
-- get graph handle
    graph_test = simGetObjectHandle('c1_c2')
--[[ ************                            get objecthandle                            ************]]
end




if (sim_call_type==sim_childscriptcall_actuation) then

-- let CPG run for once
    output:run()
-- get cpg output signal
    c1 = output:getsignal(1)
    c2 = output:getsignal(2)
-- try to graph c1 and c2

-- adjust signal for up/down joints
    up_down = motor:clip(0.4 * c2)

-- delay the cpg output
-- write the cpg signal into the delayline
    l_up_down_delay:writein(up_down)
    l_for_back_delay:writein(c1)
    r_up_down_delay:writein(up_down)
    r_for_back_delay:writein(c1)
-- read the delay signal into each joint
--delay for right hind leg
    rb0 = up_down
    rb1 = c1
--delay for right front leg
    rf0 = r_up_down_delay:readfr(16)
    rf1 = r_for_back_delay:readfr(16)
    --simSetGraphUserData(graph_test,'c2',rf1)
    --simSetGraphUserData(graph_test,'c1',rf0)
--delay for left hind leg
    lb0 = l_up_down_delay:readfr(48)
    lb1 = l_for_back_delay:readfr(48)
    --simSetGraphUserData(graph_test,'c2',lb1)
    --simSetGraphUserData(graph_test,'c1',lb0)
--delay for left front leg
    lf0 = l_up_down_delay:readfr(64)
    lf1 = l_for_back_delay:readfr(64)
    --simSetGraphUserData(graph_test,'c2',lf1)
    --simSetGraphUserData(graph_test,'c1',lf0)
-- set signal for all joints
    simSetJointTargetPosition(lbjointhandle1, lb1)
    simSetJointTargetPosition(lbjointhandle0, lb0)

    simSetJointTargetPosition(rfjointhandle1, rf1)
    simSetJointTargetPosition(rfjointhandle0, rf0)

    simSetJointTargetPosition(lfjointhandle1, lf1)
    simSetJointTargetPosition(lfjointhandle0, lf0)

    simSetJointTargetPosition(rbjointhandle1, rb1)
    simSetJointTargetPosition(rbjointhandle0, rb0)

-- addforce on the pad
    motor:addforce(lb0,h_pad_lb,40)
    motor:addforce(rf0,h_pad_rf,40)
    motor:addforce(lf0,h_pad_lf,40)
    motor:addforce(rb0,h_pad_rb,40)
-- renew step for delayline
    l_up_down_delay:step_one()
    l_for_back_delay:step_one()
    r_up_down_delay:step_one()
    r_for_back_delay:step_one()
end


if (sim_call_type==sim_childscriptcall_sensing) then

    -- Put your main SENSING code here

end


if (sim_call_type==sim_childscriptcall_cleanup) then

    -- Put some restoration code here

end

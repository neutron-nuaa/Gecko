import numpy as np
import matplotlib.pyplot as plt


class delayline():

    def __init__(self,size,delay):
        self.size = size
        self.container = np.zeros(self.size)
        self.p_in = 0
        self.p_out = self.p_in + delay
        # print('delay: ',delay)

    def D_in(self,data):
        self.p_in = self.p_in % self.size
        self.container[self.p_in] = data
        self.p_in = self.p_in + 1


    def D_out(self):
        self.p_out = self.p_out % self.size
        # print(self.p_out)
        d_out = self.container[self.p_out]
        self.p_out = self.p_out + 1
        return d_out
#!/usr/bin/env python

import rospy
from Sensor_msg.msg import Sensor3
from std_msgs.msg import Float32MultiArray as floatarray
import csv
import math

sensor_signal = []


def sensor_record(msg):
		global sensor_signal
		sensor_signal = [msg.IR1,msg.IR2,msg.RotationX,msg.RotationY,msg.RotationZ]



def joint_record(msg):
		
		for i in msg.data:
 			sensor_signal.append(i)
		with open('/home/sdh/catkin_ws/src/Sensor_msg/all_sensor_data.csv','a') as f:
			csv_write = csv.writer(f,dialect='excel')
			csv_write.writerow(sensor_signal)
			print("write over___1")


def signal_record():
    rospy.init_node('si_re',anonymous=True)

    # for publisher
    
    # for subscribler

    rospy.Subscriber('/Sensor',Sensor3,sensor_record)
    rospy.Subscriber('joint_positions',floatarray,joint_record)

    rospy.spin()


if __name__ == '__main__':
    signal_record()

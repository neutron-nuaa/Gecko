#!/usr/bin/env python
import numpy as np
import math
import matplotlib.pyplot as plt
import CPG
import RBF
import delayline

import rospy
from Sensor_msg.msg import Sensor3
from std_msgs.msg import Float32MultiArray as floatarray
import csv
import math

# Global parameters setting
# ID the motor
motordata = [11,0,21,0,31,0,41,0,12,0,22,0,32,0,42,0,13,0,23,0,33,0,43,0,14,0,24,0,34,0,44,0]

# define the initial angle of the robot
in_angle = 25
# Network initialize
# Create the CPG
CPG1 = CPG.CPG()
n_o1 = CPG1.N_o1()
n_o2 = CPG1.N_o2()
# Create the RBF
k_n = 80  # the number of kernels
s = 0.001  # width of the kernel
center = np.loadtxt('center_n.csv', delimiter=',')
center1 = center[0, :]
center2 = center[1, :]
RBF1 = RBF.RBF(k_n, s, center1, center2)
# Load weights for different joints' signals
weight = np.loadtxt('RBF_linear_weights.csv', delimiter=',')
w_j1 = weight[:, 0]
w_j2 = weight[:, 1]
w_j3 = weight[:, 2]
w_j4 = weight[:, 3]
# Crate the delayline
size_delayline = 350
tau = 175
DL_1 = delayline.delayline(size_delayline, tau)
DL_2 = delayline.delayline(size_delayline, tau)
DL_3 = delayline.delayline(size_delayline, tau)
DL_4 = delayline.delayline(size_delayline, tau)
# Create the  publisher
pub = rospy.Publisher('/multi_joint_command', floatarray, queue_size=10)

# Create the callback function
def execute_once(msg):
    global in_angle
    global n_o1
    global n_o2
    # Sensor layer
    if msg.IR1 > 0.5 and msg.IR1 < 0.01:
        in_angle = in_angle - 2

    if msg.IR2 > 0.5 and msg.IR1 < 0.01:
        in_angle = in_angle + 1

    # CPG layer
    n_o1 = CPG1.N_o1(n_o1, n_o2)
    n_o2 = CPG1.N_o2(n_o1, n_o2)


    # RBF layer
    RBF_output = RBF1.n_output(n_o1, n_o2)
    # Motor layer
    # group 1
    j1 = np.matmul(w_j1, RBF_output)
    j2 = np.matmul(w_j2, RBF_output)
    j3 = np.matmul(w_j3, RBF_output)
    j4 = np.matmul(w_j4, RBF_output)
    # group 2 (delayline tau)
    DL_1.D_in(j1)
    DL_2.D_in(j2)
    DL_3.D_in(j3)
    DL_4.D_in(j4)
    j1_d = DL_1.D_out()
    j2_d = DL_2.D_out()
    j3_d = DL_3.D_out()
    j4_d = DL_4.D_out()
    # leg 1
    motordata[1] = -j1
    motordata[3] = j2 - (in_angle * 1.0) / 180 * math.pi
    motordata[5] = j3
    motordata[7] = j4 - (in_angle * 1.0) / 180 * math.pi
    # leg 2
    motordata[9] = -j1_d
    motordata[11] = -j2_d + (in_angle * 1.0) / 180 * math.pi
    motordata[13] = j3_d
    motordata[15] = -j4_d + (in_angle * 1.0) / 180 * math.pi
    # leg 3
    motordata[17] = j1
    motordata[19] = j2 - (in_angle * 1.0) / 180 * math.pi
    motordata[21] = -j3
    motordata[23] = j4 - (in_angle * 1.0) / 180 * math.pi
    # leg 4
    motordata[25] = j1_d
    motordata[27] = -j2_d + (in_angle * 1.0) / 180 * math.pi
    motordata[29] = -j3_d
    motordata[31] = -j4_d + (in_angle * 1.0) / 180 * math.pi

    motor_signal = floatarray(data=motordata)
    pub.publish(motor_signal)


def robot_move():
    # Initialize the node
    rospy.init_node('Nyxrobot', anonymous=True)
    # Create the subscriber
    rospy.Subscriber('/Sensor', Sensor3, execute_once)
    rospy.spin()


if __name__ == '__main__':
    robot_move()


















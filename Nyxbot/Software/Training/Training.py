import numpy as np
import math
import matplotlib.pyplot as plt
import CPG
import RBF

file_dir = './'


if __name__ == '__main__':
    CPG1 = CPG.CPG()
    n_o1 = CPG1.N_o1()
    n_o2 = CPG1.N_o2()

    n1 = []
    n2 = []
    # Dataset for training
    for i in range(350):

        n_o1 = CPG1.N_o1(n_o1,n_o2)
        n_o2 = CPG1.N_o2(n_o1,n_o2)
        n1.append(n_o1)
        n2.append(n_o2)

    # fig = plt.figure()
    # plt.plot(n1)
    # plt.plot(n2,'red')
    # plt.show()
    # Sampling the data
    # set the kernel number
    k_n = 80
    s = 0.001
    center_i = np.round(np.linspace(1,len(n1),k_n))
    center_i = center_i.astype(int)
    center_i = center_i -1
    n1 = np.array(n1)
    n2 = np.array(n2)
    center_n1 = n1[center_i]
    center_n2 = n2[center_i]


    # save the verteces control for RBF

    center_n = np.row_stack((center_n1,center_n2))

    np.savetxt( "center_n.csv", center_n, delimiter=',')


    RBF_layer = RBF.RBF(k_n,s,center_n1,center_n2)

    RBF_output = RBF_layer.n_output(n1[0],n2[0])

    for i in range(1,len(n1)):
        RBF_output = np.column_stack((RBF_output, RBF_layer.n_output(n1[i],n2[i])))
    RBF_output = np.array(RBF_output)
    #
    # fig = plt.figure()
    # plt.plot(RBF_output[78,:])
    # plt.show()

    # For training
    # Load target data
    target_j1 = np.loadtxt('./TrainingTarget/oroj1.csv', delimiter=',')
    target_j2 = np.loadtxt('./TrainingTarget/oroj2.csv', delimiter=',')
    target_j3 = np.loadtxt('./TrainingTarget/oroj3.csv', delimiter=',')
    target_j4 = np.loadtxt('./TrainingTarget/oroj4.csv', delimiter=',')

    fig = plt.figure()
    plt.plot(target_j4)
    plt.show()

    # set the training parameters
    w_j1 = np.zeros(k_n)
    w_j2 = np.zeros(k_n)
    w_j3 = np.zeros(k_n)
    w_j4 = np.zeros(k_n)
    alfa = 0.1


    # train times
    train_time = 300
    for i in range(train_time):
        Motor = np.matmul(w_j1,RBF_output)
        w_j1 = w_j1 +  alfa *(target_j1[center_i] - Motor[center_i])

    for i in range(train_time):
        Motor = np.matmul(w_j2,RBF_output)
        w_j2 = w_j2 +  alfa *(target_j2[center_i] - Motor[center_i])

    for i in range(train_time):
        Motor = np.matmul(w_j3,RBF_output)
        w_j3 = w_j3 +  alfa *(target_j3[center_i] - Motor[center_i])

    for i in range(train_time):
        Motor = np.matmul(w_j4,RBF_output)
        w_j4 = w_j4 +  alfa *(target_j4[center_i] - Motor[center_i])

    train_output_j1 = np.matmul(w_j1, RBF_output)
    train_output_j2 = np.matmul(w_j2, RBF_output)
    train_output_j3 = np.matmul(w_j3, RBF_output)
    train_output_j4 = np.matmul(w_j4, RBF_output)

    # save the connections in the third layer of RBF

    w_j = np.column_stack((w_j1, w_j2, w_j3, w_j4))

    np.savetxt( "RBF_linear_weights.csv", w_j, delimiter=',')


    # fig = plt.figure()
    # plt.subplot(4, 1, 1)
    # plt.plot(train_output_j1)
    # plt.subplot(4, 1, 2)
    # plt.plot(train_output_j2)
    # plt.subplot(4, 1, 3)
    # plt.plot(train_output_j3)
    # plt.subplot(4, 1, 4)
    # plt.plot(train_output_j4)
    # plt.show()


import numpy as np
import math
import matplotlib.pyplot as plt

class RBF():
    def __init__(self,k_n,s,center1,center2):
        self.k_n = k_n
        self.s = s
        self.center1 = center1
        self.center2 = center2
        self.output = []

    def n_output(self, n1, n2):
        output = []
        for i in range(self.k_n):
            r1 = math.exp(-((n1-self.center1[i])**2+(n2-self.center2[i])**2)/self.s)
            output.append(r1)

        return np.array(output).T
import numpy as np
import math
import matplotlib.pyplot as plt

class CPG():
    def __init__(self,):
        self.w11 = 1.01
        self.w22 = self.w11
        self.w12 = 0.0058*math.pi
        self.w21 = -self.w12


    def N_o1(self,n_o1 = -0.2012, n_o2=0.0):
        return np.tanh(self.w11*n_o1 + self.w12*n_o2)

    def N_o2(self,n_o1 = -0.2012, n_o2=0.0):
        return np.tanh(self.w22*n_o2 + self.w21*n_o1)
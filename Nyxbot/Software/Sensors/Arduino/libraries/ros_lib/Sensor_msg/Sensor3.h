#ifndef _ROS_Sensor_msg_Sensor3_h
#define _ROS_Sensor_msg_Sensor3_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace Sensor_msg
{

  class Sensor3 : public ros::Msg
  {
    public:
      typedef float _IR1_type;
      _IR1_type IR1;
      typedef float _IR2_type;
      _IR2_type IR2;
      typedef float _RotationX_type;
      _RotationX_type RotationX;
      typedef float _RotationY_type;
      _RotationY_type RotationY;
      typedef float _RotationZ_type;
      _RotationZ_type RotationZ;

    Sensor3():
      IR1(0),
      IR2(0),
      RotationX(0),
      RotationY(0),
      RotationZ(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_IR1;
      u_IR1.real = this->IR1;
      *(outbuffer + offset + 0) = (u_IR1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_IR1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_IR1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_IR1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->IR1);
      union {
        float real;
        uint32_t base;
      } u_IR2;
      u_IR2.real = this->IR2;
      *(outbuffer + offset + 0) = (u_IR2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_IR2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_IR2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_IR2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->IR2);
      union {
        float real;
        uint32_t base;
      } u_RotationX;
      u_RotationX.real = this->RotationX;
      *(outbuffer + offset + 0) = (u_RotationX.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_RotationX.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_RotationX.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_RotationX.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->RotationX);
      union {
        float real;
        uint32_t base;
      } u_RotationY;
      u_RotationY.real = this->RotationY;
      *(outbuffer + offset + 0) = (u_RotationY.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_RotationY.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_RotationY.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_RotationY.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->RotationY);
      union {
        float real;
        uint32_t base;
      } u_RotationZ;
      u_RotationZ.real = this->RotationZ;
      *(outbuffer + offset + 0) = (u_RotationZ.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_RotationZ.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_RotationZ.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_RotationZ.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->RotationZ);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_IR1;
      u_IR1.base = 0;
      u_IR1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_IR1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_IR1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_IR1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->IR1 = u_IR1.real;
      offset += sizeof(this->IR1);
      union {
        float real;
        uint32_t base;
      } u_IR2;
      u_IR2.base = 0;
      u_IR2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_IR2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_IR2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_IR2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->IR2 = u_IR2.real;
      offset += sizeof(this->IR2);
      union {
        float real;
        uint32_t base;
      } u_RotationX;
      u_RotationX.base = 0;
      u_RotationX.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_RotationX.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_RotationX.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_RotationX.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->RotationX = u_RotationX.real;
      offset += sizeof(this->RotationX);
      union {
        float real;
        uint32_t base;
      } u_RotationY;
      u_RotationY.base = 0;
      u_RotationY.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_RotationY.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_RotationY.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_RotationY.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->RotationY = u_RotationY.real;
      offset += sizeof(this->RotationY);
      union {
        float real;
        uint32_t base;
      } u_RotationZ;
      u_RotationZ.base = 0;
      u_RotationZ.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_RotationZ.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_RotationZ.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_RotationZ.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->RotationZ = u_RotationZ.real;
      offset += sizeof(this->RotationZ);
     return offset;
    }

    const char * getType(){ return "Sensor_msg/Sensor3"; };
    const char * getMD5(){ return "79aed710c279c01b1582bda9f975a640"; };

  };

}
#endif

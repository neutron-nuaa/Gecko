#include <ros.h>
#include <Wire.h>
#include <JY901.h>
//#include<std_msgs/Float32.h>
#include<Sensor_msg/Sensor3.h>


ros::NodeHandle nh;

//std_msgs::Float32 str_msg;

Sensor_msg::Sensor3 str_msg;
ros::Publisher chatter("Sensor", &str_msg);



int sensorPin = A0;  // select the input pin for the potentiometer
float sensorValue0 = 0;
float dst0 = 0;
float ir_node_value0 = 0; // variable to store the value coming from the sensor

int sensorPin1 = A1;  // select the input pin for the potentiometer
float sensorValue1 = 0;
float dst1 = 0;
float ir_node_value1 = 0; // variable to store the value coming from the sensor






void setup() {
  nh.initNode();
  nh.advertise(chatter);
  JY901.StartIIC();
}

void loop() {
  // read the value from the sensor0:
  sensorValue0 = analogRead(sensorPin);
  
  if (sensorValue0>10)
    { dst0 = 1/((sensorValue0 / 1023 * 5 - 0.0496) / 12.65) - 0.42;
      if (dst0 < 30)
        ir_node_value0 = (30 - dst0) / 100;
      else
        ir_node_value0 = 0;
      }
  else
    ir_node_value0 = 0;



  // read the value from the sensor1:
  sensorValue1 = analogRead(sensorPin1);
  
  
  if (sensorValue1>10)
    { dst1 = 1/((sensorValue1 / 1023 * 5 - 0.0496) / 12.65) - 0.42;
      if (dst1 < 30)
        ir_node_value1 = (30 - dst1) / 100;
      else
        ir_node_value1 = 0;
      }
  else
    ir_node_value1 = 0;


//    Serial.print("Sensor1:");
//    Serial.println(ir_node_value0);
//    Serial.print("Sensor2:");
//    Serial.println(ir_node_value1);
//
//
//
//    
    JY901.GetAngle();
//    Serial.print("Angle:");Serial.print((float)JY901.stcAngle.Angle[0]/32768*180);Serial.print(" ");Serial.print((float)JY901.stcAngle.Angle[1]/32768*180);Serial.print(" ");Serial.println((float)JY901.stcAngle.Angle[2]/32768*180);
//  

    str_msg.IR1 = ir_node_value0;
    str_msg.IR2 = ir_node_value1;
    str_msg.RotationX = (float)JY901.stcAngle.Angle[0]/32768*180;
    str_msg.RotationY = (float)JY901.stcAngle.Angle[1]/32768*180;
    str_msg.RotationZ = (float)JY901.stcAngle.Angle[2]/32768*180;
    
 
    chatter.publish(&str_msg);
    nh.spinOnce();
    delay(35);
  
  
}

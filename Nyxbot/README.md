# Nyxbot

The **nyxbot** is gecko-inspired robot with body height adaptability. The robot can overcome the obstacle autonomously on slopes, and the ability get benefit from the body structure and control method. And the profile of the gecko-inspired robot and the Geckko gecko is shown in the Fig. 1. 

<div align="center">

<img src="Nyxbot.assets/Nyxbot_overall.jpg" alt="Nyxbot_overall"  width="600">
</div>

<div align="center">
Figure 1: Geckko gecko on the left and gecko-inspired robot (nyxbot) on the right.
</div>


## Body height adaptability

The body height adaptability is most distinct feature, which makes the **Nyxbot** different from other gecko-inspired robot. The robot obstacle-overcoming process  is shown on the Fig. 2. And this special body-height adaptability is generated from three aspects of the robot design, they are body strcuture, adhesive mechanism and neural based control respectively.

<div align="center">
<img src="Nyxbot.assets/Body_Height_adaptability.png" alt="Body_Height_adaptability" width="600">
</div>

Figure 2: Gecko-inspired robot obsacle overcoming process. Snapshots of robot showing the climbing and obstacle-overcoming abilities of the gecko-inspired robot. I, II, III on the top left corner indicate the prior-to-obstacle period, over-obstacle period, and after-obstacle-period,respectively.


#### Body structure

  By looking into the skeletal system and kinematics of real gecko, we designed **4-DOFs**  single limb for the robot. Which makes the feature of body-height adaptability feasible in robot's physical part. The morphology similarity between real gecko and gecko-inspired robto can be found in Fig. 3.

<div align="center">
  <img src="Nyxbot.assets/Body_structure.png" alt="Body_structure" width="300">
</div>
Figure 3: Structure esign of a gecko-inspired robot. The skeletal system of a gecko and the structure of the gecko-inspired robot.


#### Adhesive mechanism

  The adhesive mechanism was specially designed for gecko-inspired robot.  It can endow the robot more compact strucutre design, due to its small size and high adhesion efficiency. The functional similarity between real gecko and gecko-inspired robot can be found in Fig. 4.

<div align="center">
  <img src="Nyxbot.assets/Adhesive_mechanism.png" alt="Adhesive_mechanism" width="400">
</div>
Figure 4: The adhesive foot of a gecko and the robot. The comparison of the functional parts between the real gecko and gecko-inspired robot.


#### Neural based control

  Another important part to endow the gecko-inspired robot with body height adaptability is the **Neural based control**. Here the Central pattern generator (CPG) was used as t he periodic signal generator, provide the robot with basic time sequence. The radial basis function (RBF) was used here to generate complex moving trajectory, in order to realize the biomimic adhesion. The delta learning rule was used here to adjust the linear combination sub-layer of RBF. 

## Framework
The project is organized by six sub-folders including **Footpad**, **Linkings**, **Servo_motor**, **Control**, **Sensors**, and **Training**
- **Footpad** contains a video tell details of performance and manufacture of adhesive mechanism.
- **Linkings** contains the building blocks and linkings of gecko-inpsired robot body structure.
- **Servo_motor** contains the model file of the used actuators.
- **Control** contains all the source file (CPGs, RBF, main.py) to drive the gecko-inspired robot.
- **Sensors** contains the interface of the sensors which set as a node in ROS.
- **Training** contains the algorithmn to train the robot to obtain the attachment-detachment locomotion ability.


 




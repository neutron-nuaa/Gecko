--[[
Callback function for receiving motor positions over ROS
--]]

-- Constants
math.randomseed(os.time())

function setGravety(gravetyMagnitude, angleDeg, verbal)
    local angle1Rad = angleDeg * 0.0174532925
    local angle2Rad = (180 - 90 - angleDeg) * 0.0174532925 
    local gravetyZ = (math.cos(angle1Rad) * -1) * gravetyMagnitude
    local gravetyY = math.cos(angle2Rad) * gravetyMagnitude
    sim.setArrayParameter(sim.arrayparam_gravity,{0,gravetyY,gravetyZ})
    if verbal then
        print("gravity set to " .. angleDeg .. " deg. [x,y,z] [ 0, " .. gravetyY .. ", " .. gravetyZ .. " ]")
    end
end

function publishSimTime()
    local simulationTime = sim.getSimulationTime()
    simROS.publish(simTime_pub,{data=simulationTime})
end

function publishSimState()
    local simulationState = sim.getSimulationState()            -- 17 = simulation_running  | 19 = simulation_paused | 20 = simulation_resumed | 21 = simulation_stopping | 22 = simulation_stopped
    if simulationState ~= lastSimState then                     -- only publish simulation state if it has changed
        simROS.publish(simState_pub,{data=simulationState})
        lastSimState = simulationState
    end
end

local function roundToNthDecimal(num, n)
    local mult = 10^(n or 0)
    return math.floor(num * mult + 0.5) / mult
end

function checkFootAdheasiveForces(FloorHandler,FootHandlers,MinAdheasiveDist,AdheasiveForce)
    for i=1, table.getn(FootHandlers), 1 do
        local distance = 0
        local resoult = 0
        local frictionResoult = 0
        resoult, distance = sim.checkDistance(FootHandlers[i],FloorHandler,MinAdheasiveDist)
        local footOrientation = sim.getObjectOrientation(FootHandlers[i],-1)                                    -- orientation with referance to world frame

        if resoult == 1 and math.abs(footOrientation[1]) < ADHESIVE_FORCE_Angle and math.abs(footOrientation[2]) < ADHESIVE_FORCE_Angle then    -- distance is smaller than MinAdheasiveDist and food angle is low enough
            sim.setShapeColor(FootHandlers[i],nil,sim.colorcomponent_ambient,{1,0,0})
            sim.addForce(FootHandlers[i],{0,0,0},{0,0,-AdheasiveForce})

            --print(sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i]) .. " < " .. ADHESIVE_FORCE_Friction_const-1)
            -- if sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i]) < ADHESIVE_FORCE_Friction_const then
            --     frictionResoult = sim.setEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i],ADHESIVE_FORCE_Friction_const)
            --     local resetResoult = simResetDynamicObject(FootHandlers[i])
            -- end

        else
            sim.setShapeColor(FootHandlers[i],nil,sim.colorcomponent_ambient,{0,1,0})

            --print(sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i]) .. " > " .. 0.0+1)
            -- if sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i]) > 0.0 then
            --     frictionResoult = sim.setEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[i], 0.0)
            --     local resetResoult = simResetDynamicObject(FootHandlers[i])
            -- end

        end

    end
    --print("friction state [ " .. sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[1]) .. ", " .. sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[2]) .. ", " .. sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[3]) .. ", " .. sim.getEngineFloatParameter(sim.vortex_body_primlinearaxisfriction, FootHandlers[4]) .. " ]")
end

function sumArray(arrayToSum)
    local sum = 0.0;
    for i=1, table.getn(arrayToSum), 1 do
        sum = sum + arrayToSum[i]
    end
    return sum
end

function mean( t )
    local sum = 0
    local count= 0
    for k,v in pairs(t) do
        if type(v) == 'number' then
            sum = sum + v
            count = count + 1
        end
    end
    return (sum / count)
end

function standardDeviation( t )
    local m
    local vm
    local sum = 0
    local count = 0
    local result
    m = mean( t )
    for k,v in pairs(t) do
        if type(v) == 'number' then
            vm = v - m
            sum = sum + (vm * vm)
            count = count + 1
        end
    end
    result = math.sqrt(sum / (count-1))
    return result
end

function gaussian (mean, variance)
    return  math.sqrt(-2 * variance * math.log(math.random())) * math.cos(2 * math.pi * math.random()) + mean
end

function simGetJointVelocity (jointHandle)
    res,velocity=simGetObjectFloatParameter(jointHandle,2012)
    return  velocity
end

function absmean (prev_avg, x, n)
    return ((prev_avg * n + math.abs(x)) / (n + 1));
end

function absmean_arr (numlist)
    if type(numlist) ~= 'table' then
        print("Error in absmean_arr")
        return numlist
    end
    num = 0
    --[[
        table.foreach

        http://lua-users.org/wiki/TableLibraryTutorial
        Note: this function is deprecated in Lua 5.1, but it can still be useful for printing out a table.
        You should use the pairs() operator instead. As with pairs(), the table.foreach() method is not guaranteed to return
        indexed keys in order, contrary to what examples here might imply.
    --]]
    -- table.foreach(numlist,function(i,v) num=num+math.abs(v) end)
    for i,v in pairs(numlist) do num=num+math.abs(v) end
    return num / #numlist
end

function slip_detector( object_handle, vel_threshold, angular_vel_threshold)
    index=0
    objectsInContact,contactPt,forceDirectionAndAmplitude=sim.getContactInfo(sim.handle_all,object_handle,index)

    linearVelocity, angularVelocity = sim.getVelocity(object_handle)
    absLinearVelocity = math.sqrt((linearVelocity[1]*linearVelocity[1]) + (linearVelocity[2]*linearVelocity[2]))
    if objectsInContact then
        if absLinearVelocity > vel_threshold then
            return 1
        -- elseif math.abs(angularVelocity[3]) > angular_vel_threshold then      -- angularVelocity[3] is around z    |   angularVelocity = [x,y,z]
        --     return 1
        end
        return 0
    else return 0
    end
end

function slip_detector_dist(FloorHandler, FootHandler, vel_threshold, minDistance)

    local distance = 0
    local resoult = 0
    resoult, distance = sim.checkDistance(FootHandler, FloorHandler, minDistance)

    if resoult == 1 then    -- distance is smaller than minDistance

        linearVelocity, angularVelocity = sim.getVelocity(FootHandler)
        absLinearVelocity = math.sqrt((linearVelocity[1]*linearVelocity[1]) + (linearVelocity[2]*linearVelocity[2]))
        
        if absLinearVelocity > vel_threshold then   -- if velocity is higher than threashold
            return 1
        else
            return 0
        end
    else
        return 0
    end
end

function setTartetMotorPositions()

    if newMotorPos then

        sim.setJointTargetPosition(TC_motor0,targetMotorPos[1])
        sim.setJointTargetPosition(CF_motor0,targetMotorPos[2])
        sim.setJointTargetPosition(FT_motor0,targetMotorPos[3])
        sim.setJointTargetPosition(TT_motor0,targetMotorPos[4])

        sim.setJointTargetPosition(TC_motor1,targetMotorPos[5])
        sim.setJointTargetPosition(CF_motor1,targetMotorPos[6])
        sim.setJointTargetPosition(FT_motor1,targetMotorPos[7])
        sim.setJointTargetPosition(TT_motor1,targetMotorPos[8])

        sim.setJointTargetPosition(TC_motor2,targetMotorPos[9])
        sim.setJointTargetPosition(CF_motor2,targetMotorPos[10])
        sim.setJointTargetPosition(FT_motor2,targetMotorPos[11])
        sim.setJointTargetPosition(TT_motor2,targetMotorPos[12])

        sim.setJointTargetPosition(TC_motor3,targetMotorPos[13])
        sim.setJointTargetPosition(CF_motor3,targetMotorPos[14])
        sim.setJointTargetPosition(FT_motor3,targetMotorPos[15])
        sim.setJointTargetPosition(TT_motor3,targetMotorPos[16])

        newMotorPos = false
    end
end

function setMotorPositions_cb(msg)
    local data = msg.data
    targetMotorPos[1] = data[2]
    targetMotorPos[2] = data[4]
    targetMotorPos[3] = data[6]
    targetMotorPos[4] = data[8]

    targetMotorPos[5] = data[10]
    targetMotorPos[6] = data[12]
    targetMotorPos[7] = data[14]
    targetMotorPos[8] = data[16]

    targetMotorPos[9] = data[18]
    targetMotorPos[10] = data[20]
    targetMotorPos[11] = data[22]
    targetMotorPos[12] = data[24]

    targetMotorPos[13] = data[26]
    targetMotorPos[14] = data[28]
    targetMotorPos[15] = data[30]
    targetMotorPos[16] = data[32]
    newMotorPos = true
end

-- function setMotorPositions_cb(msg)
--     local data = msg.data

--     sim.setJointTargetPosition(TC_motor0,data[2])
--     sim.setJointTargetPosition(CF_motor0,data[4])
--     sim.setJointTargetPosition(FT_motor0,data[6])
--     sim.setJointTargetPosition(TT_motor0,data[8])

--     sim.setJointTargetPosition(TC_motor1,data[10])
--     sim.setJointTargetPosition(CF_motor1,data[12])
--     sim.setJointTargetPosition(FT_motor1,data[14])
--     sim.setJointTargetPosition(TT_motor1,data[16])

--     sim.setJointTargetPosition(TC_motor2,data[18])
--     sim.setJointTargetPosition(CF_motor2,data[20])
--     sim.setJointTargetPosition(FT_motor2,data[22])
--     sim.setJointTargetPosition(TT_motor2,data[24])

--     sim.setJointTargetPosition(TC_motor3,data[26])
--     sim.setJointTargetPosition(CF_motor3,data[28])
--     sim.setJointTargetPosition(FT_motor3,data[30])
--     sim.setJointTargetPosition(TT_motor3,data[32])

--     -- sim.setJointTargetPosition(TC_motor0,0)
--     -- sim.setJointTargetPosition(CF_motor0,0)
--     -- sim.setJointTargetPosition(FT_motor0,0)
--     -- sim.setJointTargetPosition(TT_motor0,0)

--     -- sim.setJointTargetPosition(TC_motor1,0)
--     -- sim.setJointTargetPosition(CF_motor1,0)
--     -- sim.setJointTargetPosition(FT_motor1,0)
--     -- sim.setJointTargetPosition(TT_motor1,0)

--     -- sim.setJointTargetPosition(TC_motor2,0)
--     -- sim.setJointTargetPosition(CF_motor2,0)
--     -- sim.setJointTargetPosition(FT_motor2,0)
--     -- sim.setJointTargetPosition(TT_motor2,0)

--     -- sim.setJointTargetPosition(TC_motor3,0)
--     -- sim.setJointTargetPosition(CF_motor3,0)
--     -- sim.setJointTargetPosition(FT_motor3,0)
--     -- sim.setJointTargetPosition(TT_motor3,0)
-- end

function setBackMotorPositions_cb(msg)
    local data = msg.data
    sim.setJointTargetPosition(Back_motor1, data[1])
    sim.setJointTargetPosition(Back_motor2, data[2])
    sim.setJointTargetPosition(Back_motor3, data[3])
    
    --TODO happ
    for i=1,3,1 do
        bodySignal[i] = data[i]
    end
end

function setBackMotorTorque_cb(msg)
    local data = msg.data

    sim.setJointForce(Back_motor1, data[1])
    sim.setJointForce(Back_motor2, data[2])
    sim.setJointForce(Back_motor3, data[3])
end

function publishBackMotorPosition()
    local back1Pos = sim.getJointPosition(Back_motor1)     -- joint position
    local back2Pos = sim.getJointPosition(Back_motor2)     -- joint position
    local back3Pos = sim.getJointPosition(Back_motor3)     -- joint position
    local backMotorPosArray = {back1Pos, back2Pos, back3Pos}

    simROS.publish(backJointPositionsPub, {data = backMotorPosArray})
end

function publishBackMotorVelocity()
    local back3Vel = sim.getJointPosition(Back_motor3)     -- joint position

    local jointTR1LinVel1 = 0
    local back1Vel = 0
    jointTR1LinVel1, back1Vel = sim.getObjectFloatParameter(Back_motor1,2012)     -- 2012 is the joint velocity

    local jointTR1LinVel2 = 0
    local back2Vel = 0
    jointTR1LinVel2, back2Vel = sim.getObjectFloatParameter(Back_motor2,2012)     -- 2012 is the joint velocity

    local jointTR1LinVel3 = 0
    local back3Vel = 0
    jointTR1LinVel3, back3Vel = sim.getObjectFloatParameter(Back_motor3,2012)     -- 2012 is the joint velocity

    local backMotorVelArray = {back1Vel, back2Vel, back3Vel}

    simROS.publish(backJointVelocitiesPub, {data = backMotorVelArray})
end

--[[
Initialization: Called once at the start of a simulation
--]]
if (sim_call_type==sim.childscriptcall_initialization) then
    simulationID=sim.getIntegerSignal("simulationID")

--     print("************")
--     print("Slalom: "..simulationID)
--     print("************")

    stepCounter     = 0
    mean_vel        = 0
    mean_jtor       = 0
    mean_jvel       = 0
    mean_jpower     = 0
    mean_pan        = 0
    mean_tilt       = 0
    mean_roll       = 0
    mean_height     = 0
    mean_slip       = 0
    offset_pan      = 0
    update_count    = 0
    bodyfloor_collisions = 0
    leg_collisions = 0
    collisionLast1 = false
    collisionLast2 = false
    collisionLast3 = false
    collisionLast4 = false
    collisionLastBF = false
    boolswitch     = true
    height_arr = {}
    oriX_arr = {}
    oriY_arr = {}
    orientation_arr = {}
    circlebreak = false;
    testParameters = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    largest_dist = 0
    collisions_max = 0
    geckoTape_dist = 0.0001 --0.000001
    ADHESIVE_FORCE = 5 --7  --8 -- 4  -- 9
    ADHESIVE_FORCE_Angle = 45
    ADHESIVE_FORCE_Friction_const = 6
    gravityChanged = false
    slopeDeg = 0       -- slope angle terrain
    targetMotorPos = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    newMotorPos = false

    -- TODO happ
    bodySignal = {0,0,0}

    -- Create all handles
    robotHandle=sim.getObjectAssociatedWithScript(sim.handle_self)

    TC_motor0=sim.getObjectHandle("joint1_lf")    -- Handle of the TC motor
    TC_motor1=sim.getObjectHandle("joint1_lh")    -- Handle of the TC motor
    TC_motor2=sim.getObjectHandle("joint1_rf")    -- Handle of the TC motor
    TC_motor3=sim.getObjectHandle("joint1_rh")    -- Handle of the TC motor

    CF_motor0=sim.getObjectHandle("joint2_lf")    -- Handle of the CF motor
    CF_motor1=sim.getObjectHandle("joint2_lh")    -- Handle of the CF motor
    CF_motor2=sim.getObjectHandle("joint2_rf")    -- Handle of the CF motor
    CF_motor3=sim.getObjectHandle("joint2_rh")    -- Handle of the CF motor

    FT_motor0=sim.getObjectHandle("joint3_lf")    -- Handle of the FT motor
    FT_motor1=sim.getObjectHandle("joint3_lh")    -- Handle of the FT motor
    FT_motor2=sim.getObjectHandle("joint3_rf")    -- Handle of the FT motor
    FT_motor3=sim.getObjectHandle("joint3_rh")    -- Handle of the FT motor

    TT_motor0=sim.getObjectHandle("joint4_lf")    -- Handle of the TT motor     -- TODO what to dot with this
    TT_motor1=sim.getObjectHandle("joint4_lh")    -- Handle of the TT motor
    TT_motor2=sim.getObjectHandle("joint4_rf")    -- Handle of the TT motor
    TT_motor3=sim.getObjectHandle("joint4_rh")    -- Handle of the TT motor

    Back_motor1=sim.getObjectHandle("joint_b1")    -- Handle of the backbont motor
    Back_motor2=sim.getObjectHandle("joint_b2")    -- Handle of the backbont motor
    Back_motor3=sim.getObjectHandle("joint_b3")    -- Handle of the backbont motor

    tipHandles = { sim.getObjectHandle("pad_lf"),
                   sim.getObjectHandle("pad_lh"),
                   sim.getObjectHandle("pad_rf"),
                   sim.getObjectHandle("pad_rh")}

    IMU=sim.getObjectHandle("Imu")
    distHandle_leg01=sim.getDistanceHandle("leg01")
    distHandle_leg12=sim.getDistanceHandle("leg23")

    floorHandle = sim.getObjectHandle("floor")
    distHandle_BF   =sim.getDistanceHandle("bodyfloor")
    morfHexapod=sim.getObjectHandle("geckobotiv")

    -- TODO happ, get graph handle
    graph = simGetObjectHandle('graph')

    previousTime=0

    -- Check if the required ROS plugin is loaded
    moduleName=0
    moduleVersion=0
    index=0
    pluginNotFound=true
    while moduleName do
        moduleName,moduleVersion=sim.getModuleName(index)
        if (moduleName=='RosInterface') then
            pluginNotFound=false
        end
        index=index+1
    end
    if (pluginNotFound) then
        sim.displayDialog('Error','The RosInterface was not found.',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
        printToConsole('[ERROR] The RosInterface was not found.')
    end

    -- If found then start the subscribers and publishers
    if (not pluginNotFound) then
        -- Create the subscribers
        MotorSub=simROS.subscribe('/'..'morf_sim'..simulationID..'/multi_joint_command','std_msgs/Float32MultiArray','setMotorPositions_cb')
        -- BackMotorPositionSub=simROS.subscribe('/vrep/gecko/back_joint_pos_command','std_msgs/Float32MultiArray','setBackMotorPositions_cb')
        BackMotorPositionSub=simROS.subscribe('/'..'morf_sim'..simulationID..'/multi_joint_backbone_command','std_msgs/Float32MultiArray','setBackMotorPositions_cb')
        BackMotorTorqueSub=simROS.subscribe('/vrep/gecko/back_joint_torque_command','std_msgs/Float32MultiArray','setBackMotorTorque_cb')

        -- Create the publishers
        simTime_pub = simROS.advertise('/vrep/getTime','std_msgs/Float32')                                          -- simulated time topic [sec]
        simState_pub = simROS.advertise('/vrep/getState','std_msgs/Float32')

        jointPositionsPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_positions','std_msgs/Float32MultiArray')
        jointTorquesPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_torques','std_msgs/Float32MultiArray')
        jointVelocitiesPub=simROS.advertise('/'..'morf_sim'..simulationID..'/joint_velocities','std_msgs/Float32MultiArray')

        backJointPositionsPub=simROS.advertise('/vrep/gecko/back_joint_positions','std_msgs/Float32MultiArray')
        backJointVelocitiesPub=simROS.advertise('/vrep/gecko/back_joint_velocities','std_msgs/Float32MultiArray')
        backJointTorquesPub=simROS.advertise('/'..'morf_sim'..simulationID..'/back_joint_torques','std_msgs/Float32MultiArray')
        
        imuEulerPub=simROS.advertise('/morf_sim'..simulationID..'/euler','geometry_msgs/Vector3')
        testParametersPub=simROS.advertise('/'..'morf_sim'..simulationID..'/testParameters','std_msgs/Float32MultiArray')
    end

    simROS.publish(testParametersPub,{data=testParameters})
    setGravety(9.81,0,false)

    -- WAIT FOR ROS TO START FULLY TO LAUNCH
    printToConsole('[ INFO] Initialized simulation')
end

--[[
Actuation: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_actuation) then
    -- Publish
    -- sim.setFloatSignal("mySimulationTime", sim.getSimulationTime())
    setTartetMotorPositions()
    --checkFootAdheasiveForces(floorHandle,tipHandles,geckoTape_dist,0.00)
end

--[[
Sensing: This part will be executed in each simulation step
--]]
if (sim_call_type==sim.childscriptcall_sensing) then
    -- Publish
    position_array  ={  simGetJointPosition(TC_motor0),simGetJointPosition(CF_motor0),simGetJointPosition(FT_motor0),simGetJointPosition(TT_motor0),
                        simGetJointPosition(TC_motor1),simGetJointPosition(CF_motor1),simGetJointPosition(FT_motor1),simGetJointPosition(TT_motor1),
                        simGetJointPosition(TC_motor2),simGetJointPosition(CF_motor2),simGetJointPosition(FT_motor2),simGetJointPosition(TT_motor2),
                        simGetJointPosition(TC_motor3),simGetJointPosition(CF_motor3),simGetJointPosition(FT_motor3),simGetJointPosition(TT_motor3),
                        }

    velocity_array  ={  simGetJointVelocity(TC_motor0),simGetJointVelocity(CF_motor0),simGetJointVelocity(FT_motor0),simGetJointVelocity(TT_motor0),
                        simGetJointVelocity(TC_motor1),simGetJointVelocity(CF_motor1),simGetJointVelocity(FT_motor1),simGetJointVelocity(TT_motor1),
                        simGetJointVelocity(TC_motor2),simGetJointVelocity(CF_motor2),simGetJointVelocity(FT_motor2),simGetJointVelocity(TT_motor2),
                        simGetJointVelocity(TC_motor3),simGetJointVelocity(CF_motor3),simGetJointVelocity(FT_motor3),simGetJointVelocity(TT_motor3),
                        }

    torque_array    ={  simGetJointForce(TC_motor0),simGetJointForce(CF_motor0),simGetJointForce(FT_motor0),simGetJointForce(TT_motor0),
                        simGetJointForce(TC_motor1),simGetJointForce(CF_motor1),simGetJointForce(FT_motor1),simGetJointForce(TT_motor1),
                        simGetJointForce(TC_motor2),simGetJointForce(CF_motor2),simGetJointForce(FT_motor2),simGetJointForce(TT_motor2),
                        simGetJointForce(TC_motor3),simGetJointForce(CF_motor3),simGetJointForce(FT_motor3),simGetJointForce(TT_motor3),
                        }

    back_position_array = { simGetJointPosition(Back_motor1), simGetJointPosition(Back_motor2), simGetJointPosition(Back_motor3) }
    -- -- back_velocity_array = { sim.getObjectFloatParameter(Back_motor1,2012), sim.getObjectFloatParameter(Back_motor2,2012), sim.getObjectFloatParameter(Back_motor3,2012) }
    back_velocity_array = { simGetJointVelocity(Back_motor1), simGetJointVelocity(Back_motor2), simGetJointVelocity(Back_motor3) }
    back_torque_array = { simGetJointForce(Back_motor1), simGetJointForce(Back_motor2), simGetJointForce(Back_motor3) }
    publishSimTime()
    publishSimState()
    -- publishBackMotorVelocity()  --TODO change happ
    -- publishBackMotorPosition()
    -- **************** --
    -- Fitness feedback --
    -- **************** --
    linearVelocity, aVelocity=sim.getObjectVelocity(IMU) -- m/s
    objectPosition = sim.getObjectPosition(IMU,-1)
    objectOrientation = sim.getObjectOrientation(IMU,-1)

    -- Mean velocity of robot
    mean_vel = absmean(mean_vel, linearVelocity[1], update_count)

    -- Mean power of all joint (leg + body)
    all_jtor_array = torque_array
    for key, value in pairs(back_torque_array) do
        table.insert(all_jtor_array, value)
    end
    
    all_jvel_array = velocity_array
    for key, value in pairs(back_velocity_array) do
        table.insert(all_jvel_array, value)
    end
    

    mean_jtor   = absmean(mean_jtor, absmean_arr(all_jtor_array), update_count)
    mean_jvel   = absmean(mean_jvel, absmean_arr(all_jvel_array), update_count)  
    mean_jpower = absmean(mean_jpower, mean_jtor * mean_jvel, update_count)

    -- Orientation / Stability
    mean_roll   = absmean(mean_roll, objectOrientation[1], update_count)
    mean_tilt   = absmean(mean_tilt, objectOrientation[2], update_count)
    mean_pan    = absmean(mean_pan, objectOrientation[3]-offset_pan, update_count)

    -- Position
    table.insert(height_arr, objectPosition[3])

    -- Distance between legs
    max_detect_interleg     = 0.15       -- 0.1
    max_detect_intraleg     = 0.005
    max_detect_bodyfloor    = 0.03
    collisionState = {max_detect_interleg, max_detect_interleg, max_detect_interleg, max_detect_interleg, max_detect_intraleg, max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_intraleg,max_detect_bodyfloor}

    -- inter leg.
    result, distance_leg01=sim.handleDistance(distHandle_leg01) -- m
    if distance_leg01 == nil then
        collisionState[1]=max_detect_interleg
    else
        collisionState[1] = distance_leg01
    end
    result, distance_leg12=sim.handleDistance(distHandle_leg12) -- m

    if distance_leg12 == nil then
        collisionState[2]=max_detect_interleg
    else
        collisionState[2] = distance_leg12
    end


    -- body floor.
    result, distance_BF=sim.handleDistance(distHandle_BF) -- m
    
    if distance_BF == nil then
        collisionState[11]=max_detect_bodyfloor
    else
        collisionState[8] = distance_BF
    end

    collisionState[1] = 1 - (collisionState[1])   / ( max_detect_interleg ) -- leg01
    collisionState[2] = 1 - (collisionState[2])   / ( max_detect_interleg ) -- leg12
    -- collisionState[3] = 1 - (collisionState[3])   / ( max_detect_interleg ) -- leg34
    -- collisionState[4] = 1 - (collisionState[4])   / ( max_detect_interleg ) -- leg45
    -- collisionState[5] = 1 - (collisionState[5])   / ( max_detect_intraleg ) -- leg0s
    -- collisionState[6] = 1 - (collisionState[6])   / ( max_detect_intraleg ) -- leg1s
    -- collisionState[7] = 1 - (collisionState[7])   / ( max_detect_intraleg ) -- leg2s
    collisionState[8] = 1 - (collisionState[8])   / ( max_detect_intraleg ) -- leg3s
    -- collisionState[9] = 1 - (collisionState[9])   / ( max_detect_intraleg ) -- leg4s
    -- collisionState[10] = 1 - (collisionState[10]) / ( max_detect_intraleg ) -- leg5s
    collisionState[11] = 1 - (collisionState[11]) / ( max_detect_bodyfloor) -- bodyfloor

    --print(collisionState)
    max_dist = math.max(unpack(collisionState))
    if max_dist > 1 then
        print("[ ERROR]: Please set max dist correctly")
    end

    if max_dist > collisions_max then
        collisions_max = max_dist
    end

    positionRobot=sim.getObjectPosition(morfHexapod, -1)
    distance = -positionRobot[2] -- use negative world y axis

    slip_results = {}
    for i = 1, table.getn(tipHandles), 1 do
        --table.insert(slip_results, slip_detector( tipHandles[i], 0.025, 0.025))
        table.insert(slip_results, slip_detector_dist(floorHandle, tipHandles[i], 0.025, 0.01))
    end

    -- max_slip = math.max(unpack(slip_results))
    summedSlip = sumArray(slip_results)             -- sum instead of 

    if max_slip ~= -1 then
        --mean_slip    = absmean(mean_slip, max_slip, update_count)
        mean_slip    = absmean(mean_slip, summedSlip, update_count)
    end

    -- set gravety when robot have adheared to surface
    if simGetSimulationTime() > 0.5 and not gravityChanged then
        gravityChanged = true
        setGravety(9.81,slopeDeg,true)
    end
    
    -- Remove transient period
    if simGetSimulationTime() < 1.3 then
        mean_slip=0
        mean_tilt=0
        mean_roll=0
        mean_pan =0
        collisions_max=0
        mean_height=0
        offset_pan = objectOrientation[3]
        height_arr = {0}
    end

    if simGetSimulationTime() < 1 and boolswitch then
        boolswitch = false
        -- Release the robot
        simSetObjectInt32Parameter(morfHexapod, sim_shapeintparam_static, 0)
    end

    testParameters[2]  = mean_slip -- x orientation
    testParameters[3]  = mean_tilt -- x orientation
    testParameters[4]  = mean_roll -- y orientation
    testParameters[5]  = mean_pan -- Heading = z orientation
    testParameters[6]  = collisions_max
    testParameters[7]  = 0.0 -- was body floor collisions (now included in collisions_max)
    testParameters[8]  = mean_jpower
    testParameters[9]  = mean_vel
    testParameters[10] = distance
    testParameters[11] = standardDeviation(height_arr)

    update_count = update_count + 1

    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(jointVelocitiesPub,{data=velocity_array})
    simROS.publish(jointTorquesPub,{data=torque_array})
    simROS.publish(testParametersPub,{data=testParameters})

    simROS.publish(backJointPositionsPub,{data=back_position_array})
    simROS.publish(backJointVelocitiesPub,{data=back_velocity_array})
    simROS.publish(backJointTorquesPub,{data=back_torque_array})
    checkFootAdheasiveForces(floorHandle,tipHandles,geckoTape_dist,ADHESIVE_FORCE)

    -- TODO happ 
    simSetGraphUserData(graph,"joint_b1",bodySignal[1])
    simSetGraphUserData(graph,"joint_b2",bodySignal[2])
    simSetGraphUserData(graph,"joint_b3",bodySignal[3])
end

--[[
Clean up: This part will be executed one time just before a simulation ends
--]]
if (sim_call_type==sim.childscriptcall_cleanup) then

    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(jointVelocitiesPub,{data=velocity_array})
    simROS.publish(jointTorquesPub,{data=torque_array})
    simROS.publish(testParametersPub,{data=testParameters})

    print("+====Objectives====+")
    print("Avg tilt:\t"    .. roundToNthDecimal(mean_tilt,4))
    print("Avg roll:\t"    .. roundToNthDecimal(mean_roll,4))
    print("Avg heading:\t" .. roundToNthDecimal(mean_pan,4))
    print("Avg Height:\t"  .. roundToNthDecimal(mean_height, 4))
    print("Avg power:\t"   .. roundToNthDecimal(mean_jpower,4))
    print("Robot Coll.:\t" .. roundToNthDecimal(collisions_max,5))
    print("Slipping:\t"    .. roundToNthDecimal(mean_slip, 4))
    print("Distance:\t"    .. roundToNthDecimal(distance,4))
    print("+================+")

    -- Set object static Controller
    simSetObjectInt32Parameter(morfHexapod, sim_shapeintparam_static, 1)

    -- Terminate remaining local notes
    simROS.shutdownSubscriber(MotorSub)
    simROS.shutdownSubscriber(BackMotorPositionSub)
    simROS.shutdownSubscriber(BackMotorTorqueSub)

    simROS.shutdownPublisher(jointTorquesPub)
    simROS.shutdownPublisher(jointVelocitiesPub)
    simROS.shutdownPublisher(jointPositionsPub)
    simROS.shutdownPublisher(testParametersPub)
    simROS.shutdownPublisher(imuEulerPub)
    simROS.shutdownPublisher(backJointPositionsPub)
    simROS.shutdownPublisher(backJointVelocitiesPub)
    simROS.shutdownPublisher(backJointTorquesPub)

    printToConsole('[ INFO] Lua child script stopped')
end

# Generic neural locomotion control framework

__The framework is based on neural control and black-box optimization. The neural control combines a central pattern generator (CPG) and a radial basis function (RBF) network into a CPG-RBF network. The control network acts as a neural basis that can produce arbitrary rhythmic trajectories for the joints of robots.__

- [Install](#install)
- [Getting Started](#getting-started)

## Install

First, we need to set up v-rep workers. Typically the workers are installed in your home directory - but it can be anywhere.
1. Download the latest version of V-REP (aka CoppeliaSim) [from the downloads page](http://www.coppeliarobotics.com/downloads.html)
2. Extract the downloaded .zip file in the V-REP directory as many times as you need workers (e.g., four times)
3. Go to the extracted directories - these should be called `VREP1`, `VREP2`, `VREP3`, `VREP4`, etc.
4. In `remoteApiConnections.txt` change `portIndex1_port` so that VREP1 has 19997, VREP2 has 19996, VREP3 has 19995, VREP4 has 19994, and VREP**n** has 19997-**n**
5. copy and (re)place `libv_repExtRosInterface.so` in `/utils/v-rep_simulations/v-rep_libs/reallib/libv_repExtRosInterface.so`​in all `VREP#` directories.

Now we need to install the required python libraries:

```bash
sudo apt install python3-pip
pip3 install -r requirements.txt
```

The neural controllers use ROS to communicate with v-rep. So make sure that you have `ros-xxx-desktop-full` installed ([Install ROS](http://wiki.ros.org/ROS/Installation))

You should be good to go!

#### Running Headless

If you plan to run on a headless machine, you will also need to run with a virtual framebuffer. E.g.

```bash
sudo apt-get install xvfb
xvfb-run python3 my_pyrep_app.py
```

#### Troubleshooting

Below are some problems you may encounter during installation. If none of these solves your problem, please raise an issue.
- error: command `x86_64-linux-gnu-gcc` failed
  - You may be missing packages needed for building python extensions. Try: `sudo apt-get install python3-dev`, and then re-run the installation.

## Getting Started

1. First, take a look at the V-REP [tutorials](http://www.coppeliarobotics.com/helpFiles/en/tutorials.htm).
2. Start your workers (in this example we will use one worker)
```bash
cd {VREP_WORKER_ROOT}/VREP1/
./vrep.sh FRAMWORK_PATH/CPGRBFN_compact/simulations/RL_LAIKAGO.ttt
```
or
```bash
./coppeliaSim.sh FRAMWORK_PATH/CPGRBFN_compact/simulations/RL_LAIKAGO.ttt
```
3. Then build the controller for Laikago
```bash
cd FRAMWORK_PATH/CPGRBFN_compact/interfaces/laikago/sim/build
rm CMakeCache.txt
cmake .
make
```
4. Go to the machine learning directory
```bash
cd FRAMWORK_PATH/CPGRBFN_compact/machine_learning
```
4. In `$FRAMWORK_PATH/CPGRBFN_compact/machine_learning/RL_master.py` change the number of workers to 1
5. Then run
```bash
./RL_repeater.sh -t 1 -e indirect -r LAIKAGO
```
6. The program will now be running laikago (​-r LAIKAGO)​ for 500 iterations one time (​-t 1) using an indirect (​-e indirect) encoding. Every 5th iteration will be shown visually. All others will be blacked out for performance boost.

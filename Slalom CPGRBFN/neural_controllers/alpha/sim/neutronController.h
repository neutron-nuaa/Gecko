//
// Created by mat on 12/30/17.
//

#ifndef NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
#define NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <string>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "simRosClass.h"
#include "modularController.h"
#include "neutronMotorDefinition.h"
#include "delayline.h"
#include "dualIntegralLearner.h"
#include "postProcessing.h"
#include "rbfcpg.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "environment.h"

class dualIntegralLearner;
class simRosClass;
class postProcessing;
class rbfcpg;

class neutronController {
public:
    neutronController(int argc,char* argv[]);
    ~neutronController();
    int runController();

private:
    vector<float> readParameterSet();
    double rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter);
    void tripodGaitRangeOfMotion(vector<float> policyWeights, string encoding);
    void tripodGaitRBFN();
    void fitnessLogger();
    void logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                  double positionX, double bodyVel, double angularVelocity, double jointTorque,
                  double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL);

    bool CPGLearning    = false;
    bool simulation     = true;
    bool blackOut       = false;
    int rollout         = -1;
    int simulationID    = -1;
    int CPGPeriod       = 0;
    int tau             = 300;
    int simulationTime  = 6;
    string   encoding;
    ofstream myFile;

    // If TRUE then disable rosinterfacehelper
    bool useAPItrigger  = true;

    int skipTrigger = 3;

    vector<Delayline>   delayline;
    vector<float>       positions;
    vector<float>       data;
    vector<float>       policyWeights;

    rbfcpg * CPGAdaptive;
    environment * env;
    simRosClass * rosHandle;
    modularController * CPGAdaptive_cpg;
    postProcessing * CPGPeriodPostprocessor;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

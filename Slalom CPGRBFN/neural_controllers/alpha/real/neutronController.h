//
// Created by mat on 12/30/17.
//

#ifndef NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
#define NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <math.h>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "realRosClass.h"
#include "modularController.h"
#include "neutronMotorDefinition.h"
#include "delayline.h"
#include "dualIntegralLearner.h"
#include "postProcessing.h"
#include "joystick.h"

class dualIntegralLearner;
class modularController;
class simRosClass;
class postProcessing;

class neutronController {
public:
    neutronController(int argc,char* argv[]);
    bool runController();
private:
    double rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter);
    void tripodGait();
    void anotherGait();

    bool initPos();
    bool standHigh();
    bool pokeMotion();
    int pokeState = 0;
    double BC_pos_state = 0;
    double CF_pos_state = 2;
    double FT_pos_state = -0.17;

    double BC_pos_goal_state = 0;
    double CF_pos_goal_state = 2;
    double FT_pos_goal_state = -0.17;

    int doMotion = 0;
    double heightBias = 0;
    double heightBiasStepSize = 0.02;
    double newHeight = 0;
    bool heightInc = false;
    bool heightDec = false;
    bool phiInc = false;
    bool phiDec = false;

    bool changePosition(double stepSize);
    bool actuateRobot(bool controlSingleLeg);

    void logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                  double positionX, double bodyVel, double angularVelocity, double jointTorque,
                  double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL);

    ofstream myfile;

    double pp1 = 200;
    double pp2 = 400;
    double currentPP1 = 1.60; // 2.60 blue // Green 1.60 // Hex 1.60
    double currentPP2 = 0.85; // 1.60 blue // Green 0.85 // Hex 0.85

    vector<float> positions;
    std::vector<float> data;
    vector<Delayline> tauLeftDelayLine;
    vector<Delayline> tauDelayLine;

    int tau=250;       // Tripod 18/45
    int tauLeft=250;   // Wave
    int tmpTau=0;
    bool positive = true;
    bool paintDirection = false;
    double verticalArmHight = 0;
    bool legged_or_arm = true;
    double waiter = 200; // 5 SECONDS
    double triggerCount = 0;
    double CPGamp = 0.0;
    float hfOffset = 0.0;//0.4;
    float MotorOutput4Rescaled=0;
    float MotorOutput4Neg = 0;
    double joySpeed = 0;
    double joyTurn = 0;
    bool useJoy = true;
    bool simulation = true;
    double phiParam = 0.065;//-0.003;//-0.005;

    /* 0. Vanilla CPG SO2
     * 1. Dual Learner
     * 2. Adaptive frequency
     * 3. DL + AF -> NN (Hard Limiter and SigmoidII) */

    int CPGmethod = 0;
    double errorMargin = 0.000;
    double weightBiasHL = 0.004;
    double weightBiasHLTwo = 0.0015;

    dualIntegralLearner * learner;
    modularController * CPGAdaptive;
    realRosClass * rosHandle;
    postProcessing * sensorPostprocessor;
    postProcessing * controllerPostprocessor;
    postProcessing * sensorAmpPostprocessor;
    postProcessing * controllerAmpPostprocessor;
    postProcessing * CPGAmpPostprocessor;
    postProcessing * LPFPertu;
    Joystick * joystick;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

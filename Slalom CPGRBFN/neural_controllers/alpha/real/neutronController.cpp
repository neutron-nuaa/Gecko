//
// Created by mat on 12/30/17.
//

#include "neutronController.h"

neutronController::neutronController(int argc,char* argv[]) {

    if(useJoy){
        joystick = new Joystick;
        if (!joystick->isFound())
            printf("Joystick open failed.\n");
    }


    rosHandle = new realRosClass(argc, argv);
    simulation = false;

    CPGAdaptive = new modularController(2,true);

    learner = new dualIntegralLearner(CPGmethod != 3, 20);

    sensorPostprocessor = new postProcessing();
    sensorPostprocessor->setBeta(0.25);

    controllerPostprocessor = new postProcessing();
    controllerPostprocessor->setBeta(0.25);

    sensorAmpPostprocessor = new postProcessing();
    sensorAmpPostprocessor->setBeta(0.04);

    controllerAmpPostprocessor = new postProcessing();
    controllerAmpPostprocessor->setBeta(0.04);

    CPGAmpPostprocessor = new postProcessing();
    CPGAmpPostprocessor->setBeta(0.5);

    positions.resize(22);

    for (int i = 0; i < 12; ++i) {
        if(i == 2 || i == 3) tmpTau=tau+1; else tmpTau=tau*2+1;
        Delayline tmp(tmpTau);
        tauDelayLine.push_back(tmp);
    }

    for (int k = 0; k < 4; ++k) {
        Delayline tmp(tauLeft+1);
        tauLeftDelayLine.push_back(tmp);
    }

    myfile.open ("../V-REP_DATA/log_file.dat");
    myfile << "time\tcpgPhi\terror\tmaxVel\tmaxTorque\tpositionX\tbodyVel\tangularVelocity\tjointTorque\tcontrollerOut\tsystemFeedback\thlNeuronAF\thlNeutronDL" << "\n";

    myfile.close();
    myfile.open ("/home/morf-one/data/log_file.dat", std::ios_base::app);

    if(ros::ok()) {
        if(simulation) {
            rosHandle->synchronousSimulation(static_cast<unsigned char>(true));
            rosHandle->rosSpinOnce();
            rosHandle->triggerSimNoWait();
            rosHandle->triggerSimNoWait();
        }

        doMotion = 99;
    }

    rosHandle->setLed(40, 0, 0);
}

bool neutronController::runController() {
    double amplitudeSensor, amplitudeController;
    double LPFSensorAmp, LPFControllerAmp;
    double error;
    double true_error;

    if(ros::ok()) {
        if(simulation)
            rosHandle->triggerSim();

        // Post Processing
        CPGamp = CPGAmpPostprocessor->calculateLPFAmplitude(CPGAdaptive->getCpgOutput(1));
        amplitudeSensor = sensorPostprocessor->calculateLPFAmplitude(rosHandle->jointPositions[BC4]);
        amplitudeController = controllerPostprocessor->calculateLPFAmplitude(positions.at(BC4));
        LPFSensorAmp = sensorAmpPostprocessor->calculateLPFSignal(amplitudeSensor);
        LPFControllerAmp = controllerAmpPostprocessor->calculateLPFSignal(amplitudeController);
        error = (LPFControllerAmp-errorMargin) - LPFSensorAmp;
        true_error = LPFControllerAmp - LPFSensorAmp;

//        if(error < 0.001 && error > 0)
//            error = 0;

        if(waiter >= 0) {
            error = 0;
            waiter--;
        } else
            waiter = -10;


        if(useJoy)
        {
            // AXES
            joySpeed = rosHandle->axes[1];
            joyTurn  = rosHandle->axes[2];

            // BUTTONS
            if(rosHandle->buttons[0] == 1){
                doMotion = 1;
            }

            if(rosHandle->buttons[1] == 1){
                doMotion = 2;
            }

            if(rosHandle->buttons[4] == 1){
                if(!phiDec)
                    phiInc = true;
            } else {
                phiInc = false;
            }

            if(rosHandle->buttons[3] == 1){
                if(!phiInc)
                    phiDec = true;
            } else {
                phiDec = false;
            }

            if(rosHandle->buttons[6] == 1){
                if(!heightInc)
                    heightDec = true;
            } else {
                heightDec = false;
            }

            if(rosHandle->buttons[7] == 1){
                if(!heightDec)
                    heightInc = true;
            } else {
                heightInc = false;
            }

            if(rosHandle->buttons[11] == 1){
                heightBias = 0;
            }

            if(rosHandle->buttons[14] == 1){
                phiParam = 0.14;
            }

            // If bottom down
            if(heightBias < 150 && heightInc)
                heightBias += 1;

            if(heightBias > 0 && heightDec)
                heightBias -= 1;

            if(phiParam < 0.5 && phiInc)
                phiParam += 0.0025;

            if(phiParam > 0.1 && phiDec)
                phiParam -= 0.0025;
        }

        // New Phi for robot
        double newPhi = 0;
        if(useJoy){
                newPhi = phiParam*(joySpeed);
        }
        else
            newPhi = phiParam;


        // Setup and calculate Neurons Output
        double bias = 1;

        // Sigmoid
        //double leftNeuronOutputSIGTwo = 1 / (1 + exp(-(-4000*error + 6*bias)));
        double leftNeuronOutputSIGTwo = (error > (0+weightBiasHLTwo*bias) ? 0 : 1);

        // Hard limiter
        //weightBiasHL = 0.01;
        int leftNeuronOutputHL = (error > (0+weightBiasHL*bias) ? 0 : 1);
        int rightNeuronOutputHL = (error > (0+weightBiasHL*bias) ? 1 : 0);

        // Calculate Perturbation
        float perturbation = 0;
        if(waiter < 0)
		    perturbation = rescale(0.55, -0.55, 0.20, -0.20, -rosHandle->jointPositions[BC4]);

        if (CPGmethod == 0){
            /****************************************
            * VANILLA SO2 CPG
            * Open-Loop - Does not adapt in any way.
            ****************************************/
            CPGAdaptive->setPhii(newPhi);

        } else if(CPGmethod == 1) {
            /****************************************
            * DL CPG (Dual Learner)
            * Adapts the Phi to one where it can run.
            ****************************************/
            if(waiter < 0) {
                learner->step(error, phiParam);
                CPGAdaptive->setPhii(learner->getControlOutput());
            }

        } else if (CPGmethod == 2) {
            /****************************************
            * AF CPG (frequency adaptive cpg)
            * Adapts to eigenfrequency of the system.
            ****************************************/
            if(waiter < 0) {
                learner->step(error, CPGAdaptive->getPhi());
                CPGAdaptive->setPerturbation(perturbation);
            }

        } else if (CPGmethod == 3) {
            /****************************************
             * DL + AF CPG
             * Shiftes from AF to DL when error > 0.1
             * Uses: Shunting, Sigmoid, & hardlimiter
             ****************************************/
            if(waiter < 0) {
                CPGAdaptive->setPerturbation(perturbation * leftNeuronOutputSIGTwo);

                learner->step(error * rightNeuronOutputHL, CPGAdaptive->getPhi());

                if (error * rightNeuronOutputHL != 0) // assumption 2
                    CPGAdaptive->setPhii(learner->getControlOutput());
            }

        }
        /****************************************
         * End End End End End End End End End End
         ****************************************/

        // Log Data
        // TODO

        // Take step with CPGs
        if(joySpeed != 0 && useJoy)
            CPGAdaptive->step();
        else if(!useJoy)
            CPGAdaptive->step();

        // Plot's
        data.clear();
        // RED
        data.push_back(rosHandle->jointTorques[CF0]);
        // GREEN
        data.push_back(rosHandle->jointTorques[BC0]);
        // YELLOW
        data.push_back(0);
        // BLUE
        data.push_back(rosHandle->jointTorques[FT0]);
        // PINK
        data.push_back(0);

        switch(doMotion) {
            case 0: // Walk
                tripodGait();
                break;
            case 1: // Poke
                if (pokeMotion())
                    doMotion = 0;
                break;
            case 2: // Stand High
                if (standHigh())
                    doMotion = 0;
                break;
            case 99: // Init position
//                if (initPos()) // TODO: will be implemented
                    doMotion = 0;
                break;
            default:
                break;
        }

    } else
    {
        cout << "Shutting Down out of da loop" << endl;
        return false;
    }

    rosHandle->rosSpinOnce();
    if(simulation) {
        if ((rosHandle->simState != 1 && waiter < 180) || rosHandle->terminateSimulation) {
            cout << "Shutting Down in da loop" << endl;
            myfile.close();
            ros::shutdown();
            delete rosHandle;
            delete CPGAdaptive;
            delete learner;
            delete sensorPostprocessor;
            delete controllerPostprocessor;
            delete sensorAmpPostprocessor;
            delete controllerAmpPostprocessor;
            delete CPGAmpPostprocessor;
            return !rosHandle->terminateSimulation;
        } else {
            return !rosHandle->terminateSimulation;
        }
    } else
        return true;
}

void neutronController::logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                                 double positionX, double bodyVel, double angularVelocity, double jointTorque,
                                 double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL)
{
    myfile << simtime <<"\t"<< CPGphi <<"\t"<< error <<"\t"<< maxVel <<"\t"<< maxForce <<"\t"<< positionX <<"\t" << bodyVel << "\t" << angularVelocity << "\t" << jointTorque << "\t" << controllerOut << "\t" << systemFeedback << "\t" << hlNeuronAF << "\t" << hlNeutronDL << "\n";
}

double neutronController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}

void neutronController::tripodGait() {

    tau=4;

    // Height Controller
    if(abs(abs(newHeight) - abs(heightBias * heightBiasStepSize)) > heightBiasStepSize && heightBias == 0) {
        newHeight += -1 * heightBiasStepSize;
    } else {
        newHeight = heightBias * heightBiasStepSize;
    }


    // Get network outputs
    float maxNetworkOut = 0.2;
    double BC_pos;
    double CF_pos;
    double FT_pos;

    // BC
    BC_pos = 0.3;// - (newHeight*0.1); // Worst case = 0.17, STD case = 0.2
    float MotorOutputBC=rescale(maxNetworkOut,-maxNetworkOut,BC_pos,-BC_pos, CPGAdaptive->getCpgOutput(1));
    float MotorOutputBCNeg=rescale(maxNetworkOut,-maxNetworkOut,BC_pos,-BC_pos, -CPGAdaptive->getCpgOutput(1));

    // CF
    CF_pos = 1.9;// - newHeight; // Worst case = -0.1, STD case = 0.7
    float MotorOutputCF=rescale(maxNetworkOut,-maxNetworkOut, CF_pos+0.7, CF_pos, CPGAdaptive->getCpgOutput(0));
    float MotorOutputCFNeg=rescale(maxNetworkOut,-maxNetworkOut, CF_pos+0.7, CF_pos, -CPGAdaptive->getCpgOutput(0));

    if (MotorOutputCF < CF_pos+0.275)
        MotorOutputCF = CF_pos+0.275;

    if (MotorOutputCFNeg < CF_pos+0.275)
        MotorOutputCFNeg = CF_pos+0.275;

    // FT
    FT_pos = 0.1;// - (newHeight*0.9); // Worst case = -0.4, STD case = -1.7
    float MotorOutputFT=rescale(maxNetworkOut,0, FT_pos+0.3, FT_pos, abs(CPGAdaptive->getCpgOutput(0)));
    float MotorOutputFTNeg=rescale(maxNetworkOut,0, FT_pos+0.3, FT_pos, abs(-CPGAdaptive->getCpgOutput(0)));

    double turnFactorLEFT = 1;
    double turnFactorRIGHT = 1;
    double hfOffsetSignLEFT = 1;
    double hfOffsetSignRIGHT = 1;

    if(useJoy) {
        if (joyTurn == 0) {
            turnFactorLEFT = 1;
            turnFactorRIGHT = 1;
        } else if (joyTurn > 0) {
            turnFactorLEFT = rescale(1, 0, -1, 1, fabs(joyTurn));
            turnFactorRIGHT = 1;
        } else {
            turnFactorLEFT = 1;
            turnFactorRIGHT = rescale(1, 0, -1, 1, fabs(joyTurn));
        }
    }


    // FOR STAND EXPERIMENT
    double BC11 = 0;
    double BC22 = 0;
    double CF11 = 2.059;
    double CF22 = 0.8;
    double FT11 = -0.17;
    double FT22 = -2.35;

    positions.at(BC0) = (MotorOutputBC    - (hfOffset*hfOffsetSignLEFT))  * turnFactorLEFT;
    positions.at(BC1) = (MotorOutputBCNeg)  * turnFactorLEFT;
    positions.at(BC2) = (MotorOutputBC    + (hfOffset*hfOffsetSignLEFT))  * turnFactorLEFT;
    positions.at(BC3) = (MotorOutputBCNeg  - (hfOffset*hfOffsetSignRIGHT)) * turnFactorRIGHT;
    positions.at(BC4) = (MotorOutputBC)   * turnFactorRIGHT;
    positions.at(BC5) = (MotorOutputBCNeg   + (hfOffset*hfOffsetSignRIGHT)) * turnFactorRIGHT;

    positions.at(CF0) = MotorOutputCF;
    positions.at(CF1) = MotorOutputCFNeg;
    positions.at(CF2) = MotorOutputCF;
    positions.at(CF5) = MotorOutputCFNeg;
    positions.at(CF4) = MotorOutputCF;
    positions.at(CF3) = MotorOutputCFNeg;

    if(true) {
        positions.at(FT0) = FT_pos;
        positions.at(FT1) = FT_pos;
        positions.at(FT2) = FT_pos;
        positions.at(FT5) = FT_pos;
        positions.at(FT4) = FT_pos;
        positions.at(FT3) = FT_pos;
    } else {
        positions.at(FT0) = MotorOutputFT;
        positions.at(FT1) = MotorOutputFTNeg;
        positions.at(FT2) = MotorOutputFT;
        positions.at(FT5) = MotorOutputFTNeg;
        positions.at(FT4) = MotorOutputFT;
        positions.at(FT3) = MotorOutputFTNeg;
    }

    rosHandle->setLegMotorPosition(positions);

    if(simulation)
        rosHandle->plot(data);
}

bool neutronController::pokeMotion() {

    bool controlSingleLeg = true;
    double stepSize = 0.05;

    switch(pokeState) {
        case 0: // Start Pos
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.8;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize))
                pokeState = 1;

            break;
        case 1: // lift leg back
            controlSingleLeg = true;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 2.3;
            FT_pos_goal_state = -0.6;

            if(changePosition(stepSize))
                pokeState = 2;

            break;
        case 2: // Turn BC
            controlSingleLeg = true;
            BC_pos_goal_state = -3.14/2;
            CF_pos_goal_state = 2.3;
            FT_pos_goal_state = -0.6;

            if(changePosition(stepSize))
                pokeState = 3;

            break;
        case 3: // Lift FT
            controlSingleLeg = true;
            BC_pos_goal_state = -3.14/2;
            CF_pos_goal_state = 2.3;
            FT_pos_goal_state = -1.2;

            if(changePosition(stepSize))
                pokeState = 4;

            break;
        case 4: // Lower CF and Lift FT
            controlSingleLeg = true;
            BC_pos_goal_state = -3.14/2;
            CF_pos_goal_state = 1.4;
            FT_pos_goal_state = -1.8;

            if(changePosition(stepSize))
                pokeState = 5;

            break;
        case 5:
            controlSingleLeg = true;
            BC_pos_goal_state = -3.14/2;
            CF_pos_goal_state = 2.3;
            FT_pos_goal_state = -0.6;

            if(changePosition(stepSize))
                pokeState = 6;

            break;
        case 6:
            controlSingleLeg = true;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 2.3;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize))
                pokeState = 7;

            break;
        case 7:
            controlSingleLeg = true;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.8;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize)){
                pokeState = 0;
                return true;
            }


            break;
        default:
            break;
    }

    actuateRobot(controlSingleLeg);

    return false;
}

bool neutronController::standHigh() {

    bool controlSingleLeg = true;
    double stepSize = 0.02;

    switch(pokeState) {
        case 0: // Start Pos
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.8;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize))
                pokeState = 1;
            break;
        case 1: // Start Pos
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = -0.0;
            FT_pos_goal_state = -1.97;

            if(changePosition(stepSize))
                pokeState = 2;
            break;
        case 2: // Start Pos
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.0;
            FT_pos_goal_state = -1.0;

            if(changePosition(stepSize))
                pokeState = 3;
            break;
        case 3:
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.8;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize)){
                pokeState = 0;
                return true;
            }
            break;
        default:
            break;
    }

    actuateRobot(controlSingleLeg);

    return false;
}

bool neutronController::initPos() {

    bool controlSingleLeg = true;
    double stepSize = 0.06;

    switch(pokeState) {
        case 0: // Start Pos
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 2.075;
            FT_pos_goal_state = -2.075;

            if(changePosition(stepSize))
                pokeState = 1;
            break;
        case 1: //
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 2.075;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize))
                pokeState = 2;
            break;
        case 2:
            controlSingleLeg = false;
            BC_pos_goal_state = 0;
            CF_pos_goal_state = 1.8;
            FT_pos_goal_state = -0.17;

            if(changePosition(stepSize)){
                pokeState = 0;
                return true;
            }
            break;
        default:
            break;
    }

    actuateRobot(controlSingleLeg);

    return false;
}

bool neutronController::actuateRobot(bool controlSingleLeg) {
    if(!controlSingleLeg) {
        positions.at(BC0) = BC_pos_state;
        positions.at(BC1) = BC_pos_state;
        positions.at(BC2) = BC_pos_state;
        positions.at(BC3) = BC_pos_state;
        positions.at(BC4) = BC_pos_state;
        positions.at(BC5) = BC_pos_state;

        positions.at(CF0) = CF_pos_state;
        positions.at(CF1) = CF_pos_state;
        positions.at(CF2) = CF_pos_state;
        positions.at(CF5) = CF_pos_state;
        positions.at(CF4) = CF_pos_state;
        positions.at(CF3) = CF_pos_state;

        positions.at(FT0) = FT_pos_state;
        positions.at(FT1) = FT_pos_state;
        positions.at(FT2) = FT_pos_state;
        positions.at(FT5) = FT_pos_state;
        positions.at(FT4) = FT_pos_state;
        positions.at(FT3) = FT_pos_state;

    } else
    {
        positions.at(BC0) = BC_pos_state;
        positions.at(CF0) = CF_pos_state;
        positions.at(FT0) = FT_pos_state;

    }


    // Set joint positions
    rosHandle->setLegMotorPosition(positions);
    if(simulation)
        rosHandle->plot(data);
}

bool neutronController::changePosition(double stepSize){
    double BC_DIR = 1;
    double CF_DIR = 1;
    double FT_DIR = 1;

    bool BC_inPos = false;
    bool CF_inPos = false;
    bool FT_inPos = false;

    if(fabs(BC_pos_state - BC_pos_goal_state) >= stepSize){

        if(rosHandle->jointPositions[BC0] > BC_pos_goal_state)
            BC_DIR = -1;
        else
            BC_DIR = 1;

        BC_pos_state += (BC_DIR*stepSize);
    } else
        BC_inPos = true;

    if(fabs(CF_pos_state - CF_pos_goal_state) >= stepSize){

        if(rosHandle->jointPositions[CF0] > CF_pos_goal_state)
            CF_DIR = -1;
        else
            CF_DIR = 1;

        CF_pos_state += (CF_DIR*stepSize);
    } else
        CF_inPos = true;

    if(fabs(FT_pos_state - FT_pos_goal_state) >= stepSize){

        if(rosHandle->jointPositions[FT0] > FT_pos_goal_state)
            FT_DIR = -1;
        else
            FT_DIR = 1;

        FT_pos_state += (FT_DIR*stepSize);
    } else
        FT_inPos = true;

    return BC_inPos && CF_inPos && FT_inPos;
}

//
// Created by mat on 12/21/18.
//

#ifndef MORF_CONTROLLER_RBFN_H
#define MORF_CONTROLLER_RBFN_H

#include "ann-framework/ann.h"
#include <map>
#include <queue>
#include <iostream>
#include <fstream>
#include <string.h>

class rbfn {
public:
    rbfn(int _numKernels, vector<float>, string);
    int getNumKernels();

    void setBeta(double _beta);
    void setWeights(vector<float> _weights);
    void setCenters(vector<float> _centers1, vector<float> _centers2);

    double getBeta();
    vector<float> getWeights();
    vector<float> setCenters(int center);


    vector<double> step(double input1, double input2);
    void calculateCenters(int period, vector<float> signal1, vector<float> signal2);

private:
    int            numKernels = 0;
    double         beta = 0;           // controls spread of the kernel todo make unique for each and optimize
    vector<float>  weights;
    vector<float>  centers1;
    vector<float>  centers2;
    string         encoding;

    // works for period = 97 and phi = 0.065 (alpha = 1.01).
    vector<float> weightsMatlab{0.0039, -0.6822, 1.0251, 0.3402, -1.0771, -0.3636, 0.4376, -1.6826, 0.1292, 0.4925,
                                 -0.4461, -0.1408, 1.6980, -0.5161, 0.3670, 0.9991, -0.2320 -1.1153, 0.7668, -0.0141};
    vector<float> centers1Matlab{-0.2012, -0.1877, -0.1566, -0.1108, -0.0537, 0.0103, 0.0751, 0.1326, 0.1747, 0.1964,
                                  0.1946, 0.1721, 0.1335, 0.0819, 0.0211, -0.0437, -0.1055, -0.1557, -0.1876, -0.1983};
    vector<float> centers2Matlab{0, 0.0656, 0.1251, 0.1703, 0.1953, 0.1990, 0.1828, 0.1495, 0.1020, 0.0438,
                                  -0.0336, -0.0965, -0.1492, -0.1843, -0.1984, -0.1917, -0.1667, -0.1259, -0.0728, -0.0110};

    vector<float> pythoncenter1{-0.19629085003193042, -0.1803803684879207, -0.14660317556854233, -0.10342686649460328, -0.05045993187377645, 0.009867986047237433, 0.07933262089149233, 0.13614160740066933, 0.17667538332900853, 0.1958888190969759, 0.19392284178828212, 0.17482572742092775, 0.14205531730491439, 0.09769582087460685, 0.037948756984479394, -0.023465661501728942, -0.08638426444212258, -0.1417641384352168, -0.18259972517648682, -0.19723613140844487};
    vector<float> pythoncenter2{0.006249717362699904, 0.06958848153189617, 0.1333249247426486, 0.1750117181527206, 0.19545386904493586, 0.19559877528395272, 0.17637448409300324, 0.1443176035800932, 0.10055922271712639, 0.04712398182355224, -0.01981034902610384, -0.0828615888894351, -0.13896935174419528, -0.17833782267638065, -0.19696482127064058, -0.19320885886767847, -0.17326482545456084, -0.13977641124452636, -0.08976289666171561, -0.03454561200524972};

    template<typename T> std::vector<float> linspace(T start_in, T end_in, int num_in);

};


#endif //MORF_CONTROLLER_RBFN_H

//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 7/10/2019
// version: 1.0.0
#include "Data_logger.h"

Data_logger::Data_logger()
{
  file_dir = "/home/happy/CPGRBFN_slalom/data/data_logger/";

  // // Open output wrl file
  // ofstream myFile;
  // myFile.open(file_dir+file_name, ios::out);
  // if(!myFile)
  // {
  //   cerr << "Cannot open this file for output.\n";
		// exit (-1);
  // }

  // // myFile << setw(10) << "c1" << setw(10) << "c2" << setw(10) << "pc1" << setw(10) << "pc2\n";

  // // close the output file
  // myFile.close ();
}

Data_logger::~Data_logger()
{
  file_dir = "";
}

float Data_logger::cpg_pcpg_logs(string file_name, float c1, float c2, float d1, float d2, float d3, float d4, float d5, float d6)
{

  ofstream cpgFile;
  cpgFile.open(file_dir+file_name, ios::app);
  if(!cpgFile)
  {
    cerr << "Cannot open this file for output.\n";
  	exit (-1);
  }

  cpgFile << c1 << " " << c2 << " " << d1 << " " << d2 << " " << d3 << " " << d4 << " " << d5 << " " << d6 << "\n";
  // Close the output file
	cpgFile.close ();

return 0;
}


float Data_logger::motor_sig_logs(string file_name, float sim_time,
                                          float mg0,  float mg1,  float mg2,  float mg3,
                                          float mg4,  float mg5,  float mg6,  float mg7,
                                          float mg8,  float mg9,  float mg10, float mg11,
                                          float mg12, float mg13, float mg14, float mg15 )
{
  ofstream motorFile;
  motorFile.open(file_dir+file_name, ios::app);
  if(!motorFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  motorFile   << sim_time <<  " "  << mg0 <<  " " << mg1 <<  " " << mg2 <<  " " << mg3 <<  " "
                                    << mg4 <<  " " << mg5 <<  " " << mg6 <<  " " << mg7 <<  " "
                                    << mg8 <<  " " << mg9 <<  " " << mg10 << " " << mg11 << " "
                                    << mg12 << " " << mg13 << " " << mg14 << " " << mg15 << "\n";

  // Close the output file
  motorFile.close ();

return 0;
}

float Data_logger::flex_body_logs(string file_name,
                                            float cpg_mimic1, float cpg_mimic2, float cpg_mimic3,
                                            float sin_mimic1, float sin_mimic2, float sin_mimic3,
                                            float cpg_tune1,  float cpg_tune2,  float cpg_tune3)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << cpg_mimic1 <<  " " << cpg_mimic2 <<  " " << cpg_mimic3 <<  " "
            << sin_mimic1 <<  " " << sin_mimic2 <<  " " << sin_mimic3 <<  " "
            << cpg_tune1  <<  " " << cpg_tune2  <<  " " << cpg_tune3  <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}


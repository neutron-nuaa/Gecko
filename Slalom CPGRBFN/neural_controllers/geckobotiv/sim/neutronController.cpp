//
// Created by mat on 12/30/17.
//

#include "neutronController.h"

neutronController::neutronController(int argc,char* argv[]) {


    positions.resize(22);

    simulationID    = std::stoi(argv[1]);
    rollout         = std::stoi(argv[2]);
    simulationTime  = std::stoi(argv[3]); // Seconds
    blackOut        = std::stoi(argv[4]); // Seconds
    simulation      = true;
    rosHandle       = new simRosClass(simulationID);
    env             = new environment(simulationID, useAPItrigger);
    policyWeights   = readParameterSet();

    Delayline tmp(tau);
    for (int i = 0; i < 3; ++i)      // TODO changed 16 to 3 by happ
        delayline.push_back(tmp);

    if(CPGLearning) {
        CPGAdaptive_cpg = new modularController(1, true);
        CPGAdaptive_cpg->setPhii(0.01*M_PI);
    } else {
        CPGAdaptive = new rbfcpg(policyWeights, encoding, 20);
        // CPGAdaptive->setPhii(0.01*M_PI);                             // cpg so2 model
        CPGAdaptive->setMI(0.18);                                       // cpg mi model (set new mi)
        CPGPeriodPostprocessor = new postProcessing();
    }

    // Calculate init period and fill all delay line's
    for (int i = 0; i < tau+1; ++i) {
        // CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));          // cpg so2 model
        CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOfMI(2), CPGAdaptive->getCpgOfMI(1));                 // cpg mi model
        CPGPeriod = CPGPeriodPostprocessor->getPeriod();
        CPGAdaptive->step();
        // vector<double> RBF_output = CPGAdaptive->getNetworkOutput();  // TODO change by happ
        // for (int j = 0; j < 16; ++j)
        //     delayline[j].Write(RBF_output[j]);

        // TODO added by happ
        // vector<double> Travel_body = {CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(0)};         // cpg so2 model
        vector<double> Travel_body = {CPGAdaptive->getCpgOfMI(1), CPGAdaptive->getCpgOfMI(1), CPGAdaptive->getCpgOfMI(1)};                  // cpg mi model
        for (int j = 0; j < 3; ++j)
            delayline[j].Write(Travel_body[j]);

        for (auto &i : delayline)
            i.Step();
    }

    env->start();
    env->blackoutSimulation(blackOut);
    env->synchronousTrigger();
}

int neutronController::runController() {
    if(ros::ok()) {
        if(simulation)
            env->synchronousTrigger();

        if(CPGLearning) {
            CPGAdaptive_cpg->step();
            tripodGaitRangeOfMotion(policyWeights, encoding);
        } else {
            // CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));                  // cpg so2 model
            CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOfMI(2), CPGAdaptive->getCpgOfMI(1));                         // cpg mi model
            
            CPGPeriod = CPGPeriodPostprocessor->getPeriod();
            CPGAdaptive->step();
            //tripodGaitRBFN();     // changed commented out
            tripodGaitRBFN_noDelayline();
        }

        rosHandle->setLegMotorPosition(positions);

        // TODO added by happ
        // vector<double> Travel_body = {CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(0),CPGAdaptive->getCpgOutput(0)};      // cpg so2 model
        vector<double> Travel_body = {CPGAdaptive->getCpgOfMI(1), CPGAdaptive->getCpgOfMI(1),CPGAdaptive->getCpgOfMI(1)};               // cpg mi model
        for (int j = 0; j < 3; ++j)
            delayline[j].Write(Travel_body[j]);

        int end = Travel_body.size();

        //---- setting backbone joints ----//
        // Fixed body
        rosHandle->setBackboneMotorPosition({0,0,0});

        // Standing Wave
        // rosHandle->setBackboneMotorPosition({(float)(0.3*CPGAdaptive->getCpgOfMI(1)),
        //                                     (float)(0.3*CPGAdaptive->getCpgOfMI(1)),
        //                                     (float)(0.3*CPGAdaptive->getCpgOfMI(1))});

        // rosHandle->setBackboneMotorPosition({(0.4*CPGAdaptive->getCpgOfMI(2)),
        //                                      (0.4*CPGAdaptive->getCpgOfMI(2)),
        //                                      (0.4*CPGAdaptive->getCpgOfMI(2))});
        
        // Travelling Wave
        // rosHandle->setBackboneMotorPosition({ 
        //                                       (float)(2*delayline[0].Read(0)) ,
        //                                       (float)(2*delayline[1].Read(33)),
        //                                       (float)(2*delayline[2].Read(66)) 
        //                                     });

        // logger_cpg.cpg_pcpg_logs("cpglog.txt", CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1),
        //                                        delayline[0].Read(0), delayline[0].Read(CPGPeriod*Travel_body[end-1]),
        //                                        delayline[1].Read(50), delayline[1].Read(CPGPeriod*Travel_body[end-1]),
        //                                        delayline[2].Read(100), delayline[2].Read(CPGPeriod*Travel_body[end-1])  );

        // logger_cpg.cpg_pcpg_logs("cpglog.txt", 0.2*CPGAdaptive->getCpgOfMI(2), 0.2*CPGAdaptive->getCpgOfMI(1),
        //                                        CPGPeriod, 0,
        //                                        0, 0,
        //                                        0, 0  );


        for (auto &i : delayline)
            i.Step();

        rosHandle->rosSpinOnce();
    } else {
        return 0;
    }

    // Stop simulation after 'simulationTime' seconds
    if(simulation) {
        if ( env->getSimulationTime() > simulationTime * 1000) {
            if ( rosHandle->slipping == 100.0 ){
                cout << "'";
                return 2;
            } else {
                // cout << "[ INFO] Logging fitness" << endl;
                fitnessLogger();
            }
            return 0;
        }

//        if ( (env->getSimulationTime() > 2000) && (rosHandle->slipping > 99) ) {
//            cout << "[ INFO] Restarting simulation" << endl;
//            logfitness = false;
//            return 2; // Restart code
//        }
    }
    return 1;
}

void neutronController::tripodGaitRBFN() {

    vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

    for (int j = 0; j < 16; ++j)
        delayline[j].Write(RBF_output[j]);

    int end = RBF_output.size();

    //We force a tripod gait by delaying every second joint
    positions.at(BC0) = delayline[0].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(BC1) = delayline[1].Read(0);
    positions.at(BC2) = delayline[2].Read(0);
    positions.at(BC3) = delayline[3].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(BC4) = 0;
    positions.at(BC5) = 0;

    positions.at(CF0) = delayline[4].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(CF1) = delayline[5].Read(0);
    positions.at(CF2) = delayline[6].Read(0);
    positions.at(CF3) = delayline[7].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(CF4) = 0;
    positions.at(CF5) = 0;

    positions.at(FT0) = delayline[8].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(FT1) = delayline[9].Read(0);
    positions.at(FT2) = delayline[10].Read(0);
    positions.at(FT3) = delayline[11].Read(CPGPeriod*RBF_output[end-1]);
    positions.at(FT4) = 0;
    positions.at(FT5) = 0;

    positions.at(TT0) = delayline[0].Read(CPGPeriod*RBF_output[end-1]) * -1;      // todo fix
    positions.at(TT1) = delayline[1].Read(0) * -1;
    positions.at(TT2) = delayline[2].Read(0);
    positions.at(TT3) = delayline[3].Read(CPGPeriod*RBF_output[end-1]);

}

void neutronController::tripodGaitRBFN_noDelayline() {    // added by phil

    vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

    float BC_offset = 0.0;
    float CF_offset = -0.785398163;
    float FT_offset = 0.0;
    float TT_offset = 0.785398163;


    positions.at(BC0) = RBF_output[0];
    positions.at(BC1) = RBF_output[1];
    positions.at(BC2) = RBF_output[2];
    positions.at(BC3) = RBF_output[3];
    positions.at(BC4) = 0;
    positions.at(BC5) = 0;

    positions.at(CF0) = RBF_output[4];
    positions.at(CF1) = RBF_output[4];
    positions.at(CF2) = RBF_output[6];
    positions.at(CF3) = RBF_output[7];
    positions.at(CF4) = 0;
    positions.at(CF5) = 0;

    positions.at(FT0) = RBF_output[8];
    positions.at(FT1) = RBF_output[9];
    positions.at(FT2) = RBF_output[10];
    positions.at(FT3) = RBF_output[11];
    positions.at(FT4) = 0;
    positions.at(FT5) = 0;

    positions.at(TT0) = RBF_output[12];
    positions.at(TT1) = RBF_output[13];
    positions.at(TT2) = RBF_output[14];
    positions.at(TT3) = RBF_output[15];


}

void neutronController::tripodGaitRangeOfMotion(vector<float> policyWeights, string encoding) {

    if(encoding == "indirect"){
        positions.at(BC0) = ( policyWeights[0]*CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[1];
        positions.at(BC1) = ( policyWeights[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[1];
        positions.at(BC2) = ( policyWeights[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[1];
        positions.at(BC3) = ( policyWeights[0]*CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[1];
        positions.at(BC4) = 0;
        positions.at(BC5) = 0;

        positions.at(CF0) = ( policyWeights[2]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[3];
        positions.at(CF1) = ( policyWeights[2]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[3];
        positions.at(CF2) = ( policyWeights[2]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[3];
        positions.at(CF3) = ( policyWeights[2]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[3];
        positions.at(CF4) = 0;
        positions.at(CF5) = 0;

        positions.at(FT0) = ( policyWeights[4]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(FT1) = ( policyWeights[4]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[5];
        positions.at(FT2) = ( policyWeights[4]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(FT3) = ( policyWeights[4]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[5];
        positions.at(FT4) = 0;
        positions.at(FT5) = 0;
    }

    if(encoding == "sindirect"){
        positions.at(BC0) = ( policyWeights[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + policyWeights[1];
        positions.at(BC1) = ( policyWeights[2]*-CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[3];
        positions.at(BC2) = ( policyWeights[0]*-CPGAdaptive_cpg->getCpgOutput(1)  ) + policyWeights[1];
        positions.at(BC3) = ( policyWeights[2]*CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[3];
        positions.at(BC4) = 0;
        positions.at(BC5) = 0;

        positions.at(CF0) = ( policyWeights[4]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(CF1) = ( policyWeights[6]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[7];
        positions.at(CF2) = ( policyWeights[4]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(CF3) = ( policyWeights[6]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[7];
        positions.at(CF4) = 0;
        positions.at(CF5) = 0;

        positions.at(FT0) = ( policyWeights[8]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[9];
        positions.at(FT1) = ( policyWeights[10]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[11];
        positions.at(FT2) = ( policyWeights[8]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[9];
        positions.at(FT3) = ( policyWeights[10]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[11];
        positions.at(FT4) = 0;
        positions.at(FT5) = 0;

        positions.at(FT0) = ( policyWeights[12]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(FT1) = ( policyWeights[14]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[5];
        positions.at(FT2) = ( policyWeights[12]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[5];
        positions.at(FT3) = ( policyWeights[14]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[5];
        positions.at(FT4) = 0;
        positions.at(FT5) = 0;
    }

    if(encoding == "direct"){
        positions.at(BC0) = ( policyWeights[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + policyWeights[1];
        positions.at(BC1) = ( policyWeights[2]*-CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[3];
        positions.at(BC2) = ( policyWeights[4]*-CPGAdaptive_cpg->getCpgOutput(1)  ) + policyWeights[5];
        positions.at(BC3) = ( policyWeights[6]*CPGAdaptive_cpg->getCpgOutput(1) ) + policyWeights[7];
        positions.at(BC4) = 0;
        positions.at(BC5) = 0;

        positions.at(CF0) = ( policyWeights[8]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[9];
        positions.at(CF1) = ( policyWeights[10]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[11];
        positions.at(CF2) = ( policyWeights[12]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[13];
        positions.at(CF3) = ( policyWeights[14]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[15];
        positions.at(CF4) = 0;
        positions.at(CF5) = 0;

        positions.at(FT0) = ( policyWeights[16]*CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[17];
        positions.at(FT1) = ( policyWeights[18]*-CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[19];
        positions.at(FT2) = ( policyWeights[20]*-CPGAdaptive_cpg->getCpgOutput(0)  ) + policyWeights[21];
        positions.at(FT3) = ( policyWeights[22]*CPGAdaptive_cpg->getCpgOutput(0) ) + policyWeights[23];
        positions.at(FT4) = 0;
        positions.at(FT5) = 0;
    }
}

void neutronController::fitnessLogger() {
    // Average power:  P = T·ω
    // Calculated at each time step
    float avgPower          = rosHandle->avgPower;
    // Average Energy: E = P·t
    // Calculated at each time step
    float avgEnergy         = avgPower / 10; // 6 seconds simulation. change this value if sim time changes! //avgPower*env->getSimulationTime(); // TODO
    // Minimum distance between colliding parts of the robot
    // Includes intra & inter leg collision and body floor
    float robotColl         = rosHandle->robotCollision;
    // Velocity of the robot body
    // Only for the y-direction/heading direction
    float bodyVelocity      = rosHandle->avgBodyVel;
    // Distance moved
    // In the negative world y-axis/heading direction
    float distance          = rosHandle->distance;
    // Height Variance
    // Standard variance for entire run
    float bodyHeightVar     = rosHandle->heightVariance;
    // Panning around z-axis or heading Direction
    // Absolute mean from the entire run (init pan = 0)
    float headingDirection  = rosHandle->headingDirection;
    // Tilting around x-axis
    // Absolute mean from the entire run (init tilt = 0)
    float tilt              = rosHandle->tilt;
    // Rolling around y-axis
    // Absolute mean from the entire run (init roll = 0)
    float roll              = rosHandle->roll;
    // Foot slipping (for all feet)
    // Total amount of slip vs. non slipping ground contact
    float slipping          = rosHandle->slipping;

    float MORF_weight       = 2.45;       // KG
    float gravity           = 9.81;       // m/s^2

    float CoT               = avgEnergy / (MORF_weight * gravity * distance);
    float avgEnergyMeter    = avgEnergy / distance;

    if (isinf(avgEnergyMeter))
        avgEnergyMeter = 0;
    if (isinf(avgEnergy))
        avgEnergy = 0;


    /* Fitness Sub-Objectives */
    // Note: Slipping, stability, and collision are distance invariant
    float stability = bodyHeightVar*1 + headingDirection*1 + tilt*1 + roll*1;

    float collision = pow(robotColl,30);
    slipping    = slipping * 0.1;
    distance    = distance * 3;
    stability   = stability * 1;
    avgPower    = avgPower * 0.2;

    // if(distance < 0)
    // {
    //     distance = 0;
    // }

    float collision_T;
    if (collision > 1.5)
        collision_T = 1.5;
    else
        collision_T = collision;

    if (stability > 1)
        stability = 1;

    /* Fitness Function */
    float fitnessValue = (distance) - (stability + collision + slipping + avgPower);

//    cout << "[ INFO] Roll-out sub-rewards"   << endl;
//    cout << "- stability: " << stability  << " (" << bodyHeightVar*1 << "/" << headingDirection*2 << "/" << tilt*1 << "/" << roll*1 <<  ")" << endl;
//    cout << "- collision: " << collision  << " (" << collision_T << ")" << endl;
//    cout << "- slipping : " << slipping   << endl;
//    cout << "- distance : " << distance   << endl;
//    cout << "- fitness  : " << fitnessValue << endl;

    collision = robotColl;
    float power = avgEnergy;

    // Write json file with fitness
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    /******************************************************************
     * ADD SUB-FITNESS FUNCTIONS HERE AND INCLUDE THEM IN RL_master.py
     ******************************************************************/
    document.AddMember("FitnessValue", fitnessValue, allocator);
    document.AddMember("Fitness_Stab", stability, allocator);
    document.AddMember("Fitness_Coll", collision, allocator);
    document.AddMember("Fitness_Powr", power, allocator);
    document.AddMember("Fitness_Dist", distance, allocator);
    document.AddMember("Fitness_Slip", slipping, allocator);
    document.AddMember("Distance", distance, allocator);
    document.AddMember("Energy", power, allocator);

    // Write to json file
    string jsonfilename = "./../data/jobs/answers/answer_" + std::to_string(rollout) +".json";
    ofstream ofs(jsonfilename);
    rapidjson::OStreamWrapper osw(ofs);
    rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(osw);
    document.Accept(writer);

    writer.Flush();
    usleep(100);

    if (!writer.IsComplete()) {
        cout << "[ ERROR] In write JSON - rm answer_" + std::to_string(rollout) << endl;
        cout << rosHandle->slipping << " + " << env->getSimulationTime() << endl;
    }
}

vector<float> neutronController::readParameterSet() {
    ifstream ifs("./../data/jobs/RL_job.json");
    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document document;
    document.ParseStream(isw);

    assert(document.IsObject());
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    assert(document.HasMember("ParameterSet"));
    assert(document["ParameterSet"].IsArray());
    rapidjson::Value& paramset = document["ParameterSet"];

    std::string noise_name = "noise_" + std::to_string(rollout); //rollout not set in time!

    if(!document.HasMember(noise_name.c_str())){
        cout << "[ERROR] No noise member called: " << noise_name.c_str() << endl;
        cout << "[ERROR] Using noise member \"noise_0\" again" << endl;
        noise_name = "noise_0";
    }

    rapidjson::Value& noise = document[noise_name.c_str()];

    vector<float> policyWeights;
    for (rapidjson::SizeType i = 0; i < paramset.Size(); i++) // Uses SizeType instead of size_t
        policyWeights.push_back(paramset[i].GetDouble() + noise[i].GetDouble());

    // Get encoding
    assert(document.HasMember("checked"));
    assert(document["checked"].IsString());
    rapidjson::Value& _encoding = document["checked"];
    encoding = _encoding.GetString();

    ifs.close();

    return policyWeights;
}

void neutronController::logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                                 double positionX, double bodyVel, double angularVelocity, double jointTorque,
                                 double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL){
    myFile << simtime <<"\t"<< CPGphi <<"\t"<< error <<"\t"<< maxVel <<"\t"<< maxForce <<"\t"<< positionX <<"\t" << bodyVel << "\t" << angularVelocity << "\t" << jointTorque << "\t" << controllerOut << "\t" << systemFeedback << "\t" << hlNeuronAF << "\t" << hlNeutronDL << "\n";
}

double neutronController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}

neutronController::~neutronController() {
    if(CPGLearning)
        delete CPGAdaptive_cpg;
    else delete CPGAdaptive;
    delete CPGPeriodPostprocessor;

    env->stop();
    env->endConnection();
    delete env;

    ros::shutdown();
    delete rosHandle;
}

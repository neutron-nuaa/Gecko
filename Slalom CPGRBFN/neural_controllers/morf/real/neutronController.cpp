//
// Created by mat on 12/30/17.
//

#include "neutronController.h"

neutronController::neutronController(int argc,char* argv[]) {
    rosHandle = new realRosClass(argc, argv);
    simulation = false;

    weightsMatlab = readParameterSet("/home/morf-one/RL_job.json");

    cout << "[ INFO] Encoding is set to: " << encoding << endl;

    if(!CPGLearning)
        CPGAdaptive = new rbfcpg(weightsMatlab, encoding, 20);
    else
        CPGAdaptive_cpg = new modularController(1, true);

    learner = new dualIntegralLearner(CPGmethod != 3, 20);

    sensorPostprocessor = new postProcessing();
    sensorPostprocessor->setBeta(0.25);

    controllerPostprocessor = new postProcessing();
    controllerPostprocessor->setBeta(0.25);

    sensorAmpPostprocessor = new postProcessing();
    sensorAmpPostprocessor->setBeta(0.04);

    controllerAmpPostprocessor = new postProcessing();
    controllerAmpPostprocessor->setBeta(0.04);

    CPGPeriodPostprocessor = new postProcessing();

    positions.resize(22);

    for (int i = 0; i < 18; ++i) {
        Delayline tmp(tau);
        phaseShift.push_back(tmp);
    }

    if(ros::ok()) {
        if(simulation) {
//            rosHandle->synchronousSimulation(true);
//            rosHandle->rosSpinOnce();
        }
    }
}

bool neutronController::runController() {
    double amplitudeSensor, amplitudeController;
    double LPFSensorAmp, LPFControllerAmp;
    double error;
    double true_error;

    if(ros::ok()) {
        if(simulation)
            rosHandle->triggerSim();

        // start/stop controller
        if(rosHandle->buttons[0] == 1) // A
            run_controller = false;

        if(rosHandle->buttons[1] == 1) { // B
            weightsMatlab = readParameterSet("/home/morf-one/RL_job.json");
            run_controller = true;
            broken_middleleg = false;
            CPGAdaptive->setEncoding(encoding);
            CPGAdaptive->setWeights(weightsMatlab);

            for (int k = 0; k <= tau * 2 + 1; ++k) {
                CPGAdaptive->step();
                vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

                for (int j = 0; j < 18; ++j)
                    phaseShift[j].Write(RBF_output[j]);

                for (auto &i : phaseShift)
                    i.Step();
            }
        }

        if(rosHandle->buttons[3] == 1 && broken_middleleg) { // Y
            weightsMatlab = readParameterSet("/home/morf-one/RL_job.json");
            broken_middleleg = false;
            CPGAdaptive->setEncoding(encoding);
            CPGAdaptive->setWeights(weightsMatlab);

            for (int k = 0; k <= tau*2+1; ++k) {
                CPGAdaptive->step();
                vector<double> RBF_output = CPGAdaptive->getNetworkOutput();
                for (int j = 0; j < 18; ++j)
                    phaseShift[j].Write(RBF_output[j]);
                for (auto &i : phaseShift)
                    i.Step();
            }
        }

        if(rosHandle->buttons[3] == 1 && !broken_middleleg) { // Y
            weightsMatlab = readParameterSet("/home/morf-one/RL_job_bm.json");
            broken_middleleg = true;
            CPGAdaptive->setEncoding(encoding);
            CPGAdaptive->setWeights(weightsMatlab);

            for (int k = 0; k <= tau*2+1; ++k) {
                CPGAdaptive->step();

                vector<double> RBF_output = CPGAdaptive->getNetworkOutput();
                for (int j = 0; j < 18; ++j)
                    phaseShift[j].Write(RBF_output[j]);
                for (auto &i : phaseShift)
                    i.Step();
            }
        }


        if(CPGmethod == 1) {
            // POST PROCESSING
            amplitudeSensor = sensorPostprocessor->calculateLPFAmplitude(rosHandle->jointPositions[BC4]);
            amplitudeController = controllerPostprocessor->calculateLPFAmplitude(positions.at(BC4));
            LPFSensorAmp = sensorAmpPostprocessor->calculateLPFSignal(amplitudeSensor);
            LPFControllerAmp = controllerAmpPostprocessor->calculateLPFSignal(amplitudeController);
            error = (LPFControllerAmp - errorMargin) - LPFSensorAmp;
            true_error = LPFControllerAmp - LPFSensorAmp;
        }

        // CALCULATE RBF CENTERS
        if(!CPGLearning) {
            CPGPeriodPostprocessor->calculateAmplitude(CPGAdaptive->getCpgOutput(0), CPGAdaptive->getCpgOutput(1));
            CPGPeriod = CPGPeriodPostprocessor->getPeriod();
        }

        CPGPeriod = 200; // TODO HARDCODED

        /*
        if(CPGPeriodPostprocessor->periodTrust){
            CPGAdaptive->calculateRBFCenters(CPGPeriodPostprocessor->getPeriod(),CPGPeriodPostprocessor->getSignalPeriod(0),CPGPeriodPostprocessor->getSignalPeriod(1));
        } */

        // New Phi for robot
        double newPhi = phiParam;

        if(!CPGLearning) {
            if (CPGmethod == 0) {
                /****************************************
                * VANILLA SO2 CPG
                * Open-Loop - Does not adapt in any way.
                ****************************************/
                CPGAdaptive->setPhii(newPhi);

            } else if (CPGmethod == 1) {
                /****************************************
                * DL CPG (Dual Learner)
                * Adapts the Phi to one where it can run.
                ****************************************/
                if (waiter >= 0) {
                    error = 0;
                    waiter--;
                } else {
                    waiter = -10;
                    learner->step(error, phiParam);
                    CPGAdaptive->setPhii(learner->getControlOutput());
                }
            }

            // Plot's
            data.clear();
            // RED
            data.push_back(positions.at(BC0));//CPGAdaptive->getNetworkOutput()[0]);//controllerPostprocessor->periodViz);
            // GREEN
            data.push_back(positions.at(CF0));//CPGAdaptive->getNetworkOutput()[1]);//rosHandle->jointTorques[BC0]);
            // YELLOW
            data.push_back(positions.at(FT0));//CPGAdaptive->getCpgOutput(0));
            // BLUE
            data.push_back(0);//rosHandle->jointTorques[FT0]);
            // PINK
            data.push_back(0);//CPGAdaptive->getNetworkOutput()[2]);

            // STEP CPG AND EXECUTE TRIPOD CONTROLLER
            if(run_controller) {
                if (transientState) {
                    // Fill up delay lines
                    for (int k = 0; k <= tau*2+1; ++k) {
                        CPGAdaptive->step();
                        vector<double> RBF_output = CPGAdaptive->getNetworkOutput();
                        for (int j = 0; j < 18; ++j)
                            phaseShift[j].Write(RBF_output[j]);
                        for (auto &i : phaseShift)
                            i.Step();
                    }
                    transientState = false;
                } else {
                    CPGAdaptive->step();
                    tripodGaitRBFN();
                }
            }

        } else {

            // Plot's
            data.clear();
            // RED
            data.push_back(positions.at(BC0));//CPGAdaptive->getNetworkOutput()[0]);//controllerPostprocessor->periodViz);
            // GREEN
            data.push_back(positions.at(CF0));//CPGAdaptive->getNetworkOutput()[1]);//rosHandle->jointTorques[BC0]);
            // YELLOW
            data.push_back(positions.at(FT0));//CPGAdaptive->getCpgOutput(0));
            // BLUE
            data.push_back(0);//rosHandle->jointTorques[FT0]);
            // PINK
            data.push_back(0);//CPGAdaptive->getNetworkOutput()[2]);

            CPGAdaptive_cpg->setPhii(newPhi);
            CPGAdaptive_cpg->step();
            tripodGaitRangeOfMotion(weightsMatlab, encoding);
        }

    } else
    {
        cout << "Closing in the loop" << endl;
        return false;
    }

    rosHandle->rosSpinOnce();
    if(simulation) {
        if ((rosHandle->simState != 1 && rosHandle->simulationTime > 1) || rosHandle->terminateSimulation) {
            cout << "Closing and logging" << endl;
            cout << " " << endl;
            cout << " " << endl;
            fitnessLogger();
            myfile.close();
            rosHandle->synchronousSimulation(false);
            ros::shutdown();
            delete rosHandle;
            if(!CPGLearning)
                delete CPGAdaptive;
            else delete CPGAdaptive_cpg;
            delete learner;
            delete sensorPostprocessor;
            delete controllerPostprocessor;
            delete sensorAmpPostprocessor;
            delete controllerAmpPostprocessor;
            delete CPGPeriodPostprocessor;
            return false;
        } else {
            return !rosHandle->terminateSimulation;
        }
    } else
        return true;
}

void neutronController::tripodGaitRBFN() {

    vector<double> RBF_output = CPGAdaptive->getNetworkOutput();

    for (int j = 0; j < 18; ++j)
        phaseShift[j].Write(RBF_output[j]);

    int end = RBF_output.size();

    // We force a tripod gait by delaying every second joint
    positions.at(BC0) = phaseShift[0].Read(0);
    positions.at(BC1) = phaseShift[1].Read(CPGPeriod*RBF_output[end-5]);
    positions.at(BC2) = phaseShift[2].Read(0);
    positions.at(BC3) = phaseShift[3].Read(CPGPeriod*RBF_output[end-3]);
    positions.at(BC4) = phaseShift[4].Read(0);
    positions.at(BC5) = phaseShift[5].Read(CPGPeriod*RBF_output[end-1]);

    positions.at(CF0) = phaseShift[6].Read(0);
    positions.at(CF1) = phaseShift[7].Read(CPGPeriod*RBF_output[end-5]);
    positions.at(CF2) = phaseShift[8].Read(0);
    positions.at(CF3) = phaseShift[9].Read(CPGPeriod*RBF_output[end-3]);
    positions.at(CF4) = phaseShift[10].Read(0);
    positions.at(CF5) = phaseShift[11].Read(CPGPeriod*RBF_output[end-1]);

    positions.at(FT0) = phaseShift[12].Read(0);
    positions.at(FT1) = phaseShift[13].Read(CPGPeriod*RBF_output[end-5]);
    positions.at(FT2) = phaseShift[14].Read(0);
    positions.at(FT3) = phaseShift[15].Read(CPGPeriod*RBF_output[end-3]);
    positions.at(FT4) = phaseShift[16].Read(0);
    positions.at(FT5) = phaseShift[17].Read(CPGPeriod*RBF_output[end-1]);

    if(broken_middleleg){
        positions.at(BC1) = 0;
        positions.at(CF1) = 2.8;
        positions.at(FT1) = -0.28;
    }

    rosHandle->setLegMotorPosition(positions);

    if(simulation)
        rosHandle->plot(data);

    // Step delay lines
    for (auto &i : phaseShift)
        i.Step();
}

void neutronController::tripodGaitRangeOfMotion(vector<float> weightsMatlab, string encoding) {

    if(encoding == "indirect"){
        positions.at(BC0) = ( weightsMatlab[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[1];
        positions.at(BC1) = ( weightsMatlab[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[1];
        positions.at(BC2) = ( weightsMatlab[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[1];
        positions.at(BC3) = ( weightsMatlab[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[1];
        positions.at(BC4) = ( weightsMatlab[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[1];
        positions.at(BC5) = ( weightsMatlab[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[1];

        positions.at(CF0) = ( weightsMatlab[2]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[3];
        positions.at(CF1) = ( weightsMatlab[2]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[3];
        positions.at(CF2) = ( weightsMatlab[2]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[3];
        positions.at(CF3) = ( weightsMatlab[2]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[3];
        positions.at(CF4) = ( weightsMatlab[2]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[3];
        positions.at(CF5) = ( weightsMatlab[2]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[3];

        positions.at(FT0) = ( weightsMatlab[4]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[5];
        positions.at(FT1) = ( weightsMatlab[4]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[5];
        positions.at(FT2) = ( weightsMatlab[4]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[5];
        positions.at(FT3) = ( weightsMatlab[4]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[5];
        positions.at(FT4) = ( weightsMatlab[4]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[5];
        positions.at(FT5) = ( weightsMatlab[4]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[5];
    }

    if(encoding == "sindirect"){
        positions.at(BC0) = ( weightsMatlab[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[1];
        positions.at(BC1) = ( weightsMatlab[2]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[3];
        positions.at(BC2) = ( weightsMatlab[4]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[5];
        positions.at(BC3) = ( weightsMatlab[0]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[1];
        positions.at(BC4) = ( weightsMatlab[2]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[3];
        positions.at(BC5) = ( weightsMatlab[4]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[5];

        positions.at(CF0) = ( weightsMatlab[6]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[7];
        positions.at(CF1) = ( weightsMatlab[8]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[9];
        positions.at(CF2) = ( weightsMatlab[10]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[11];
        positions.at(CF3) = ( weightsMatlab[6]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[7];
        positions.at(CF4) = ( weightsMatlab[8]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[9];
        positions.at(CF5) = ( weightsMatlab[10]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[11];

        positions.at(FT0) = ( weightsMatlab[12]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[13];
        positions.at(FT1) = ( weightsMatlab[14]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[15];
        positions.at(FT2) = ( weightsMatlab[16]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[17];
        positions.at(FT3) = ( weightsMatlab[12]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[13];
        positions.at(FT4) = ( weightsMatlab[14]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[15];
        positions.at(FT5) = ( weightsMatlab[16]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[17];
    }

    if(encoding == "direct"){
        positions.at(BC0) = ( weightsMatlab[0]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[1];
        positions.at(BC1) = ( weightsMatlab[2]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[3];
        positions.at(BC2) = ( weightsMatlab[4]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[5];
        positions.at(BC3) = ( weightsMatlab[6]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[7];
        positions.at(BC4) = ( weightsMatlab[8]*CPGAdaptive_cpg->getCpgOutput(1)  ) + weightsMatlab[9];
        positions.at(BC5) = ( weightsMatlab[10]*-CPGAdaptive_cpg->getCpgOutput(1) ) + weightsMatlab[11];

        positions.at(CF0) = ( weightsMatlab[12]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[13];
        positions.at(CF1) = ( weightsMatlab[14]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[15];
        positions.at(CF2) = ( weightsMatlab[16]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[17];
        positions.at(CF3) = ( weightsMatlab[18]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[19];
        positions.at(CF4) = ( weightsMatlab[20]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[21];
        positions.at(CF5) = ( weightsMatlab[22]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[23];

        positions.at(FT0) = ( weightsMatlab[24]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[25];
        positions.at(FT1) = ( weightsMatlab[26]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[27];
        positions.at(FT2) = ( weightsMatlab[28]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[29];
        positions.at(FT3) = ( weightsMatlab[30]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[31];
        positions.at(FT4) = ( weightsMatlab[32]*CPGAdaptive_cpg->getCpgOutput(0)  ) + weightsMatlab[33];
        positions.at(FT5) = ( weightsMatlab[34]*-CPGAdaptive_cpg->getCpgOutput(0) ) + weightsMatlab[35];
    }

    rosHandle->setLegMotorPosition(positions);

    if(simulation)
        rosHandle->plot(data);

    // Step delay lines
    for (auto &i : phaseShift)
        i.Step();
}

void neutronController::fitnessLogger() {
    // Average power:  P = T·ω
    // Calculated at each time step
    float avgPower          = rosHandle->avgPower;
    // Average Energy: E = P·t
    // Calculated at each time step
    float avgEnergy         = avgPower*rosHandle->simulationTime;
    // Minimum distance between colliding parts of the robot
    // Includes intra & inter leg collision and body floor
    float robotColl         = rosHandle->robotCollision;
    // Velocity of the robot body
    // Only for the y-direction/heading direction
    float bodyVelocity      = rosHandle->avgBodyVel;
    // Distance moved
    // In the negative world y-axis/heading direction
    float distance          = rosHandle->distance;
    // Height Variance
    // Standard variance for entire run
    float bodyHeightVar     = rosHandle->heightVariance;
    // Panning around z-axis or heading Direction
    // Absolute mean from the entire run (init pan = 0)
    float headingDirection  = rosHandle->headingDirection;
    // Tilting around x-axis
    // Absolute mean from the entire run (init tilt = 0)
    float tilt  = rosHandle->tilt;
    // Rolling around y-axis
    // Absolute mean from the entire run (init roll = 0)
    float roll  = rosHandle->roll;
    // Foot slipping (for all feet)
    // Total amount of slip vs. non slipping ground contact
    float slipping = rosHandle->slipping;

    float MORF_weight       = 4.2;  // KG
    float gravity           = 9.82; // m/s^2

    float CoT               = avgEnergy / (MORF_weight * gravity * distance);
    float avgEnergyMeter    = avgEnergy / distance;

    if (isinf(avgEnergyMeter))
        avgEnergyMeter = 0;
    if (isinf(avgEnergy))
        avgEnergy = 0;

    /* Fitness Sub-Objectives */
    // Note: Slipping, stability, and collision are distance invariant
    float stability = bodyHeightVar*1 + headingDirection*1 + tilt*1 + roll*1;
    float collision = pow(robotColl,20);
    slipping    = slipping * 1;
    distance    = distance * 3;
    stability   = stability;

    float collision_T;
    if (collision > 1.5)
        collision_T = 1.5;
    else
        collision_T = collision;

    if (stability > 1)
        stability = 1;

    /* Fitness Function */
    float fitnessValue = (distance) - (stability + collision + slipping);

//    if (fitnessValue < -0.5)
//        fitnessValue = -0.5;

    cout << "**** Roll out Info ****"   << endl;
    cout << "stability: " << stability  << " (" << bodyHeightVar*1 << "/" << headingDirection*2 << "/" << tilt*1 << "/" << roll*1 <<  ")" << endl;
    cout << "collision: " << collision  << " (" << collision_T << ")" << endl;
    cout << "slipping : " << slipping   << endl;
    cout << "distance : " << distance   << endl;
    cout << "fitness  : " << fitnessValue << "\n" << endl;

    collision = robotColl;
    float power = avgEnergy;

    // Write json file with fitness
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    /******************************************************************
     * ADD SUB-FITNESS FUNCTIONS HERE AND INCLUDE THEM IN RL_master.py
     ******************************************************************/
    document.AddMember("FitnessValue", fitnessValue, allocator);
    document.AddMember("Fitness_Stab", stability, allocator);
    document.AddMember("Fitness_Coll", collision, allocator);
    document.AddMember("Fitness_Powr", power, allocator);
    document.AddMember("Fitness_Dist", distance, allocator);
    document.AddMember("Fitness_Slip", slipping, allocator);
    document.AddMember("Distance", distance, allocator);
    document.AddMember("Energy", power, allocator);

    // Write to json file
    ofstream ofs("/home/mat/workspace/gorobots/utils/CPGRBFN/RL_data/answer_dir/answer_" + std::to_string(rosHandle->rollout) +".json");
    rapidjson::OStreamWrapper osw(ofs);
    rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(osw);
    document.Accept(writer);

    writer.Flush();
    usleep(100);

    if (!writer.IsComplete())
        cout << "[ ERROR] IN WRITE JSON!" << endl;
}

vector<float> neutronController::readParameterSet(string job) {
    ifstream ifs(job);
    rapidjson::IStreamWrapper isw(ifs);
    rapidjson::Document document;
    document.ParseStream(isw);

    assert(document.IsObject());
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

    assert(document.HasMember("ParameterSet"));
    assert(document["ParameterSet"].IsArray());
    rapidjson::Value& paramset = document["ParameterSet"];

    std::string noise_name = "noise_" + std::to_string(rosHandle->rollout); //rollout not set in time!

    if(!document.HasMember(noise_name.c_str())){
        cout << "[ERROR] No noise member called: " << noise_name.c_str() << endl;
        cout << "[ERROR] Using noise member \"noise_0\" again" << endl;
        noise_name = "noise_0";
    }

    rapidjson::Value& noise = document[noise_name.c_str()];

    vector<float> weightsMatlab;
    for (rapidjson::SizeType i = 0; i < paramset.Size(); i++) // Uses SizeType instead of size_t
        weightsMatlab.push_back(paramset[i].GetDouble());// + noise[i].GetDouble());

    // Get encoding
    assert(document.HasMember("checked"));
    assert(document["checked"].IsString());
    rapidjson::Value& _encoding = document["checked"];
    encoding = _encoding.GetString();

    ifs.close();

    return weightsMatlab;
}

void neutronController::logData( double simtime, double CPGphi, double error, double maxVel, double maxForce,
                                 double positionX, double bodyVel, double angularVelocity, double jointTorque,
                                 double controllerOut, double systemFeedback, double hlNeuronAF, double hlNeutronDL){
    myfile << simtime <<"\t"<< CPGphi <<"\t"<< error <<"\t"<< maxVel <<"\t"<< maxForce <<"\t"<< positionX <<"\t" << bodyVel << "\t" << angularVelocity << "\t" << jointTorque << "\t" << controllerOut << "\t" << systemFeedback << "\t" << hlNeuronAF << "\t" << hlNeutronDL << "\n";
}

double neutronController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}

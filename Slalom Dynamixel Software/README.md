# <center> Slalom Dynamixel Software </center>

This guide assumes that you are running Ubuntu 18.04 and that you want to control dynamixel XM-430 servos using the U2D2. 

Credit: Mathias Thor

## 1) SET USB LATENCY

Start by setting the USB latency on the PC for fast communication. This can be done by running the following command:

```
sudo usermod -aG dialout $USER && echo 1 | sudo tee /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
```

You can check it by running the following:

```
cat /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
```

## 2) Install dependencies

The following dependencies need to be installed on the PC.

### 2.1) ROS

To install ROS melodic on ubuntu 18.04 run the following commands:

```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

sudo apt-get update

sudo apt-get install ros-melodic-desktop-full
```


### 2.2) Dynamixel SDK and ROS Controller

Run the following commands to install the remaining dependencies:

```
sudo apt-get install -y git cmake python-tempita python-catkin-tools python-lxml xsltproc qt4-qmake libqt4-dev libqscintilla2-dev
```

Run the following to install the Dynamixel Workbench used for communicating with the servos through ROS:

```
sudo apt-get install ros-melodic-dynamixel-sdk
```

```
mkdir ~/catkin_ws && cd ~/catkin_ws

mkdir src && cd src
```

```
git clone https://github.com/worasuch/my_dynamixel_workbench.git
git clone https://github.com/worasuch/dynamixel-workbench.git
git clone https://github.com/ROBOTIS-GIT/dynamixel-workbench-msgs.git
git clone https://github.com/stonier/qt_ros.git
```

```
cd dynamixel-workbench-msgs && git checkout f91ae7dbd5d368a3121ca5bb901771b2e6471c01
```

```
source /opt/ros/melodic/setup.bash
source /home/$USER/catkin_ws/devel/setup.sh
```

In order to add the above command to your .bashrc use the following command:

```
gedit ~/.bashrc
```

and add the following in the end of the file:

```
source /opt/ros/melodic/setup.bash
source /home/$USER/catkin_ws/devel/setup.sh
```

Compile the dynamixel ros controller:

```
cd ../.. && catkin_make
```

Connect the servos and then run the following command:

```
roslaunch my_dynamixel_workbench_tutorial multiple_motor_test.launch
```

***Servos ready to control !!!***

<br >
<br >

## Additionally, in the case of running more than 19 motors

This software is for running maximum 19 motors . If you want to run more than 19 motors, you need to change the existing code as mentioned below.

**For example, changing from 19 to 20 number of motors** <br >
Search number of 19 in the codes under dynamixel-workbench
```
cd ~/catkin_ws/src/dynamixel-workbench
grep -nr "19"
```

Change 19 to 20 in the four files as mentioned below
```
dynamixel_workbench_controllers/include/dynamixel_workbench_controllers/position_control.h:71:    uint8_t dxl_id_[19];
dynamixel_workbench_toolbox/include/dynamixel_workbench_toolbox/dynamixel_tool.h:43:  DXLInfo dxl_info_[19];
dynamixel_workbench_toolbox/include/dynamixel_workbench_toolbox/dynamixel_item.h:26:#define AX_18A     19
dynamixel_workbench_toolbox/src/dynamixel_workbench_toolbox/dynamixel_workbench.cpp:388:  std::vector<std::vector<int32_t>> data(6, std::vector<int>(19));

```

Make the new code
```
cd ~/catkin_ws/
catkin_make
```

<br >

## Issues

Often got error "Sync read.." that we cannot get the information from motors such as current, torque, and etc.
We still don't know the root cause of the problem exactly. By the way, please check about the power supply and wire connectors.

## Another source to try (from Donghao)
https://github.com/bishopAL/GeRot









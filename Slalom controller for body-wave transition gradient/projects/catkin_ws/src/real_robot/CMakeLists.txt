cmake_minimum_required(VERSION 2.8.3)
project(real_robot)

set (CMAKE_CXX_STANDARD 11)

SET (GOROBOTS "../../../../..")
SET (CONTROLLER "${GOROBOTS}/controllers")
SET (REAL_ROBOT "${CONTROLLER}/slalom/real_robots")
SET (UTILS "${GOROBOTS}/utils")
#SET (REAL_ROBOT "${UTILS}/real_robots/slalom/catkin_ws/src/my_dynamixel_workbench/src")

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS std_msgs geometry_msgs roscpp )

include_directories("${catkin_INCLUDE_DIRS}"
        "${GOROBOTS}"
        "${UTILS}")

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

add_executable(
        real_robot
	${REAL_ROBOT}/realRobot
	)
target_link_libraries(real_robot ${catkin_LIBRARIES})
add_dependencies(real_robot ${catkin_EXPORTED_TARGETS})

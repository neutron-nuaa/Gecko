--*******************************************
--*                                         *
--*this is modify version for flexible body.*
--*                                         *
--*******************************************
-- modify by: Worasuchad Hoamachai
-- detail: developing gecko robot to have flexible body
-- update: 19/05/2022
-- version: 2.0.0

--*******************************************
--*                                         *
--*              description                *
--*                                         *
--*******************************************
-- creating vrep/coppeliasim simulation of the gecko robot and starting rosnode named "vrep_ros_interface"

--*******************************************
--*                                         *
--*             define variable             *
--*                                         *
--*******************************************
geckoTape_dist = 0.0025 -- maximum distance for enable adhesive force
epsilon = 0.001 -- very small value

ADHESIVE_FORCE = 4

--*******************************************
--*                                         *
--*          define function                *
--*                                         *
--*******************************************
function addforce(handle,efference,dist,addedForce)

    -- description:
    --
    --              add force "addedForce" newton to foot handle "handle" when "efference" signal
    -- is zero and "dist"ance between foot and surface is lower than "geckoTape_dist". This function
    -- has no output.

    if (dist <= geckoTape_dist) and (efference <= epsilon) then
        sim.addForce(handle,{0,0,0},{0,0,-addedForce})
    end
end

function simGetJointVelocity (jointHandle)
    res,velocity=simGetObjectFloatParameter(jointHandle,2012)
    return  velocity
end

--*******************************************
--*                                         *
--*          ros callback function          *
--*                                         *
--*******************************************
function body_cb(msg)
    data = msg.data
    for i=1,3,1 do
        bodySignal[i] = data[i]
    end
end

function cpg_cb(msg)
    data = msg.data
    cpgSignal[1] = data[1]
    cpgSignal[2] = data[2]
    cpgSignal[3] = data[3]
    cpgSignal[4] = data[4]
end

function motor_cb(msg)
    data = msg.data
    for i=1,16,1 do
        motorSignal[i] = data[i]
    end
end

function sim_motor_cb(msg)
    data = msg.data
    for i=1,16,1 do
        sim_motorSignal[i] = data[i]
    end
end

function real_imu_cb(msg)
    data = msg.data
    accel[1] = data[1]
    accel[2] = data[2]
    accel[3] = data[3]
    gyroData[1] = data[4]
    gyroData[2] = data[5]
    gyroData[3] = data[6]
    realRobot = true
end

--*******************************************
--*                                         *
--*  to set slope angle of the surface      *
--*                                         *
--*******************************************
function setGravety(gravetyMagnitude, angleDeg, verbal)
    local angle1Rad = angleDeg * 0.0174532925
    local angle2Rad = (180 - 90 - angleDeg) * 0.0174532925 
    local gravetyZ = (math.cos(angle1Rad) * -1) * gravetyMagnitude
    local gravetyX = (math.cos(angle2Rad) * -1) * gravetyMagnitude
    sim.setArrayParameter(sim.arrayparam_gravity,{gravetyX,0,gravetyZ})
    if verbal then
        print("gravity set to " .. angleDeg .. " deg. [x,y,z] [ ".. gravetyX .. ", 0, " .. gravetyZ .. " ]")
    end
end

-- initialize section
function sysCall_init()

    --*******************************************
    --*                                         *
    --*         create global variable          *
    --*                                         *
    --*******************************************

    -- ***************************  neural control *********************************
    bodySignal = {0,0,0, 0,0,0} -- Body signal  [cpg , sine]
    cpgSignal = {0,0,0,0} -- CPG signal
    motorSignal = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    sim_motorSignal = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    effeSignal = {0,0,0,0}

    -- *********  convert motor signal to simulated motor signal *******************



    -- **********************  simulated object handle *****************************
    legName = {'lf','lh','rh','rf'}
    jointHandle = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}
    footHandle = {0,0,0,0}
    forceHandle = {0,0,0,0}
    -- Create all object handles including joint, foot and force sensor

    -- *****************************  sensory signal *******************************
    forceData = {0,0,0,0} -- simulated force signal from foot contact sensor
    jointTorque = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    real_forceData = {0,0,0,0}
    distance = {0,0,0,0} -- distance between foot and the surface
    accel = {0,0,0} -- acceleration from imu
    gyroData={0,0,0} -- angular vilocity from imu
    bodyAng = {0,0,0} -- body inclination of the robot
    real_bodyAng = {0,0,0}

    -- *************************  parameter for rostopic ****************************
    forceTopic = {} -- simulated force parameter
    imuTopic = {} -- simulated imu parameter including acceleration and angular vilocity
    simTimeTopic = {} -- simulated time
    simBodyAngTopic = {} -- simulated robot's body angle
    simWalkingDistTopic = {} -- simulated robot walking distance

    -- ************************ performance measurement  ***************************
    stab = 0 -- stability
    harm = 0 -- harmony
    walking_dist = 0 -- distance

    -- ************************ slope angle of the surface  ************************
    gravityChanged = false
    slopeDeg = 0  -- 0 15 20 25 30 deg for prelim-exp using simple leg traj    

    --*******************************************
    --*                                         *
    --*          create object handle           *
    --*                                         *
    --*******************************************
    geckoHandle=sim.getObjectAssociatedWithScript(sim.handle_self)
    for i=1,4,1 do
        for j=1,4,1 do
            jointHandle[i][j] = simGetObjectHandle('joint'..tostring(j).."_"..legName[i])
        end
        footHandle[i] = simGetObjectHandle('pad_'..legName[i])
        forceHandle[i] = simGetObjectHandle('fc_'..legName[i])
    end
    body = simGetObjectHandle('body')
    floor = simGetObjectHandle('floor')
    jointB1 = simGetObjectHandle('joint_b1')
    jointB2 = simGetObjectHandle('joint_b2')
    jointB3 = simGetObjectHandle('joint_b3')

    -- get graph handle
    graph = simGetObjectHandle('graph')
    graph_motor = simGetObjectHandle('graph_motor')
    g_test = simGetObjectHandle('test')
    -- g_leg_traj = simGetObjectHandle('graph_leg')

    -- -- visualize trajectories
    -- traj_lf = simGetObjectHandle('traj_lf')
    -- draw_traj_lf = simAddDrawingObject(sim.drawing_lines|sim.drawing_cyclic,5,0,-1,1000,{1, 0, 0})
    -- pos_traj_lf = simGetObjectPosition(traj_lf,-1)

    -- traj_b1 = simGetObjectHandle('traj_b1')
    -- draw_traj_b1 = simAddDrawingObject(sim.drawing_lines|sim.drawing_cyclic,5,0,-1,1000,{238/255, 130/255, 238/255})
    -- pos_traj_b1 = simGetObjectPosition(traj_b1,-1)

    -- traj_b2 = simGetObjectHandle('traj_b2')
    -- draw_traj_b2 = simAddDrawingObject(sim.drawing_lines|sim.drawing_cyclic,5,0,-1,1000,{64/255, 224/255, 208/255})
    -- pos_traj_b2 = simGetObjectPosition(traj_b2,-1)

    -- traj_b3 = simGetObjectHandle('traj_b3')
    -- draw_traj_b3 = simAddDrawingObject(sim.drawing_lines|sim.drawing_cyclic,5,0,-1,1000,{1, 101/255, 71/255})
    -- pos_traj_b3 = simGetObjectPosition(traj_b3,-1)


    -- initial position
    body_init_posi = simGetObjectPosition(body,floor)

    -- gyro sensor
    modelBaseG=sim.getObjectAssociatedWithScript(sim.handle_self)
    refG=sim.getObjectHandle('GyroSensor_reference')
    uiG=simGetUIHandle('GyroSensor_UI')
    simSetUIButtonLabel(uiG,0,sim.getObjectName(modelBaseG))
    gyroCommunicationTube=sim.tubeOpen(0,'gyroData'..sim.getNameSuffix(nil),1)
    oldTransformationMatrix=sim.getObjectMatrix(refG,-1)
    lastTime=sim.getSimulationTime()

    -- accelero sensor
    modelBaseA=sim.getObjectAssociatedWithScript(sim.handle_self)
    massObject=sim.getObjectHandle('Accelerometer_mass')
    accSensor=sim.getObjectHandle('Accelerometer_forceSensor')
    _,mass=sim.getObjectFloatParameter(massObject,sim.shapefloatparam_mass)
    uiA=simGetUIHandle('Accelerometer_UI')
    simSetUIButtonLabel(uiA,0,sim.getObjectName(modelBaseA))
    accelCommunicationTube=sim.tubeOpen(0,'accelerometerData'..sim.getNameSuffix(nil),1)

    -- init slope angle of the surface
    setGravety(9.81,0,false)

    --*******************************************
    --*                                         *
    --*              setup ros node             *
    --*                                         *
    --*******************************************

    -- Check if the required ROS plugin is loaded
    moduleName=0
    moduleVersion=0
    index=0
    pluginNotFound=true
    while moduleName do
        moduleName,moduleVersion=sim.getModuleName(index)
        if (moduleName=='ROS') then  --RosInterface
            pluginNotFound=false
        end
        index=index+1
    end
    if (pluginNotFound) then
        sim.displayDialog('Error','The RosInterface was not found.',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
    end

    -- If found then start the subscribers and publishers



    if (not pluginNotFound) then


        -- ************************ publisher  ***************************

        simForcePub = simROS.advertise('/sim_force_topic','std_msgs/Float32MultiArray')-- simulated force topic
        simIMUPub = simROS.advertise('/sim_imu_topic','std_msgs/Float32MultiArray') -- simulated imu topic
        simTimePub = simROS.advertise('/sim_time_topic','std_msgs/Float32MultiArray') -- simulated time topic
        simBodyAngPub = simROS.advertise('/sim_bodyAng_topic','std_msgs/Float32MultiArray') -- body inclination topic
        simWalkingDistPub = simROS.advertise('/sim_walking_dist_topic','std_msgs/Float32MultiArray') -- walking distance topic

        jointPositionsPub=simROS.advertise('/sim_legs_joint_positions','std_msgs/Float32MultiArray') -- leg joint position topic
        jointTorquesPub=simROS.advertise('/sim_legs_joint_torques','std_msgs/Float32MultiArray') -- leg joint torques topic
        jointVelocitiesPub=simROS.advertise('/sim_legs_joint_velocities','std_msgs/Float32MultiArray') -- leg joint velocity topic

        bodyJointPositionsPub=simROS.advertise('/sim_body_joint_positions','std_msgs/Float32MultiArray') -- body joint position topic
        bodyJointVelocitiesPub=simROS.advertise('/sim_body_joint_velocities','std_msgs/Float32MultiArray') -- body joint velocity topic
        bodyJointTorquesPub=simROS.advertise('/sim_body_joint_torques','std_msgs/Float32MultiArray') -- body joint torques topic


        -- ************************ subscriber  ***************************

        bodyOutputSub=simROS.subscribe('/sim_body_topic','std_msgs/Float32MultiArray', 'body_cb') -- body signal
        CPGOutputSub=simROS.subscribe('/cpg_topic','std_msgs/Float32MultiArray', 'cpg_cb') -- cpg signal
        MOTOROutputSub=simROS.subscribe('/motor_topic','std_msgs/Float32MultiArray', 'motor_cb') -- motor topic
        simMOTOROutputSub=simROS.subscribe('/sim_legs_topic','std_msgs/Float32MultiArray', 'sim_motor_cb') -- motor topic
        realIMUSub=simROS.subscribe('/real_imu_topic','std_msgs/Float32MultiArray', 'real_imu_cb') -- confidence inspired concentration


        -- Start the client application (c++ node)

        -- node to run during simulation start
        -- geko_controller -> \slalom : neural control
        -- real_robot -> \realRobot : drive real robot
        -- sim_cot -> \simCOT : calculate COT of simulation robot
        -- real_cot -> \realCOT : calculate COT of real robot

        local rosnode = {'gecko_controller', 'real_robot', 'real_cot'} --'gecko_controller','sim_cot','real_robot', 'real_cot'
        for i = 1,table.getn(rosnode),1 do
            result=sim.launchExecutable(simGetStringParameter(sim_stringparam_scene_path) .. '/../../../projects/slalom/catkin_ws/src/'..rosnode[i]..'/bin/'..rosnode[i],'/cpg_topic',0)
        end

        if (result==false) then
            sim.displayDialog('Error','External ROS-Node not found',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
        end
    end
end


--[[
Actuation: This part will be executed in each simulation step
--]]
function sysCall_actuation()

    t = sim.getSimulationTime()

    for i=1,4,1 do --leg

        for j=1,4,1 do -- joint
            -- start simulation after 10 simulated second
            if(t > 0) then  -- version 1 (t > 10)
                -- sim.setJointPosition(jointHandle[i][j], sim_motorSignal[4*(i-1)+j])
                sim.setJointTargetPosition(jointHandle[i][j], sim_motorSignal[4*(i-1)+j])


                -- fixed body
                -- sim.setJointPosition(jointB1, 0)
                -- sim.setJointPosition(jointB2, 0)
                -- sim.setJointPosition(jointB3, 0)

                -- standing wave
                sim.setJointTargetPosition(jointB1, bodySignal[1])
                sim.setJointTargetPosition(jointB2, bodySignal[2])
                sim.setJointTargetPosition(jointB3, bodySignal[3]) 

            end
        end

        -- get force
        forceData[i] = -10
        if(t > 0) then -- version 1 (t > 10)
            _,force,torque = simReadForceSensor(forceHandle[i])
            forceData[i] = force[3]
        end

        -- get distance
        -- res,dist = simCheckDistance(floor,footHandle[i],1)
        -- distance[i] = math.abs(dist[3]-dist[6])

        -- add force at the foot pad by considering
        -- motorSignal and distance
        -- addforce(footHandle[i],motorSignal[4*(i-1)+2]*0,distance[i],ADHESIVE_FORCE)
    end

    -- publishing topic

    forceTopic['data'] = forceData
    imuTopic['data'] = {accel[1],accel[2],accel[3],gyroData[1],gyroData[2],gyroData[3]}
    simTimeTopic['data'] = {t}
    simBodyAngTopic['data'] = {bodyAng[1],bodyAng[2],bodyAng[3]}

    simROS.publish(simForcePub,forceTopic)
    simROS.publish(simIMUPub,imuTopic)
    simROS.publish(simTimePub,simTimeTopic)
    simROS.publish(simBodyAngPub,simBodyAngTopic)
    --print(imuTopic)


    -- calculate walking distance
    local current_body_position = simGetObjectPosition(body,floor)

    walking_dist = math.abs(current_body_position[2]-body_init_posi[2])

    -- publishing topic

    simWalkingDistTopic['data'] = {walking_dist} 

    simROS.publish(simWalkingDistPub,simWalkingDistTopic)

    -- stop the simulation
    --time = simGetStringParameter
    --if(0) then
        --sim.stopSimulation()
    --end

    -- if(t < 10) then
    --     body_init_posi = simGetObjectPosition(body,floor)
    -- end

    -- if(t>=30) then
    --     simPauseSimulation()
    -- end

end

function sysCall_sensing()

    -- get simulated angluar vilocity and acceleration from imu

    local transformationMatrix=sim.getObjectMatrix(refG,-1)
    local oldInverse=simGetInvertedMatrix(oldTransformationMatrix)
    local m=sim.multiplyMatrices(oldInverse,transformationMatrix)
    local euler=sim.getEulerAnglesFromMatrix(m)
    local currentTime=sim.getSimulationTime()
    local ang=sim.getEulerAnglesFromMatrix(transformationMatrix)

    local dt=currentTime-lastTime
    if (dt~=0) then
        if(realRobot ~= 1) then
            gyroData[1]=euler[1]/dt
            gyroData[2]=euler[2]/dt
            gyroData[3]=euler[3]/dt
        end
    end
    bodyAng[1] = ang[1]*180/3.14159
    bodyAng[2] = ang[2]*180/3.14159
    bodyAng[3] = ang[3]*180/3.14159
    oldTransformationMatrix=sim.copyMatrix(transformationMatrix)
    lastTime=currentTime

    _,accForce=sim.readForceSensor(accSensor)


    if (realRobot ~= 1) then
        accel={accForce[1]/mass,accForce[2]/mass,accForce[3]/mass}
    else
    end

    -- set gravety when robot have adheared to surface
    if currentTime > 1.5 and not gravityChanged then
        gravityChanged = true
        setGravety(9.81,slopeDeg,true)
    end

    position_array  ={  simGetJointPosition(jointHandle[1][1]),simGetJointPosition(jointHandle[1][2]),simGetJointPosition(jointHandle[1][3]),simGetJointPosition(jointHandle[1][4]),
                        simGetJointPosition(jointHandle[2][1]),simGetJointPosition(jointHandle[2][2]),simGetJointPosition(jointHandle[2][3]),simGetJointPosition(jointHandle[2][4]),
                        simGetJointPosition(jointHandle[3][1]),simGetJointPosition(jointHandle[3][2]),simGetJointPosition(jointHandle[3][3]),simGetJointPosition(jointHandle[3][4]),
                        simGetJointPosition(jointHandle[4][1]),simGetJointPosition(jointHandle[4][2]),simGetJointPosition(jointHandle[4][3]),simGetJointPosition(jointHandle[4][4]),
                        }

    velocity_array  ={  simGetJointVelocity(jointHandle[1][1]),simGetJointVelocity(jointHandle[1][2]),simGetJointVelocity(jointHandle[1][3]),simGetJointVelocity(jointHandle[1][4]),
                        simGetJointVelocity(jointHandle[2][1]),simGetJointVelocity(jointHandle[2][2]),simGetJointVelocity(jointHandle[2][3]),simGetJointVelocity(jointHandle[2][4]),
                        simGetJointVelocity(jointHandle[3][1]),simGetJointVelocity(jointHandle[3][2]),simGetJointVelocity(jointHandle[3][3]),simGetJointVelocity(jointHandle[3][4]),
                        simGetJointVelocity(jointHandle[4][1]),simGetJointVelocity(jointHandle[4][2]),simGetJointVelocity(jointHandle[4][3]),simGetJointVelocity(jointHandle[4][4]),
                        }

    torque_array    ={  simGetJointForce(jointHandle[1][1]),simGetJointForce(jointHandle[1][2]),simGetJointForce(jointHandle[1][3]),simGetJointForce(jointHandle[1][4]),
                        simGetJointForce(jointHandle[2][1]),simGetJointForce(jointHandle[2][2]),simGetJointForce(jointHandle[2][3]),simGetJointForce(jointHandle[2][4]),
                        simGetJointForce(jointHandle[3][1]),simGetJointForce(jointHandle[3][2]),simGetJointForce(jointHandle[3][3]),simGetJointForce(jointHandle[3][4]),
                        simGetJointForce(jointHandle[4][1]),simGetJointForce(jointHandle[4][2]),simGetJointForce(jointHandle[4][3]),simGetJointForce(jointHandle[4][4]),
                        }

    body_position_array = { simGetJointPosition(jointB1), simGetJointPosition(jointB2), simGetJointPosition(jointB3) }
    body_velocity_array = { simGetJointVelocity(jointB1), simGetJointVelocity(jointB2), simGetJointVelocity(jointB3) }  -- 2012 is the joint velocity https://www.coppeliarobotics.com/helpFiles/en/objectParameterIDs.htm
    body_torque_array = { simGetJointForce(jointB1), simGetJointForce(jointB2), simGetJointForce(jointB3) }


    simSetGraphUserData(graph,"gyrox",(gyroData[1]))
    simSetGraphUserData(graph,"gyroy",(gyroData[2]))
    simSetGraphUserData(graph,"gyroz",(gyroData[3]))
    simSetGraphUserData(graph,"accex",(accel[1]))
    simSetGraphUserData(graph,"accey",(accel[2]))
    simSetGraphUserData(graph,"accez",(accel[3]))


    simSetGraphUserData(graph,"f_lf",(forceData[1]))
    simSetGraphUserData(graph,"f_rf",(forceData[2]))
    simSetGraphUserData(graph,"f_rh",(forceData[3]))
    simSetGraphUserData(graph,"f_lh",(forceData[4]))
    simSetGraphUserData(graph,"body_x",(bodyAng[1]))
    simSetGraphUserData(graph,"body_y",(bodyAng[2]))

    simSetGraphUserData(graph,"stab",stab)
    simSetGraphUserData(graph,"harm",harm)

    simSetGraphUserData(graph,"cpg_sig1",cpgSignal[1])
    simSetGraphUserData(graph,"cpg_sig2",cpgSignal[2])
    simSetGraphUserData(graph,"cpg_sig3",cpgSignal[3])
    -- simSetGraphUserData(graph,"cpg_sig4",cpgSignal[4])

    simSetGraphUserData(graph_motor,"cpg_sig1",cpgSignal[1])
    simSetGraphUserData(graph_motor,"cpg_sig2",cpgSignal[2])
    simSetGraphUserData(graph_motor,"m2_v",simGetJointVelocity(jointHandle[1][2]))
    simSetGraphUserData(graph_motor,"m6_v",simGetJointVelocity(jointHandle[2][2]))
    simSetGraphUserData(graph_motor,"m10_v",simGetJointVelocity(jointHandle[3][2]))
    simSetGraphUserData(graph_motor,"m14_v",simGetJointVelocity(jointHandle[4][2]))
    
    
    -- local l_lf={pos_traj_lf[1],pos_traj_lf[2],pos_traj_lf[3]}
    -- pos_traj_lf=simGetObjectPosition(traj_lf,-1)
    -- l_lf[4]=pos_traj_lf[1]
    -- l_lf[5]=pos_traj_lf[2]
    -- l_lf[6]=pos_traj_lf[3]
    -- simAddDrawingObjectItem(draw_traj_lf,l_lf)

    -- local l_b1={pos_traj_b1[1],pos_traj_b1[2],pos_traj_b1[3]}
    -- pos_traj_b1=simGetObjectPosition(traj_b1,-1)
    -- l_b1[4]=pos_traj_b1[1]
    -- l_b1[5]=pos_traj_b1[2]
    -- l_b1[6]=pos_traj_b1[3]
    -- simAddDrawingObjectItem(draw_traj_b1,l_b1)

    -- local l_b2={pos_traj_b2[1],pos_traj_b2[2],pos_traj_b2[3]}
    -- pos_traj_b2=simGetObjectPosition(traj_b2,-1)
    -- l_b2[4]=pos_traj_b2[1]
    -- l_b2[5]=pos_traj_b2[2]
    -- l_b2[6]=pos_traj_b2[3]
    -- simAddDrawingObjectItem(draw_traj_b2,l_b2)

    -- local l_b3={pos_traj_b3[1],pos_traj_b3[2],pos_traj_b3[3]}
    -- pos_traj_b3=simGetObjectPosition(traj_b3,-1)
    -- l_b3[4]=pos_traj_b3[1]
    -- l_b3[5]=pos_traj_b3[2]
    -- l_b3[6]=pos_traj_b3[3]
    -- simAddDrawingObjectItem(draw_traj_b3,l_b3)

    -- simSetGraphUserData(g_leg_traj,"cpg_sig1",cpgSignal[1])
    -- simSetGraphUserData(graph_leg_traj,"traj_lf_y",pos_traj_lf[2])
    -- simSetGraphUserData(graph_leg_traj,"traj_lf_z",pos_traj_lf[3])

    

    --simSetGraphUserData(graph,"jb1",cpgSignal[1]*-0.2)
    --simSetGraphUserData(graph,"jb2",cpgSignal[1]*-0.2)
    --simSetGraphUserData(graph,"jb3",cpgSignal[1]*-0.2)

    -- simSetGraphUserData(graph,"jb1",bodySignal[1])
    -- simSetGraphUserData(graph,"jb2",bodySignal[2])
    -- simSetGraphUserData(graph,"jb3",bodySignal[3])

    -- simSetGraphUserData(graph,"s_jb1",bodySignal[4])
    -- simSetGraphUserData(graph,"s_jb2",bodySignal[5])
    -- simSetGraphUserData(graph,"s_jb3",bodySignal[6])

    -- simSetGraphUserData(graph,"M_sig0",motorSignal[1])
    -- simSetGraphUserData(graph,"M_sig1",motorSignal[2])
    -- simSetGraphUserData(graph,"M_sig2",motorSignal[3])
    -- simSetGraphUserData(graph,"M_sig3",motorSignal[4])
    -- simSetGraphUserData(graph,"M_sig4",motorSignal[5])
    -- simSetGraphUserData(graph,"M_sig5",motorSignal[6])
    -- simSetGraphUserData(graph,"M_sig6",motorSignal[7])
    -- simSetGraphUserData(graph,"M_sig7",motorSignal[8])
    -- simSetGraphUserData(graph,"M_sig8",motorSignal[9])
    -- simSetGraphUserData(graph,"M_sig9",motorSignal[10])
    -- simSetGraphUserData(graph,"M_sig10",motorSignal[11])
    -- simSetGraphUserData(graph,"M_sig11",motorSignal[12])
    -- simSetGraphUserData(graph,"M_sig12",motorSignal[13])
    -- simSetGraphUserData(graph,"M_sig13",motorSignal[14])
    -- simSetGraphUserData(graph,"M_sig14",motorSignal[15])
    -- simSetGraphUserData(graph,"M_sig15",motorSignal[16])
    -- print(bodyAng)

    simROS.publish(jointPositionsPub,{data=position_array})
    simROS.publish(jointVelocitiesPub,{data=velocity_array})
    simROS.publish(jointTorquesPub,{data=torque_array})
    simROS.publish(bodyJointPositionsPub,{data=body_position_array})
    simROS.publish(bodyJointVelocitiesPub,{data=body_velocity_array})
    simROS.publish(bodyJointTorquesPub,{data=body_torque_array})

end

function sysCall_cleanup()
    -- do some clean-up here
    walking_dist = math.abs(simGetObjectPosition(body,floor)[2]-body_init_posi[2])
    t = sim.getSimulationTime()
    print("walking distance " .. walking_dist .. " m")
    print("average speed of this test case is " .. walking_dist/(t) .. " m/s")

    simROS.shutdownSubscriber(bodyOutputSub)
    simROS.shutdownSubscriber(CPGOutputSub)
    simROS.shutdownSubscriber(MOTOROutputSub)
    simROS.shutdownSubscriber(simMOTOROutputSub)
    simROS.shutdownSubscriber(realIMUSub)

    simROS.shutdownPublisher(jointTorquesPub)
    simROS.shutdownPublisher(jointVelocitiesPub)
    simROS.shutdownPublisher(jointPositionsPub)
    simROS.shutdownPublisher(bodyJointTorquesPub)
    simROS.shutdownPublisher(bodyJointVelocitiesPub)
    simROS.shutdownPublisher(bodyJointPositionsPub)
    simROS.shutdownPublisher(simWalkingDistPub)

    -- run file manager script
    -- os.execute ("/home/happy/Slalom_body_wave/controllers/slalom/manager.sh")
    os.execute("rosnode kill /slalom")
    -- os.execute("rosnode kill /simCOT")
    os.execute("rosnode kill /realCOT")
    os.execute("rosnode kill /realRobot")

end
//*******************************************
//*                                         *
//*        main cost of transport           *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// date: 26/10/2020
// version: 4.0.0

// c++ lib
#include <cmath>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <array>
#include <vector>
#include <string>
#include <chrono>

using namespace std;
using namespace std::chrono;

// standard ros lib
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>
#include <sensor_msgs/JointState.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Int32.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"


#include "realCOT_data_logger.h"

//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10           // reflesh rate of ros
#define DATA_LOGGER 1     // logging data or not

//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray energySignal;
// TODO add new dynamixel
sensor_msgs::JointState joint_state_msg;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************
// TODO add value feedback of joints
vector<float> jointPositions;
vector<float> jointVelocities;
vector<float> jointCurrent;

// global variable
const int numMotor = 19;                  // number of motors 
array<float, numMotor> currentSig;        // current feedback from real robot
array<float, numMotor> voltageSig;        // voltage feedback from real robot
array<float, numMotor> torqueSig;         // torque feedback from real robot (Nm)
array<float, numMotor> velocitySig;       // velocity feedback from real robot (rad/s)
float sim_time = 0;                       // simulation time (sec)

float Energy_In = 0;                      // energy (J)
float Energy_Out = 0;                     // energy (J)
float Energy_In_CK = 0;                   // energy (J)  -for checking
float Energy_Out_CK = 0;                  // energy (J)  -for checking
float previousTime = 0;                   // time (sec)
float timeChange = 0;                     // dulation (sec)

float Current_I[numMotor] = {};           // robot use current (mA)
float Voltage_V[numMotor] = {};           // (v)
// float Torque_t[numMotor] = {};
// float Velocity_v[numMotor] = {};

float Power_In[numMotor] = {};            // power (watt), this is the power that robot consumed for producing work
float Power_Out[numMotor] = {};           // power (watt), this is the power that robot can work.

//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************
/*
void currentCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      currentSig[i] = *it;
      i++;
    }
    return;
}


void voltageCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      voltageSig[i] = *it;
      i++;
    }
    return;
}


void torqueCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      torqueSig[i] = *it;
      i++;
    }
    return;
}


void velocityCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      velocitySig[i] = *it;
      i++;
    }
    return;
}
*/

// TODO happ commented, changed to real time base
// /*
void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      sim_time = *it;
    }
    return;
}
// */

// TODO add new dynamixel feeback
void jointStatesCallback(const sensor_msgs::JointState& msg)
{
    joint_state_msg = msg;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){

    // create ros node
    std::string nodeName("realCOT");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("realCOT node just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);


    // ------------- real time ---------------------- //

    // chrono::steady_clock sc;      // create an object of `steady_clock` class
    // auto start = sc.now();        // start timer
    // auto previousEnd = start;
    steady_clock::time_point realtime_start = steady_clock::now();
    steady_clock::time_point realtime_previous = steady_clock::now();

    // cot real time
    steady_clock::time_point cot_time_now = steady_clock::now();
    steady_clock::time_point cot_previous_time = steady_clock::now();


    // data logger
    #if DATA_LOGGER
      RealCOT_data_logger *logger_cot;
      logger_cot = new RealCOT_data_logger("cotlog.txt");

      RealCOT_data_logger *logger_current;
      logger_current = new RealCOT_data_logger("currentlog.txt");
      // RealCOT_data_logger logger_voltage("voltagelog.txt");
    #endif


    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    ros::Publisher outputEnergy;
    outputEnergy = node.advertise<std_msgs::Float32MultiArray>("/energy_topic",1);

    // ros::Subscriber simCorrentSub = node.subscribe("/morf_hw/joint_current",10, currentCB);
    // ros::Subscriber simVoltageSub = node.subscribe("/morf_hw/joint_inputVoltage",10, voltageCB);
    // ros::Subscriber simTorqueSub = node.subscribe("/morf_hw/joint_torques",10, torqueCB);
    // ros::Subscriber simVolocitySub = node.subscribe("/morf_hw/joint_velocities",10, velocityCB);
    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);                   // TODO happ commented, changed to real time base
    
    // TODO add new dynamixel feedback
    ros::Subscriber jointStateSub = node.subscribe("/db_dynamixel_ROS_driver/hexapod_joint_feedback", 1, jointStatesCallback);


    float Power_In_Sum = 0;
    float Power_Out_Sum = 0;

    float dynamixelVoltage = 12.0;    // (v)

    while(ros::ok())
    {
        //*******************************************
        //*                                         *
        //*              ros parameter              *
        //*                                         *
        //*******************************************

        // TODO add new dynamixel feedback
        jointPositions.clear();
        jointVelocities.clear();
        jointCurrent.clear();

        int index = 0;
        for(string name : joint_state_msg.name)
        {
          jointPositions.push_back(joint_state_msg.position[index]);            // (rad)
          jointVelocities.push_back(joint_state_msg.velocity[index]);           // (rad/s)
          jointCurrent.push_back(joint_state_msg.effort[index]);                // (mA)
          index++;
        }

        timeChange = sim_time - previousTime;
        steady_clock::time_point cot_time_now = steady_clock::now();
        duration<double> cot_time_change = duration_cast<duration<double>>(cot_time_now - cot_previous_time);
        

        // if(cot_time_change.count() > 0.25)            // compute every 0.25 sec (real time base)
        if(timeChange > 0.25)                      // compute every 0.25 sec (sim time base)
        {
          int ip = 0;
          for(vector<float>::const_iterator it = jointCurrent.begin(); it != jointCurrent.end(); ++it)
          {
            // collected the values of current
            Current_I[ip] = *it;       // (mA)

            // collected the values of power in for each motor
            // power_in mean the power that motor consume 
            Power_In[ip] = abs(*it/1000) * dynamixelVoltage;                    // Watt ((mA/1000)*volt)

            // sum of power for all motor
            Power_In_Sum = Power_In_Sum + (abs(*it/1000) * dynamixelVoltage);   // Watt ((mA/1000)*volt)
            
            ip++;
          }
          previousTime = sim_time;           // sim time base
          // cot_previous_time = cot_time_now;     // real time base
          
          // std::cout << "It took me " << cot_time_change.count() << " seconds.";
          // std::cout << std::endl;
        }

        int ie = 0;
        for(vector<float>::const_iterator it = jointCurrent.begin(); it != jointCurrent.end(); ++it)
        {
          // calculate energy in from each motor power
          Energy_In = Energy_In + (Power_In[ie] * timeChange);                             // Joule (Watt*sec)   (sim time base)
          // Energy_In = Energy_In + (Power_In[ie] * cot_time_change.count());                   // Joule (Watt*sec)   (real time base)

          ie++;
        }

        // calculate energy in from sum of power (CHECKER)
        Energy_In_CK = Energy_In_CK + (Power_In_Sum * timeChange);                         // Joule (Watt*sec)   (sim time base)
        // Energy_In_CK = Energy_In_CK + (Power_In_Sum * cot_time_change.count());               // Joule (Watt*sec)   (real time base)


        //----------------- Old Version of Energy Calculation ------------------------------------------
        //----------------------------------------------------------------------------------------------
        /*
        timeChange = sim_time - previousTime;

        if(timeChange > 0.25)  // compute energy every 0.25 sec  
        {
          for(int i = 0; i < torqueSig.size() ; i++)
          {
            // collected the values of current and voltage
            Current_I[i] = currentSig[i];       // (mA)
            Voltage_V[i] = voltageSig[i];       // (v)

            // collected the values of power in/out for each motor
            Power_In[i]  = (abs(currentSig[i]/1000) * abs(voltageSig[i]));     // Watt ((mA/1000)*volt)
            Power_Out[i] = (abs(torqueSig[i]) * abs(velocitySig[i]));          // Watt (Nm*rad/s)

            // sum of power for all motors
            Power_In_Sum  = Power_In_Sum + (abs(currentSig[i]/1000) * abs(voltageSig[i]));     // Watt ((mA/1000)*volt)
            Power_Out_Sum = Power_Out_Sum + (abs(torqueSig[i]) * abs(velocitySig[i]));         // Watt (Nm*rad/s)
          }
          previousTime = sim_time;
        }
        
        for(int i = 0; i < torqueSig.size() ; i++)
        {
          // calculate energy in/out from each motor power 
          Energy_In  = Energy_In  + (Power_In[i]  * timeChange);              // milliJoule (Watt*Sec)
          Energy_Out = Energy_Out + (Power_Out[i] * timeChange);              // Joule (Watt*Sec)
        }

        // calculate energy in/out from sum of power (CHECKER)
        Energy_In_CK  = Energy_In_CK  + (Power_In_Sum  * timeChange);            // milliJoule (Watt*Sec)
        Energy_Out_CK = Energy_Out_CK + (Power_Out_Sum * timeChange);            // Joule (Watt*Sec)
        */


        // ------------- real time ---------------------- //

        // auto end = sc.now();       // end timer (starting & ending is done by measuring the time at the moment the process started & ended respectively)
        // auto time_span  = static_cast<chrono::duration<double>>(end - start);         // measure time span between start & end
        // auto time_delta = static_cast<chrono::duration<double>>(end - previousEnd);   // measure delta time
        // auto previousEnd = end;
        // cout<<"Operation took: "<<time_span.count()<<" seconds !!!";
        steady_clock::time_point realtime_now = steady_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(realtime_now - realtime_start);
        duration<double> time_delta = duration_cast<duration<double>>(realtime_now - realtime_previous);
        realtime_previous = realtime_now;


        #if DATA_LOGGER
          // use data logger for analize cpg and pcpg

          // --- sim time base
          logger_cot->cot_logs("cotlog.txt", sim_time, time_span.count(), Power_In_Sum, Power_Out_Sum, Energy_In, Energy_Out, Energy_In_CK, Energy_Out_CK);

          logger_current->current_logs("currentlog.txt", sim_time, time_span.count(), Current_I[0],  Current_I[1],  Current_I[2],  Current_I[3],  Current_I[4],  Current_I[5],  Current_I[6],  Current_I[7],  Current_I[8], Current_I[9],
                                                         Current_I[10], Current_I[11], Current_I[12], Current_I[13], Current_I[14], Current_I[15], Current_I[16], Current_I[17], Current_I[18], Current_I[19], Current_I[20]);

          // --- real time base
          /*
          logger_cot->cot_logs("cotlog.txt", time_span.count(), time_delta.count(), Power_In_Sum, Power_Out_Sum, Energy_In, Energy_Out, Energy_In_CK, Energy_Out_CK);

          logger_current->current_logs("currentlog.txt", time_span.count(), time_delta.count(), Current_I[0],  Current_I[1],  Current_I[2],  Current_I[3],  Current_I[4],  Current_I[5],  Current_I[6],  Current_I[7],  Current_I[8], Current_I[9],
                                                         Current_I[10], Current_I[11], Current_I[12], Current_I[13], Current_I[14], Current_I[15], Current_I[16], Current_I[17], Current_I[18], Current_I[19], Current_I[20]);

          */
          // logger_voltage.voltage_logs("voltagelog.txt", sim_time, Voltage_V[0],  Voltage_V[1],  Voltage_V[2],  Voltage_V[3],  Voltage_V[4],  Voltage_V[5],  Voltage_V[6],  Voltage_V[7],  Voltage_V[8], Voltage_V[9],
          //                                                         Voltage_V[10], Voltage_V[11], Voltage_V[12], Voltage_V[13], Voltage_V[14], Voltage_V[15], Voltage_V[16], Voltage_V[17], Voltage_V[18]);
        #endif
        

        // clear power in/out
        Power_In_Sum = 0;
        Power_Out_Sum = 0;


        energySignal.data.clear();
        energySignal.data.push_back(Energy_In);
        energySignal.data.push_back(Energy_Out);
        outputEnergy.publish(energySignal);

        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok

    delete logger_cot;
    delete logger_current;
    return 0;
}

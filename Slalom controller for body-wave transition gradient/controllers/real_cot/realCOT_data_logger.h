//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// date: 26/10/2020
// version: 4.0.0

#ifndef REALCOT_DATA_LOGGER_H
#define REALCOT_DATA_LOGGER_H

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

class RealCOT_data_logger
{
public:

  // constructor
  RealCOT_data_logger(string file_name);

  // destructor
  ~RealCOT_data_logger();

  // cot logger
  float cot_logs(string file_name,
                                  float time_sim,    float time_real,
                                  float power_in,     float power_out, 
                                  float energy_in,    float energy_out,
                                  float energy_in_ck, float energy_out_ck);

  // current logger
  float current_logs(string file_name, float time_sim, float time_real,
                                       float i_m1,  float i_m2,  float i_m3,  float i_m4,  float i_m5,  float i_m6,  float i_m7,  float i_m8,  float i_m9, 
                                       float i_m10, float i_m11, float i_m12, float i_m13, float i_m14, float i_m15, float i_m16, float i_m17, float i_m18,
                                       float i_m19, float i_m20, float i_m21);

  // voltage logger
  // float voltage_logs(string file_name, float time,
  //                                 float v_m1, float v_m2, float v_m3, float v_m4, float v_m5, float v_m6, float v_m7, float v_m8, float v_m9, 
  //                                 float v_m10, float v_m11, float v_m12, float v_m13, float v_m14, float v_m15, float v_m16, float v_m17, float v_m18, float v_m19);

private:
  string file_dir;
  char buffer[80];

};

#endif  //COT_LOGGER_H

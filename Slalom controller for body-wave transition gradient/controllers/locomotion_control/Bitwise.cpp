//*******************************************
//*                                         *
//*       Bitwise operation                 *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// data: 11/05/2022
// version: 1.0.0

#include "Bitwise.h"

// initialize bitwise
Bitwise::Bitwise()
{
    // initial class attributes
    this->arrSize = 0;
    this->step = 0;
}

// set the size of array.
void Bitwise::setParams(int arrSize){

    this->arrSize = arrSize;
    this->bits_ptr = new int[arrSize];
    for(int i=0; i<this->arrSize; i++)
    {
        this->bits_ptr[i] = 0;
    }
}

// put the signal in the array
void Bitwise::write(int data){
    this->bits_ptr[this->step] = data;
}

/* this is debugging function to print out
    the data in the array */
void Bitwise::printArrBits(int indx)
{
    cout << this->bits_ptr[indx] << '\t' << indx << endl;
}

// private method
    // special modulation function
int Bitwise::mod(int x,int m){
    return x < 0 ? x+m : x;
}

// get data from array
int Bitwise::read(int data)
{
    int y = this->bits_ptr[this->mod(this->step - data, this->arrSize)];
    return y;
}

// update the delay line
void Bitwise::run(){
    this->step += 1;
    if(((this->step) % (this->arrSize)) == 0){
        this->step = 0;
    }
}
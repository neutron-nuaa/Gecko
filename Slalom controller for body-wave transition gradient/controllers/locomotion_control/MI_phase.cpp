//*******************************************
//*                                         *
//*         flex body controller            *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 26/05/2022
// version: 1.0.0

#include "MI_phase.h"

MI_phase::MI_phase()
{
    // MI 0.12 (0.16 Hz)


    // MI 0.18 (0.33 Hz)  
    this->MI18_PH0   = {0, 0, 0};       // 0   %Trav 
    this->MI18_PH20  = {0, 1, 2};       // 16  %Trav
    this->MI18_PH40  = {0, 3, 4};       // 33  %Trav
    this->MI18_PH60  = {0, 5, 6};       // 50 %Trav normal {0, 5, 6}; (AIS paper used {0, 5, 7})
    this->MI18_PH80  = {0, 6, 8};       // 66  %Trav
    this->MI18_PH100 = {0, 8, 10};      // 83  %Trav
    this->MI18_PH120 = {0, 10, 12};     // 100 %Trav

    // MI 0.24 (0.47 Hz)
    this->MI24_PH0   = {0, 0, 0};       
    this->MI24_PH20  = {0, 1, 2};
    this->MI24_PH40  = {0, 2, 3};
    this->MI24_PH60  = {0, 4, 5};
    this->MI24_PH80  = {0, 5, 7};
    this->MI24_PH100 = {0, 7, 8};
    this->MI24_PH120 = {0, 8, 10};

    // MI 0.30 (0.58 Hz)
    this->MI30_PH0   = {0, 0, 0};
    this->MI30_PH20  = {0, 1, 2};
    this->MI30_PH40  = {0, 2, 3};
    this->MI30_PH60  = {0, 3, 4};
    this->MI30_PH80  = {0, 4, 6};
    this->MI30_PH100 = {0, 6, 7};
    this->MI30_PH120 = {0, 7, 8};
}
//*******************************************
//*                                         *
//*         flex body controller            *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com,
// data: 23/05/2019
// version: 1.0.0

#include "Flex_body_controller.h"

Flex_body_controller::Flex_body_controller()
{
  // initial class attributes
  this->A1 = 0.0050;
  this->A2 = 0.0082;
  this->A3 = 0.0047;
  this->T1 = (0.1124+0.0922+0.0950)/3;    //0.1124;
  this->T2 = (0.1124+0.0922+0.0950)/3;    //0.0922;
  this->T3 = (0.1124+0.0922+0.0950)/3;    //0.0950;
  this->GainA = 50;
  this->GainT = 100;
  this->Phs = 0.00;
  this->Ph1 = 5.78 + this->Phs;   // pi-1.2
  this->Ph2 = 0.80 + this->Phs;
  this->Ph3 = 1.94 + this->Phs;
}

float Flex_body_controller::flex_body_wave(int bd_joint, float t)
{
  if(bd_joint == 1)
  {
    return this->GainA*this->A1*sin( (2*3.14/(this->T1*this->GainT))*t + this->Ph1);
  }
  else if(bd_joint == 2)
  {
    return this->GainA*this->A2*sin( (2*3.14/(this->T2*this->GainT))*t + this->Ph2);
  }
  else if(bd_joint == 3)
  {
    return this->GainA*this->A3*sin( (2*3.14/(this->T3*this->GainT))*t + this->Ph3);
  }
return 0;
}

float Flex_body_controller::mapping_mi_delay(int bd_joint, float mi)
{
  if(bd_joint == 1)
  {
    return 14;
  }
  else if(bd_joint == 2)
  {
    return 19;
  }
  else if(bd_joint == 3)
  {
    return 32;
  }

return 0;
}

//*******************************************
//*                                         *
//*       MI and Phase                      *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// data: 26/05/2022
// version: 1.0.0

#ifndef MI_PHASE_H
#define MI_PHASE_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdint.h>
#include <vector>

using namespace std;

class MI_phase {
public:

    MI_phase();

    // MI 0.18
    vector<float> MI18_PH0 ;
    vector<float> MI18_PH20;
    vector<float> MI18_PH40;
    vector<float> MI18_PH60;
    vector<float> MI18_PH80;
    vector<float> MI18_PH100;
    vector<float> MI18_PH120;

    // MI 0.24
    vector<float> MI24_PH0 ;
    vector<float> MI24_PH20;
    vector<float> MI24_PH40;
    vector<float> MI24_PH60;
    vector<float> MI24_PH80;
    vector<float> MI24_PH100;
    vector<float> MI24_PH120;

    // MI 0.30
    vector<float> MI30_PH0 ;
    vector<float> MI30_PH20;
    vector<float> MI30_PH40;
    vector<float> MI30_PH60;
    vector<float> MI30_PH80;
    vector<float> MI30_PH100;
    vector<float> MI30_PH120;

private:

};



#endif

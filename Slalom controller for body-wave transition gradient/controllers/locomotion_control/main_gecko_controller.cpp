//*******************************************
//*                                         *
//*        main geckobot controller         *
//*                                         *
//*******************************************
// author: worasuchad haomachai
// contract: haomachai@gmail.com
// update: 10/05/2022
// version: 2.0.0

//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************
// generate motor signal from open-loop modular neural control
// the leg and body motor signal are seperated for controlling
// a controller is desinged for running mix body patterns with simple leg trajectory

// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>

// modular robot controller library
#include "Basic_cpg_controller.h"
#include "Delay_line.h"
#include "Flex_body_controller.h"
#include "Data_logger.h"
#include "Bitwise.h"
#include "MI_phase.h"
// #include "Basic_pcpg_controller.h"
// #include "Pcpg_controller.h"
// #include "VRN.h"


//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define DATA_LOGGER true        // logging data or not
#define DELAY_LINE false        // drive motor signal by delay line or not

#define RATE 10                 // reflesh rate of ros
// #define GAIT_CHANGE_TIME 20     // time when MI is changed from CPG_MI to CPG_MI2

// cpg parameters
#define CPG_OUTPUT 0.01
#define CPG_BIAS 0.0
#define CPG_W11_22 1.4
#define CPG_WD1 0.18
#define CPG_MI 0.18
// #define CPG_MI2 0.30

//**    MI used in experiments              *
//* 0.08, 0.12, 0.18, 0.22, 0.26 (MI)       *
//* 0.10, 0.15, 0.19, 0.22, 0.25 (Hz)       *

//**    In case of using delay line         *
//* MI = 0.04 for wave gait                 *
//* MI = 0.08 for new wave gait             *
//* MI = 0.12 for trot gait                 *

/*
// vrn parameters
#define VRN_OUTPUT 0.01
#define VRN_W1358 1.7246
#define VRN_W2467 -1.7246
#define VRN_W9_10 0.5
#define VRN_W11_12 -0.5
#define VRN_BIAS -2.48285
#define VRN_INPUTY_1 1
#define VRN_INPUTY_2 -1

// basic pcpg parameters
#define B_PCPG_THRES_1 0.60
#define B_PCPG_THRES_2 0.85

// pcpg parameters
#define PCPG_THRES 0        // 0.77, 0.87 // control upward slope of pcpg signal
#define PCPG_1_SLOPE 2      // 2, 20.     // control the downward slope of pcpg signal
#define PCPG_2_SLOPE 2      // 20, 2.
#define PCPG_P_SLOPE 200    // 200
#define PCPG_P_THRES 0.85   // 0.85
*/

// motor neuron parameter
#define SIGNAL_AMP 0.1      // amplitude of step length
#define LIFT_AMP 0.15       // amplitute of foot lifting
#define LEFT_GAIN 1         // relative step length
#define GAMMA 1             // pealing gain

// delayline parameters
#define DELAYSIZE 80        // 80 size of the delay
// #define DELAY_RH 0          // 0 delay for right hind leg
// #define DELAY_LH 20         // 20 delay for left hind leg
// #define DELAY_LF 40         // 40 delay for left front leg
// #define DELAY_RF 60         // 60 delay for right front leg
// #define DELAY_PEEL 40       // 40 


// bitwise parameters
#define BITSIZE 5


//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray cpgSignal;
std_msgs::Float32MultiArray motorSignal;
std_msgs::Float32MultiArray sim_motorSignal;
std_msgs::Float32MultiArray sim_bodySignal;
// std_msgs::Float32MultiArray multiJointCommandSignal;

//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable

// body amplitude gain
float BODY_AMP_GAIN = 0.3;          

// 0.3 used for paper IROS2021 
// 0.4 used for paper IROS2023 gradient transition(only sim) 
// (0.3352f, 0.5497f, 0.3150f) for paper AIS open-loop control



float motorSig[19] = {0};           // array store motor signal
float sim_motorSig[16] = {0};       // array store joint signal
float freezeSignal = 0;             // freeze or update

float c1 = 0;                       // cpg signals
float c2 = 0;
float c1_old = 0;
float c2_old = 0;
float delta_c1 = 0;
float delta_c2 = 0;

/*
float v1 = 0;                       // vrn signals
float v2 = 0;

float pc1 = 0;                      // pcpg signals
float pc2 = 0;
float pcp = 0;
float b_pcpg_11 = 0;
float b_pcpg_12 = 0;
float b_pcpg_21 = 0;
float b_pcpg_22 = 0;
*/

float c_bodyJ1 = 0;                 // body signal
float c_bodyJ2 = 0;
float c_bodyJ3 = 0;
float s_bodyJ1 = 0;
float s_bodyJ2 = 0;
float s_bodyJ3 = 0;
float standing_wave_J1 = 0;
float standing_wave_J2 = 0;
float standing_wave_J3 = 0;

float sim_time = 0;                 // simulation time


bool bit_bool = true; 
bool bit_bool_old = true;
int bit_arr[5] = {0,0,0,0,0};       // bitwise values
int bit_data = 0;
int bit_logic = 0;
int bit_present = 0;
int bit_present_old = 0;
int stride = 0;


int phase_bj1 = 0;                  // phase shift of body joints
int phase_bj2 = 0;
int phase_bj3 = 0;


// sim motor direction
float sim_motorDirection[16] = {1,1,1,1,
                                1,1,1,1,
                                1,1,1,1,
                                1,1,1,1};

const float sim_motorInitPos[16] =  {0,-0.8, 0, 0.78,
                                     0,-0.8, 0, 0.78,
                                     0,-0.8, 0, 0.78,
                                     0,-0.8, 0, 0.78};  // 0,-0.8, 0, 0.78,


//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************

void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        sim_time = *it;
        i++;
    }
    return;
}

void freezeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        freezeSignal = *it;
        i++;
    }
    return;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node called slalom
    std::string nodeName("slalom");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    // create publisher and subscriber
    ros::Publisher outputCPG;
    outputCPG = node.advertise<std_msgs::Float32MultiArray>("/cpg_topic",1);

    ros::Publisher outputMOTOR;
    outputMOTOR = node.advertise<std_msgs::Float32MultiArray>("/motor_topic",1);

    ros::Publisher outputSIMMOTOR;
    outputSIMMOTOR = node.advertise<std_msgs::Float32MultiArray>("/sim_legs_topic",1);

    ros::Publisher outputSIMBODY;
    outputSIMBODY = node.advertise<std_msgs::Float32MultiArray>("/sim_body_topic",1);

    // ros::Publisher outputMultiJointCommand;
    // outputMultiJointCommand = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",1);

    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);
    ros::Subscriber freezeSub = node.subscribe("/freezeSignal_topic",10,freezeCB);


    // data logger
    #if DATA_LOGGER
      Data_logger *logger_cpg;
      logger_cpg = new Data_logger("cpglog.txt");

      Data_logger *logger_motor;
      logger_motor = new Data_logger("motorlog.txt");

      // Data_logger logger_cpg("cpglog.txt");
      // Data_logger logger_motor("motorlog.txt");
      // Data_logger logger_flex_body("flexbodylog.txt");
    #endif


    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************

    // MI Phase
    MI_phase mi_phase;

    // bitwise is for counting number of stride according to cpg circle
    Bitwise bitwise_oper;
    bitwise_oper.setParams(BITSIZE);

    // basic cpg
    Basic_cpg_controller basic_cpg;
    basic_cpg.setParameter(CPG_OUTPUT,CPG_BIAS,CPG_W11_22,CPG_WD1,CPG_MI);

    /*
    // vrn
    VRN vrn1;
    vrn1.setParameter(VRN_OUTPUT,VRN_INPUTY_1,VRN_W1358,VRN_W2467,VRN_W9_10,VRN_W11_12,VRN_BIAS);

    VRN vrn2;
    vrn2.setParameter(VRN_OUTPUT,VRN_INPUTY_2,VRN_W1358,VRN_W2467,VRN_W9_10,VRN_W11_12,VRN_BIAS);


    // basic pcpg
    Basic_pcpg_controller basic_pcpg_1;
    basic_pcpg_1.setParameter(B_PCPG_THRES_1);

    Basic_pcpg_controller basic_pcpg_2;
    basic_pcpg_2.setParameter(B_PCPG_THRES_2);


    // pcpg
    Pcpg_controller pcpg_1;
    pcpg_1.setParameter(PCPG_1_SLOPE,PCPG_THRES);

    Pcpg_controller pcpg_2;
    pcpg_2.setParameter(PCPG_2_SLOPE,PCPG_THRES);

    Pcpg_controller pcpg_peel;
    pcpg_peel.setParameter(PCPG_P_SLOPE,PCPG_P_THRES);
    */


    // create delay line for each joint of leg
    // Delay_line peeling_line;
    // peeling_line.setParameter(DELAYSIZE);

    Delay_line joint0_delay;
    joint0_delay.setParameter(DELAYSIZE);

    Delay_line joint1_delay;
    joint1_delay.setParameter(DELAYSIZE);

    Delay_line joint2_delay;
    joint2_delay.setParameter(DELAYSIZE);

    Delay_line joint3_delay;
    joint3_delay.setParameter(DELAYSIZE);


    // create delay line for each body joint of body 
    Delay_line body_j1_delay;
    body_j1_delay.setParameter(DELAYSIZE);

    Delay_line body_j2_delay;
    body_j2_delay.setParameter(DELAYSIZE);

    Delay_line body_j3_delay;
    body_j3_delay.setParameter(DELAYSIZE);

    /*
    // flex body signal
    Flex_body_controller flex_body;

    // sine body delay
    Delay_line body_sin_j1_delay;
    body_sin_j1_delay.setParameter(DELAYSIZE);

    Delay_line body_sin_j2_delay;
    body_sin_j2_delay.setParameter(DELAYSIZE);

    Delay_line body_sin_j3_delay;
    body_sin_j3_delay.setParameter(DELAYSIZE);
    */

    // update or freeze cpg signal
    bool updateCondition = true;                 
    // float pcpg_p[2] = {0};

    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*               neural control            *
        //*                                         *
        //*******************************************

        // if((sim_time > GAIT_CHANGE_TIME) && (GAIT_CHANGE_TIME != 0))
        // {
        //     basic_cpg.setMI(CPG_MI2);
        // }

        // freeze the robot
        if(freezeSignal >= 1)
        {
            updateCondition = false;
        }else{
            updateCondition = true;
        }

        // body singal
        if(updateCondition)
        {
          sim_bodySignal.data.clear();
        //   multiJointCommandSignal.data.clear();
        }

        // cpg signal
        if(updateCondition)
        {
            basic_cpg.run();
        }
        cpgSignal.data.clear();
        c1 = basic_cpg.getSignal(1);
        c2 = basic_cpg.getSignal(2);

        /*
        // vrn signal
        if(updateCondition)
        {
            vrn1.run(c2);
            vrn2.run(c2);
        }
        v1 = vrn1.getSignal();
        v2 = vrn2.getSignal();

        // pcpg and basic pcpg signal
        if(updateCondition)
        {
            pcpg_1.run(c1,c2);
            pcpg_2.run(c1,c2);
            pcpg_peel.run(c1,c2);

            basic_pcpg_1.run(c1,-c1);
            basic_pcpg_2.run(c2,-c2);

        }
        pc1 = pcpg_1.getSignal(1);
        pc2 = pcpg_2.getSignal(2);

        b_pcpg_11 = basic_pcpg_1.getSignal(1);
        b_pcpg_12 = basic_pcpg_1.getSignal(2);

        b_pcpg_21 = basic_pcpg_2.getSignal(1);
        b_pcpg_22 = basic_pcpg_2.getSignal(2);
        */

        //*******************************************
        //*                                         *
        //*             legs driver                 *
        //*    drive with or without delay line     *
        //*******************************************
        #if DELAY_LINE
            //*******************************************
            // ***  write singal to leg dalay line    ***
            //*******************************************
            //  joint0_delay.writeIn(0.2*pc2);//0.2
            joint0_delay.writeIn(SIGNAL_AMP*3.14*pc1);

            //  for simulation: -0.7*(0.5*(pc1+1.0))-0.5
            //  for real robot:  0.35*(0.5*(pc1+1.0))-0.6
            joint1_delay.writeIn(-0.5*(0.5*(pc2+1.0))-0.6);

            //  joint2_delay.writeIn(-SIGNAL_AMP*2*pc2);
            if(pc2 > 0)
            {
                joint2_delay.writeIn(-SIGNAL_AMP*0.0*pc1);
            }else{
                joint2_delay.writeIn(-SIGNAL_AMP*0.0*pc1);
            }

            //  joint3_delay.writeIn(0.2*0.4*pc2+0.2); // 0.2 + 1.4
            joint3_delay.writeIn(0.2*0.4*pc1+0.3);

            //*******************************************
            // ***  write singal to body dalay line   ***
            //*******************************************
            body_j1_delay.writeIn(c2);
            body_j2_delay.writeIn(c2);
            body_j3_delay.writeIn(c2);

            body_sin_j1_delay.writeIn(flex_body.flex_body_wave(1, sim_time));
            body_sin_j2_delay.writeIn(flex_body.flex_body_wave(2, sim_time));
            body_sin_j3_delay.writeIn(flex_body.flex_body_wave(3, sim_time));


            //*******************************************
            // ***  read singal leg dalay line        ***
            //*******************************************
            // shoulder joint
            motorSig[12] = joint0_delay.readFr(DELAY_RH);
            motorSig[0]  = joint0_delay.readFr(DELAY_RF);
            motorSig[8]  = joint0_delay.readFr(DELAY_LH);
            motorSig[4]  = joint0_delay.readFr(DELAY_LF);

            // delay for right hind leg
            motorSig[13] = joint1_delay.readFr(DELAY_RH);
            motorSig[14] = joint2_delay.readFr(DELAY_RH);
            motorSig[15] = joint3_delay.readFr(DELAY_RH);

            // delay for right front leg
            motorSig[1] = joint1_delay.readFr(DELAY_RF);
            motorSig[2] = joint2_delay.readFr(DELAY_RF);
            motorSig[3] = joint3_delay.readFr(DELAY_RF);

            // delay for left hind leg
            motorSig[9]  = joint1_delay.readFr(DELAY_LH) ;
            motorSig[10] = joint2_delay.readFr(DELAY_LH);
            motorSig[11] = joint3_delay.readFr(DELAY_LH);

            // delay for left front leg
            motorSig[5] = joint1_delay.readFr(DELAY_LF) ;
            motorSig[6] = joint2_delay.readFr(DELAY_LF);
            motorSig[7] = joint3_delay.readFr(DELAY_LF);

            // delay for using cpg mimic real gecko body posture
            c_bodyJ1 = body_j1_delay.readFr(flex_body.mapping_mi_delay(1, CPG_MI2));    // 11.976, 14
            c_bodyJ2 = body_j2_delay.readFr(flex_body.mapping_mi_delay(2, CPG_MI2));    // 18, 19
            c_bodyJ3 = body_j3_delay.readFr(flex_body.mapping_mi_delay(3, CPG_MI2));    // 26.5212, 32

            // delay for standing wave
            standing_wave_J1 = body_j1_delay.readFr(1);
            standing_wave_J2 = body_j2_delay.readFr(1);
            standing_wave_J3 = body_j3_delay.readFr(1);

            // delay for using sine wave mimic real gecko body posture
            s_bodyJ1 = body_sin_j1_delay.readFr(1);
            s_bodyJ2 = body_sin_j2_delay.readFr(1);
            s_bodyJ3 = body_sin_j3_delay.readFr(1);

        #else
            //*******************************************
            //    drive the leg without dalay line    ***
            //*******************************************
            body_j1_delay.writeIn(c1);
            body_j2_delay.writeIn(c1);
            body_j3_delay.writeIn(c1);

            delta_c1 = c1 - c1_old;
            delta_c2 = c2 - c2_old;
            
            motorSig[1]  = (delta_c1>0) ? 0.0:0.00;     //0.35   //RF       // if(delta_c1>0) {motorSig[1]  = 0.0;}else{motorSig[1]  = 0.35;}
            motorSig[9]  = (delta_c1>0) ? 0.0:0.35;     //0.35   //LH       // if(delta_c1>0) {motorSig[9]  = 0.0;}else{motorSig[9]  = 0.35;}
            motorSig[5]  = (delta_c1<=0)? 0.0:0.35;     //0.35   //RH       // if(delta_c1<=0){motorSig[5]  = 0.0;}else{motorSig[5]  = 0.35;}
            motorSig[13] = (delta_c1<=0)? 0.0:0.00;     //0.35   //LF       // if(delta_c1<=0){motorSig[13] = 0.0;}else{motorSig[13] = 0.35;}

            bit_bool = (delta_c1<=0) ? true:false;
            bit_data = (delta_c1<=0) ? 0:1;

            // bitwise write in the array
            if(bit_bool != bit_bool_old)
            {
                bitwise_oper.write(bit_data);
                // bitwise read in the array
                for(int i=0; i<BITSIZE; i++)
                {
                    bit_arr[i] = bitwise_oper.read(i);
                    // cout<<bit_arr[i];
                }
                // cout<<" "<<endl;
                bitwise_oper.run();
            }

            // logic = 21 if the robot walks 1 stride
            bit_logic = bit_arr[0]*1 + bit_arr[1]*2 + bit_arr[2]*4 + bit_arr[3]*8 + bit_arr[4]*16;
            bit_present = (bit_logic == 21) ? 1:0;                              // bit_logic == 21 ? bit_present = 1 : bit_present = 0;

            // stride counting
            if(bit_present_old == 0 && bit_present == 1) stride++;


            //***********************************************
            // change MI and Phase acconding to stride    ***
            //***********************************************
            
            // standing constant bending
            basic_cpg.setMI(0.18);
            phase_bj1 = mi_phase.MI18_PH0[0];
            phase_bj2 = mi_phase.MI18_PH0[1];
            phase_bj3 = mi_phase.MI18_PH0[2];

            // traveling constant bending
            // basic_cpg.setMI(0.30);
            // phase_bj1 = mi_phase.MI30_PH20[0];
            // phase_bj2 = mi_phase.MI30_PH20[1];
            // phase_bj3 = mi_phase.MI30_PH20[2];

            // basic_cpg.setMI(0.30);
            // phase_bj1 = mi_phase.MI30_PH40[0];
            // phase_bj2 = mi_phase.MI30_PH40[1];
            // phase_bj3 = mi_phase.MI30_PH40[2];



            // strategy of body patterns
            /*
            if(stride == 0)
            {
                basic_cpg.setMI(0.18);
                BODY_AMP_GAIN = 0.3;
                phase_bj1 = mi_phase.MI18_PH20[0];
                phase_bj2 = mi_phase.MI18_PH20[1];
                phase_bj3 = mi_phase.MI18_PH20[2];
            }
            else if(stride == 1){
                basic_cpg.setMI(0.24);
                BODY_AMP_GAIN = 0.3;
                phase_bj1 = mi_phase.MI24_PH40[0];
                phase_bj2 = mi_phase.MI24_PH40[1];
                phase_bj3 = mi_phase.MI24_PH40[2];
            }
            else if(stride == 2){
                basic_cpg.setMI(0.30);
                BODY_AMP_GAIN = 0.3;
                phase_bj1 = mi_phase.MI30_PH40[0];
                phase_bj2 = mi_phase.MI30_PH40[1];
                phase_bj3 = mi_phase.MI30_PH40[2];
            }
            */

            // phase shift for each joint of the body
            standing_wave_J1 = body_j1_delay.readFr(phase_bj1);
            standing_wave_J2 = body_j2_delay.readFr(phase_bj2);
            standing_wave_J3 = body_j3_delay.readFr(phase_bj3);

            // collect previous signal
            bit_bool_old = bit_bool;
            bit_present_old = bit_present;

            c1_old = c1;
            c2_old = c2;

            // put signal to drive legs
            motorSig[0] = 0;
            motorSig[2] = 0;
            motorSig[3] = 0;    // + 0.78 use for runing body pattern experiment

            motorSig[4] = 0;
            motorSig[6] = 0;
            motorSig[7] = 0; 

            motorSig[8]  = 0;
            motorSig[10] = 0;
            motorSig[11] = 0;

            motorSig[12] = 0;
            motorSig[14] = 0;
            motorSig[15] = 0;

            // put signal to drive body
            motorSig[16] =  BODY_AMP_GAIN * (standing_wave_J1);  // BODY_AMP_GAIN // 0.3352f  
            motorSig[17] =  BODY_AMP_GAIN * (standing_wave_J2);  // BODY_AMP_GAIN // 0.5497f
            motorSig[18] =  BODY_AMP_GAIN * (standing_wave_J3);  // BODY_AMP_GAIN // 0.3150f
        #endif


        // put signal to drive simulation
        for(int i =0 ;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
              sim_motorSig[4*i+j] = (sim_motorDirection[4*i+j] * motorSig[4*i+j]) + sim_motorInitPos[4*i+j];
            }
        }


        #if DATA_LOGGER
            // use data logger for analize cpg and pcpg
            // logger_cpg.cpg_pcpg_logs("cpglog.txt", c1, c2, v1, v2, b_pcpg_11, b_pcpg_12, sim_time);
            logger_cpg->cpg_pcpg_logs("cpglog.txt", sim_time, standing_wave_J1, standing_wave_J2, standing_wave_J3, c1, c2, 0);

            // use data logger for analize motor signals
            logger_motor->motor_sig_logs("motorlog.txt", sim_time,
                                                motorSig[0],  motorSig[1],   motorSig[2],  motorSig[3],
                                                motorSig[4],  motorSig[5],   motorSig[6],  motorSig[7],
                                                motorSig[8],  motorSig[9],   motorSig[10], motorSig[11],
                                                motorSig[12], motorSig[13],  motorSig[14], motorSig[15]);
            /*
            // use data logger for flex body
            logger_flex_body.flex_body_logs("flexbodylog.txt",
                                                c_bodyJ1, c_bodyJ2, c_bodyJ3,
                                                s_bodyJ1, s_bodyJ2, s_bodyJ3,
                                                standing_wave_J1,  standing_wave_J2,  standing_wave_J3 );
            */
        #endif


        if(updateCondition)
        {
            joint0_delay.step_one();
            joint1_delay.step_one();
            joint2_delay.step_one();
            joint3_delay.step_one();
            body_j1_delay.step_one();
            body_j2_delay.step_one();
            body_j3_delay.step_one();
            // body_sin_j1_delay.step_one();
            // body_sin_j2_delay.step_one();
            // body_sin_j3_delay.step_one();
        }

        //*******************************************
        //*                                         *
        //*         put data to ros variable        *
        //*                                         *
        //*******************************************
        // drive simulation body
        sim_bodySignal.data.push_back(BODY_AMP_GAIN *(standing_wave_J1));         // BODY_AMP_GAIN // 0.3352f    // 0.4*standing_wave_Jx in body wave experiment
        sim_bodySignal.data.push_back(BODY_AMP_GAIN *(standing_wave_J2));         // BODY_AMP_GAIN // 0.5497f  
        sim_bodySignal.data.push_back(BODY_AMP_GAIN *(standing_wave_J3));         // BODY_AMP_GAIN // 0.3150f

        /*
        // drive real body robot
        multiJointCommandSignal.data.push_back(15);                                                     // 3.0 is motor id of body joint1
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*(standing_wave_J1));                       // BODY_AMP_GAIN*0.5*(c1)// 0.5 -0.5
        multiJointCommandSignal.data.push_back(25);                                                     // 14.0 is motor id of body joint2
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*(standing_wave_J1));                       // 0.32 -0.82
        multiJointCommandSignal.data.push_back(35);                                                     // 1.0 is motor id of body joint3
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*(standing_wave_J1));                       // 0.40 -0.47
        */

        cpgSignal.data.push_back(standing_wave_J1);
        cpgSignal.data.push_back(standing_wave_J2);
        cpgSignal.data.push_back(standing_wave_J3);
        cpgSignal.data.push_back(bit_present);
        // cpgSignal.data.push_back(motorSig[5]);
        // cpgSignal.data.push_back(stride);

        // drive real robot
        motorSignal.data.clear();
        for(int j=0;j<19;j++)
        {
            motorSignal.data.push_back(motorSig[j]);
        }

        // driv simulation legs
        sim_motorSignal.data.clear();
        for(int j=0;j<16;j++)
        {
            sim_motorSignal.data.push_back(sim_motorSig[j]);
        }


        outputSIMBODY.publish(sim_bodySignal);
        outputCPG.publish(cpgSignal);
        outputMOTOR.publish(motorSignal);
        outputSIMMOTOR.publish(sim_motorSignal);
        // outputMultiJointCommand.publish(multiJointCommandSignal);

        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok
    delete logger_cpg;
    delete logger_motor;
    return 0;
}

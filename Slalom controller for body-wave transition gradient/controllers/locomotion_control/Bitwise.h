//*******************************************
//*                                         *
//*       Bitwise operation                 *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// data: 11/05/2022
// version: 1.0.0

#ifndef BITWISE_H
#define BITWISE_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdint.h>

using namespace std;


class Bitwise {
public:

    // initialize Delay line.
    Bitwise();

    // set the size of delay line.
    void setParams(int arrSize);

    // put the signal in the delay line
    void write(int data);

    /* this is debugging function to print out
    the data in the delay line */
    void printArrBits(int indx);

    // get data from delay line
    int read(int data);

    // update the delay line
    void run();


private:
    // private attribute
    int arrSize;            // tableSize
    int step;               // store current step of the delay line
    int *bits_ptr;        // pointer to array of float

    // private method
    // special modulation function
    int mod(int x,int m);
};


#endif // BITWISE_H

//*******************************************
//*                                         *
//*        real slalom controller           *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract:  haomachai@gmail.com
// update: 31/05/2022
// version: 4.0.0


//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************

// c++ library
#include <cmath>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

// standard ros library
#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <rosgraph_msgs/Clock.h>
#include <sensor_msgs/JointState.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"



//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10 // reflesh rate of ros


//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
// std_msgs::Float32MultiArray dynamixelSignal;
sensor_msgs::JointState dynamixelSensorMsgs;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable

float motorSig[19] = {0}; // array store motor signal

// legs configuration
const float motorDirection[16]  =   {1.0, 1.0,-1.0,-1.0,
                                     1.0,-1.0,-1.0, 1.0,
                                    -1.0, 1.0, 1.0,-1.0,
                                    -1.0,-1.0, 1.0, 1.0};

// const float motorInitPos[16]    =   {0.0,-0.8, 0.0,-0.78,
//                                      0.0, 0.8, 0.0, 0.78,
//                                      0.0,-0.8, 0.0,-0.78,
//                                      0.0, 0.8, 0.0, 0.78};

// TODO modify for 30 deg slope
const float motorInitPos[16]    =   {-0.1,-0.68, 0.0,-0.78,
                                      0.0, 0.8, 0.0, 0.78,
                                      0.0,-0.8, 0.0,-0.78,
                                      0.1, 0.68, 0.0, 0.78};

const int LEG_NUM[4] = {1,2,3,4};                       // for construting leg dynamixel ID
const int JOINT_NUM[4] = {1,2,3,4};


// body configuration
const float bodyDirection[5]    =   {-1.0,-1.0,-1.0};
const float bodyInitPos[5]      =   {0.0, 0.0, 0.0};
const int bodyDynamixelID[5]    =   {15, 25, 35};       // dynamixel ID of body joint 1, 2, 3, respectively

// with head and tail
// const float bodyDirection[5]    =   {-1.0,-1.0,-1.0,-1.0,-1.0};
// const float bodyInitPos[5]      =   {0.0, 0.0, 0.0, 0.0, 0.0};
// const int bodyDynamixelID[5]    =   {15, 25, 35, 45, 55};       // dynamixel ID of body joint 1, 2, 3, respectively


//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************

void motorCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        motorSig[i] = *it;
        i++;
    }
    return;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node
    std::string nodeName("realRobot");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    // create publisher and subscriber
    // ros::Publisher dynamixelPub;
    // dynamixelPub = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",10);

    ros::Publisher dynamixelSensorMsgsPub;
    dynamixelSensorMsgsPub = node.advertise<sensor_msgs::JointState>("/db_dynamixel_ROS_driver/hexapod_state_commands",1);


    ros::Subscriber motorSub = node.subscribe("/motor_topic",10,motorCB);
    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************

    float temp = 0;
    int indx = 0;
    while(ros::ok())
    {

        // dynamixelSignal.data.clear();
        dynamixelSensorMsgs.name.clear();
        dynamixelSensorMsgs.position.clear();

        // legs signal
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                if((LEG_NUM[i] <= 0)||(LEG_NUM[i] == 0))
                {
                    continue;
                }
                // dynamixelSignal.data.push_back(JOINT_NUM[j]*10 + LEG_NUM[i]);
                // indx = (LEG_NUM[i]-1)*4+(JOINT_NUM[j]-1);
                // temp = motorSig[indx];
                // temp = (temp * motorDirection[indx]) + motorInitPos[indx];
                // dynamixelSignal.data.push_back(temp);

                dynamixelSensorMsgs.name.push_back("id_"+ to_string(JOINT_NUM[j]*10 + LEG_NUM[i]));
                indx = (LEG_NUM[i]-1)*4+(JOINT_NUM[j]-1);
                temp = motorSig[indx];
                temp = (temp * motorDirection[indx]) + motorInitPos[indx];
                dynamixelSensorMsgs.position.push_back(temp);
            }
        }

        // body signal
        // dynamixelSignal.data.push_back(bodyDynamixelID[0]);
        // dynamixelSignal.data.push_back(bodyDirection[0]*motorSig[16] + bodyInitPos[0]);
        // dynamixelSignal.data.push_back(bodyDynamixelID[1]);
        // dynamixelSignal.data.push_back(bodyDirection[1]*motorSig[17] + bodyInitPos[1]);
        // dynamixelSignal.data.push_back(bodyDynamixelID[2]);
        // dynamixelSignal.data.push_back(bodyDirection[2]*motorSig[18] + bodyInitPos[2]);

        dynamixelSensorMsgs.name.push_back("id_"+to_string(bodyDynamixelID[0]));
        dynamixelSensorMsgs.name.push_back("id_"+to_string(bodyDynamixelID[1]));
        dynamixelSensorMsgs.name.push_back("id_"+to_string(bodyDynamixelID[2]));
        // dynamixelSensorMsgs.name.push_back("id_"+to_string(bodyDynamixelID[3]));                 // added head/tail
        // dynamixelSensorMsgs.name.push_back("id_"+to_string(bodyDynamixelID[4]));                 // added head/tail
        dynamixelSensorMsgs.position.push_back(bodyDirection[0]*motorSig[16] + bodyInitPos[0]);
        dynamixelSensorMsgs.position.push_back(bodyDirection[1]*motorSig[17] + bodyInitPos[1]);
        dynamixelSensorMsgs.position.push_back(bodyDirection[2]*motorSig[18] + bodyInitPos[2]);
        // dynamixelSensorMsgs.position.push_back(bodyDirection[3]*0.0          + bodyInitPos[3]);  // added haed/tail
        // dynamixelSensorMsgs.position.push_back(bodyDirection[4]*0.0          + bodyInitPos[4]);  // added haed/tail


        // dynamixelPub.publish(dynamixelSignal);
        dynamixelSensorMsgsPub.publish(dynamixelSensorMsgs);
        ros::spinOnce();
        loop_rate.sleep();


    } // main loop -> ros::ok
    return 0;
}
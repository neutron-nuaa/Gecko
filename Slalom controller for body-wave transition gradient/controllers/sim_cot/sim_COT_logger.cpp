//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// date: 12/02/2021
// version: 1.0.0

#include "sim_COT_logger.h"

sim_COT_logger::sim_COT_logger(string file_name)
{
  file_dir = "/home/happy/Slalom_body_wave/controllers/slalom/data_logger/";

  // Open output wrl file
  ofstream myFile;
  myFile.open(file_dir+file_name, ios::out);
  if(!myFile)
  {
    cerr << "Cannot open this file for output.\n";
		exit (-1);
  }

  // myFile << setw(10) << "c1" << setw(10) << "c2" << setw(10) << "pc1" << setw(10) << "pc2\n";

  // close the output file
  myFile.close ();
}

sim_COT_logger::~sim_COT_logger()
{
  file_dir = "";
}



// cot data logger

float sim_COT_logger::sim_cot_logs(string file_name,
                                  float sim_time,           float time_change,
                                  float leg_power_out_sum,  float body_power_out_sum, 
                                  float leg_energy_out,     float body_energy_out,
                                  float leg_energy_out_ck,  float body_energy_out_ck,
                                  float sim_walking_dist,   float sim_cot)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << sim_time    <<    " " << time_change <<   " " << leg_power_out_sum   <<  " "
            << body_power_out_sum <<  " " << leg_energy_out    <<  " " << body_energy_out <<  " " << leg_energy_out_ck    <<  " " << body_energy_out_ck << " " << sim_walking_dist    <<  " " << sim_cot <<   "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}



// torques data logger

float sim_COT_logger::sim_torques_logs(string file_name, float time,
                                  float t_m1, float t_m2, float t_m3, float t_m4, float t_m5, float t_m6, float t_m7, float t_m8, float t_m9, 
                                  float t_m10, float t_m11, float t_m12, float t_m13, float t_m14, float t_m15, float t_m16, float t_b1, float t_b2, float t_b3)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << time  <<   " " << t_m1   <<  " " << t_m2   <<  " " << t_m3   <<  " " << t_m4   <<  " " << t_m5   <<  " " << t_m6   <<  " " << t_m7   <<  " " << t_m8   <<  " " << t_m9   <<  " "
            << t_m10 <<   " " << t_m11  <<  " " << t_m12  <<  " " << t_m13  <<  " " << t_m14  <<  " " << t_m15  <<  " " << t_m16  <<  " " << t_b1  <<  " " << t_b2   << " " << t_b3  <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}



// velocity data logger

float sim_COT_logger::sim_velocity_logs(string file_name, float time,
                                  float v_m1, float v_m2, float v_m3, float v_m4, float v_m5, float v_m6, float v_m7, float v_m8, float v_m9, 
                                  float v_m10, float v_m11, float v_m12, float v_m13, float v_m14, float v_m15, float v_m16, float v_b1, float v_b2, float v_b3)
{
  ofstream bodyFile;
  bodyFile.open(file_dir+file_name, ios::app);
  if(!bodyFile)
  {
    cerr << "Cannot open this file for output.\n";
    exit (-1);
  }

  bodyFile  << time  <<   " " << v_m1   <<  " " << v_m2   <<  " " << v_m3   <<  " " << v_m4   <<  " " << v_m5   <<  " " << v_m6   <<  " " << v_m7   <<  " " << v_m8   <<  " " << v_m9   <<  " "
            << v_m10 <<   " " << v_m11  <<  " " << v_m12  <<  " " << v_m13  <<  " " << v_m14  <<  " " << v_m15  <<  " " << v_m16  <<  " " << v_b1  <<  " " << v_b2   << " " << v_b3  <<  "\n";

  // Close the output file
  bodyFile.close ();

return 0;
}
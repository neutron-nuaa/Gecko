//*******************************************
//*                                         *
//*        main cost of transport           *
//*      calculate from simulation          *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// date: 12/02/2021
// version: 1.0.0

// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>
#include <array>

#include "sim_COT_logger.h"

using namespace std;

//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10           // reflesh rate of ros
#define DATA_LOGGER 1     // log data? 1 (yes) or 0 (no)

//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray simEnergySignal;


//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

// global variable
const int numLegJoint = 16;                       // number of legs
const int numBodyJoint = 3;                       // number of body

array<float, numLegJoint> legTorqueSig;           // torque feedback from real robot (Nm)
array<float, numLegJoint> legVelocitySig;         // velocity feedback from real robot (rad/s)
array<float, numBodyJoint> bodyTorqueSig;         // torque feedback from real robot (Nm)
array<float, numBodyJoint> bodyVelocitySig;       // velocity feedback from real robot (rad/s)

float simTime = 0;                           // simulation time (sec)
float simPreviousTime = 0;                   // time (sec)
float simTimeChange = 0;                     // dulation (sec)
float simWalkingDist = 0;                    // robot walike distacne in simulation

float legEnergyOut = 0;                     // energy (J)
float legEnergyOut_CK = 0;                  // energy (J)  -for checking
float bodyEnergyOut = 0;                    // energy (J)
float bodyEnergyOut_CK = 0;                 // energy (J)  -for checking


float _legTorque[numLegJoint] = {};        
float _legVelocity[numLegJoint] = {};              
float _bodyTorque[numBodyJoint] = {};
float _bodyVelocity[numBodyJoint] = {};

float legPowerOut[numLegJoint] = {};            // power (watt)
float bodyPowerOut[numBodyJoint] = {};          // power (watt)

float SIMCOT = 0;  // cost of transport

//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************


void legTorqueCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      legTorqueSig[i] = *it;
      i++;
    }
    return;
}

void legVelocityCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      legVelocitySig[i] = *it;
      i++;
    }
    return;
}

void bodyTorqueCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      bodyTorqueSig[i] = *it;
      i++;
    }
    return;
}

void bodyVelocityCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      bodyVelocitySig[i] = *it;
      i++;
    }
    return;
}

void sim_TimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      simTime = *it;
    }
    return;
}

void sim_WalkingDistCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
      simWalkingDist = *it;
    }
    return;
}


//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){

    // create ros node
    std::string nodeName("simCOT");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    // data logger
    #if (DATA_LOGGER)
      sim_COT_logger sim_logger_cot("sim_cotlog.txt");
      sim_COT_logger sim_logger_torques("sim_torqueslog.txt");
      sim_COT_logger sim_logger_velocity("sim_velocitylog.txt");
    #endif


    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    ros::Publisher simOutputEnergy;
    simOutputEnergy = node.advertise<std_msgs::Float32MultiArray>("/sim_energy_topic",1);

    ros::Subscriber simLegTorqueSub = node.subscribe("/sim_legs_joint_velocities",10, legTorqueCB);
    ros::Subscriber simLegVolocitySub = node.subscribe("/sim_legs_joint_torques",10, legVelocityCB);
    ros::Subscriber simBodyTorqueSub = node.subscribe("/sim_body_joint_velocities",10, bodyTorqueCB);
    ros::Subscriber simBodyVolocitySub = node.subscribe("/sim_body_joint_torques",10, bodyVelocityCB);
    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,sim_TimeCB);
    ros::Subscriber simWalkingDistSub = node.subscribe("/sim_walking_dist_topic",10,sim_WalkingDistCB);

    float legPowerOutSum = 0;
    float bodyPowerOutSum = 0;

    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*              ros parameter              *
        //*                                         *
        //*******************************************

        simTimeChange = simTime - simPreviousTime;

        //*******************************************
        //*  collected data every time step         *
        //******************************************* 
        if(simTimeChange > 0.2)  // compute energy every 0.2 sec  
        {
          for(int i = 0; i < legTorqueSig.size() ; i++)
          {
            // collected the values of torques and velocity of each leg joints
            _legTorque[i] = abs(legTorqueSig[i]);        //Nm
            _legVelocity[i] = abs(legVelocitySig[i]);    //red/s

            // collected the values of power out for each leg joints
            legPowerOut[i] = (abs(legTorqueSig[i]) * abs(legVelocitySig[i]));                             // Watt (Nm*rad/s)

            // sum of power for all leg joints
            legPowerOutSum = legPowerOutSum + (abs(legTorqueSig[i]) * abs(legVelocitySig[i]));            // Watt (Nm*rad/s)
          }

          for(int i = 0; i < bodyTorqueSig.size() ; i++)
          {
            // collected the values of torques and velocity of each body joints
            _bodyTorque[i] = abs(bodyTorqueSig[i]);        //Nm
            _bodyVelocity[i] = abs(bodyVelocitySig[i]);    //red/s

            // collected the values of power in/out for each body joints
            bodyPowerOut[i] = (abs(bodyTorqueSig[i]) * abs(bodyVelocitySig[i]));                          // Watt (Nm*rad/s)

            // sum of power for all body joints
            bodyPowerOutSum = bodyPowerOutSum + (abs(bodyTorqueSig[i]) * abs(bodyVelocitySig[i]));        // Watt (Nm*rad/s)
          }
          simPreviousTime = simTime;
        }

        //*******************************************
        //*       query data and compute            *
        //******************************************* 
        for(int i = 0; i < legTorqueSig.size() ; i++)
        {
          // calculate energy out from each leg joints
          legEnergyOut = legEnergyOut + (legPowerOut[i] * simTimeChange);                // Joule (Watt*Sec)
        }

        for(int i = 0; i < bodyTorqueSig.size() ; i++)
        {
          // calculate energy out from each leg joints
          bodyEnergyOut = bodyEnergyOut + (bodyPowerOut[i] * simTimeChange);             // Joule (Watt*Sec)
        }

        // calculate energy out from sum of power (CHECKER)
        legEnergyOut_CK = legEnergyOut_CK + (legPowerOutSum * simTimeChange);               // Joule (Watt*Sec)
        bodyEnergyOut_CK = bodyEnergyOut_CK + (bodyPowerOutSum * simTimeChange);            // Joule (Watt*Sec)


        SIMCOT = (legEnergyOut+bodyEnergyOut) / (2.45*9.81*simWalkingDist);                 // COT = E/mgd this is the COT of simulation


        #if (DATA_LOGGER)
          // use data logger for analize cpg and pcpg
          sim_logger_cot.sim_cot_logs("sim_cotlog.txt", simTime, simTimeChange, legPowerOutSum, bodyPowerOutSum, legEnergyOut, bodyEnergyOut, legEnergyOut_CK, bodyEnergyOut_CK, simWalkingDist, SIMCOT);

          sim_logger_torques.sim_torques_logs("sim_torqueslog.txt", simTime, _legTorque[0],  _legTorque[1],  _legTorque[2],  _legTorque[3],  _legTorque[4],  _legTorque[5],  _legTorque[6],  _legTorque[7],  _legTorque[8], _legTorque[9],
                                                                             _legTorque[10], _legTorque[11], _legTorque[12], _legTorque[13], _legTorque[14], _legTorque[15], _bodyTorque[0], _bodyTorque[1], _bodyTorque[2]);


          sim_logger_velocity.sim_velocity_logs("sim_velocitylog.txt", simTime, _legVelocity[0],  _legVelocity[1],  _legVelocity[2],  _legVelocity[3],  _legVelocity[4],  _legVelocity[5],  _legVelocity[6],  _legVelocity[7],  _legVelocity[8], _legVelocity[9],
                                                                                _legVelocity[10], _legVelocity[11], _legVelocity[12], _legVelocity[13], _legVelocity[14], _legVelocity[15], _bodyVelocity[0], _bodyVelocity[1], _bodyVelocity[2]);
        #endif
        
        // clear power out
        legPowerOutSum = 0;
        bodyPowerOutSum = 0;


        simEnergySignal.data.clear();
        simEnergySignal.data.push_back(legEnergyOut+bodyEnergyOut);
        // simEnergySignal.data.push_back(bodyEnergyOut);
        simOutputEnergy.publish(simEnergySignal);
        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok
    return 0;
}

//*******************************************
//*                                         *
//*             Data Logger                 *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com,
// date: 12/02/2021
// version: 1.0.0

#ifndef SIM_COT_LOGGER_H
#define SIM_COT_LOGGER_H

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

class sim_COT_logger
{
public:

  // constructor
  sim_COT_logger(string file_name);

  // constructor
  ~sim_COT_logger();


  // cot logger
  float sim_cot_logs(string file_name,
                                  float sim_time,           float time_change,
                                  float leg_power_out_sum,  float body_power_out_sum, 
                                  float leg_energy_out,     float body_energy_out,
                                  float leg_energy_out_ck,  float body_energy_out_ck,
                                  float sim_walking_dist,   float sim_cot);

  // torques logger
  float sim_torques_logs(string file_name, float time,
                                  float t_m1, float t_m2, float t_m3, float t_m4, float t_m5, float t_m6, float t_m7, float t_m8, float t_m9, 
                                  float t_m10, float t_m11, float t_m12, float t_m13, float t_m14, float t_m15, float t_m16, float t_b1, float t_b2, float t_b3);

  // velocity logger
  float sim_velocity_logs(string file_name, float time,
                                  float v_m1, float v_m2, float v_m3, float v_m4, float v_m5, float v_m6, float v_m7, float v_m8, float v_m9, 
                                  float v_m10, float v_m11, float v_m12, float v_m13, float v_m14, float v_m15, float v_m16, float v_b1, float v_b2, float v_b3);

private:
  string file_dir;

};

#endif  //COT_LOGGER_H

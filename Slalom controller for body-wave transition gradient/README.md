# <center> Slalom controller for body-wave transition gradient </center>

## Introduction
We used our gecko-inspired robot (called Slalom) with 3-DoFs bendable body designed based on a gecko structure to explore the large space of lateral undulation patterns from standing to traveling waves with different locomotion speeds for inclined surface climbing. Investigating the gradient of body patterns between purely standing (C-shaped) and extremely traveling waves (S-shaped) enabled us to analyze robot climbing performance (i.e., cost of transport (CoT) and locomotion speed) and design an energy-efficient strategy of simultaneous body-wave transitions for climbing enhancement.

## Framework

The project is organized by four sub-folders including **controllers**, **projects**, and **simulation**.

- **controllers** consists of the code of the control methods.
- **projects** contains the configuration files for managing the software.
- **simulation** stores the simulation model which is based on CoppeliaSim.  It has a gecko-inspired robot: Slalom.


## The implementation of the project
### Install necessary software on Ubuntu 18.04 or later version.
- The v4_3_0 or the latest version of the CoppeliaSim is necessary to run the simulation. The CoppeliaSim provides an platform to execute the simulated Slalom. The CoppeliaSim can be download in here https://www.coppeliarobotics.com/.
- The vortex physical engine is necessary to execute dynamical computation of the simulation. The software and its activation can be seen in this link: https://www.cm-labs.com/vortex-studio/software/vortex-studio-academic-access/

### Steps to run the simulation
- open a terminal to run command: roscore
- start the CoppeliaSim and open the simulated Slalom model at simulation/Slalom.ttt of this project.
- Click the run button at the toolbox of the CoppeliaSim.
- After click the button, the simulation is running.


## Reference
W. Haomachai, Z. Dai and P. Manoonpong, "Transition Gradient from Standing to Traveling Waves for Energy-Efficient Slope Climbing of a Gecko-Inspired Robot​," in IEEE Robotics and Automation Letters, (Under review)​


If you have any questions/doubts  about how to implement this project on your computer, you are welcome to raise issues and email to me. My email address is haomachai@gmail.com

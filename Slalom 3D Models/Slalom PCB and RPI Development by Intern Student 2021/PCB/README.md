# PCB Salalom

Design using: [EasyEDA](https://easyeda.com/)

https://oshwlab.com/Slalom_PCB/Schematic_Board
https://easyeda.com/editor#id=|7c18ab3e2bce4fe39a5f6afc584ec409|beb484a93f764cf8b071d73c28a19a27|04c656761dd64e88a1b0201b1512d5dd


_**Designed by:** Budsakorn Thaiprasert, Thitiya Boontongloun and Pannawat Chimprasert_ <br> 
_**Supervised by:** Worasuchad Haomachai_


### Schematics and Circuit Diagrams

![Schematic](https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Slalom%203D%20Models/Slalom%20PCB%20and%20RPI%20Development%20by%20Intern%20Student%202021/PCB/Schematic_Label.png)

1. Current sensor
    - *U1* -> ACS712 
( [Datasheet](https://datasheet.lcsc.com/lcsc/1811061527_Allegro-MicroSystems--LLC-ACS712ELCTR-05B-T_C44471.pdf) )
    - *U6-U7* -> Power Connectors: XT60-M For battery 12V*2
( [Datasheet](https://datasheet.lcsc.com/lcsc/1810251322_Changzhou-Amass-Elec-XT60_C98733.pdf) )
    - *F1* -> Fuse and Fuse holder case
    - *SW1* -> Switche On/Off
( [Datasheet](https://datasheet.lcsc.com/lcsc/1804162049_BBJ-C67849_C67849.pdf) )

2. U2D2 Hub
    - *CN1-CN6* -> Connector: 4 poles
( [Datasheet](https://datasheet.lcsc.com/lcsc/1811080906_BOOMELE-Boom-Precision-Elec-XH-4A_C37815.pdf) )

3. Voltage Regulator 5V
    - *U4* -> LM2576HV-12 Series SIMPLE SWITCHER 3A Step-Down Voltage Regulator
( [Datasheet](https://datasheet.lcsc.com/lcsc/1804240220_Texas-Instruments-LM2576HVT-12_C36148.pdf) )
    - *C3* -> Multilayer Ceramic Chip Capacitor 100µF
( [Datasheet](https://datasheet.lcsc.com/lcsc/1811061121_IHHEC-HOLY-STONE-ENTERPRISE-CO---LTD-C1206X106K025T_C106027.pdf) )
    - *C4* -> Capacitor 1000 µF (using inductor symbol because this size is fit)
    - *D2* -> Schottky Barrier Rectifiers MBR360
( [Datasheet](https://www.onsemi.com/pdf/datasheet/mbr350-d.pdf) )
    - *L1* -> Inductor 100 µH
( [Datasheet](https://datasheet.lcsc.com/lcsc/2108131730_SXN-Shun-Xiang-Nuo-Elec-SMDRS1275-101N_C163688.pdf) )
    - *R1* -> Resistor SMD 2.2kΩ 1206
    - *R2* -> VR Potentiometer Variable Resistor 20kΩ 

4. Connecting pin to raspberry pi
    - *H2* -> Female Hole Pin 14*2 pins
( [Datasheet](https://datasheet.lcsc.com/lcsc/1811132110_BOOMELE-Boom-Precision-Elec-C35165_C35165.pdf) )

5. A/D Converters with SPI Serial Interface
    - *U5* -> MCP3008T-I/SL
( [Datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/194720/MICROCHIP/MCP3008T-I/SL.html) )

6. Regulator 12 V
    - *PS1* -> Pololu 12V, 15A Step-Down Voltage Regulator D24V150F12
( [Datasheet](https://www.pololu.com/product/2885) )
    - *R3* ->  Surface Mount Resistor 3.6kΩ
    - *LED2* -> Surface Mount LED 0603 

7. Other
    - *CH7-CH13* -> WAFER Connector 2-3Pins,Pitch 2.50mm,Straight Pin
( [Datasheet for 2 pins](https://www.es.co.th/Schemetic/PDF/A2502.PDF) , [Datasheet for 3 pins](https://www.es.co.th/Schemetic/PDF/A2502.PDF) )
    - *SW2-SW3* -> Push Switch Lock
( [Datasheet](https://www.allnewstep.com/product/779/%E0%B8%AA%E0%B8%A7%E0%B8%B4%E0%B8%95%E0%B8%8A%E0%B9%8C%E0%B8%81%E0%B8%94%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%81%E0%B8%94%E0%B8%94%E0%B8%B1%E0%B8%9A-lock-switch) )
    - *P1-P6* -> Male pin header 2.54mm 3*1 pins and Jumper header
    - *R6-R9* -> Resistor SMD 2.2kΩ 1206

![PCB_PCBv.3](https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Slalom%203D%20Models/Slalom%20PCB%20and%20RPI%20Development%20by%20Intern%20Student%202021/PCB/PCB_PCBv.3.png "Logo Title Text 1")

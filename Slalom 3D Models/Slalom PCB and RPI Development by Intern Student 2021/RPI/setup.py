##  ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD
from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

#  fetch values from package.xml
setup_pkg = generate_distutils_setup(
    packages = ["common_slalom_pkg"],
    package_dir = {"": "src"}
)
setup(**setup_pkg)

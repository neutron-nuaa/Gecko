#!/usr/bin/env python3

import os
import adafruit_mcp3xxx.mcp3008 as MCP
import Adafruit_GPIO.SPI as SPI
import digitalio
import board
import busio
import time
import numpy

from adafruit_mcp3xxx.analog_in import AnalogIn
from common_slalom_pkg.DistanceRemap import DistanceRemap
from common_slalom_pkg.ForceRemap import ForceRemap

import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Bool

#######################################
#   Define Chanel in MCP3008
#   Chanel0 --> FSR (1)
#   Chanel1 --> FSR (2)
#   Chanel2 --> FSR (3)
#   Chanel3 --> FSR (4)
#   Chanel4 --> IR (1)
#   Chanel5 --> IR (2)
#   Chanel6 --> Current
#   Chanel7 --> N/A
#######################################

read_ADC = []

def ADC_init():
    #   Create SPI bus
    spi = busio.SPI(clock = board.SCK, MISO = board.MISO, MOSI = board.MOSI)
    #   Chip select Ref to PIN out
    #   Ref https://pinout.xyz/pinout/ (Rasberry PI)
    #   Example when we use PIN GPIO 22 --> use board.D15    
    cs = digitalio.DigitalInOut(board.D5)
    #   Create MCP3008 Object
    global mcp, ADC_index
    mcp = MCP.MCP3008(spi, cs)  
    #   Use AnalogIn to read data from MCP3008 
    #   Example when we read data from chanel0 of MCP --> AnalogIn(mcp (*object to read data),MCP.P0 (*read from ChanelXX))
    #   read data from ADC 7 chanel 
    ADC_index = [MCP.P0, MCP.P1, MCP.P2, MCP.P3, MCP.P4, MCP.P5, MCP.P6]

def ros_init():
    rospy.init_node("analog_data", anonymous = True)
    global analog_pub,ir1, ir2, rate, sensor_data, sensor_ir1, sensor_ir2
    analog_pub = rospy.Publisher("sensor/ADC_data", Float64MultiArray, queue_size = 1000)
    ir1 = rospy.Publisher("sensor/IR1_detect", Bool, queue_size = 1000)
    ir2 = rospy.Publisher("sensor/IR2_detect", Bool, queue_size = 1000)
    rate = rospy.Rate(10)
    sensor_data = Float64MultiArray()
    sensor_ir1 = Bool()
    sensor_ir2 = Bool()

#   main program
if __name__ == "__main__":
    try:
        ADC_init()
        ros_init()
        while not rospy.is_shutdown():
            sensor_data.data.clear()
            
            value = AnalogIn(mcp, MCP.P0)
            distance = DistanceRemap(value.voltage)
            print("Distance is 0 : ", distance.vol2dis())
            sensor_data.data.append(distance.vol2dis())
            
            value = AnalogIn(mcp, MCP.P1)
            force = ForceRemap(value.voltage)
            print("Force is 1 : ", force.vol2force())
            sensor_data.data.append(force.vol2force())

            value = AnalogIn(mcp, MCP.P2)
            distance = DistanceRemap(value.voltage)
            print("Distance is 2 : ", distance.vol2dis())
            sensor_data.data.append(distance.vol2dis())
            
            value = AnalogIn(mcp, MCP.P3)
            force = ForceRemap(value.voltage)
            print("Force is 3 : ", force.vol2force())
            sensor_data.data.append(force.vol2force())

            value = AnalogIn(mcp, MCP.P4)
            force = ForceRemap(value.voltage)
            print("Force is 4 : ", force.vol2force())
            sensor_data.data.append(force.vol2force())

            value = AnalogIn(mcp, MCP.P5)
            force = ForceRemap(value.voltage)
            print("Force  is 5 : ", force.vol2force())
            sensor_data.data.append(force.vol2force())

            value = AnalogIn(mcp, MCP.P6)
            sensor_data.data.append(0)
            # for i in range(7):
            #     # for i in range [0, 3] it FSR data
            #     if i < 4:
            #         value = AnalogIn(mcp, ADC_index[i])
            #         force = ForceRemap(value.voltage)
            #         print("Force is : ", force.vol2force())
            #         sensor_data.data.append(force.vol2force())
            #     elif i >= 4 and i < 6:
            #         value = AnalogIn(mcp, ADC_index[i])
            #         distance = DistanceRemap(value.voltage)
            #         if i == 4:
            #             if distance.vol2dis() == -1:
            #                 sensor_ir1.data = False
            #             else:
            #                 sensor_ir1.data = True
            #             ir1.publish(sensor_ir1)
            #         else:
            #             if distance.vol2dis() == -1:
            #                 sensor_ir2.data = False
            #             else:
            #                 sensor_ir2.data = True
            #             ir2.publish(sensor_ir2)
            #         print("Distance is : ", distance.vol2dis())
            #         sensor_data.data.append(distance.vol2dis())
            #     else:
            #         value = AnalogIn(mcp, ADC_index[i])
            #         sensor_data.data.append(value.voltage)
                
            analog_pub.publish(sensor_data)
            rate.sleep()

    except rospy.ROSInterruptException:
        pass










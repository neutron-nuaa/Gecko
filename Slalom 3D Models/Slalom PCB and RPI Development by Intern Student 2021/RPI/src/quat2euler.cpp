#include <ros/ros.h>
#include <cmath>
#include "quat2euler.h"


//Constructor
Quat2Euler::Quat2Euler(){
    
}
//De-Constructor
Quat2Euler::~Quat2Euler(){

}
double *Quat2Euler::q2econv(double* pw,double* px,double* py,double* pz){
    
    double roll, pitch, yaw;
    //Rad to Degree+
    double RD = (180/M_PI);
    std::cout << "Oren w :" << *pw << "\n";
    std::cout << "Oren x :" << *px << "\n";
    std::cout << "Oren y :" << *py << "\n";
    std::cout << "Oren z :" << *pz << "\n";  
    //Roll     
    double roll_num = 2 * (((*pw) * (*px)) + ((*py) * (*pz)));
    double roll_den = 1 - 2 * (pow((*px), 2) - pow((*py), 2));
    roll = atan2(roll_num, roll_den) * RD;
    //Pitch
    double pitch_num = 2 * (((*pw) * (*py)) - ((*pz) * (*px)));
    if(std::abs(pitch_num) >= 1)
        pitch = copysign(M_PI/2, pitch_num) * RD;
    else
        pitch = asin(pitch_num) * RD;
    //Yaw
    double yaw_num = 2 * (((*pw) * (*pz)) + ((*px) * (*py)));
    double yaw_den = 1 - 2 * (pow((*py), 2) + pow((*pz), 2));
    yaw = atan2(yaw_num, yaw_den) *  RD;
    double euler_value[3] = {roll, pitch, yaw};
    static double euler_vector[3] = {};
    for(int i = 0; i < 3; i++){
        euler_vector[i] = euler_value[i];
    }
    std::cout << "Address of Euler Vector : " << &euler_vector << std::endl;
    return euler_vector;
}
#include <ros/ros.h>
#include <iostream>
#include <stdlib.h>
#include <std_msgs/Float32MultiArray.h>
#include <sensor_msgs/Imu.h>
#include "quat2euler.h"
#include <iostream>
#include <stdio.h>

double imu_w, imu_x, imu_y, imu_z;
char *euler_tag[3] = {"Roll angle : ", "Pitch angle : ", "Yaw angle : "};

void eulerv_callback(const sensor_msgs::Imu::ConstPtr& arr){

    imu_w = arr->orientation.w;
    imu_x = arr->orientation.x;
    imu_y = arr->orientation.y;
    imu_z = arr->orientation.z;
    // std::cout << "Get data Finish" << std::endl;
    // ROS_INFO("IMU orientation w : %f", imu_w);
    // ROS_INFO("IMU orientation x : %f", imu_x);
    // ROS_INFO("IMU orientation y : %f", imu_y);
    // ROS_INFO("IMU orientation z : %f", imu_z);
    return;
    
}

int main(int argc, char** argv){

    ros::init(argc, argv, "q2econv");
    ros::NodeHandle np_, ns_;
    ros::Publisher q2e_pub = np_.advertise<std_msgs::Float32MultiArray>("sensor/imu_euler", 100);
    ros::Subscriber q2e_sub = ns_.subscribe("imu/data", 1000, eulerv_callback);
    ros::Rate loop_rate(10);
    
    Quat2Euler q2e;
    double *p_euler;
    std_msgs::Float32MultiArray euler_vec;
    
    while( ros::ok() ){
        euler_vec.data.clear();
        std::cout << "[Euler Vector]" << "\n";
        p_euler = q2e.q2econv(&imu_w, &imu_x, &imu_y, &imu_z);
        for(int i = 0; i < 3; i++){
            euler_vec.data.push_back(*(p_euler + i));
            std::cout << euler_tag[i] << "[" << *(p_euler + i) << "]" << "\n";
        }
        q2e_pub.publish(euler_vec);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
    
}
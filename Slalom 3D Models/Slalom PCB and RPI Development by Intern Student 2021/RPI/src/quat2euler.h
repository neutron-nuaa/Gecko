// #ifndef QUAT2EULER_H
// #define QUAT2EULER_H

#include <ros/ros.h>
#include <iostream>
#include <stdlib.h>
#include <sensor_msgs/Imu.h>

using namespace std;

class Quat2Euler
{  
public:
    Quat2Euler();
    ~Quat2Euler();
    double *q2econv(double* w,double* x,double* y,double* z);
};

// #endif QUAT2EULER_H
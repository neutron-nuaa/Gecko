#!/usr/bin/env python3
#   Force Remap from Voltage
#   For 10-bit ADC data
#   Sensor Interlink 402
#   Find characteristic of sensor 

class ForceRemap:

    def __init__(self, value):
        #   Define Parameter to use Remapping data
        #   value range from 0 to 1023 (10-Bit) --> 1024 level
        #   resolution of ADC @ Vref = 3.3V = 3.3/2^10 = 3.3/1024 ~ 3.2227mV
        self._voltage = value
        #   Coefficient from MATLAB
        self.coe1 = [0.3650199, -1.2071054, 1.3522538, 0.00]
        self.coe2 = [625.8087824, -6554.5481799, 25747.8454855, -44951.4400236, 29425.5146810]
        
    def vol2force(self):
        #   When value = 0 --> "Voltage = 0V"
        #   When value = 1023 --> "Voltage = 3.3V"
        force = 0.0
        if self._voltage >= 0.0 and self._voltage < 2.5:
            for i in range(4):
                force += self.coe1[i]*(self._voltage**(3-i))
            return force 
        elif self._voltage >= 2.5 and self._voltage <= 3.3:
            for i in range(5):
                force += self.coe2[i]*(self._voltage**(5-i))
            return force
        else:
            print("Out of range")
            #   return error "out of range"
            return -1
        
        
    
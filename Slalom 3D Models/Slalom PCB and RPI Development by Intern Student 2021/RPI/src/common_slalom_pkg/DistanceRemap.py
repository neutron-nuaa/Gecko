#!/usr/bin/env python3
#   Distance Remap from Voltage
#   For 10-bit ADC data
#   Distance can measure in range 4 - 30 cm *Sensor IR SHARP 0A41SK0F
#   Find characteristic of sensor 

class DistanceRemap:

    def __init__(self, value):
        #   Define Parameter to use Remapping data
        #   value range from 0 to 1023 (10-Bit) --> 1024 level
        #   resolution of ADC @ Vref = 3.3V = 3.3/2^10 = 3.3/1024 ~ 3.2227mV
        self._voltage = float(value)
        #   Coefficient from MATLAB
        self.coe = [0.7875762, -11.9319199, 76.4653569, -270.8840689, 582.6225146, -788.3347674, 673.6823705, -354.1888390, 104.6384688]
        
    def vol2dis(self):
        #   When value = 0 --> "Voltage = 0V"
        #   When value = 1023 --> "Voltage = 3.3V"
        if self._voltage >= 0.0 and self._voltage <= 3.3:
            distance = 0.0
            for i in range(9):
                distance += self.coe[i]*(self._voltage**(8-i))
            #   return distance       
            return distance
        else:
            print("Out of range")
            #   return error "out of range"
            return -1  

        # if self._voltage >= 0.4 and self._voltage <= 3.3:
        #     distance = 0.0
        #     for i in range(9):
        #         distance += self.coe[i]*(self._voltage**(8-i))
        #     if distance >= 4 and distance <= 30:
        #         return distance
        #     else:
        #         return -1
        # else:
        #     return -1
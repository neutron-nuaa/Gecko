# SLALOM SENSOR PACKAGE
This tutorial run on Ubuntu MATE 20.04 LTS and that you install ROS noetic using the **Raspberry Pi 4B** . <br>  

_**Developed by:** Pannawat Chimprasert, Budsakorn Thaiprasert and Thitiya Boontongloun <br> 
**Supervised by:** Worasuchad Haomachai_


## Overview
This package is used with many sensors such as
* Phidsgets Spatial(IMU)
* MCP3008(ADC)
* FSR Interlink 402(Force Sensor)
* SHARP 0A41SK(Infrared Sensor)
* ACS702(Current Sensor)
* Intel Realsense D435i

## 1. Install Dependencies
The following dependencies need to be installed on the PC.
### 1.1 ROS
Assume that you install ROS noetic on Ubuntu MATE 20.04 LTS following to the [ROS Noetic installation guide](http://wiki.ros.org/noetic/Installation/Ubuntu).
### 1.1.1 Setup your sources.list
```bash
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```
### 1.1.2 Set up your keys
```bash
$ sudo apt install curl # if you haven't already installed curl
$ curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```
### 1.1.3 Install ROS
First, You should check the Debian package index is up-to-date.
```bash
$ sudo apt update
```
And install ROS Noetic on you computer.
```bash
$ sudo apt install ros-noetic-desktop-full
```
### 1.1.4 Environment setup
Use this command to source ROS everytime when you open terminal.
```bash
$ sudo nano ~/.bashrc
# Add this command at the end of file
$ source /opt/ros/noetic/setup.bash
# Save and exit 
# Run this command in terminal
$ source ~/.bashrc
```
### 1.1.5 Dependencies for building packages
Install dependencies for building ROS packages.
```bash
$ sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
```
#### 1.1.5.1 Initialize rosdep
if you don't install rosdep on your PC, Use this command.
```bash
sudo apt install python3-rosdep
# Initial rosdep
$ sudo rosdep init
$ rosdep update
```
### 1.1.6 Create Workspace
Open terminal and run this command.
```bash
# Create workspace {workspace_name}/src
$ mkdir -p catkin_ws/src
$ cd ~/catkin_ws/
$ catkin_make
```
Use this command to source ROS workspace everytime when you run new terminal.
```bash
$ sudo nano ~/.bashrc
# Add this command to the end of file
$ source ~/catkin_ws/devel/setup.bash
# Now you see this in .bashrc file 
$ source /opt/ros/noetic/setup.bash
$ source ~/catkin_ws/devel/setup.bash
# Save and exit
# Run this command in terminal
$ source ~/.bashrc
```
### 1.2 Install Dependencies packages for SLALOM SENSOR PACKAGE
#### 1.2.1 PIP3
We will use pip3 to install the necessary Python libraries.
```bash
$ sudo apt-get install python3-pip
```
#### 1.2.2 Library for CircuitPython & MCP3008
```bash
$ sudo pip3 install adafruit-blinka
$ sudo pip3 install adafruit-circuitpython-mcp3xxx
```
#### 1.2.3 Phidsget Spartial & IMU Filter Madgwick
You can install following to the [Phidgets Driver for ROS guide](https://github.com/ros-drivers/phidgets_drivers/tree/noetic) and [Madgwick filter for ROS guide](https://github.com/ccny-ros-pkg/imu_tools).
First, make sure you have git installed.
```bash
$ sudo apt-get install git-core
```
Change directory to the source folder in your workspace.
```bash
$ cd ~/catkin_ws/src
$ git clone -b noetic https://github.com/ros-drivers/phidgets_drivers.git
$ git clone -b noetic https://github.com/ccny-ros-pkg/imu_tools.git
# Install dependencies using rosdep
$ rosdep install phidgets_drivers
$ rosdep install imu_tools
# Catkin_make at your workspace
$ cd ~/catkin_ws
$ catkin_make
source ~/.bashrc
```
#### Udev rule Setup
Please make sure your workspace has been complied without error, and run following command.
```bash
$ roscd phidgets_api
$ sudo cp debian/udev /etc/udev/rules.d/99-phidgets.rules
$ sudo udevadm control --reload-rules
# Unplug USB or Run 
$ sudo udevadm trigger
```
#### 1.2.4 Realsense Camera Package
1.2.4.1 Install **RealSense SDK/librealsense** </br>
You can install following to the [Linux Ubuntu Installation](https://github.com/IntelRealSense/librealsense/blob/master/doc/installation.md). <br>
```bash
# Download the complete source tree with git
git clone https://github.com/IntelRealSense/librealsense.git
# Or Download and unzip the latest stable version from master branch: https://github.com/IntelRealSense/librealsense/archive/master.zip
cd ~/librealsense
# Run Intel Realsense permissions script from librealsense root directory:
$ ./scripts/setup_udev_rules.sh
# Notice: One can always remove permissions by running: 
$ ./scripts/setup_udev_rules.sh --uninstall
$ mkdir build && cd build
$ cmake .. -DBUILD_EXAMPLES=true -DCMAKE_BUILD_TYPE=Release -DFORCE_LIBUVC=true
$ make -j1
$ sudo make install
```
1.2.4.2 Install **ROS Wrapper for Intel® RealSense™ Devices**
You can install following to the [Installation Instructions](https://github.com/IntelRealSense/realsense-ros). <br>
Specifically, make sure that the ros package **ddynamic_reconfigure** is installed.

```bash
# Method 1:
$ sudo apt-get install ros-$ROS_DISTRO-realsense2-camera
# Method 2 **if Method 1 not working:
# - Go to a catkin workspace Ubuntu
$ cd ~/catkin_ws/src/
# Clone the latest Intel® RealSense™ ROS
$ git clone https://github.com/IntelRealSense/realsense-ros.git
$ cd realsense-ros/
$ git checkout `git tag | sort -V | grep -P "^2.\d+\.\d+" | tail -1`
$ catkin_init_workspace 
$ cd ~/catkin_ws
$ catkin_make clean
$ catkin_make -DCATKIN_ENABLE_TESTING=False -DCMAKE_BUILD_TYPE=Release
$ catkin_make install
```
## 2. Install SLALOM SENSOR PACKAGE
Install following below command.
```bash
$ cd ~/catkin_ws/src/
$ git clone https://gitlab.com/pannawatzx2030/rpi_slalom_pkg.git
# Enter Username and Password
$ cd ~/catkin_ws/
$ catkin_make
$ source ~/.bashrc
```


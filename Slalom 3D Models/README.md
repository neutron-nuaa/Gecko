# Slalom CAD Models


- [ ] Slalom CAD with Wire Power Supply  <br />
`the CAD and electronic boards of Slalom with the wire power supply version`

- [ ] Slalom PCB and RPI Development by Intern Student 2021 <br />
`PCB and R-PI developed for stand-alone version of Slalom`
    - **[PCB](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%203D%20Models/Slalom%20PCB%20and%20RPI%20Development%20by%20Intern%20Student%202021/PCB)**
    - **[RPI](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%203D%20Models/Slalom%20PCB%20and%20RPI%20Development%20by%20Intern%20Student%202021/RPI)** 

- [ ] Gecko Feet  <br />
`Gecko feet and mold of gecko feet design`

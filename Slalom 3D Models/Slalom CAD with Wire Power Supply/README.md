# Slalom CAD with Wire Power Supply

This is the CAD and electronics board of Slalom with the wire power supply version. We are developing the next version which can walk stand-alone mode.   

<div align="center">
<img src="https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Slalom%203D%20Models/Slalom%20CAD%20with%20Wire%20Power%20Supply/img/list_in_stl.png" width="800"> 
</div>

## 3D Printing List

Ready to print by using [STL files](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%203D%20Models/Slalom%20CAD%20with%20Wire%20Power%20Supply/stl%20files)

| STL model name    | amount    |
|-------------------|-----------|
|link1              |4          | 
|link2_1            |4          |
|link2_2            |4          |
|link3              |4          |
|link4              |4          |
|ring               |4          |
|body1              |2          |
|body2              |1          |
|body3              |1          |
|body4              |2          |
|body5              |2          |


<h2>

```diff
**Noted**
These 3D parts do not include "aluminum foot structure"
Please order 4 foots from an engineer (or contract Donghao)
```

</h2>


## Buying List
| model name                                                                                                | amount    |
|-----------------------------------------------------------------------------------------------------------|-----------|
| DYNAMIXEL XM430-W350-R                                                                                     |19|
| Motor connectors                                                                                           |19|
| U2D2                                                                                                       |1|
| Adaptor AC to DC 12V 5A                                                                                    |1|
| Micro USB (5m didn't work well for transmitting data)                                                      |1|   
|【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30 <br /> 颜色分类：M2.5*6 (20粒)适用2mm扳手             |5|
|【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30 <br /> 颜色分类：M2.5*8 (20粒)适用2mm扳手             |5|
|【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30 <br /> 颜色分类：M2.5*10 (20粒)适用2mm扳手            |5|
|【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30 <br /> 颜色分类：M2*5 (20粒)适用1.5mm扳手             |5|
|【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30 <br /> 颜色分类：M2*8 (20粒)适用1.5mm扳手             |5|
| 304不锈钢六角螺母316螺帽201螺丝帽M1.6/M2/M2.5/M3/M4/M5-M10  <br /> 颜色分类：304材质 M2.0 (20粒)                  |10|
| 39，后着要到他而心家对子说啊 https://m.tb.cn/h.fhFabDG?sm=84a6b8 <br /> EX2DXL电源模块 Dynamixel舵机适用 接口扩展模块 SMPS2Dynamixel <br /> <br /> Model: EX2DXL-B |2|


#### More detail

 【M2 M2.5】304不锈钢内六角螺丝钉M2 M2.5*3/4/5/6/7/8/10/12-30  <br />
 https://detail.tmall.com/item.htm?spm=a1z0d.6639537.1997196601.40.542b7484gIEIOa&id=19469628200

 304不锈钢六角螺母316螺帽201螺丝帽M1.6/M2/M2.5/M3/M4/M5-M10  <br />
 https://item.taobao.com/item.htm?spm=a1z0d.6639537.1997196601.51.542b7484gIEIOa&id=605497487432

 Micro USB <br />
 https://detail.tmall.com/item.htm?spm=a1z0d.6639537.1997196601.4.ac9b7484lEQIa2&id=587567307854

 Motor connector <br />
 智能佳（ZNJ） HN12-I101 副舵盘套件 X系列舵机【图片 价格 品牌 评论】-京东 (jd.com) <br />
 https://item.m.jd.com/product/63195008700.html?utm_campaign=t_1001328990



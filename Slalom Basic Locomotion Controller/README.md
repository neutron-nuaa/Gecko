# Slalom Basic Locomotion Controller

To make the robot run...

### 1. open a new terminal, run roscore

```
roscore
```

### 2. open a new terminal, run CopeliaSim

```
cd ~/CoppeliaSim_Edu_V4_2_0_Ubuntu18_04

./coppeliaSim.sh
```

Use GUI of CopeliaSim to open the model at ~/utils/v-rep_simulations/slalom/slalom_basic_locomotion.ttt


### 3. open a new terminal, compile the code

```
cd ~/projects/slalom/catkin_ws

catkin_make
```

### 4. open a new terminal, run dynamixel motors

Reference to [Slalom Dynamixel Software](https://gitlab.com/neutron-nuaa/Gecko/-/tree/master/Slalom%20Dynamixel%20Software) <br >


```
cd ~/catkin_ws/

source ./devel/setup.bash 

roslaunch my_dynamixel_workbench_tutorial multiple_motor_test.launch
```

### Finally  ->  Click Run button via CopeliaSim
The robot should walk similar posture to the simulation.<br >
_**If yes, congratulation!!!** <br >
**If no, please check the configuration below.**_
<br >
<br >

### In addition, the initial configuration of robot

**Black**: Real robot configuration <br >
**Red**: Simulation configuration <br >

<div align="center">
<img src="https://gitlab.com/neutron-nuaa/Gecko/-/raw/master/Slalom%20Basic%20Locomotion%20Controller/documents/initial_posture.png" width="800"> 
</div>



--*******************************************
--*                                         *
--*        Basic Locomotion Control         *
--*                                         *
--*******************************************
-- update: 10/01/2022
-- version: 2.0.0


--*******************************************
--*                                         *
--*              description                *
--*                                         *
--*******************************************
-- creating vrep simulation of the gecko robot and starting rosnode named "vrep_ros_interface"


--*******************************************
--*                                         *
--*             define variable             *
--*                                         *
--*******************************************


--*******************************************
--*                                         *
--*          ros callback function          *
--*                                         *
--*******************************************
function body_cb(msg)
    data = msg.data
    for i=1,3,1 do
        bodySignal[i] = data[i]
    end
end

function cpg_cb(msg)
    data = msg.data
    cpgSignal[1] = data[1]
    cpgSignal[2] = data[2]
    cpgSignal[3] = data[3]
    cpgSignal[4] = data[4]
    cpgSignal[5] = data[5]
    cpgSignal[6] = data[6]
end

function motor_cb(msg)
    data = msg.data
    for i=1,16,1 do
        motorSignal[i] = data[i]
    end
end

function sim_motor_cb(msg)
    data = msg.data
    for i=1,16,1 do
        sim_motorSignal[i] = data[i]
    end
end


-- initialize section
function sysCall_init()

    --*******************************************
    --*                                         *
    --*         create global variable          *
    --*                                         *
    --*******************************************

    -- ***************************  neural control *********************************
    bodySignal = {0,0,0, 0,0,0} -- Body signal
    cpgSignal = {0,0,0,0,0,0} -- CPG signal
    motorSignal = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    sim_motorSignal = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}

    -- **********************  simulated object handle *****************************
    legName = {'lf','lh','rh','rf'}
    jointHandle = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}
    footHandle = {0,0,0,0}
    forceHandle = {0,0,0,0}
    -- Create all object handles including joint, foot and force sensor

    -- *****************************  sensory signal *******************************
    forceData = {0,0,0,0} -- simulated force signal from foot contact sensor
    jointTorque = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}
    distance = {0,0,0,0} -- distance between foot and the surface
    accel = {0,0,0} -- acceleration from imu
    gyroData={0,0,0} -- angular vilocity from imu
    bodyAng = {0,0,0} -- body inclination of the robot

    -- *************************  parameter for rostopic ****************************
    forceTopic = {} -- simulated force parameter
    imuTopic = {} -- simulated imu parameter including acceleration and angular vilocity
    simTimeTopic = {} -- simulated time
    simBodyAngTopic = {} -- simulated robot's body angle

    -- ************************ performance measurement  ***************************
    walking_dist = 0 -- distance

    --*******************************************
    --*                                         *
    --*          create object handle           *
    --*                                         *
    --*******************************************
    geckoHandle=sim.getObjectAssociatedWithScript(sim.handle_self)
    for i=1,4,1 do
        for j=1,4,1 do
            jointHandle[i][j] = simGetObjectHandle('joint'..tostring(j).."_"..legName[i])
        end
        footHandle[i] = simGetObjectHandle('pad_'..legName[i])
        forceHandle[i] = simGetObjectHandle('fc_'..legName[i])
    end
    body = simGetObjectHandle('body')
    floor = simGetObjectHandle('floor')
    jointB1 = simGetObjectHandle('joint_b1')
    jointB2 = simGetObjectHandle('joint_b2')
    jointB3 = simGetObjectHandle('joint_b3')


    -- get graph handle
    graph = simGetObjectHandle('graph')

    -- initial position
    body_init_posi = simGetObjectPosition(body,floor)

    -- gyro sensor
    modelBaseG=sim.getObjectAssociatedWithScript(sim.handle_self)
    refG=sim.getObjectHandle('GyroSensor_reference')
    uiG=simGetUIHandle('GyroSensor_UI')
    simSetUIButtonLabel(uiG,0,sim.getObjectName(modelBaseG))
    gyroCommunicationTube=sim.tubeOpen(0,'gyroData'..sim.getNameSuffix(nil),1)
    oldTransformationMatrix=sim.getObjectMatrix(refG,-1)
    lastTime=sim.getSimulationTime()

    -- accelero sensor
    modelBaseA=sim.getObjectAssociatedWithScript(sim.handle_self)
    massObject=sim.getObjectHandle('Accelerometer_mass')
    accSensor=sim.getObjectHandle('Accelerometer_forceSensor')
    _,mass=sim.getObjectFloatParameter(massObject,sim.shapefloatparam_mass)
    uiA=simGetUIHandle('Accelerometer_UI')
    simSetUIButtonLabel(uiA,0,sim.getObjectName(modelBaseA))
    accelCommunicationTube=sim.tubeOpen(0,'accelerometerData'..sim.getNameSuffix(nil),1)

    -- proximity  sensor
    -- irlSensor=sim.getObjectHandle("IRL") -- Handle of the left proximity sensor
    -- irrSensor=sim.getObjectHandle("IRR") -- Handle of the right proximity sensor

    --*******************************************
    --*                                         *
    --*              setup ros node             *
    --*                                         *
    --*******************************************

    -- Check if the required ROS plugin is loaded
    moduleName=0
    moduleVersion=0
    index=0
    pluginNotFound=true
    while moduleName do
        moduleName,moduleVersion=sim.getModuleName(index)
        if (moduleName=='RosInterface') then
            pluginNotFound=false
        end
        index=index+1
    end
    if (pluginNotFound) then
        sim.displayDialog('Error','The RosInterface was not found.',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
    end

    -- If found then start the subscribers and publishers
    if (not pluginNotFound) then


        -- ************************ publisher  ***************************

        forcePublisher = simROS.advertise('/sim_force_topic','std_msgs/Float32MultiArray')-- simulated force topic
        imuPublisher = simROS.advertise('/sim_imu_topic','std_msgs/Float32MultiArray') -- simulated imu topic
        simTimePublisher = simROS.advertise('/sim_time_topic','std_msgs/Float32MultiArray') -- simulated time topic
        simBodyAngPublisher = simROS.advertise('/sim_bodyAng_topic','std_msgs/Float32MultiArray') -- body inclination topic
        -- simInfraredPublisher = simROS.advertise('/sim_infrared_topic','std_msgs/Float32MultiArray') -- simulated infrared topic

        -- ************************ subscriber  ***************************

        bodyOutputSub=simROS.subscribe('/sim_body_topic','std_msgs/Float32MultiArray', 'body_cb') -- body signal
        CPGOutputSub=simROS.subscribe('/cpg_topic','std_msgs/Float32MultiArray', 'cpg_cb') -- cpg signal
        MOTOROutputSub=simROS.subscribe('/motor_topic','std_msgs/Float32MultiArray', 'motor_cb') -- motor topic
        simMOTOROutputSub=simROS.subscribe('/sim_motor_topic','std_msgs/Float32MultiArray', 'sim_motor_cb') -- motor topic


        -- Start the client application (c++ node)

        -- node to run during simulation start
        -- slalom -> \slalom : neural control
        -- real_robot -> \real_robots : running real robot

        local rosnode = {'slalom', 'real_robot'}
        for i = 1,table.getn(rosnode),1 do
            result=sim.launchExecutable(simGetStringParameter(sim_stringparam_scene_path) .. '/../../../projects/slalom/catkin_ws/src/'..rosnode[i]..'/bin/'..rosnode[i],'/cpg_topic',0)
        end

        if (result==false) then
            sim.displayDialog('Error','External ROS-Node not found',sim.dlgstyle_ok,false,nil,{0.8,0,0,0,0,0},{0.5,0,0,1,1,1})
        end
    end
end


--[[
Actuation: This part will be executed in each simulation step
--]]
function sysCall_actuation()

    t = sim.getSimulationTime()

    for i=1,4,1 do --leg

        for j=1,4,1 do -- joint
            -- start simulation after 10 simulated second
            if(t > 10) then
                sim.setJointPosition(jointHandle[i][j], sim_motorSignal[4*(i-1)+j])

                -- body
                sim.setJointPosition(jointB1, bodySignal[1])
                sim.setJointPosition(jointB2, bodySignal[2])
                sim.setJointPosition(jointB3, bodySignal[3]) 

            end
        end

        -- get force
        forceData[i] = 0 -- -10
        if(t > 10) then
            _,force,torque = simReadForceSensor(forceHandle[i])
            forceData[i] = force[3]
        end

        -- get distance
        res,dist = simCheckDistance(floor,footHandle[i],1)
        distance[i] = math.abs(dist[3]-dist[6])

    end

    -- Read the proximity sensor
    -- IRL_dist=sim.readProximitySensor(irlSensor)
    -- IRR_dist=sim.readProximitySensor(irrSensor)

    -- publishing topic

    forceTopic['data'] = forceData
    imuTopic['data'] = {accel[1],accel[2],accel[3],gyroData[1],gyroData[2],gyroData[3]}
    simTimeTopic['data'] = {t}
    simBodyAngTopic['data'] = {bodyAng[1],bodyAng[2],bodyAng[3]}
    -- simInfraredTopic['data'] = {IRL_dist,IRR_dist}
    

    simROS.publish(forcePublisher,forceTopic)
    simROS.publish(imuPublisher,imuTopic)
    simROS.publish(simTimePublisher,simTimeTopic)
    simROS.publish(simBodyAngPublisher,simBodyAngTopic)
    -- simROS.publish(simInfraredPublisher,simInfraredTopic)


    -- calculate walking distance
    local current_body_position = simGetObjectPosition(body,floor)

    walking_dist = math.abs(current_body_position[2]-body_init_posi[2])

    -- stop the simulation
    --time = simGetStringParameter
    --if(0) then
        --sim.stopSimulation()
    --end

    if(t < 10) then
        body_init_posi = simGetObjectPosition(body,floor)
    end

end

function sysCall_sensing()

    -- get simulated angluar vilocity and acceleration from imu

    local transformationMatrix=sim.getObjectMatrix(refG,-1)
    local oldInverse=simGetInvertedMatrix(oldTransformationMatrix)
    local m=sim.multiplyMatrices(oldInverse,transformationMatrix)
    local euler=sim.getEulerAnglesFromMatrix(m)
    local currentTime=sim.getSimulationTime()
    local ang=sim.getEulerAnglesFromMatrix(transformationMatrix)

    local dt=currentTime-lastTime
    if (dt~=0) then
        gyroData[1]=euler[1]/dt
        gyroData[2]=euler[2]/dt
        gyroData[3]=euler[3]/dt
    end
    bodyAng[1] = ang[1]*180/3.14159
    bodyAng[2] = ang[2]*180/3.14159
    bodyAng[3] = ang[3]*180/3.14159
    oldTransformationMatrix=sim.copyMatrix(transformationMatrix)
    lastTime=currentTime

    _,accForce=sim.readForceSensor(accSensor)


    accel={accForce[1]/mass,accForce[2]/mass,accForce[3]/mass}

    simSetGraphUserData(graph,"gyrox",(gyroData[1]))
    simSetGraphUserData(graph,"gyroy",(gyroData[2]))
    simSetGraphUserData(graph,"gyroz",(gyroData[3]))
    simSetGraphUserData(graph,"accex",(accel[1]))
    simSetGraphUserData(graph,"accey",(accel[2]))
    simSetGraphUserData(graph,"accez",(accel[3]))

    -- simSetGraphUserData(graph,"irLeft",(IRL_dist))
    -- simSetGraphUserData(graph,"irRight",(IRR_dist))


    simSetGraphUserData(graph,"f_lf",(forceData[1]))
    simSetGraphUserData(graph,"f_rf",(forceData[2]))
    simSetGraphUserData(graph,"f_rh",(forceData[3]))
    simSetGraphUserData(graph,"f_lh",(forceData[4]))
    simSetGraphUserData(graph,"body_x",(bodyAng[1]))
    simSetGraphUserData(graph,"body_y",(bodyAng[2]))

    -- simSetGraphUserData(graph,"cpg_sig1",cpgSignal[1])
    -- simSetGraphUserData(graph,"cpg_sig2",cpgSignal[2])
    -- simSetGraphUserData(graph,"pcpg_sig1",cpgSignal[3])
    -- simSetGraphUserData(graph,"pcpg_sig2",cpgSignal[4])
    -- simSetGraphUserData(graph,"m1",cpgSignal[5])
    -- simSetGraphUserData(graph,"m13",cpgSignal[6])

end

function sysCall_cleanup()
    -- do some clean-up here
    walking_dist = math.abs(simGetObjectPosition(body,floor)[2]-body_init_posi[2])
    t = sim.getSimulationTime()
    print("average speed of this test case is " .. walking_dist/(t-9) .. "m/s")

    simROS.shutdownSubscriber(bodyOutputSub)
    simROS.shutdownSubscriber(CPGOutputSub)
    simROS.shutdownSubscriber(MOTOROutputSub)
    simROS.shutdownSubscriber(simMOTOROutputSub)

    -- killing rosnode
    os.execute ('rosnode kill /slalom /geckoDynamixel')
end

//*******************************************
//*                                         *
//*    Slalom Besic Locomotion Control      *
//*                                         *
//*******************************************
// author: Worasuchad Haomachai
// contract: haomachai@gmail.com
// update: 09/01/2022
// version: 2.0.0


//*******************************************
//*                                         *
//*               description               *
//*                                         *
//*******************************************
// generate motor signal from open-loop modular neural control
// the leg and body motor signals are seperated
// and also seperated between simulation and real robot signals!


// standard ros library
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include <rosgraph_msgs/Clock.h>
#include <cmath>
#include <iostream>


// modular robot controller library
#include "Basic_cpg_controller.h"
#include "Pcpg_controller.h"
#include "Delay_line.h"


//*******************************************
//*                                         *
//*            define parameter             *
//*                                         *
//*******************************************

#define RATE 10                 // reflesh rate of ros
#define GAIT_CHANGE_TIME 20     // time when MI is changed from CPG_MI to CPG_MI2

// cpg parameters
#define CPG_OUTPUT 0.01
#define CPG_BIAS 0.0
#define CPG_W11_22 1.4
#define CPG_WD1 0.18
#define CPG_MI 0.12
#define CPG_MI2 0.12

//** Noted: in case of using delay line **//
// MI = 0.04 for wave gait
// MI = 0.08 for new wave gait
// MI = 0.12 for trot gait


// pcpg parameters
#define PCPG_THRES 0.85     // control upward slope of pcpg signal
#define PCPG_1_SLOPE 2      // control the downward slope of pcpg signal
#define PCPG_2_SLOPE 20     
#define PCPG_P_SLOPE 200    
#define PCPG_P_THRES 0.85   

// delayline parameters
#define DELAYSIZE 80        // 80 size of the delay
#define DELAY_LF 0          // 0  delay for left front leg
#define DELAY_LH 20         // 20 delay for left hind leg
#define DELAY_RH 40         // 40 delay for right hind leg
#define DELAY_RF 60         // 60 delay for right front leg

// body amplitude gain
#define BODY_AMP_GAIN 0.2 


//*******************************************
//*                                         *
//*               ros variable              *
//*                                         *
//*******************************************

// buffer (array of float) to store signal before publish to ros
std_msgs::Float32MultiArray cpgSignal;
std_msgs::Float32MultiArray sim_motorSignal;
std_msgs::Float32MultiArray sim_bodySignal;
std_msgs::Float32MultiArray motorSignal;
std_msgs::Float32MultiArray multiJointCommandSignal;

//*******************************************
//*                                         *
//*            global variable              *
//*                                         *
//*******************************************

float motorSig[16] = {0};             // array store motor signal
float sim_motorSig[16] = {0};         // array store joint signal
float sim_motorTmp[16] = {0};         // array store joint signal (tmp)
float freezeSignal = 0;               // freeze or update

float c1 = 0;                         // cpg signals
float c2 = 0;

float pc1 = 0;                        // pcpg signals
float pc2 = 0;

float start_amp = 0;

float standing_wave_J1 = 0;           // body signal
float standing_wave_J2 = 0;
float standing_wave_J3 = 0;

float sim_time = 0;                   // simulation time

float sim_motorDirection[16] = {1,1,1,1,
                                1,1,1,1,
                                1,1,1,1,
                                1,1,1,1};

const float sim_motorInitPos[16] =  {0,-0.8, 0, 0.5,
                                     0,-0.8, 0, 0.5,
                                     0,-0.8, 0, 0.5,
                                     0,-0.8, 0, 0.5};

//*******************************************
//*                                         *
//*            global function              *
//*                                         *
//*******************************************

void simTimeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        sim_time = *it;
        i++;
    }
    return;
}

void freezeCB(const std_msgs::Float32MultiArray::ConstPtr& array)
{
    int i = 0;
    // print all the remaining numbers
    for(std::vector<float>::const_iterator it = array->data.begin(); it != array->data.end(); ++it)
    {
        freezeSignal = *it;
        i++;
    }
    return;
}

//*******************************************
//*                                         *
//*              main program               *
//*                                         *
//*******************************************

int main(int argc, char *argv[]){
    // create ros node
    std::string nodeName("slalom");
    ros::init(argc,argv,nodeName);

    // check robot operating system
    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");
    ros::NodeHandle node("~");

    ROS_INFO("simROS just started!");

    // set reflesh rate
    ros::Rate* rate;
    rate = new ros::Rate(RATE);
    ros::Rate loop_rate(RATE);

    //*******************************************
    //*                                         *
    //*    define publisher and subscriber      *
    //*                                         *
    //*******************************************

    ros::Publisher outputCPG;
    outputCPG = node.advertise<std_msgs::Float32MultiArray>("/cpg_topic",1);

    ros::Publisher outputSIMMOTOR;
    outputSIMMOTOR = node.advertise<std_msgs::Float32MultiArray>("/sim_motor_topic",1);

    ros::Publisher outputSIMBODY;
    outputSIMBODY = node.advertise<std_msgs::Float32MultiArray>("/sim_body_topic",1);

    ros::Publisher outputMOTOR;
    outputMOTOR = node.advertise<std_msgs::Float32MultiArray>("/motor_topic",1);

    ros::Publisher outputMultiJointCommand;
    outputMultiJointCommand = node.advertise<std_msgs::Float32MultiArray>("/morf_hw/multi_joint_command",1);

    ros::Subscriber simTimeSub = node.subscribe("/sim_time_topic",10,simTimeCB);
    ros::Subscriber freezeSub = node.subscribe("/freezeSignal_topic",10,freezeCB);


    //*******************************************
    //*                                         *
    //*      initialized neural control         *
    //*                                         *
    //*******************************************

    // basic cpg
    Basic_cpg_controller basic_cpg;
    basic_cpg.setParameter(CPG_OUTPUT,CPG_BIAS,CPG_W11_22,CPG_WD1,CPG_MI);

    // pcpg
    Pcpg_controller pcpg_1;
    pcpg_1.setParameter(PCPG_1_SLOPE,PCPG_THRES);

    Pcpg_controller pcpg_2;
    pcpg_2.setParameter(PCPG_2_SLOPE,PCPG_THRES);


    // create delay line for each joint
    Delay_line joint0_delay;
    joint0_delay.setParameter(DELAYSIZE);

    Delay_line joint1_delay;
    joint1_delay.setParameter(DELAYSIZE);

    Delay_line joint2_delay;
    joint2_delay.setParameter(DELAYSIZE);

    Delay_line joint3_delay;
    joint3_delay.setParameter(DELAYSIZE);


    // create delay line for body delay
    Delay_line body_j1_delay;
    body_j1_delay.setParameter(DELAYSIZE);

    Delay_line body_j2_delay;
    body_j2_delay.setParameter(DELAYSIZE);

    Delay_line body_j3_delay;
    body_j3_delay.setParameter(DELAYSIZE);


    // create delay lime for each sim joint
    Delay_line sim_joint0_delay;
    sim_joint0_delay.setParameter(DELAYSIZE);

    Delay_line sim_joint1_delay;
    sim_joint1_delay.setParameter(DELAYSIZE);

    Delay_line sim_joint2_delay;
    sim_joint2_delay.setParameter(DELAYSIZE);

    Delay_line sim_joint3_delay;
    sim_joint3_delay.setParameter(DELAYSIZE);

    bool updateCondition = true;                 // update or freeze cpg signal


    while(ros::ok())
    {

        //*******************************************
        //*                                         *
        //*               neural control            *
        //*                                         *
        //*******************************************

        if((sim_time > GAIT_CHANGE_TIME) && (GAIT_CHANGE_TIME != 0))
        {
            basic_cpg.setMI(CPG_MI2);
        }

        if(freezeSignal >= 1)
        {
            updateCondition = false;
        }else{
            updateCondition = true;
        }

        // body singal
        if(updateCondition)
        {
          sim_bodySignal.data.clear();
          multiJointCommandSignal.data.clear();
        }

        // cpg signal
        if(updateCondition)
        {
            basic_cpg.run();
        }
        cpgSignal.data.clear();
        c1 = basic_cpg.getSignal(1);
        c2 = basic_cpg.getSignal(2);

        // pcpg and basic pcpg signal
        if(updateCondition)
        {
            pcpg_1.run(c1,c2);
            pcpg_2.run(c1,c2);
        }
        pc1 = pcpg_1.getSignal(1);
        pc2 = pcpg_2.getSignal(1);
        

        //*******************************************
        // ***  write singal to leg dalay line    ***
        //*******************************************
        if (sim_time < 40)
        {
            start_amp = sim_time/40.0;
        }
        else 
        {
            start_amp = 1.0;
        }

        joint0_delay.writeIn(0.314*pc1 * start_amp);
        sim_joint0_delay.writeIn(0.314*pc1 * start_amp);

        joint1_delay.writeIn(-0.25*pc2 * start_amp);
        sim_joint1_delay.writeIn(-0.25*pc2 * start_amp);

        if(pc2 > 0)
        {
            joint2_delay.writeIn(-0.0*pc1 * start_amp);
            sim_joint2_delay.writeIn(-0.0*pc1 * start_amp);
        }
        else
        {
            joint2_delay.writeIn(-0.1*pc1 * start_amp);
            sim_joint2_delay.writeIn(-0.1*pc1 * start_amp);
        }

        joint3_delay.writeIn(-0.15*pc2 * start_amp);
        sim_joint3_delay.writeIn(-0.15*pc2 * start_amp);


        //*******************************************
        // ***  write singal to body dalay line   ***
        //*******************************************

        body_j1_delay.writeIn(c2 * start_amp);
        body_j2_delay.writeIn(c2 * start_amp);
        body_j3_delay.writeIn(c2 * start_amp);


        //*******************************************
        // ***  read singal leg dalay line        ***
        //*******************************************

        // *** SIMULATION  ***
        // shoulder joint
        sim_motorTmp[0]  = sim_joint0_delay.readFr(DELAY_LF);
        sim_motorTmp[4]  = sim_joint0_delay.readFr(DELAY_LH);
        sim_motorTmp[8]  = sim_joint0_delay.readFr(DELAY_RH);
        sim_motorTmp[12] = sim_joint0_delay.readFr(DELAY_RF);

        // delay for right front leg 
        sim_motorTmp[13] = sim_joint1_delay.readFr(DELAY_RF);
        sim_motorTmp[14] = sim_joint2_delay.readFr(DELAY_RF);
        sim_motorTmp[15] = sim_joint3_delay.readFr(DELAY_RF);

        // delay for left front leg
        sim_motorTmp[1] = sim_joint1_delay.readFr(DELAY_LF);
        sim_motorTmp[2] = sim_joint2_delay.readFr(DELAY_LF);
        sim_motorTmp[3] = sim_joint3_delay.readFr(DELAY_LF);

        // delay for right hind leg
        sim_motorTmp[9]  = sim_joint1_delay.readFr(DELAY_RH);
        sim_motorTmp[10] = sim_joint2_delay.readFr(DELAY_RH);
        sim_motorTmp[11] = sim_joint3_delay.readFr(DELAY_RH);

        // delay for left hind leg
        sim_motorTmp[5] = sim_joint1_delay.readFr(DELAY_LH);
        sim_motorTmp[6] = sim_joint2_delay.readFr(DELAY_LH);
        sim_motorTmp[7] = sim_joint3_delay.readFr(DELAY_LH);


        // *** REAL ROBOT ***
        // shoulder joint
        motorSig[0]  = joint0_delay.readFr(DELAY_LF);
        motorSig[4]  = joint0_delay.readFr(DELAY_LH);
        motorSig[8]  = joint0_delay.readFr(DELAY_RH);
        motorSig[12] = joint0_delay.readFr(DELAY_RF);

        // delay for right front leg
        motorSig[13] = joint1_delay.readFr(DELAY_RF);
        motorSig[14] = joint2_delay.readFr(DELAY_RF);
        motorSig[15] = joint3_delay.readFr(DELAY_RF);

        // delay for left front leg
        motorSig[1] = joint1_delay.readFr(DELAY_LF);
        motorSig[2] = joint2_delay.readFr(DELAY_LF);
        motorSig[3] = joint3_delay.readFr(DELAY_LF);

        // delay for right hind leg
        motorSig[9]  = joint1_delay.readFr(DELAY_RH) ;
        motorSig[10] = joint2_delay.readFr(DELAY_RH);
        motorSig[11] = joint3_delay.readFr(DELAY_RH);

        // delay for left front leg
        motorSig[5] = joint1_delay.readFr(DELAY_LH) ;
        motorSig[6] = joint2_delay.readFr(DELAY_LH);
        motorSig[7] = joint3_delay.readFr(DELAY_LH);



        //*******************************************
        // ***  read singal body dalay line       ***
        //*******************************************

        // delay for standing wave
        standing_wave_J1 = body_j1_delay.readFr(0);
        standing_wave_J2 = body_j2_delay.readFr(0);
        standing_wave_J3 = body_j3_delay.readFr(0);


        for(int i =0 ;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
              sim_motorSig[4*i+j] = (sim_motorDirection[4*i+j] * sim_motorTmp[4*i+j]) + sim_motorInitPos[4*i+j];
            }
        }


        if(updateCondition)
        {
            joint0_delay.step_one();
            joint1_delay.step_one();
            joint2_delay.step_one();
            joint3_delay.step_one();
            body_j1_delay.step_one();
            body_j2_delay.step_one();
            body_j3_delay.step_one();
            sim_joint0_delay.step_one();
            sim_joint1_delay.step_one();
            sim_joint2_delay.step_one();
            sim_joint3_delay.step_one();
        }

        //*******************************************
        //*                                         *
        //*         put data to ros variable        *
        //*                                         *
        //*******************************************
        // drive simulation body
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*standing_wave_J1);
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*standing_wave_J2);
        sim_bodySignal.data.push_back(BODY_AMP_GAIN*standing_wave_J3);

        //*******************************************
        //***    drive real body robot            ***
        //*******************************************
        multiJointCommandSignal.data.push_back(15);                                                     // set motor id of body joint1
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*standing_wave_J1);                         
        multiJointCommandSignal.data.push_back(25);                                                     // set motor id of body joint2
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*standing_wave_J2);                       
        multiJointCommandSignal.data.push_back(35);                                                     // set motor id of body joint3
        multiJointCommandSignal.data.push_back(BODY_AMP_GAIN*standing_wave_J3);  


        cpgSignal.data.push_back(c1);
        cpgSignal.data.push_back(c2);
        cpgSignal.data.push_back(pc1);
        cpgSignal.data.push_back(pc2);
        cpgSignal.data.push_back(motorSig[1]);
        cpgSignal.data.push_back(motorSig[13]);


        // drive simulation legs
        sim_motorSignal.data.clear();
        motorSignal.data.clear();
        for(int j=0;j<16;j++)
        {
            sim_motorSignal.data.push_back(sim_motorSig[j]);
            motorSignal.data.push_back(motorSig[j]);
        }

        outputSIMBODY.publish(sim_bodySignal);
        outputCPG.publish(cpgSignal);
        outputSIMMOTOR.publish(sim_motorSignal);
        outputMOTOR.publish(motorSignal);
        outputMultiJointCommand.publish(multiJointCommandSignal);

        // wait
        ros::spinOnce();
        loop_rate.sleep();

    } // main loop -> ros::ok
    return 0;
}
